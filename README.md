## Uncertainty Quantification for Vadere Crowd Simulation

This project contains mainly three modules: global sensitivity analysis, inversion methods, forward propagation (or uncertainty analysis). 

Uses the suq-controller ([SUQC](https://gitlab.lrz.de/vadere/suq-controller)) for communication with Vadere ([Vadere](https://gitlab.lrz.de/vadere/vadere)). 

Currently, only tested with Python 3.7. 

### Sensitivity analysis

1.  Sobol' indices  
1.1. Using SALib ([SALib package](https://github.com/SALib/SALib)): Sobol' indices  
1.2. Using chaospy (generalized polynomial chaos expansion as surrogate to calculate Sobol' indices)  
1.3. Monte Carlo approximation of Sobol' indices (Saltelli's method)  
2. Activity scores ([constantine-2017](10.1016/j.ress.2017.01.013))
3. First eigenvector metric ([constantine-2017](10.1016/j.ress.2017.01.013))
4. Derivative-based sensitivity metrics ([sobol-2009](10.1016/j.matcom.2009.01.023))


### Inversion 
1) Bayesian inversion with MCMC
2) Approximate Bayesian Computation 

### Propagation  
1) Monte Carlo
2) Stochastic collocation using chaospy ([chaospy package](https://github.com/jonathf/chaospy))


## Authors 
Marion Gödel 

## License

This software is licensed under the GNU Lesser General Public License ([LGPL](https://gitlab.lrz.de/vadere/uncertainty-quantification-private/blob/master/LICENSE.txt)).

For more information: http://www.gnu.org/licenses/lgpl.html

