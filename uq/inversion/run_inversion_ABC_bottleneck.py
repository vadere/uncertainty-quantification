import logging
import os
import pickle
import time

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.inversion.calc.RejectionSampling import RejectionSampling
from uq.inversion.ioput.FileWriterInversion import FileWriterInversion
from uq.inversion.ioput.InversionPlotter import InversionPlotter
from uq.inversion.routines_check_input import check_inputs
from uq.utils.datatype import unbox, get_dimension
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.logging import init_logging, finish_logging
from uq.utils.model.Surrogate1DFactory import Surrogate1DFactory
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

""" --------------------------------------- parameters -------------------------------------------------------- """

config = 1

if config == 1:
    # choose method
    method = "ABC"
    # method = "BayesianInversion"

    # choose uncertain parameters (to be inferred)
    key = "attributesPedestrian.speedDistributionMean"
    # key = "obstPotentialHeight"
    # key = ["attributesPedestrian.speedDistributionMean", "pedPotentialHeight", "obstPotentialHeight"]

    # choose quantity of interest (scenario file must provide the output)
    # qoi = "mean_density.txt"
    # qoi = "egress.txt"
    # qoi = "evac_time.txt"
    qoi = "flow.txt"

    # prior_params = {"Type": "Uniform", "Low": np.array([0.75, 1.0, 1.0]),
    # "High": np.array([1.75, 12.0, 12.0])}  # 2D input

    initial_point_input = 0.75
    # initial_point_input = np.array([0.75, 5.0, 3.0])

    legal_limits_parameter = np.array([0.1, 3.0])
    # legal_limits_parameter = np.array([0.1, 30.0])

    # legal_limits_parameter = np.array([[0.1, 1.0, 0.1], [3.0, 70.0, 15.0]])  # 3D input

    # prior
    # prior_params = {"Type": "Normal", "Mean": 1.0, "Variance": 1.0}
    prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.5, "Dim": 1, "Limits": legal_limits_parameter}
    # prior_params = {"Type": "Uniform", "Low": 1.0, "High": 15.0}

    # data
    bool_real_data = False  # otherwise run simulation  with true_parameter_value
    real_data = np.array([1.288, 1.674, 1.900, 2.123, 2.364])  # flow from seyfried-2009 with N= 60 participants
    # real_data = np.array(
    #    [1.685245862, 1.779737702, 1.995344531, 2.280780588, 2.595320277])  # flow from vadere with 2 populations
    # real_data = np.array([[1.0408, 1.0531, 1.2127, 1.4140, 1.6657],
    #                      [2.9570, 3.3107, 3.5432, 4.1281, 4.7139]])  # flow from vadere with 2 populations

    # real_data = np.array([26.8])
    real_data = np.nan
    # real_data = np.array([9.60, 9.60, 7.20, 8.40, 5.60, 8.80, 7.20, 8.80, 10.40, 6.40, 5.60, 8., 6., 9.60, 7.20,
    # 4.80, 10.40, 10.40, 5.20, 5.60, 6.80, 6.40, 8., 10.80, 6.80, 9.60, 8., 11.20, 10.40, 10.40, 16., 24.40, 32.80,
    # 22.40, 27.60, 28.40, 15.60, 20.80, 21.20, 14.80, 23.60, 26., 14.40, 18.40, 24., 18., 14.40, 27.60, 18.40, 26.40,
    # 23.60, 16.80, 25.60, 29.60, 20., 24.80, 30.80, 34., 32.80, 30.])

    eps_noise = 0.5 / 2 + 0.10
    eps_noise = 0
    qoi_dim = 60
    qoi_dim = 5
    true_parameter_value = np.array([1.34, 5.0, 3.0])  # only if no real data is present
    true_parameter_value = None
    # true_parameter_value = np.array([1.34, 5.0, 3.0])
    true_parameter_value = np.array(1.34)

    # config of simulator
    path2tutorial = os.path.dirname(os.path.realpath(__file__))
    path2model = os.path.join(path2tutorial, "../scenarios", "vadere-console.jar")

    path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_all_in_one_N60.scenario")
    # path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_egress.scenario")
    # path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_small_source.scenario")
    # path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_individual_egress.scenario")
    # path2scenario4data = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_individual_egress_2_pop.scenario")

    run_local = False
    # seed = int(np.random.uniform(0, 2 ** 32 - 1))
    seed = 1072346

    nr_steps = 10 ** 5
    nr_steps = 10 ** 3


    no_runs_averaged = 1  # average simulation runs

    burn_in = int(0.1 * nr_steps)
    burn_in = 0  # just for BayInv

    # ABC parameters
    abc_threshold_relative = 0.4
    abc_threshold_relative = 0.5
    abc_threshold_relative = 0.5
    surrogate_noise_std = 0.04

    # jump width
    jump_widths = [0.5]
    bool_adaptive_jump_width = True
    acceptance_rate_limits = np.array([0.4, 0.6])
    # acceptance_rate_limits = np.array([0.5, 1.0])

    bool_noise = False
    meas_noise = 0.0

    batch_jump_width = 100

    """ Parameters for surrogate """
    bool_surrogate = False
    bool_surrogate_data_misfit = False  # Surrogate for data misfit function

    nr_points_surrogate = 50
    nr_points_surrogate_error = 25
    nr_points_averaged_surrogate = 30

    nr_points_surrogate = 5
    nr_points_surrogate_error = 3
    nr_points_averaged_surrogate = 2


    nr_points_surrogate = 5
    nr_points_averaged_surrogate = 3

    limits_surrogate = legal_limits_parameter
    # limits_surrogate = np.array([150, 250])
    # limits_surrogate = np.array([[0.5, 150.0], [2.2, 250.0]])

    surrogate_fit_type = "spline"  # types: 'rf','spline','rational', 'poly'
    bool_load_surrogate = False
    # surrogate_path = 'results/2019-08-27_11-37-42_547252/data_misfit_surrogate.pickle'
    # surrogate_path = 'results/2019-08-30_15-18-26_130573/data_misfit_surrogate.pickle'
    surrogate_path = 'results/2019-10-03_09-44-52_894611/data_misfit_surrogate.pickle'  # poly fit

if __name__ == "__main__":  # main required by Windows to run in parallel

    start = time.time()

    # initialize random object
    random_state = RandomState(seed)

    data_saver = DataSaver(path2tutorial)

    init_logging(log_file_path=data_saver.get_path_to_files())
    # sys.stdout = Logger(folder_results) # problematic in suq controller

    main_logger = logging.getLogger("run_inversion.main")
    main_logger.info("*** METHOD: %s ***" % method)

    main_logger.info(path2model)
    #  main_logger.info(path2scenario)
    main_logger.info("Seed %d" % seed)

    burn_in, run_local = check_inputs(bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                      bool_surrogate=bool_surrogate, nr_points_surrogate=nr_points_surrogate,
                                      surrogate_fit_type=surrogate_fit_type,
                                      nr_points_averaged_surrogate=nr_points_averaged_surrogate, method=method,
                                      burn_in=burn_in, bool_load_surrogate=bool_load_surrogate, run_local=run_local)

    key_dim = get_dimension(key)

    results = dict()
    results_acceptance_rate = dict()

    # configure model
    vadere = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                         n_jobs=-1, log_lvl="OFF", qoi_dim=qoi_dim)
    vadere.set_data_saver(data_saver)
    # vadere.set_n_jobs(30)  # for GNM
    vadere.set_bool_fixed_seed(False)

    #  vadere_data = VadereModel(run_local=run_local, path2scenario=path2scenario4data, path2model=path2model,
    #  key="obstPotentialHeight", qoi=qoi, n_jobs=-1, log_lvl="OFF", qoi_dim=qoi_dim)
    # vadere_data.set_bool_fixed_seed(False)

    # data
    if bool_real_data:
        data = real_data
    else:
        # generate data from Vadere
        if bool_noise:
            # todo: only works for scalar QoI !
            dim_qoi = get_dimension(qoi)

            noise = unbox(random_state.normal(0, meas_noise, size=dim_qoi))
        else:
            noise = np.zeros(qoi_dim)

        # generate data with true parameter
        main_logger.info("Generate (synthetical) data point for inversion")
        # model_eval, _, _ = vadere.eval_model_averaged(parameter_value=true_parameter_value, no_runs_averaged=100)
        model_eval, _, _ = vadere.eval_model_averaged(parameter_value=random_state.randn(100) * 0.26 + 1.34,
                                                        nr_runs_averaged=1)
        # model_eval, _, _ = vadere.eval_model_averaged(parameter_value=true_parameter_value, no_runs_averaged=1)
        # model_eval, _, _ = vadere_data.eval_model_averaged(parameter_value=3.0, no_runs_averaged=1)

        data = model_eval + noise
        main_logger.info("Generated (synthetical) data point for inversion")

    for jump_width in jump_widths:
        random_state = RandomState(seed)

        # main_logger.info("*** Meas noise = {} *** ".format(meas_noise))
        if np.size(jump_widths) > 1:
            main_logger.info("\n*** Jump width = {} *** ".format(jump_width))

        # construct surrogate
        if bool_surrogate:
            if bool_load_surrogate:  # load existing surrogate
                main_logger.info("Load surrogate model for simulator.")
                vadere_model = pickle.load(open(os.path.abspath(surrogate_path), 'rb'))
                r_squared = vadere_model.r_squared
                main_logger.info("Loaded surrogate model for simulator.")
            else:  # generate new surrogate
                main_logger.info("Construct surrogate model for simulator.")
                surrogate_factory = Surrogate1DFactory()
                vadere_model = surrogate_factory.create_surrogate_from_model(model=vadere, limits=limits_surrogate,
                                                                             nr_points=nr_points_surrogate,
                                                                             n_keys=key_dim)
                # vadere_model = Surrogate1D(model=vadere, limits=limits_surrogate, nr_points=nr_points_surrogate,
                #                           n_keys=key_dim)
                vadere_model.set_data_saver(data_saver)
                vadere_model.construct_surrogate(surrogate_fit_type)

                # Surrogate for data misfit
                data_misfit_surrogate = None
                main_logger.info("Constructed surrogate model for simulator.")

        else:
            vadere_model = vadere

        # prior
        my_prior = PriorFactory().create_prior_by_type(params=prior_params)
        initial_point = initial_point_input
        if initial_point_input is None:
            initial_point = my_prior.get_mean()

        # initial_point = 230

        # just as a test
        my_prior_eval = my_prior.eval_prior(value=initial_point)

        # initialise sampling method
        if method == "BayesianInversion":
            sampling_config = MetropolisSampling(model=vadere_model, nr_iterations=nr_steps, random_state=random_state,
                                                 initial_point=initial_point, jump_width=jump_width, data=data,
                                                 meas_noise=meas_noise, batch_jump_width=batch_jump_width,
                                                 acceptance_rate_limits=acceptance_rate_limits, prior_dist=my_prior,
                                                 bool_acceptance_rate=bool_adaptive_jump_width,
                                                 limits=legal_limits_parameter,
                                                 dim=key_dim, qoi_dim=qoi_dim)

        elif method == "ABC":
            sampling_config = RejectionSampling(model=vadere_model, nr_iterations=nr_steps, random_state=random_state,
                                                data=data, meas_noise=meas_noise, prior_dist=my_prior,
                                                limits=legal_limits_parameter,
                                                dim=key_dim, threshold_relative=abc_threshold_relative,
                                                folder_results=data_saver.get_path_to_files(),
                                                surr_noise=surrogate_noise_std,
                                                qoi_dim=qoi_dim)
        else:
            raise Exception("This method is not known: %s" % method)

        # Surrogate for data misfit function
        if bool_surrogate_data_misfit:
            if bool_load_surrogate:
                main_logger.info("Load surrogate model for data misfit.")
                data_misfit_surrogate = pickle.load(open(os.path.abspath(surrogate_path), 'rb'))
                main_logger.info("Loaded surrogate model for data misfit.")

            else:
                main_logger.info("Construct surrogate model for data misfit.")

                surrogate_factory = Surrogate1DFactory()
                surrogate_factory.create_data_misfit_surrogate(model=vadere,
                                                               function_type=surrogate_fit_type,
                                                               sampling_strategy=sampling_config,
                                                               limits=limits_surrogate,
                                                               nr_points=nr_points_surrogate, n_keys=key_dim,
                                                               nr_points_averaged=nr_points_averaged_surrogate,
                                                               data_saver=data_saver)

                # data_misfit_surrogate = Surrogate1DDataMisfit(model=vadere, limits=limits_surrogate,
                #                                               nr_points=nr_points_surrogate, n_keys=key_dim,
                #                                              sampling_config=sampling_config,
                #                                              nr_points_averaged_surrogate=nr_points_averaged_surrogate)
                # data_misfit_surrogate.set_data_saver(data_saver)
                # in_n_keys=dim), in_metropolis_config=metropolis_config)
                # data_misfit_surrogate.construct_surrogate(surrogate_fit_type)
                main_logger.info("Constructed surrogate model for data misfit.")

            sampling_config.set_surrogate_misfit(data_misfit_surrogate)

            # save surrogate to file
            data_saver.save_to_pickle(None, data_misfit_surrogate, name='data_misfit_surrogate')
        else:
            data_misfit_surrogate = None

        main_logger.info("Started sampling.")
        samples, posterior_samples, computation_time, candidates, posterior_candidates, jump_widths, acceptance_rate, \
        data_misfit, data_misfit_accepted = sampling_config.run_sampling(no_runs_averaged=no_runs_averaged)

        # data_saver = data_saver
        # data_saver.write_data_misfit_to_file(key=key, qoi=qoi, parameters=candidates, results=data_misfit)

        main_logger.info("Finished sampling.")

        # save data to file
        main_logger.info("Save data to file")
        writer = FileWriterInversion(data_saver=data_saver)
        folder = writer.save_results_to_file(sampling_config, vadere_model)
        data_saver.save_to_pickle(folder, samples, "samples")

        if not bool_surrogate:
            vadere_model.get_map_results().pop('scenario_file_hash')

        # Save parameters to file
        main_logger.info("Save parameters to file")

        writer.write_parameters(run_local=run_local, sampling_config=sampling_config)

        main_logger.info("Saved data to file")

        # evaluation of results
        main_logger.info("Evaluate results")
        if method == "ABC":
            # no evoluation of acceptance rate
            total_acceptance_rate = acceptance_rate
            writer = FileWriterInversion(data_saver=data_saver)
            writer.print_results(samples=samples, burn_in=burn_in, computation_time=computation_time,
                                 acceptance_rate=total_acceptance_rate, nr_steps=nr_steps, dim=key_dim)
        else:
            total_acceptance_rate = acceptance_rate[len(acceptance_rate) - 1]

        if data_saver is not None:
            data_saver.write_data_misfit_to_file(key, qoi, candidates, data_misfit)

        writer = FileWriterInversion(data_saver=data_saver)
        writer.print_results(samples=samples, burn_in=burn_in, computation_time=computation_time,
                             acceptance_rate=total_acceptance_rate, nr_steps=nr_steps, dim=key_dim)
        writer.write_result(folder=folder, samples=samples, burn_in=burn_in,
                            computation_time=computation_time, acceptance_rate=total_acceptance_rate,
                            nr_steps=nr_steps, dim=key_dim, jump_width=jump_widths)

        main_logger.info("Evaluated results")

        # plot results
        main_logger.info("Plot results")
        plotter = InversionPlotter(data_saver=data_saver)
        plotter.plot_results(vadere_model=vadere_model, sampling_config=sampling_config)

        main_logger.info("Plotted results")

        results_acceptance_rate[jump_width] = total_acceptance_rate

    main_logger.info("Total computation time: {} min".format((time.time() - start) / 60))

    finish_logging()
