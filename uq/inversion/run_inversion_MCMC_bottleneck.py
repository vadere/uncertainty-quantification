import logging
import os
import pickle
import time

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.inversion.calc.RejectionSampling import RejectionSampling
from uq.inversion.ioput.FileWriterInversion import FileWriterInversion
from uq.inversion.ioput.InversionPlotter import InversionPlotter
from uq.inversion.ioput.InversionPlotterMetropolis import InversionPlotterMetropolis
from uq.inversion.routines_check_input import check_inputs
from uq.utils.datatype import get_dimension, unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.logging import init_logging, finish_logging
from uq.utils.model.Surrogate1DFactory import Surrogate1DFactory
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

# --------------------------------------- Parameters --------------------------------------------------------


method = "BayesianInversion"
key = "speedDistributionMean"

qoi = "flow.txt"

initial_point_input = 0.75

# legal_limits_parameter = [0.5, 2.0]
legal_limits_parameter = np.array([0.1, 3.0])

prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.2, "Dim": 1, "Limits": legal_limits_parameter}

true_parameter_value = 1.34

path2tutorial = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.join(path2tutorial, "../scenarios", "vadere-console.jar")
path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_all_in_one_N60.scenario")

run_local = False
# seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 1072346

nr_steps = 10 ** 5
nr_steps = 10
burn_in = int(0.1 * nr_steps)

# ABC parameters
abc_threshold_relative = 1.0
abc_threshold_relative = None
surrogate_noise_std = 0.04
surrogate_noise_std = None

# jump width
jump_widths = [0.5]
bool_adaptive_jump_width = True
acceptance_rate_limits = np.array([0.4, 0.6])  # adaptive acceptance rate
batch_jump_width = 100

bool_noise = True
meas_noise = 0.04

# Parameters for surrogate
bool_surrogate = False
bool_surrogate_data_misfit = True  # Surrogate for data misfit function

nr_points_surrogate = 50
nr_points_surrogate_error = 25
nr_points_averaged_surrogate = 30

nr_points_surrogate = 5
nr_points_averaged_surrogate = 3

limits_surrogate = legal_limits_parameter

surrogate_fit_type = "spline"  # types: 'rf','spline','rational', 'poly'
bool_load_surrogate = False
surrogate_path = None


def run_inversion_fct(method: str, nr_steps: int, run_local: bool, burn_in: int, jump_widths: np.ndarray, seed: int,
                      bool_write_data: bool = True, bool_plot: bool = True, bool_surrogate_data_misfit: bool = False):
    start = time.time()

    if bool_write_data:
        data_saver = DataSaver(path2tutorial)
        init_logging(log_file_path=data_saver.get_path_to_files())
    else:
        data_saver = None

    main_logger = logging.getLogger("run_inversion.main")
    main_logger.info("*** METHOD: %s ***" % method)

    main_logger.info(path2model)
    #  main_logger.info(path2scenario)
    main_logger.info("Seed %d" % seed)
    random_state = RandomState(seed)  # initialize random state

    burn_in, run_local = check_inputs(bool_surrogate_data_misfit, bool_surrogate, nr_points_surrogate,
                                      surrogate_fit_type, nr_points_averaged_surrogate, method, burn_in,
                                      bool_load_surrogate, run_local)

    dim = get_dimension(key)

    results = dict()
    results_acceptance_rate = dict()

    # for meas_noise in [1, 0.5, 0.1, 0.01, 0.001]:
    # for meas_noise in [0.01]:

    # generate data from Vadere
    if bool_noise:
        # todo: only works for scalar QoI !
        dim_qoi = get_dimension(qoi)
        noise = unbox(random_state.normal(0, meas_noise, size=dim_qoi))
    else:
        noise = 0

    # configure model
    vadere = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")
    vadere.set_data_saver(data_saver)

    # generate data
    main_logger.info("Generate (synthetical) data point for inversion")
    data = vadere.eval_model(true_parameter_value, random_state=random_state) + noise
    main_logger.info("Generated (synthetical) data point for inversion")

    # for jump_width in [1e-1,9e-2, 8e-2,7e-2, 6e-2, 5e-2,4e-2,3e-2,2e-2, 1e-2,9e-3, 8e-3,7e-3,6e-3, 5e-3, 4e-3, 3e-3, \
    # 2e-3,1e-3]:
    for jump_width in jump_widths:  # 1e-1, 7e-2, 5e-2,3e-2,1e-2,7e-3, 5e-3, 1e-3]:

        # initialize random state
        random_state = RandomState(seed)

        # main_logger.info("*** Meas noise = {} *** ".format(meas_noise))
        if np.size(jump_widths) > 1:
            main_logger.info("\n*** Jump width = {} *** ".format(jump_width))

        # construct surrogate
        if bool_surrogate:
            if bool_load_surrogate:  # load existing surrogate
                main_logger.info("Load surrogate model for simulator.")
                vadere_model = pickle.load(open(os.path.abspath(surrogate_path), 'rb'))
                r_squared = vadere_model.r_squared
                main_logger.info("Loaded surrogate model for simulator.")
            else:  # generate new surrogate
                main_logger.info("Construct surrogate model for simulator.")
                surrogate_factory = Surrogate1DFactory()
                vadere_model = surrogate_factory.create_surrogate_from_model(model=vadere,
                                                                             function_type=surrogate_fit_type,
                                                                             limits=limits_surrogate,
                                                                             nr_points=nr_points_surrogate,
                                                                             n_keys=dim, data_saver=data_saver)
                # vadere_model = Surrogate1D(vadere, limits=limits_surrogate, nr_points=nr_points_surrogate,
                #                           n_keys=dim)
                # vadere_model.set_data_saver(data_saver)
                # vadere_model.construct_surrogate(surrogate_fit_type)

                # Surrogate for data misfit
                data_misfit_surrogate = None
                main_logger.info("Constructed surrogate model for simulator.")

        else:
            vadere_model = vadere

        # prior
        my_prior = PriorFactory().create_prior_by_type(params=prior_params)
        initial_point = initial_point_input
        if initial_point_input is None:
            initial_point = my_prior.get_mean()

        # initial_point = 230

        # just as a test
        my_prior_eval = my_prior.eval_prior(initial_point)

        # initialise sampling method
        if method == "BayesianInversion":
            sampling_config = MetropolisSampling(model=vadere_model, nr_iterations=nr_steps, random_state=random_state,
                                                 initial_point=initial_point, jump_width=jump_width, data=data,
                                                 meas_noise=meas_noise, batch_jump_width=batch_jump_width,
                                                 acceptance_rate_limits=acceptance_rate_limits, prior_dist=my_prior,
                                                 bool_acceptance_rate=bool_adaptive_jump_width,
                                                 limits=legal_limits_parameter,
                                                 dim=dim)

        elif method == "ABC":
            if bool_write_data:
                folder_results = data_saver.get_path_to_files()
            else:
                folder_results = None

            sampling_config = RejectionSampling(model=vadere_model, nr_iterations=nr_steps, random_state=random_state,
                                                data=data,
                                                meas_noise=meas_noise, prior_dist=my_prior,
                                                limits=legal_limits_parameter, dim=dim,
                                                threshold_relative=abc_threshold_relative,
                                                folder_results=folder_results,
                                                surr_noise=surrogate_noise_std)
        else:
            raise Exception("This method is not known: %s" % method)

        # Surrogate for data misfit function
        if bool_surrogate_data_misfit:
            if bool_load_surrogate:
                main_logger.info("Load surrogate model for data misfit.")
                data_misfit_surrogate = pickle.load(open(os.path.abspath(surrogate_path), 'rb'))
                main_logger.info("Loaded surrogate model for data misfit.")

            else:
                main_logger.info("Construct surrogate model for data misfit.")

                surrogate_factory = Surrogate1DFactory()
                surrogate_factory.create_data_misfit_surrogate(model=vadere,
                                                               function_type=surrogate_fit_type,
                                                               sampling_strategy=sampling_config,
                                                               limits=limits_surrogate,
                                                               nr_points=nr_points_surrogate,
                                                               n_keys=dim,
                                                               nr_points_averaged=nr_points_averaged_surrogate,
                                                               data_saver=data_saver)

                # data_misfit_surrogate = Surrogate1DDataMisfit(vadere,
                #                                              limits=limits_surrogate,
                #                                              nr_points=nr_points_surrogate,
                #                                              n_keys=dim,
                #                                              sampling_config=sampling_config,
                #                                              nr_points_averaged_surrogate=nr_points_averaged_surrogate)
                # data_misfit_surrogate.set_data_saver(data_saver)
                # in_n_keys=dim), in_metropolis_config=metropolis_config)
                # data_misfit_surrogate.construct_surrogate(surrogate_fit_type)
                main_logger.info("Constructed surrogate model for data misfit.")

            sampling_config.set_surrogate_misfit(data_misfit_surrogate)

            if bool_write_data:
                # save surrogate to file
                data_saver.save_to_pickle(None, data_misfit_surrogate, name='data_misfit_surrogate')
        else:
            data_misfit_surrogate = None

        main_logger.info("Started sampling.")
        samples, posterior_samples, computation_time, candidates, posterior_candidates, jump_widths, acceptance_rate, \
        data_misfit, data_misfit_accepted = sampling_config.run_sampling()
        main_logger.info("Finished sampling.")

        if not bool_surrogate:
            vadere_model.get_map_results().pop('scenario_file_hash')

        if bool_write_data:
            # save data to file
            main_logger.info("Save data to file")
            writer = FileWriterInversion(data_saver=data_saver)
            folder = writer.save_results_to_file(sampling_config=sampling_config, vadere_model=vadere_model)

            # Save parameters to file
            writer.write_parameters(run_local)
            main_logger.info("Saved data to file")

            # evaluation of results
        main_logger.info("Evaluate results")
        if method == "ABC":
            # no evaluation of acceptance rate
            total_acceptance_rate = acceptance_rate
            writer = FileWriterInversion(data_saver=data_saver)
            writer.print_results(samples=samples, burn_in=burn_in, computation_time=computation_time,
                                 acceptance_rate=total_acceptance_rate, nr_steps=nr_steps, dim=dim)
        else:
            total_acceptance_rate = acceptance_rate[len(acceptance_rate) - 1]

        if bool_write_data:
            writer = FileWriterInversion(data_saver=data_saver)
            writer.print_results(samples=samples, burn_in=burn_in, computation_time=computation_time,
                                 acceptance_rate=total_acceptance_rate, nr_steps=nr_steps, dim=dim)
            writer.write_result(folder=folder, samples=samples, burn_in=burn_in,
                                computation_time=computation_time, acceptance_rate=total_acceptance_rate,
                                nr_steps=nr_steps, dim=dim, jump_width=jump_widths)

            main_logger.info("Evaluated results")

        if bool_plot:
            # plot results
            main_logger.info("Plot results")

            plotter = InversionPlotter(data_saver=data_saver)
            plotter.plot_results(vadere_model=vadere_model, sampling_config=sampling_config)
            main_logger.info("Plotted results")

        # results[jump_width] = effective_sample_size(samples, burn_in, nr_steps, dim)
        results_acceptance_rate[jump_width] = total_acceptance_rate

    main_logger.info("Total computation time: {} min".format((time.time() - start) / 60))

    if bool_plot:
        plotter = InversionPlotterMetropolis(param=None, result=None, data_saver=data_saver)
        plotter.plot_results_post_runs(results_ess=results, results_acceptance_rate=results_acceptance_rate,
                                       jump_widths=jump_widths)

    finish_logging()

    return samples, candidates, acceptance_rate


if __name__ == "__main__":
    run_inversion_fct(method, nr_steps, run_local, burn_in, jump_widths, seed)
