import logging
import os
import time

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.inversion.ioput.InversionPlotterMetropolis import InversionPlotterMetropolis
from uq.inversion.routines_check_input import check_inputs
from uq.utils.datatype import get_dimension, unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.logging import init_logging, finish_logging
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

""" --------------------------------------- parameters -------------------------------------------------------- """

"""
key = "speedDistributionMean"
# key = "sources.[id==-1].distributionParameters"
# key = "sources.[id==-1].spawnNumber"
# key = ["speedDistributionMean", "sources.[id==-1].spawnNumber"]

# qoi = "evac_time.txt"
# qoi = "waitingtime.txt"
qoi = "mean_density.txt"

prior_params = {"Type": "Normal", "Mean": 1.0, "Variance": 1.0}
# prior_params = {"Type": "Uniform", "Low": 1.0, "High": 5.0}
# prior_params = {"Type": "Uniform", "Low": 150.0, "High": 250.0}
# prior_params = {"Type": "Uniform", "Low": np.array([1.0, 150.0]), "High": np.array([2.0, 250.0])}


legal_limits_parameter = [0.5, 2.2]
# legal_limits_parameter = [0, 1000]
# legal_limits_parameter = np.array([[0.5, 150.0], [2.2, 250.0]])

true_parameter_value = 1.34
# true_parameter_value = 180.0
# true_parameter_value = np.array([[1.34], [180]])

# key = ["attributesPedestrian.speedDistributionMean", "sources.[id==-1].spawnNumber", "sources.[id==-1]. \
#        distributionParameters"]  # uncertain parameters
# qoi = "mean_density.txt"

path2tutorial = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.join(path2tutorial, "scenarios", "vadere0_7rc.jar")
path2model = os.path.join(path2tutorial, "scenarios", "vadere_console.jar")

# path2scenario = os.path.join(path2tutorial, "scenarios", "rimea_01_pathway_discrete.scenario")
# path2scenario = os.path.join(path2tutorial, "scenarios", "bridge_coordinates_kai_origin_0_UNIT.scenario")
path2scenario = os.path.join(path2tutorial, "scenarios", "Liddle_bhm.scenario")

run_local = True
seed = int(np.random.uniform(0, 2**32 - 1))


# nr_steps = 100000
nr_steps = 10**5
burn_in = int(0.1*nr_steps)

# jump width
bool_adaptive_jump_width = True
acceptance_rate_limits = np.array([0.4, 0.6])
jump_widths = [1]
# jump_widths = [0.1]

bool_noise = True
meas_noise = 0.01
batch_jump_width = 100

 # Parameters for surrogate 
bool_surrogate = False
bool_surrogate_data_misfit = False   # Surrogate for data misfit function

# nr_points_surrogate = 100
nr_points_surrogate = 25
# nr_points_surrogate = 4  # per direction

nr_points_surrogate_error = 200

limits_surrogate = np.array([0.5, 2.2])
# limits_surrogate = np.array([150, 250])
# limits_surrogate = np.array([[0.5, 150.0], [2.2, 250.0]])

surrogate_fit_type = "spline"  # types: 'rf','spline','rationa
bool_load_surrogate = False
surrogate_path = 'results/2019-02-27_11-34-16_554659/model_config.pickle'
 """
config = 1

if config == 1:
    # method = "ABC"
    method = "BayesianInversion"
    key = "speedDistributionMean"

    qoi = "mean_density.txt"
    # qoi = "mean_flow.txt"

    initial_point_input = 0.75

    # legal_limits_parameter = [0.5, 2.0]
    legal_limits_parameter = np.array([0.1, 3.0])

    # prior_params = {"Type": "Normal", "Mean": 1.0, "Variance": 1.0}
    prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.5, "Dim": 1, "Limits": legal_limits_parameter}

    true_parameter_value = 1.34

    path2tutorial = os.path.dirname(os.path.realpath(__file__))
    # path2model = os.path.join(path2tutorial, "scenarios", "vadere0_7rc.jar")
    path2model = os.path.join(path2tutorial, "../scenarios", "vadere-console.jar")

    # path2scenario = os.path.join(path2tutorial, "scenarios", "Liddle_bhm.scenario")
    path2scenario = os.path.join(path2tutorial, "../scenarios", "Liddle_bhm_v3_fixed_seed.scenario")

    bool_averaged = False
    no_runs_averaged = None

    run_local = True
    # seed = int(np.random.uniform(0, 2 ** 32 - 1))
    seed = 1072346

    nr_steps = 10 ** 5
    # nr_steps = 10
    burn_in = int(0.1 * nr_steps)

    # ABC parameters
    abc_threshold_relative = 1.0
    surrogate_noise_std = 0.04

    # jump width
    jump_widths = [0.5]
    bool_adaptive_jump_width = True
    acceptance_rate_limits = np.array([0.4, 0.6])
    # acceptance_rate_limits = np.array([0.5, 1.0])

    bool_noise = True
    meas_noise = 0.04

    batch_jump_width = 100

    """ Parameters for surrogate """
    bool_surrogate = False
    bool_surrogate_data_misfit = True  # Surrogate for data misfit function

    nr_points_surrogate = 50
    nr_points_surrogate_error = 25
    nr_points_averaged_surrogate = 30

    nr_points_surrogate = 5
    nr_points_averaged_surrogate = 3

    limits_surrogate = legal_limits_parameter
    # limits_surrogate = np.array([150, 250])
    # limits_surrogate = np.array([[0.5, 150.0], [2.2, 250.0]])

    surrogate_fit_type = "spline"  # types: 'rf','spline','rational', 'poly'
    bool_load_surrogate = False
    # surrogate_path = 'results/2019-08-27_11-37-42_547252/data_misfit_surrogate.pickle'
    # surrogate_path = 'results/2019-08-30_15-18-26_130573/data_misfit_surrogate.pickle'
    surrogate_path = 'results/2019-10-03_09-44-52_894611/data_misfit_surrogate.pickle'  # poly fit

if config == 3:  # 2D Inversion

    # method = "ABC"
    method = "BayesianInversion"
    key = ["speedDistributionMean", "sources.[id==-1].spawnNumber"]  # 2D input

    qoi = "mean_density.txt"

    initial_point_input = np.array([0.75, 220])  # 2D input
    legal_limits_parameter = np.array([[0.5, 150.0], [2.2, 250.0]])  # 2D input
    true_parameter_value = np.array([1.34, 180])  # 2D input

    prior_params = {"Type": "Uniform", "Low": np.array([1.0, 150.0]), "High": np.array([2.0, 250.0]),
                    "Limits": legal_limits_parameter}  # 2D input

    current_dir = os.path.dirname(os.path.realpath(__file__))

    path2model = os.path.join(current_dir, "../scenarios", "vadere-console.jar")
    path2scenario = os.path.join(current_dir, "../scenarios", "Liddle_bhm_v3_fixed_seed.scenario")
    path2tutorial = current_dir

    run_local = True
    # seed = int(np.random.uniform(0, 2 ** 32 - 1))
    seed = 1072346

    nr_steps = 10 ** 5
    # nr_steps = 10
    burn_in = int(0.1 * nr_steps)

    # ABC parameters
    abc_threshold_relative = 1.0
    surrogate_noise_std = 0.04

    # jump width
    jump_widths = [0.5]
    bool_adaptive_jump_width = True
    acceptance_rate_limits = np.array([0.4, 0.6])
    # acceptance_rate_limits = np.array([0.5, 1.0])

    bool_noise = True
    meas_noise = 0.04

    batch_jump_width = 100

    """ Parameters for surrogate """
    bool_surrogate = False
    bool_surrogate_data_misfit = True  # Surrogate for data misfit function

    nr_points_surrogate = 50
    nr_points_surrogate_error = 25
    nr_points_averaged_surrogate = 30

    nr_points_surrogate = 5
    nr_points_averaged_surrogate = 3

    limits_surrogate = legal_limits_parameter

    surrogate_fit_type = "spline"  # types: 'rf','spline','rational', 'poly'
    bool_load_surrogate = False

''' if config == 2:  # spawnNumber -> mean_density
    key = "sources.[id==-1].spawnNumber"
    qoi = "mean_density.txt"
    prior_params = {"Type": "Uniform", "Low": 150.0, "High": 250.0}
    legal_limits_parameter = [0, 1000]

    true_parameter_value = 180.0

    path2tutorial = os.path.dirname(os.path.realpath(__file__))
    path2model = os.path.join(path2tutorial, "scenarios", "vadere0_7rc.jar")
    path2scenario = os.path.join(path2tutorial, "scenarios", "Liddle_bhm.scenario")

    run_local = False
    seed = int(np.random.uniform(0, 2 ** 32 - 1))

    nr_steps = 10**5
    burn_in = int(0.1 * nr_steps)

    # jump width
    bool_adaptive_jump_width = False
    acceptance_rate_limits = np.array([0.4, 0.6])
    jump_widths = [0.1]

    bool_noise = True
    meas_noise = 0.01
    batch_jump_width = 10

    """ Parameters for surrogate """
    bool_surrogate = False
    bool_surrogate_data_misfit = True  # Surrogate for data misfit function

    nr_points_surrogate = 25
    nr_points_surrogate_error = 25
    nr_points_averaged_surrogate = 1

    limits_surrogate = np.array([150, 250])

    surrogate_fit_type = "spline"  # types: 'rf','spline','rational'
    bool_load_surrogate = False

'''


def run_inversion_fct(method: str, nr_steps: int, run_local: bool, burn_in: int, jump_widths: np.ndarray, seed: int,
                      bool_write_data: bool = True, bool_plot: bool = True,
                      bool_surrogate_data_misfit: bool = False) -> InversionResult:
    start = time.time()

    if bool_write_data:
        data_saver = DataSaver(path2tutorial, "summary")
        init_logging(log_file_path=data_saver.get_path_to_files())
    else:
        data_saver = None

    main_logger = logging.getLogger("run_inversion.main")
    main_logger.info("*** METHOD: %s ***" % method)

    main_logger.info(path2model)
    main_logger.info("Seed %d" % seed)
    random_state = RandomState(seed)  # initialize random state

    burn_in, run_local = check_inputs(bool_surrogate_data_misfit, bool_surrogate, nr_points_surrogate,
                                      surrogate_fit_type, nr_points_averaged_surrogate, method, burn_in,
                                      bool_load_surrogate, run_local)

    results_ess = dict()
    results_acceptance_rate = dict()

    # Generate data from Vadere
    if bool_noise:
        # todo: only works for scalar QoI !
        dim_qoi = get_dimension(qoi)
        noise = unbox(random_state.normal(0, meas_noise, size=dim_qoi))
    else:
        noise = 0

    # Configure model
    vadere = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")
    vadere.set_data_saver(data_saver)

    # Generate data
    main_logger.info("Generate (synthetical) data point for inversion")
    data = vadere.eval_model(true_parameter_value, random_state=random_state) + noise
    main_logger.info("Generated (synthetical) data point for inversion")

    rho = PriorFactory().create_prior_by_type(params=prior_params)

    inv_params = InversionParameterMetropolis(model=vadere, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                              no_runs_averaged=no_runs_averaged,
                                              path2results=data_saver.get_path_to_files(),
                                              bool_surrogate=bool_surrogate, bool_load_surrogate=bool_load_surrogate,
                                              surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                                              nr_points_surrogate=nr_points_surrogate,
                                              surrogate_fit_type=surrogate_fit_type,
                                              nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                                              bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                              nr_points_surrogate_error=nr_points_surrogate_error,
                                              bool_write_data=bool_write_data, n_samples=nr_steps, data=data,
                                              true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                              meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                              bool_plot=bool_plot, run_local=run_local,
                                              initial_point=initial_point_input, jump_width=None,
                                              bool_adaptive_jump_width=bool_adaptive_jump_width,
                                              batch_jump_width=batch_jump_width,
                                              acceptance_rate_limits=acceptance_rate_limits, burn_in=burn_in)

    for jump_width in jump_widths:

        if np.size(jump_widths) > 1:
            main_logger.info("\n*** Jump width = {} *** ".format(jump_width))

        inv_params.set_jump_width(jump_width)
        result = InversionResultMetropolis()
        inv_calc = InversionCalculator(param=inv_params, result=result)
        sampling = MetropolisSampling(inv_params)
        inv_calc.set_sampling_strategy(sampling)
        inv_calc.inversion()
        # result = inv_calc.get_result()

        # results[jump_width] = effective_sample_size(result.get_samples(), burn_in, nr_steps, key_dim)
        results_acceptance_rate[jump_width] = result.get_acceptance_ratio()
        results_ess[jump_width] = result.get_effective_sample_size()

    main_logger.info("Total computation time: {} min".format((time.time() - start) / 60))

    if bool_plot:
        plotter = InversionPlotterMetropolis(param=inv_params, result=result, data_saver=data_saver)
        plotter.plot_results_post_runs(results_ess=results_ess, results_acceptance_rate=results_acceptance_rate,
                                       jump_widths=jump_widths)

    finish_logging()

    return result


if __name__ == "__main__":
    run_inversion_fct(method, nr_steps, run_local, burn_in, jump_widths, seed)
