import logging
import os

import numpy as np

import uq
from uq.inversion.calc.InversionParameter import InversionParameter
from uq.inversion.calc.InversionResult import InversionResult
from uq.utils.data_eval import sample_mean, sample_mode, sample_var
from uq.utils.find_software_versions import get_current_uq_state
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.ioput.FileWriter import FileWriter


class FileWriterInversion(FileWriter):

    def __init__(self, param: InversionParameter, result: InversionResult = None, data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    # GETTERS

    def get_param(self) -> InversionParameter:
        return super().get_param()

    def get_result(self) -> InversionResult:
        return super().get_result()

    def get_sampling_config_str(self) -> str:
        pass

    # OTHERS

    def save_results_to_file(self, sampling_config: "SamplingStrategy", vadere_model: "Model") -> str:
        data_saver = self.get_datasaver()
        folder_results = self.get_datasaver().get_path_to_files()

        data_saver.dump_model_to_file(model=vadere_model, name="model_config")
        data_saver.dump_var_to_file(var=sampling_config, name="sampling_config")
        data_saver.dump_var_to_file(var=self.get_result().get_data_misfit(), name="data_misfit")


        data_saver.write_var_to_file(self.get_result().get_candidates(), "candidates")
        data_saver.write_var_to_file(self.get_result().get_samples(), "samples")
        data_saver.write_var_to_file(self.get_result().get_data_misfit(), "data_misfit")
        data_saver.write_var_to_file(self.get_result().get_data_misfit_accepted(), "data_misfit_accepted")
        if self.get_param().is_surrogate_data_misfit():
            surrogate_model = sampling_config.get_surrogate_misfit()
            data_saver.write_var_to_file(surrogate_model.get_model_input(), "surrogate_in_x")
            data_saver.write_var_to_file(surrogate_model.get_model_output(), "surrogate_in_y")
            if self.get_param().get_model().get_dimension() == 1:
                plot_x = np.linspace(np.min(surrogate_model.get_model_input()),
                                     np.max(surrogate_model.get_model_input()),
                                     1000)
                surrogate_model_evals = surrogate_model.eval_model(plot_x)
                data_saver.write_var_to_file(np.vstack((plot_x, surrogate_model_evals)), "surrogate_evals_for_plot")

        data_saver.write_var_to_file(self.get_result().get_acceptance_ratio(), "acceptance_ratio")

        return folder_results

    def write_parameters(self, run_local: bool, computation_time_surrogate: float = None,
                         sampling_config: "SamplingStrategy" = None) -> None:

        folder = self.get_datasaver().get_path_to_files()
        param = self.get_param()
        model = param.get_model()
        prior_params = param.get_prior().to_str()

        with open(os.path.join(folder, "results.txt"), 'w') as file:

            file.write("** Parameters **\n")

            # Parameters
            file.write('Key(s): \t\t\t\t\t\t\t {} \n'.format(model.get_key()))
            file.write('QoI: \t\t\t\t\t\t\t\t {} \n'.format(model.get_qoi()))
            file.write('Data: \t\t\t\t\t\t\t\t {} \n'.format(param.get_data()))
            file.write('Prior params: \t\t\t\t\t\t {} \n'.format(prior_params))
            file.write('Seed: \t\t\t\t\t\t\t\t {} \n'.format(param.get_seed()))
            file.write('Local run: \t\t\t\t\t\t\t {} \n'.format(run_local))
            file.write('Number of steps / samples: \t\t\t {} \n'.format(param.get_n_samples()))
            file.write('Number of runs averaged: \t\t\t\t {} \n'.format(param.get_no_runs_averaged()))

            sampling_config_str = self.get_sampling_config_str()
            if sampling_config_str is not None:
                file.write(sampling_config_str)

            true_parameter_value = param.get_true_parameter_value()
            if not type(param.get_true_parameter_value()) is np.ndarray:
                true_parameter_value = np.array(true_parameter_value)

            file.write('Bool noise:  \t\t\t\t\t\t {} \n'.format(param.is_noisy_data()))
            file.write('True parameter value: \t\t\t\t {} \n'.format(true_parameter_value))

            if param.is_surrogate() or param.is_surrogate_data_misfit():
                # if model.get_dimension() == 1:
                file.write('\n\n** Surrogate parameters (s) ** \n')
                if param.is_surrogate_data_misfit():
                    file.write('Surrogate for data misfit\n')
                else:
                    file.write('Surrogate for model evaluations\n')
                file.write('Number of points (s): \t\t\t\t {} \n'.format(param.get_nr_points_surrogate()))
                file.write('Number of points averaged (s): \t\t {} \n'.format(param.get_nr_points_averaged_surrogate()))
                file.write('Limits (s):  \t\t\t\t\t\t {} \n'.format(param.get_limits_surrogate()))
                file.write('Type (s): \t\t\t\t\t\t\t {}\n'.format(param.get_surrogate_fit_type()))
                file.write('Computation time (s): \t\t\t\t {} min\n'.format(computation_time_surrogate))
                if param.is_surrogate_data_misfit():
                    file.write('Surrogate coefficients: \t\t\t\t {} \n'.format(sampling_config.get_surrogate_misfit().get_fit() ))
                elif param.is_surrogate():
                    file.write('Surrogate coefficients: \t\t\t\t {} \n'.format(sampling_config.get_surrogate().get_fit()))

            else:
                file.write('Bool surrogate:  \t\t\t\t\t\t {} \n'.format(False))

            # Software versions

            file.write('\n\n** Software versions **: \n')
            file.write('UQ Software (version): \t\t\t\t {} \n'.format(uq.__version__))
            file.write('UQ Software (git_commit_hash): \t\t {} \n'.format(get_current_uq_state()["git_hash"]))
            file.write('UQ Software (uncommited_changes):\t {} \n'.format(
                str.replace(get_current_uq_state()["uncommited_changes"], '\n M', ';')))

            if model.get_version_str() is not None:  # vadere version + scenario
                file.write(model.get_version_str())

            file.close()

    def write_result(self, burn_in: int = 0) -> None:

        result = self.get_result()
        folder = self.get_datasaver().get_path_to_files()
        samples = result.get_samples()
        acceptance_rate = result.get_total_acceptance_ratio()

        with open(os.path.join(folder, "results.txt"), 'a') as file:
            file.write("\n\n** Results ** \n")
            file.write('Computation time (sampling):  \t\t{} min\n'.format(result.get_computation_time() / 60.0))
            file.write('Mean of samples (without burn-in): \t{}\n'.format(sample_mean(samples, burn_in)))
            file.write('Mode of samples (without burn-in): \t{}\n'.format(sample_mode(samples, burn_in)))
            file.write('Var of samples (without burn-in): \t{}\n'.format(sample_var(samples, burn_in)))
            file.write('Overall acceptance rate: \t\t\t{}\n'.format(acceptance_rate))
            file.close()

    def print_results(self, burn_in: int = 0, computation_time_surrogate: float = None):
        result = self.get_result()

        logger = logging.getLogger("Results")
        logger.info(' ')
        if self.get_param().is_surrogate_data_misfit() or self.get_param().is_surrogate():
            logger.info('Computation time (surrogate):  \t\t\t\t{:.4f} min'.format(computation_time_surrogate))
        logger.info('Mean of samples (without burn-in): \t\t\t{}'.format(sample_mean(result.get_samples(), burn_in)))
        logger.info('Mode of samples (without burn-in): \t\t\t{}'.format(sample_mode(result.get_samples(), burn_in)))
        logger.info('Var of samples (without burn-in): \t\t\t{}'.format(sample_var(result.get_samples(), burn_in)))
        logger.info('Overall (mean) acceptance rate: \t\t\t\t{:.4f}'.format(np.mean(result.get_acceptance_ratio())))
