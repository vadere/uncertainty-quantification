import logging
import os

import numpy as np

from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.ioput.FileWriterInversion import FileWriterInversion
from uq.utils.ioput.DataSaver import DataSaver


class FileWriterInversionMetropolis(FileWriterInversion):

    def __init__(self, param: InversionParameterMetropolis, result: InversionResultMetropolis = None,
                 data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    # GETTERS

    def get_param(self) -> InversionParameterMetropolis:
        return super().get_param()

    def get_result(self) -> InversionResultMetropolis:
        return super().get_result()

    def get_sampling_config_str(self) -> str:
        burn_in_str = 'Burn-in (MCMC): \t\t\t\t\t {} \n'.format(self.get_param().get_burn_in())
        jump_width_str = 'Jump Width (MCMC): \t\t\t\t\t {} \n'.format(self.get_param().get_jump_width())
        batch_jump_width_str = 'Batch Jump Width (MCMC): \t\t\t {} \n'.format(
            self.get_param().get_batch_size_jump_width())
        acceptance_rate_limits_str = 'Acceptance Rate Limits (MCMC): \t\t {} \n'.format(
            self.get_param().get_acceptance_rate_limits())

        return burn_in_str + jump_width_str + batch_jump_width_str + acceptance_rate_limits_str

    # OTHERS

    def save_results_to_file(self, sampling_config: "SamplingStrategy", vadere_model: "Model") -> str:
        data_saver = self.get_datasaver()
        folder_results = self.get_datasaver().get_path_to_files()

        data_saver.write_var_to_file(self.get_result().get_jump_width(), "jump_width")
        data_saver.write_var_to_file(self.get_result().get_acf_of_samples(), "acf")
        data_saver.write_var_to_file(self.get_result().get_effective_sample_size(), "ess")

        super().save_results_to_file(sampling_config=sampling_config, vadere_model=vadere_model)

        return folder_results

    def write_result(self, burn_in: int = 1) -> None:
        super().write_result(burn_in=self.get_param().get_burn_in())

        folder = self.get_datasaver().get_path_to_files()
        result = self.get_result()
        with open(os.path.join(folder, "results.txt"), 'a') as file:
            # logger.info('Auto-correlation of samples (without burn_in): \t {}'
            # .format(autocorrelation_samples(samples, burn_in)))
            file.write('Effective sample size: \t\t\t\t{}\n'.format(result.get_effective_sample_size()))
            file.write('Median of actual jump width: \t\t{}\n'.format(np.median(result.get_jump_width())))
            file.close()

    def print_results(self, burn_in: int = 0, computation_time_surrogate: float = None):
        burn_in = self.get_param().get_burn_in()

        logger = logging.getLogger("Results")
        super().print_results(burn_in=burn_in, computation_time_surrogate=computation_time_surrogate)
        logger.info('Effective sample size: \t\t\t\t\t\t{}'.format(self.get_result().get_effective_sample_size()))
        # logger.info('Auto-correlation of samples (without burn_in): \t {}'
        # .format(autocorrelation_samples(samples, burn_in)))
