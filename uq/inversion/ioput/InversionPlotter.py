import logging

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import statsmodels.api as sm

from uq.inversion.calc.InversionParameter import InversionParameter
from uq.inversion.calc.InversionResult import InversionResult
from uq.utils.data_eval import sample_mean, sample_std, sample_mode, averaging_simulation_data
from uq.utils.datatype import unbox, length, assure_vec
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.ioput.Plotter import Plotter
from uq.utils.model.Function import Function
from uq.utils.model.Model import Model
from uq.utils.model.Surrogate import Surrogate
from uq.utils.model.Surrogate1DFactory import Surrogate1DFactory


class InversionPlotter(Plotter):
    def __init__(self, param: InversionParameter, result: InversionResult = None, data_saver: DataSaver = None):
        super().__init__(param=param, result=result)
        if data_saver is None:
            data_saver = param.get_model().get_data_saver()
        self.__data_saver = data_saver

    # GETTERS

    def get_param(self) -> InversionParameter:
        return super().get_param()

    def get_result(self) -> InversionResult:
        return super().get_result()

    def get_data_saver(self) -> DataSaver:
        return self.__data_saver

    # OTHERS

    def plot_results(self, sampling_config: "SamplingStrategy", vadere_model: Model):

        data_misfit_surrogate = sampling_config.get_surrogate_misfit()

        if self.get_param().get_n_samples() > 0:
            self.plot_candidates_evolution()
            self.plot_hist_pos_evol()
            self.plot_prior_posterior_hist(sampling_config)
            self.plot_data_misfit_accepted_rejected()

            if self.get_param().get_model().get_dimension() == 1:
                self.plot_map_of_results()
                self.plot_computation_time()

        if self.get_param().is_surrogate():
            self.plot_surrogate_model(surrogate_model=vadere_model)
            if vadere_model.get_surrogate_type() == 'rational':
                self.plot_surrogate_error(vadere_model)
        if self.get_param().is_surrogate_data_misfit():
            self.plot_surrogate_model(surrogate_model=data_misfit_surrogate)
            self.plot_surrogate_model_average(sampling_config=sampling_config)

        # plt.show()
        plt.close()

    def plot_map_of_results(self) -> None:
        data_saver = self.get_data_saver()
        param = self.get_param()
        model = param.get_model()

        if "scenario_file_hash" in model.get_map_results().keys():
            model.get_map_results().pop('scenario_file_hash')

        if len(model.get_map_results().keys()) > 0:  # map of result is not empty

            hdl = plt.figure(3)
            ax = plt.gca()
            plt.plot(param.get_true_parameter_value(), param.get_data(), 'ro')
            plt.plot(model.get_map_results().keys(), model.get_map_results().values(), 'xb',
                     linestyle='None')
            x_min, x_max = ax.get_xlim()
            plt.plot([x_min, x_max], np.ones(shape=(2,)) * param.get_data(), '--r')
            plt.xlabel(model.get_key())
            plt.ylabel(model.get_qoi())
            self.save_figure(data_saver, handle=hdl, name="model_evaluations")

    def plot_computation_time(self) -> None:
        data_saver = self.get_data_saver()
        param = self.get_param()
        model_eval_time_per_call = param.get_model().get_computation_time()
        nr_steps = param.get_n_samples()

        if np.size(model_eval_time_per_call) > 1:
            hdl = plt.figure(4)
            ax = plt.gca()
            plt.hist(model_eval_time_per_call.values(), bins=np.max([10, int(nr_steps / 100)]))
            y_min, y_max = ax.get_ylim()
            plt.xlabel('Computation time per iteration [s]')
            plt.ylabel('Frequency')
            plt.plot(model_eval_time_per_call / nr_steps * np.ones(shape=(2,)), [y_min, y_max])
            self.save_figure(data_saver, handle=hdl, name="computation_time")

    def plot_surrogate_error(self, surrogate_model: Surrogate) -> None:
        vadere = self.get_param().get_model()

        data_saver = self.get_data_saver()
        param = self.get_param()
        nr_points_surrogate_error = param.get_nr_points_surrogate_error()
        nr_points_surrogate = param.get_nr_points_surrogate()
        limits_surrogate = param.get_limits_surrogate()
        dim = param.get_model().get_dimension()

        true_parameter_value = param.get_true_parameter_value()

        logger = logging.getLogger("utils_plot_routines.plot_surrogate_error")
        """ works only for free-flow ! """

        if nr_points_surrogate_error > nr_points_surrogate:
            surrogate_factory = Surrogate1DFactory()
            vadere_model_compare = surrogate_factory.create_surrogate_from_model(model=vadere,
                                                                                 limits_surrogate=limits_surrogate,
                                                                                 nr_points=nr_points_surrogate_error,
                                                                                 n_keys=dim)
            # vadere_model_compare = Surrogate1D(vadere, limits=limits_surrogate, nr_points=nr_points_surrogate_error,
            #                                  n_keys=dim)
        else:
            vadere_model_compare = surrogate_model

        data_surrogate = surrogate_model.eval_model(true_parameter_value)
        data_vadere = vadere.eval_model(true_parameter_value)

        handle = plt.figure(13)
        eval_surrogate = surrogate_model.eval_model(vadere_model_compare.model_input)  # parameters from \
        # surrogate with less points
        eval_vadere = vadere_model_compare.model_output

        # find best parameter with bisection
        resolution = 10e-10
        b = 1.33
        param_a = 1.35
        res = 1
        xn = 0
        while res >= resolution:
            xn = (param_a + b) / 2
            res = Function.rational1(xn, *surrogate_model.popt) - data_vadere
            if res < 0:
                param_a = xn
            else:
                b = xn
        logger.info(xn)

        logger.info("Best parameter with surrogate model: {:.10e}".format(xn))

        plt.plot(true_parameter_value, np.abs(data_vadere - data_surrogate) / data_vadere, 'ro',
                 label='True parameter value')
        plt.plot(vadere_model_compare.model_input, np.abs(eval_surrogate - eval_vadere) / eval_vadere, 'x:')
        plt.xlabel(param.get_model().get_key())
        plt.ylabel("Relative error of surrogate model")
        plt.legend()
        self.save_figure(data_saver, handle=handle, name="surrogate_error")

    def plot_samples_candidates(self) -> None:
        result = self.get_result()
        data_saver = self.get_data_saver()
        param = self.get_param()

        if result.get_samples() is not None and result.get_candidates() is not None:
            # plot samples & candidates
            handle = plt.figure(9)
            ax = plt.gca()
            plt.plot(result.get_candidates(), label="Candidates", marker='.', linestyle='dotted')
            plt.plot(result.get_samples(), label="Samples", marker='.', linestyle='dashed')
            x_min, x_max = ax.get_xlim()
            plt.plot([x_min, x_max], np.ones(shape=(2,)) * param.get_true_parameter_value(), '--r')
            plt.legend()
            plt.xlabel('Iteration')
            plt.ylabel(param.get_model().get_key())
            self.save_figure(data_saver, handle=handle, name="samples_candidates")

    def plot_candidates_evolution(self) -> None:
        data_saver = self.get_data_saver()
        candidates = self.get_result().get_candidates()
        key = self.get_param().get_model().get_key()

        batch_size = 1000
        if candidates is not None and np.size(candidates) > 4 * batch_size:
            # Evaluate several batches
            n_bins = int(batch_size / 50)
            n_candidates = len(candidates)
            handle2 = plt.figure(60)

            # Part 1
            plt.subplot(2, 2, 1)
            plt.title('First block %d candidates' % batch_size)
            plt.hist(candidates[0: batch_size], bins=n_bins, stacked=True, histtype='stepfilled', density=True,
                     label='Candidates', alpha=0.7, )
            plt.xlabel(key)
            plt.xlim([np.min(candidates), np.max(candidates)])

            # part 2
            plt.subplot(2, 2, 2)
            plt.title('Second block of %d candidates' % batch_size)
            plt.hist(candidates[int(n_candidates / 3): int(n_candidates / 3) + batch_size], bins=n_bins, stacked=True,
                     histtype='stepfilled', density=True,
                     label='Candidates', alpha=0.7)
            plt.xlabel(key)
            plt.xlim([np.min(candidates), np.max(candidates)])

            # Part 3
            plt.subplot(2, 2, 3)
            plt.title('Third block of %d candidates' % batch_size)
            plt.hist(candidates[int(n_candidates / 3) * 2: 2 * int(n_candidates / 3) + batch_size], bins=n_bins,
                     stacked=True, histtype='stepfilled', density=True,
                     label='Candidates', alpha=0.7)
            plt.xlabel(key)
            plt.xlim([np.min(candidates), np.max(candidates)])

            # Part 4
            plt.subplot(2, 2, 4)
            plt.title('Fourth block of %d candidates' % batch_size)
            plt.hist(candidates[n_candidates - batch_size:-1], bins=n_bins, stacked=True, histtype='stepfilled',
                     density=True,
                     label='Candidates', alpha=0.7)
            plt.xlabel(key)
            plt.xlim([np.min(candidates), np.max(candidates)])

            self.save_figure(data_saver, handle=handle2, name="hist_candidates_evolution")

    def plot_hist_pos_evol(self) -> None:
        data_saver = self.get_data_saver()
        samples = self.get_result().get_samples()
        key = self.get_param().get_model().get_key()

        batch_size = 1000

        if samples is not None and np.size(samples) > batch_size * 4:
            # Evaluate several batches
            n_bins = int(batch_size / 50)
            n_candidates = len(samples)
            handle2 = plt.figure(60)

            # block nr 1
            plt.subplot(2, 2, 1)
            plt.title('First block %d samples' % batch_size)
            plt.hist(samples[0: batch_size], bins=n_bins, stacked=True, histtype='stepfilled', density=True,
                     label='Samples', alpha=0.7, )
            plt.xlabel(key)
            plt.xlim([np.min(samples), np.max(samples)])

            # block nr 2
            plt.subplot(2, 2, 2)
            plt.title('Second block of %d samples' % batch_size)
            plt.hist(samples[int(n_candidates / 3): int(n_candidates / 3) + batch_size], bins=n_bins, stacked=True,
                     histtype='stepfilled', density=True,
                     label='Samples', alpha=0.7)
            plt.xlabel(key)
            plt.xlim([np.min(samples), np.max(samples)])

            # block nr 3
            plt.subplot(2, 2, 3)
            plt.title('Third block of %d samples' % batch_size)
            plt.hist(samples[int(n_candidates / 3) * 2: 2 * int(n_candidates / 3) + batch_size], bins=n_bins,
                     stacked=True,
                     histtype='stepfilled', density=True,
                     label='Samples', alpha=0.7)
            plt.xlabel(key)
            plt.xlim([np.min(samples), np.max(samples)])

            # block nr 4
            plt.subplot(2, 2, 4)
            plt.title('Fourth block of %d samples' % batch_size)
            plt.hist(samples[n_candidates - batch_size:-1], bins=n_bins, stacked=True, histtype='stepfilled',
                     density=True, label='Samples', alpha=0.7)
            plt.xlabel(key)
            plt.xlim([np.min(samples), np.max(samples)])

            self.save_figure(data_saver, handle=handle2, name="hist_posterior_evolution")

    def plot_posterior_sample_candidates(self) -> None:
        data_saver = self.get_data_saver()
        result = self.get_result()

        if result.get_posterior_samples() is not None and result.get_posterior_candidates() is not None:
            hdl = plt.figure(10)
            plt.plot(result.get_posterior_candidates(), label="Candidates Posterior", marker='.', linestyle='dotted')
            plt.plot(result.get_posterior_samples(), label="Samples Posterior", marker='.', linestyle='dashed')
            plt.legend()
            plt.xlabel('Iteration')
            plt.ylabel('Posterior')

            self.save_figure(data_saver, handle=hdl, name="posterior")

    def plot_samples_hist(self, limits: np.ndarray, burn_in: int = 1) -> None:
        data_saver = self.get_data_saver()
        result = self.get_result()
        samples = result.get_samples()
        meas_noise = self.get_param().get_meas_noise()
        nr_steps = self.get_param().get_n_samples()

        handle = plt.figure(62)

        label_str = 'Meas noise: {:.0e}\nMean: {:.4f}\nMode: {:.4f}\nStd: {:.2e}'.format(
            meas_noise, sample_mean(samples, burn_in), sample_mode(samples, burn_in), sample_std(samples, burn_in))

        plt.hist(samples[burn_in:len(samples)], bins=np.max([10, int(nr_steps / 100)]), density=True,
                 label=label_str)
        plt.xlabel('Samples of posterior distribution')
        plt.ylabel('Frequency')
        plt.legend()
        plt.xlim(limits)

        self.save_figure(data_saver, handle=handle, name="histogram_samples")

    def plot_samples_hist_evolution(self, burn_in: int = 1) -> None:
        data_saver = self.get_data_saver()
        result = self.get_result()
        nr_steps = self.get_param().get_n_samples()

        # evolution of posterior through samples
        batch_size = int(nr_steps / 10)

        handle = plt.figure(63)
        for i in range(int(nr_steps / batch_size)):
            tmp_batch = result.get_samples()[burn_in:burn_in + (i + 1) * batch_size]
            plt.hist(tmp_batch, bins=np.max([10, int(len(tmp_batch) / 100)]), label='{:.2e}'.format(i), histtype='step')
        plt.xlabel('Evolution of samples of posterior distribution')
        plt.ylabel('Frequency')
        plt.legend()

        self.save_figure(data_saver=data_saver, handle=handle, name="hist_post_samples_evolution")

    def plot_surrogate_model(self, surrogate_model: Surrogate) -> None:
        data_saver = self.get_data_saver()
        key = self.get_param().get_model().get_key()
        qoi = self.get_param().get_model().get_qoi()

        if surrogate_model.get_dimension() == 1:
            hdl = plt.figure(5)
            plt.plot(surrogate_model.get_model_input(), surrogate_model.get_model_output(), '.', label='Vadere evaluations')
            x_av, y_av = averaging_simulation_data(surrogate_model.get_model_input(), surrogate_model.get_model_output(),
                                                   surrogate_model.get_nr_points_averaged())
            plt.plot(x_av, y_av, ':d', label='Median of Vadere evaluations')
            plot_x = np.linspace(np.min(surrogate_model.get_model_input()), np.max(surrogate_model.get_model_input()), 1000)
            plt.plot(plot_x, surrogate_model.eval_model(plot_x), label="Surrogate model")
            # plt.plot(vadere_model.get_model_input(), Functions.rational1(vadere_model.model_input, *vadere_model.popt),
            #         label='Surrogate model')
            plt.xlabel(key)
            if self.get_param().is_surrogate_data_misfit():
                plt.ylabel("Data misfit (%s)" % qoi)
            else:
                plt.ylabel(qoi)
            plt.legend()
            self.save_figure(data_saver, handle=hdl, name="surrogate")

    def save_figure(self, data_saver: DataSaver, handle, name: str):
        data_saver = self.get_data_saver()
        if data_saver is not None:
            data_saver.save_figure(handle=handle, name=name)

    def plot_surrogate_model_average(self, sampling_config: "SamplingStrategy") -> None:

        data_misfit_surrogate = sampling_config.get_surrogate_misfit()

        data_saver = self.get_data_saver()
        key = self.get_param().get_model().get_key()
        qoi = self.get_param().get_model().get_qoi()

        if self.get_param().get_model().get_dimension() == 1:
            hdl = plt.figure(55)
            plt.plot()

            all_y_data_misfit = None
            all_x_values = data_misfit_surrogate.get_model_input()
            if self.get_param().is_surrogate_data_misfit():
                all_y_values = data_misfit_surrogate.get_model_response()
                all_y_data_misfit = data_misfit_surrogate.get_model_output()
            else:
                all_y_values = data_misfit_surrogate.get_model_output()

            if all_x_values is not None:

                if not self.get_param().is_surrogate_data_misfit():
                    all_y_values_reshaped = np.reshape(np.array(all_y_values),
                                                       newshape=(-1, data_misfit_surrogate.get_nr_points_averaged()))
                    min_y_values = np.min(all_y_values_reshaped, axis=1)
                    max_y_values = np.max(all_y_values_reshaped, axis=1)

                    min_y_data_misfit = sampling_config.evaluate_data_misfit(min_y_values)
                    max_y_data_misfit = sampling_config.evaluate_data_misfit(max_y_values)
                    all_y_data_misfit = sampling_config.evaluate_data_misfit(model_eval=all_y_values)

                else:
                    n_av = self.get_param().get_nr_points_averaged_surrogate()
                    n_point = self.get_param().get_nr_points_surrogate()
                    min_y_data_misfit = np.min(np.reshape(all_y_data_misfit, (n_point, n_av)), axis=1)
                    max_y_data_misfit = np.max(np.reshape(all_y_data_misfit, (n_point, n_av)), axis=1)

                ax1 = plt.gca()
                ax1.fill_between(np.unique(data_misfit_surrogate.get_model_input()), min_y_data_misfit, max_y_data_misfit,
                                 color='lightblue', alpha=0.3, label="Vadere evaluations")

                plt.plot(all_x_values, all_y_data_misfit, '.', color="royalblue", alpha=0.7, markersize=3)

                plot_x = np.linspace(np.min(data_misfit_surrogate.get_model_input()),
                                     np.max(data_misfit_surrogate.get_model_input()), 1000)
                plt.plot(plot_x, data_misfit_surrogate.eval_model(plot_x), label="Surrogate model",
                         color="coral", linewidth=2)

                plt.xlabel(key)

                if self.get_param().is_surrogate_data_misfit():
                    plt.ylabel('Data misfit (%s)' % qoi)
                else:
                    plt.ylabel(qoi)
                plt.legend()
                self.save_figure(data_saver, handle=hdl, name="surrogate_average")

                # evaluate deviation

                # all_x, all_y are Vadere evaluations
                # surrogate is surrogate for data misfit
                if not self.get_param().is_surrogate_data_misfit():
                    all_y_data_misfit = sampling_config.evaluate_data_misfit(np.array(all_y_values))

                surrogate_y = data_misfit_surrogate.eval_model(all_x_values)
                dev = all_y_data_misfit - surrogate_y
                handle2 = plt.figure(56)
                n_values = np.size(dev)
                plt.hist(unbox(dev), bins=np.max([np.min([int(n_values / 20), 1000]), 10]))
                plt.xlabel('Deviation between Data Misfit Surrogate and Data Misfit Evaluations')

                self.save_figure(data_saver, handle=handle2, name="hist_deviations")

                # save results to reproduce plot outside
                data_saver.write_var_to_file(dev, "deviation_surrogate")

    def plot_prior_posterior_hist(self, sampling_config: "SamplingStrategy", burn_in: int = 1) -> None:
        from uq.inversion.calc.MetropolisSampling import MetropolisSampling

        vadere_model = self.get_param().get_model()
        surrogate_data_misfit = sampling_config.get_surrogate_misfit()

        data_saver = self.get_data_saver()
        result = self.get_result()
        samples = result.get_samples()
        param = self.get_param()
        nr_steps = param.get_n_samples()
        rho = param.get_prior()
        key = param.get_model().get_key()
        meas_noise = param.get_meas_noise()
        legal_limits_parameter = param.get_legal_limits_parameters()

        if np.size(samples) > 0:
            if vadere_model.get_dimension() == 1:

                handle = plt.figure(22)
                label_str = 'Samples\nMeas noise: {:.0e}\nMean: {:.4f}\nMode: {:.4f}\nStd: {:.2e}'.format(
                    meas_noise, sample_mean(samples, burn_in), sample_mode(samples, burn_in),
                    sample_std(samples, burn_in))

                plt.hist(samples[burn_in:len(samples)], bins=np.max([10, int(nr_steps / 100)]),
                         density=True, label=label_str)

                x_min = legal_limits_parameter[0]  # np.max([x_min, legal_limits_parameter[0]])
                x_max = legal_limits_parameter[1]  # np.min([x_max, legal_limits_parameter[1]])
                x_values = np.linspace(x_min, x_max, 1000)
                plt.plot(x_values, np.transpose(rho.eval_prior(x_values)), label='Prior')

                # Analytical posterior
                if param.is_surrogate() or param.is_surrogate_data_misfit():
                    x_values = np.linspace(x_min, x_max)
                    n_values = len(x_values) - 1

                    if param.is_surrogate():
                        surrogate_eval = vadere_model.eval_model(x_values)
                        surrogate_eval = assure_vec(surrogate_eval)

                    prior_eval = rho.eval_prior(x_values)
                    if np.ndim(prior_eval) > 1:
                        prior_eval = prior_eval[0]
                    if isinstance(sampling_config, MetropolisSampling):  # likelihood available
                        if param.is_surrogate():
                            likelihood = np.zeros(shape=(length(surrogate_eval)))
                            for i in range(0, n_values):
                                likelihood[i] = sampling_config.evaluate_likelihood(surrogate_eval[i])
                        elif param.is_surrogate_data_misfit():
                            likelihood = sampling_config.evaluate_likelihood_from_misfit(
                                surrogate_data_misfit.eval_model(x_values))
                        theo_posterior = prior_eval * likelihood
                        sum_of_posterior_values = np.sum(np.mean(np.diff(x_values)) * theo_posterior)
                        plt.plot(x_values, theo_posterior / sum_of_posterior_values, label='Analytical posterior')
                plt.legend()
                plt.xlabel(key)
                plt.ylabel("Frequency")
                plt.xlim([0.0, 3.0])
                self.save_figure(data_saver, handle=handle, name="prior_posterior")
            elif vadere_model.get_dimension() > 1:
                N = vadere_model.get_dimension()
                handle = plt.figure()
                for i in range(0, N):
                    plt.subplot(N, 1, i + 1)

                    nbins = int(np.max([10, np.round(nr_steps / 100)]))
                    n_samples = np.size(samples,1)
                    label_i = 'Samples\nMeas noise: {:.0e}\nMean: {:.4f}\nMode: {:.4f}\nStd: {:.2e}'.format(
                        meas_noise, sample_mean(samples, burn_in)[i], sample_mode(samples, burn_in)[i],
                        sample_std(samples, burn_in)[i])
                    plt.hist(samples[:, burn_in:n_samples][i], bins=nbins, density=True, label=label_i)
                    plt.legend()
                    plt.xlabel(key[i])
                    plt.ylabel("Frequency")
                self.save_figure(data_saver, handle=handle, name="prior_posterior")

    def plot_data_misfit_accepted_rejected(self) -> None:
        data_misfit = self.get_result().get_data_misfit()
        data_misfit_accepted = self.get_result().get_data_misfit_accepted()

        data_saver = self.get_data_saver()
        result = self.get_result()
        candidates = result.get_candidates()
        samples = result.get_samples()

        param = self.get_param()
        legal_limits_parameter = param.get_legal_limits_parameters()
        key = param.get_model().get_key()
        dim = param.get_model().get_dimension()

        if data_misfit is not None:
            hdl = plt.figure(59)
            if dim == 1:
                plt.plot(candidates, data_misfit, 'o', label='Candidates')
                plt.plot(samples, data_misfit_accepted, '.', label='Samples')
                plt.xlabel(key)
                plt.ylabel('Data misfit function')
                plt.xlim(legal_limits_parameter)
                plt.legend()

            elif dim == 2:
                ax = plt.axes(projection='3d')  # needs from mpl_toolkits import mplot3d
                ax.scatter3D(candidates[0, :], candidates[1, :], data_misfit, marker='^', label='Candidates')
                ax.scatter3D(samples[0, :], samples[1, :], data_misfit_accepted, label='Samples')
                ax.set_xlabel(key[0])
                ax.set_ylabel(key[1])
                ax.set_zlabel('Data misfit function')
                plt.legend()
            else:
                for i in range(0, dim):
                    plt.subplot(dim, 1, i + 1)
                    plt.plot(candidates[i, :], data_misfit, '^', label='Candidates')
                    plt.plot(samples[i, :], data_misfit_accepted, 'd', label='Samples')
                    plt.xlabel(key[i])
                    plt.ylabel('Data misfit function')
                    plt.legend()

            self.save_figure(data_saver, hdl, "data_misfit")

    def evaluate_deviation_from_mean(self, results_reshaped: np.ndarray, result_averaged: np.ndarray, value_input,
                                     no_points_averaged: int = 1, dim: int = None) -> None:
        data_saver = self.get_data_saver()

        dev = results_reshaped - np.tile(np.expand_dims(result_averaged, axis=1), (1, no_points_averaged))
        if dim == 1:
            plt.figure()
            value_input_formatted = np.expand_dims(value_input, axis=1)
            plt.plot(np.tile(value_input_formatted, reps=(1, no_points_averaged)), dev, marker='.')

            # n_param_values = np.size(dev, axis=0) + 1
            # ax1 = plt.subplot(n_param_values, 1, 1)
            for i in range(0, np.size(dev, axis=0)):
                h3 = plt.figure()
                # plt.subplot(n_param_values, 1, i + 1).\
                plt.hist(dev[i, :], stacked=False, label=str(value_input[i]), density=True)
                # plt.xlim([-0.1, 0.1])
                plt.title(str(value_input[i]))
                self.save_figure(data_saver, h3, ("deviations_over_parameter%d" % i))

        h1 = plt.figure()
        plt.hist(np.ravel(dev), 100, density='true')
        mean_est = np.mean(np.ravel(dev))
        std_est = np.std(np.ravel(dev))
        x = np.linspace(np.min(np.ravel(dev)), np.max(np.ravel(dev)))
        random_normal = stats.norm(mean_est, std_est).pdf(x)
        plt.plot(x, random_normal, '--r', label='Fitted normal distribution')
        plt.legend()
        self.save_figure(data_saver, h1, "histogram_deviations_vadere")

        sm.qqplot(np.ravel(dev), line='s')
        h2 = plt.gcf()
        self.save_figure(data_saver, h2, "qqplot_deviations_vadere")
        plt.close(h2)

        vadere_logger = logging.getLogger("vaderemodel.evaluate_deviation_from_mean")
        vadere_logger.info("Vadere evaluations: Deviations from average")
        vadere_logger.info("Mean: %s, Std: %s" % (np.array2string(mean_est), np.array2string(std_est)))

        # skewtest needs at least 8 samples
        if len(np.ravel(dev)) >= 20:
            alpha = 0.01
            k2, p = stats.normaltest(np.ravel(dev))
            vadere_logger.info("p = {:g}".format(p))
            if p < alpha:  # null hypothesis: x comes from a normal distribution
                vadere_logger.info("The null hypothesis can be rejected")
            else:
                vadere_logger.info("The null hypothesis cannot be rejected")
