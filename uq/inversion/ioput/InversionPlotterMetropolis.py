import logging
import time
from typing import Union

import matplotlib.pyplot as plt
import numpy as np

from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.SamplingStrategy import SamplingStrategy
from uq.inversion.ioput.InversionPlotter import InversionPlotter
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Model import Model
from uq.utils.model.Surrogate1D import Surrogate1D


class InversionPlotterMetropolis(InversionPlotter):

    def __init__(self, param: InversionParameterMetropolis, result: InversionResultMetropolis = None,
                 data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    # GETTERS

    def get_result(self) -> InversionResultMetropolis:
        return super().get_result()

    def get_param(self) -> InversionParameterMetropolis:
        return super().get_param()

    # OTHERS


    def plot_results(self, vadere_model: Union[Model, Surrogate1D], sampling_config: SamplingStrategy) -> None:

        acceptance_rate_limits = self.get_param().get_acceptance_rate_limits()

        super().plot_results(vadere_model=vadere_model, sampling_config=sampling_config)

        if self.get_param().get_n_samples() > 0:
            self.plot_jump_width(acceptance_rate_limits=acceptance_rate_limits)
            if self.get_param().get_model().get_dimension() == 1:
                self.plot_autocorrelation_samples()

    def plot_jump_width(self, acceptance_rate_limits: np.ndarray) -> None:
        data_saver = self.get_data_saver()
        result = self.get_result()

        if result.get_jump_width() is not None:
            hdl = plt.figure(2)
            ax = plt.gca()
            plt.plot(result.get_jump_width(), 'o-', label='Jump Width')
            plt.plot(result.get_acceptance_ratio(), 'x-', label='Acceptance rate')
            plt.legend()
            x_min, x_max = ax.get_xlim()
            plt.plot([x_min, x_max], acceptance_rate_limits[0] * np.ones(shape=(2,)), linestyle='dashed', color='black')
            plt.plot([x_min, x_max], acceptance_rate_limits[1] * np.ones(shape=(2,)), linestyle='dashed', color='black')
            plt.xlabel("Iteration")
            plt.ylabel("Jump width | Acceptance rate")
            plt.ylim([0, 1.2])

            self.save_figure(data_saver, handle=hdl, name="jump_widths")

    def plot_autocorrelation_samples(self) -> None:
        data_saver = self.get_data_saver()
        samples = self.get_result().get_samples()
        nr_steps = self.get_param().get_n_samples()
        jump_width = self.get_param().get_jump_width()

        logger = logging.getLogger("utils_plot_routines.plot_autocorrelation_samples")
        if samples is not None and len(samples) > 1:
            start = time.time()
            acf_values = self.get_result().get_acf_of_samples()
            hdl = plt.figure(8)
            label_str = 'Jump width = {}'.format(jump_width)
            plt.plot(acf_values, label=label_str)
            plt.ylabel("Autocorrelation of samples")
            plt.legend()
            self.save_figure(data_saver, handle=hdl, name="autocorrelation")

            logger.info("Computation time - ACF ({:.0e} samples): \t{:.2f} s".format(nr_steps, time.time() - start))

    def plot_prior_posterior_hist(self, sampling_config: SamplingStrategy, burn_in: int = 1) -> None:
        super().plot_prior_posterior_hist(sampling_config=sampling_config, burn_in=self.get_param().get_burn_in())

    def plot_samples_hist(self, limits: np.ndarray, burn_in: int = 1) -> None:
        super().plot_samples_hist(limits=limits, burn_in=self.get_param().get_burn_in())

    def plot_samples_hist_evolution(self, burn_in: int = 1) -> None:
        super().plot_samples_hist_evolution(burn_in=self.get_param().get_burn_in())

    def plot_results_post_runs(self, results_ess: dict, results_acceptance_rate: dict,
                               jump_widths: Union[float, np.ndarray]) -> None:

        if jump_widths is not None:
            if len(jump_widths) > 1:
                # evolution of ess and acceptance rate for different jump widths
                self.plot_jump_width_vs_ess(results_ess)
                self.plot_jump_width_vs_acceptance_rate(results_acceptance_rate)

    def plot_jump_width_vs_ess(self, results: dict) -> None:
        data_saver = self.get_data_saver()

        if len(results.keys()) > 1:
            hdl = plt.figure(57)
            plt.semilogx(results.keys(), results.values(), 'o-', label='Jump width')
            plt.ylabel("Effective sample size")
            plt.xlabel("Jump width")
            self.save_figure(data_saver, hdl, "jump_width_vs_ess")

    def plot_jump_width_vs_acceptance_rate(self, results_acceptance_rate: dict) -> None:
        data_saver = self.get_data_saver()

        if len(results_acceptance_rate.keys()) > 1:
            hdl = plt.figure(58)
            plt.semilogx(results_acceptance_rate.keys(), results_acceptance_rate.values(), 'o-',
                         label='Acceptance rate')
            plt.ylabel("Acceptance rate")
            plt.xlabel("Jump width")
            self.save_figure(data_saver, hdl, "jump_width_vs_acceptance")
