import logging
import os
import time

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.calc.RejectionSampling import RejectionSampling
from uq.inversion.routines_check_input import check_inputs
from uq.utils.datatype import unbox, get_dimension
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.logging import init_logging, finish_logging
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

""" --------------------------------------- parameters -------------------------------------------------------- """

# choose uncertain parameters (to be inferred)
key = "attributesPedestrian.speedDistributionMean"

# choose quantity of interest (scenario file must provide the output)

qoi = "flow.txt"

legal_limits_parameter = np.array([0.1, 3.0])

# prior
prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.5, "Dim": 1, "Limits": legal_limits_parameter}

# data
bool_real_data = True  # otherwise run simulation  with true_parameter_value
real_data = np.array([[1.288, 1.674, 1.900, 2.123, 2.364]])

# artificial data
# true_parameter_value = np.array([1.34, 5.0, 3.0])
true_parameter_value = np.array(1.34)
nr_runs_averaged_data = None

eps_noise = 0.5 / 2 + 0.10
eps_noise = 0
qoi_dim = 60
qoi_dim = 5

# config of simulator
path2tutorial = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.join(path2tutorial, "../scenarios", "vadere-console.jar")

path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_all_in_one_N60.scenario")
# path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_egress.scenario")
# path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_small_source.scenario")
# path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_individual_egress.scenario")
# path2scenario4data = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_individual_egress_2_pop.scenario")
# path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_single_N60_12m.scenario")


run_local = False
# seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 1072346

nr_steps = 10 ** 5

bool_averaged = False
no_runs_averaged = None  # average simulation runs

# ABC parameters
abc_threshold_relative_vec = np.array([0.0685])
surrogate_noise_std = 0.04

bool_noise = False
meas_noise = 0.0

""" Parameters for surrogate """
bool_surrogate = False
bool_surrogate_data_misfit = False  # Surrogate for data misfit function

nr_points_surrogate = None
nr_points_surrogate_error = None
nr_points_averaged_surrogate = None

limits_surrogate = None

surrogate_fit_type = None
bool_load_surrogate = None
surrogate_path = None

bool_write_data = True
bool_plot = True

if __name__ == "__main__":  # main required by Windows to run in parallel

    start = time.time()

    # initialize random object
    random_state = RandomState(seed)

    data_saver = DataSaver(path2tutorial)

    init_logging(log_file_path=data_saver.get_path_to_files())
    # sys.stdout = Logger(folder_results) # problematic in suq controller

    main_logger = logging.getLogger("run_inversion.main")
    main_logger.info("*** METHOD: ABC ***")

    main_logger.info(path2model)
    #  main_logger.info(path2scenario)
    main_logger.info("Seed %d" % seed)

    # todo move to constructor of Parameter file
    _, run_local = check_inputs(bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                bool_surrogate=bool_surrogate, nr_points_surrogate=nr_points_surrogate,
                                surrogate_fit_type=surrogate_fit_type,
                                nr_points_averaged_surrogate=nr_points_averaged_surrogate, method="ABC",
                                burn_in=None, bool_load_surrogate=bool_load_surrogate, run_local=run_local)

    key_dim = get_dimension(key)

    results = dict()
    results_acceptance_rate = dict()

    # configure model
    vadere = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                         n_jobs=-1, log_lvl="OFF", qoi_dim=qoi_dim)
    vadere.set_data_saver(data_saver)
    # vadere.set_n_jobs(30)  # for GNM
    vadere.set_bool_fixed_seed(False)

    #  vadere_data = VadereModel(run_local=run_local, path2scenario=path2scenario4data, path2model=path2model,
    #  key="obstPotentialHeight", qoi=qoi, n_jobs=-1, log_lvl="OFF", qoi_dim=qoi_dim)
    # vadere_data.set_bool_fixed_seed(False)

    # data
    if bool_real_data:
        data = real_data
    else:
        # generate data from Vadere
        if bool_noise:
            # todo: only works for scalar QoI !
            dim_qoi = get_dimension(qoi)
            noise = unbox(random_state.normal(0, meas_noise, size=dim_qoi))
        else:
            noise = np.zeros(qoi_dim)

        # generate data with true parameter
        main_logger.info("Generate (synthetical) data point for inversion")
        # average many runs so that data point is a "mean" value
        model_eval, _, _ = vadere.eval_model_averaged(parameter_value=true_parameter_value,
                                                      nr_runs_averaged=nr_runs_averaged_data,
                                                      random_state=random_state)

        data = model_eval + noise
        main_logger.info("Generated (synthetical) data point for inversion")

    for abc_threshold_relative in abc_threshold_relative_vec:
        my_prior = PriorFactory().create_prior_by_type(prior_params)
        path2results = os.getcwd()

        param = InversionParameterRejection(model=vadere, prior=my_prior, seed=seed, bool_averaged=bool_averaged,
                                            no_runs_averaged=no_runs_averaged, path2results=path2results,
                                            bool_surrogate=bool_surrogate, bool_load_surrogate=bool_load_surrogate,
                                            surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                                            nr_points_surrogate=nr_points_surrogate,
                                            surrogate_fit_type=surrogate_fit_type,
                                            nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                                            bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                            nr_points_surrogate_error=nr_points_surrogate_error,
                                            bool_write_data=bool_write_data, n_samples=nr_steps, data=data,
                                            true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                            meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                            bool_plot=bool_plot, run_local=run_local,
                                            abc_threshold_relative=abc_threshold_relative,
                                            surrogate_noise_std=surrogate_noise_std)

        inv_calc = InversionCalculator(param=param, result=InversionResult())
        sampling = RejectionSampling(param)
        inv_calc.set_sampling_strategy(sampling)
        inv_calc.inversion()
        inv_results = inv_calc.get_result()

        inv_calc.get_data_saver().dump_var_to_file(var=inv_results, name="inv_result")
        inv_calc.get_data_saver().dump_var_to_file(var=param, name="inv_param")

    main_logger.info("Total computation time: {} min".format((time.time() - start) / 60))

    finish_logging()
