import logging
import os
import time
from typing import List

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.routines_check_input import check_inputs
from uq.utils.datatype import get_dimension, unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.logging import init_logging, finish_logging
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

""" --------------------------------------- parameters -------------------------------------------------------- """

# choose method
method = "ABC"
# method = "BayesianInversion"

# choose uncertain parameters (to be inferred)
key = ["simTimeStepLength", "pedPotentialHeight"]

# choose quantity of interest (scenario file must provide the output)
# qoi = "mean_density.txt"
qoi = "cTimeStep.fundamentalDiagram"
qoi_dim = 1

initial_point_input = np.array([0.15, 1.0])  # 2D input

legal_limits_parameter = np.array([[0.1, 0.0], [1.0, 5.0]])  # 2D input

# prior
# prior_params = {"Type": "Normal", "Mean": 1.0, "Variance": 1.0}
prior_params = {"Type": "Uniform", "Low": np.array([0.1, 0.0]), "High": np.array([1.0, 5.0]),
                "Limits": legal_limits_parameter, "Dim": len(key)}  # 2D input

# data
bool_real_data = True  # otherwise run simulation  with true_parameter_value
# real_data = np.array([1.288])  # [1.288;1.674; 1.900; 2.123;2.364];  # flow from seyfried-2009 with N= 60 participants
# real_data = np.array([1.288, 1.674, 1.900, 2.123, 2.364])  # flow from seyfried-2009 with N= 60 participants
real_data = np.array([1.34, 1.913, 5.4])  # parameter for fd-diagram

# todo: get real data from experiment

true_parameter_value = 1.34

# config of simulator
cur_dir = os.path.dirname(os.path.realpath(__file__))
path2tutorial = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.abspath(os.path.join(cur_dir, "../scenarios", "vadere-console.jar"))
path2scenario = os.path.abspath(os.path.join(cur_dir, "../scenarios/C-180-180-180.scenario"))

run_local = True
# seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 1072346

nr_steps = 10 ** 5
nr_steps = 10 ** 4
nr_steps = 5

burn_in = int(0.1 * nr_steps)

# ABC parameters
# abc_threshold_relative = 0.5
abc_threshold_relative = 10.0
surrogate_noise_std = 0.04

# jump width
jump_widths = np.array([0.5])
bool_adaptive_jump_width = True
acceptance_rate_limits = np.array([0.4, 0.6])
# acceptance_rate_limits = np.array([0.5, 1.0])

bool_noise = False
meas_noise = 0.0

batch_jump_width = 100

# averaging of model evaluations
bool_averaged = False
no_runs_averaged = None

""" Parameters for surrogate """
bool_surrogate = False
bool_surrogate_data_misfit = False  # Surrogate for data misfit function

# nr_points_surrogate = 50
nr_points_surrogate_error = 25
# nr_points_averaged_surrogate = 30

nr_points_surrogate = 5
nr_points_averaged_surrogate = 3

limits_surrogate = legal_limits_parameter
# limits_surrogate = np.array([150, 250])
# limits_surrogate = np.array([[0.5, 150.0], [2.2, 250.0]])

surrogate_fit_type = "spline"  # types: 'rf','spline','rational', 'poly'
bool_load_surrogate = False
surrogate_path = 'results/2019-10-03_09-44-52_894611/data_misfit_surrogate.pickle'  # poly fit

bool_plot = True


def run_inversion_ABC_corridor_fct(run_local: bool, nr_steps: int, burn_in: int, jump_widths: np.ndarray, seed: int,
                                   method: str, bool_write_data: bool = True) -> List[InversionResult]:
    start = time.time()

    data_saver = DataSaver(path2tutorial, "summary")
    init_logging(log_file_path=data_saver.get_path_to_files())

    main_logger = logging.getLogger("run_inversion.main")
    main_logger.info("*** METHOD: %s ***" % method)

    main_logger.info(path2model)
    main_logger.info("Seed %d" % seed)
    outer_random_state = RandomState(seed)

    burn_in, run_local = check_inputs(bool_surrogate_data_misfit, bool_surrogate, nr_points_surrogate,
                                      surrogate_fit_type, nr_points_averaged_surrogate, method, burn_in,
                                      bool_load_surrogate, run_local)

    # configure model
    vadere = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")
    vadere.set_data_saver(data_saver)

    # data
    if bool_real_data:
        data = real_data
    else:
        # generate data from Vadere
        if bool_noise:
            # todo: only works for scalar QoI !
            dim_qoi = get_dimension(qoi)
            noise = unbox(outer_random_state.normal(0, meas_noise, size=dim_qoi))
        else:
            noise = 0

        # generate data with true parameter
        main_logger.info("Generate (synthetical) data point for inversion")
        data = vadere.eval_model(parameter_value=true_parameter_value) + noise
        main_logger.info("Generated(synthetical) data point for inversion")

    rho = PriorFactory().create_prior_by_type(prior_params)
    param = InversionParameterRejection(model=vadere, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                        no_runs_averaged=no_runs_averaged, path2results=None,
                                        bool_surrogate=bool_surrogate,
                                        bool_load_surrogate=bool_load_surrogate, surrogate_path=surrogate_path,
                                        limits_surrogate=limits_surrogate, nr_points_surrogate=nr_points_surrogate,
                                        surrogate_fit_type=surrogate_fit_type,
                                        nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                                        bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                        nr_points_surrogate_error=nr_points_surrogate_error,
                                        bool_write_data=bool_write_data,
                                        n_samples=nr_steps, data=data, true_parameter_value=true_parameter_value,
                                        bool_noise=bool_noise, meas_noise=meas_noise,
                                        legal_limits_parameters=legal_limits_parameter, bool_plot=bool_plot,
                                        run_local=run_local, abc_threshold_relative=abc_threshold_relative,
                                        surrogate_noise_std=surrogate_noise_std)

    list_of_results = list()
    acceptance_rate = dict()

    for jump_width in jump_widths:  # 1e-1, 7e-2, 5e-2,3e-2,1e-2,7e-3, 5e-3, 1e-3]:

        if np.size(jump_widths) > 1:
            main_logger.info("\n*** Jump width = {} *** ".format(jump_width))

        param.set_path2results(data_saver.get_path_to_files())
        calc = InversionCalculator(param=param, result=InversionResult())
        calc.inversion()
        result = calc.get_result()

        acceptance_rate[jump_width] = result.get_acceptance_ratio()

        list_of_results.append(result)

    main_logger.info("Total computation time: {} min".format((time.time() - start) / 60))

    finish_logging()

    return list_of_results


if __name__ == "__main__":  # main required by Windows to run in parallel
    run_inversion_ABC_corridor_fct(run_local, nr_steps, burn_in, jump_widths, seed=1646748349, method="ABC")
    run_inversion_ABC_corridor_fct(run_local, nr_steps, burn_in, jump_widths, seed=1646748349,
                                   method="BayesianInversion")
