import logging
import os
import time

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.inversion.ioput.InversionPlotterMetropolis import InversionPlotterMetropolis
from uq.utils.datatype import unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.logging import init_logging, finish_logging
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

# --------------------------------------- Parameters --------------------------------------------------------


method = "BayesianInversion"
key = ["speedDistributionMean", "pedPotentialHeight"]
key = ["obstPotentialHeight", "pedPotentialHeight"]
key = "speedDistributionMean"

qoi = "flow.txt"
qoi_dim = 5
qoi_dim = 1
qoi_dim = 5

initial_point_input = np.array([0.75, 7.0])
initial_point_input = np.array([5.0, 7.0])
initial_point_input = np.array(0.75)

# legal_limits_parameter = [0.5, 2.0]
legal_limits_parameter = np.array([0.1, 3.0])
legal_limits_parameter = np.array([[0.1, 1.0], [3.0, 10.0]])
legal_limits_parameter = np.array([[1.0, 1.0], [6.0, 10.0]])
legal_limits_parameter = np.array([0.1, 3.0])

prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.2, "Dim": 1, "Limits": legal_limits_parameter}
prior_params = {"Type": "Uniform", "Low": np.array([0.5, 3.0]), "High": np.array([2.2, 10.0]), "Dim": 2,
                "Limits": legal_limits_parameter}

prior_params = {"Type": "Uniform", "Low": np.array([2.0, 3.0]), "High": np.array([5.0, 10.0]), "Dim": 2,
                "Limits": legal_limits_parameter}
prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.2, "Dim": 1, "Limits": legal_limits_parameter}

true_parameter_value = 1.34
true_parameter_value = np.array([1.34, 4.0])
true_parameter_value = np.array([3.0, 4.0])
true_parameter_value = 1.34

bool_data = False
real_data = np.array([1.288, 1.674, 1.900, 2.123, 2.364])  # flow from seyfried-2009 with N= 60 participants

path2tutorial = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.join(path2tutorial, "../scenarios", "vadere-console.jar")
path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_all_in_one_N60.scenario")

run_local = False
# seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 1072346

nr_steps = 10 ** 5

burn_in = int(0.1 * nr_steps)

no_runs_averaged = 10
bool_averaged = False

# jump width
jump_widths = np.array([0.5, 1.0])
jump_widths = np.array([0.75, 1.0])
jump_widths = np.array([0.5])

bool_adaptive_jump_width = True
acceptance_rate_limits = np.array([0.4, 0.6])  # adaptive acceptance rate
batch_jump_width = 100

bool_noise = True
meas_noise = 0.04

# Parameters for surrogate
bool_surrogate = False
bool_surrogate_data_misfit = True  # Surrogate for data misfit function

nr_points_surrogate = 50
nr_points_surrogate_error = 25
nr_points_averaged_surrogate = 10

limits_surrogate = legal_limits_parameter

surrogate_fit_type = "spline"  # types: 'rf','spline','rational', 'poly'
surrogate_fit_type = "poly"
bool_load_surrogate = False
surrogate_path = None


def run_inversion_fct(method: str, nr_steps: int, run_local: bool, burn_in: int, jump_widths: np.ndarray, seed: int,
                      bool_write_data: bool = True, bool_plot: bool = True, bool_surrogate_data_misfit: bool = False):
    start = time.time()

    if bool_write_data:
        global_data_saver = DataSaver(path2tutorial, "summary")
        init_logging(log_file_path=global_data_saver.get_path_to_files())
    else:
        global_data_saver = None

    main_logger = logging.getLogger("run_inversion.main")
    main_logger.info("*** METHOD: %s ***" % method)

    main_logger.info(path2model)
    #  main_logger.info(path2scenario)
    main_logger.info("Seed %d" % seed)
    random_state = RandomState(seed)  # initialize random state

    results_ess = dict()
    results_acceptance_rate = dict()

    # Configure model
    vadere = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                         qoi_dim=qoi_dim, n_jobs=-1, log_lvl="OFF")
    vadere.set_data_saver(global_data_saver)

    # Generate data from Vadere
    main_logger.info("Generate (synthetical) data point for inversion")
    if bool_noise:
        noise = unbox(random_state.normal(0, meas_noise, size=qoi_dim))
    else:
        noise = np.zeros(qoi_dim)

    if not bool_data:
        data = vadere.eval_model(true_parameter_value, random_state=random_state) + noise
        main_logger.info("Generated (synthetical) data point for inversion")
    else:
        data = real_data

    print("Data: ")
    print(data)

    rho = PriorFactory().create_prior_by_type(params=prior_params)
    inv_params = InversionParameterMetropolis(model=vadere, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                              no_runs_averaged=no_runs_averaged,
                                              path2results=global_data_saver.get_path_to_files(),
                                              bool_surrogate=bool_surrogate,
                                              bool_load_surrogate=bool_load_surrogate,
                                              surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                                              nr_points_surrogate=nr_points_surrogate,
                                              surrogate_fit_type=surrogate_fit_type,
                                              nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                                              bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                              nr_points_surrogate_error=nr_points_surrogate_error,
                                              bool_write_data=bool_write_data, n_samples=nr_steps, data=data,
                                              true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                              meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                              bool_plot=bool_plot, run_local=run_local,
                                              initial_point=initial_point_input, jump_width=None,
                                              bool_adaptive_jump_width=bool_adaptive_jump_width,
                                              batch_jump_width=batch_jump_width,
                                              acceptance_rate_limits=acceptance_rate_limits, burn_in=burn_in)

    for jump_width in jump_widths:
        if len(key) > 1:
            jump_width = jump_widths
        data_saver = DataSaver(global_data_saver.get_path_to_files())
        vadere = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                             n_jobs=-1, log_lvl="OFF", qoi_dim=qoi_dim)
        vadere.set_data_saver(data_saver)

        inv_params.set_model(vadere)
        inv_params.set_jump_width(jump_width=jump_width)
        inv_params.set_path2results(data_saver.get_path_to_files())
        inv_params.set_data_saver(data_saver)

        result = InversionResultMetropolis()
        calc = InversionCalculator(param=inv_params, result=result)
        sampling = MetropolisSampling(param=inv_params)
        calc.set_sampling_strategy(sampling_config=sampling)
        calc.inversion()
        # result = calc.get_result()

        print(result.get_effective_sample_size())
        results_ess[jump_width] = unbox(result.get_effective_sample_size())
        results_acceptance_rate[jump_width] = unbox(result.get_total_acceptance_ratio())

    main_logger.info("Total computation time: {} min".format((time.time() - start) / 60))

    if bool_plot:
        plotter = InversionPlotterMetropolis(param=inv_params, result=None, data_saver=global_data_saver)
        plotter.plot_results_post_runs(results_ess=results_ess, results_acceptance_rate=results_acceptance_rate,
                                       jump_widths=jump_widths)

    finish_logging()

    return None


if __name__ == "__main__":
    run_inversion_fct(method=method, nr_steps=nr_steps, run_local=run_local, burn_in=burn_in, jump_widths=jump_widths,
                      seed=seed, bool_surrogate_data_misfit=bool_surrogate_data_misfit)
