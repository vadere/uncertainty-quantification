%% clean up
clear
close all
clc

rng(263726236)

%% define colors
% sns.color_palette("colorblind")
col = [0.00392156862745098, 0.45098039215686275, 0.6980392156862745;
    0.8705882352941177, 0.5607843137254902, 0.0196078431372549;
    0.00784313725490196, 0.6196078431372549, 0.45098039215686275;
    0.8352941176470589, 0.3686274509803922, 0.0;
    0.8, 0.47058823529411764, 0.7372549019607844 ;
    0.792156862745098, 0.5686274509803921, 0.3803921568627451 ;
    0.984313725490196, 0.6862745098039216, 0.8941176470588236;
    0.5803921568627451, 0.5803921568627451, 0.5803921568627451;
    0.9254901960784314, 0.8823529411764706, 0.2;
    0.33725490196078434, 0.7058823529411765, 0.9137254901960784];

col_abc = col(1,:);
col_pe = col(2,:);
col_data = col(3,:);

%% ABC params
prior_upper = 5;
prior_lower = 0.1;

N_cand = 1E6;

abc_tolerance = 0.01;
tolerance_vec = [1e-1 1e-2 1e-3 1e-4 1e-5 1e-6];
count = 1;
bool_plot = false;

%% noise
noise_level = 0.01;

%% data for theoretical model (helbing-2000)
x = [0.25, 0.5 0.75 1 1.25, 1.5:0.5:5.0];
y = [1.1, 1.22, 1.27  1.3 1.27, 1.1, 0.82, 0.6, 0.45, 0.35, 0.3, 0.22, 0.2];

my_fit = fit(x',y','cubicinterp');
fun = @(x)feval(my_fit,x);

widths = [0.8:0.1:1.2];
% fun5d = @(x)( feval(my_fit,x) +  randn(length(x),1)*noise_level)* widths ;
% fun5d_pure = @(x)( feval(my_fit,x) + randn(length(x),1))* widths;

fun5d = @(x) feval(my_fit,x)* widths +  randn(length(x),5)*noise_level;
%     fun5d_pure = @(x)( feval(my_fit,x)*factor_data)* widths +  randn(length(x),5)*noise_level;
%
% fun5d = @(x) (feval(my_fit,x) + randn(length(x),1)*noise_level)* widths ;


%% data for inversion (seyfried-2009)
% data_seyfried = [1.288, 1.674, 1.900, 2.123, 2.364];
% data = data_seyfried;
true_parameter = 0.5;
data = fun5d(true_parameter);
% data = feval(my_fit,true_parameter) * widths

%% plot data

xlab = 'Desired speed [m/s]';
ylab = 'Flow';

tmp_x = linspace(prior_lower, prior_upper,1000);


%
% figure();
% plot(x,y,'o','LineWidth',2)
% hold on
% plot(tmp_x, fun(tmp_x),'r--')
% h_l = legend('Data','Fit','Data for inversion');
% set(h_l, 'Interpreter','latex')
%
% xlabel('Desired velocity [m/s]','Interpreter','latex')
% ylabel('Pedestrian flow $J$ [1/(m$\cdot$s)], divided by desired velocity','Interpreter','latex')
% set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
% set(gca,'FontSize',12)

%% kladek curve
% kladek =@(rho) 1.34*(1-exp(-1.913 * (1./rho - 1/5.4)));
%
% figure();
% tmp_rho = linspace(0.0, 5.4,1000);
% plot(tmp_rho, tmp_rho.*kladek(tmp_rho),'--')
% xlabel('Density [1/m^2]')
% ylabel('Flow [1/(m s)]')


%% flow (not divided by desired velocity)
my_fit2 = fit(x',(x.*y)','cubicinterp');
fun_flow = @(x)feval(my_fit2,x);

figure();
plot(x, x.*y, 'o')
hold on
plot(tmp_x, fun_flow(tmp_x),'r--')
xlabel(xlab,'Interpreter','latex')
ylabel('Specific flow $J_s$ [1/(m$\cdot$s)]','Interpreter','latex')
h_l = legend('Data from helbing-2000','Interpolation');
set(h_l, 'Interpreter','latex')
set(h_l, 'Location','SouthEast')
set(gca,'TickLabelInterpreter','latex')
set(gca,'FontSize',16)
saveas(gcf, 'data_helbing_interpolation.eps','epsc')



%% for different bottleneck widths
if 1
    figure();
    tmp_x = linspace(prior_lower, prior_upper,1000);
    % plot(x,y,'o','LineWidth',2)
    hold on
    plot(tmp_x, fun5d(tmp_x)','-')
    
    for i = 1:length(widths)
        plot(get(gca,'xlim'), data(i)*ones(2,1),'-.','Color','black')
    end
    % plot(true_parameter, data, 'd','LineWidth',2,'Color',col_data)
    
    xlabel(xlab,'Interpreter','latex')
    ylabel('Pedestrian flow $J$ [1/s]','Interpreter','latex')
    set(gca,'TickLabelInterpreter','latex')
    
    h_l = legend('$0.8$\,m','$0.9$\,m','$1.0$\,m','$1.1$\,m','$1.2$\,m','Data');
    set(h_l, 'NumColumns',3)
    set(h_l, 'Interpreter','latex');
    set(gca,'FontSize',16)
    ylim([0 2])
    box on
    saveas(gcf,'emulator_noise_1e-2.eps','epsc')
end


%% candidates


candidates = rand(N_cand,1)*(prior_upper -prior_lower) + prior_lower;


%% eval model at prior

model_at_candidates = fun5d(candidates);
distance_measure = sum( (model_at_candidates - data).^2, 2); % sum of squares

%% point estimate
[point_estimate_val , idx_pe] = min(distance_measure);
point_estimate_cand = candidates(idx_pe);

fprintf('Tolerance & Acceptance rate & Data CI Size & Size CI PE & Size CI ABC \\\\ \\hline\n')

for abc_tolerance = tolerance_vec
    
    rng(263726236)
    
    
    
    %% find samples
    idx = find(distance_measure <= abc_tolerance);
    if isempty(idx)
        min_val = min(distance_measure);
        tmp = sort(distance_measure);
        abc_tolerance = tmp(0.01*N_cand);
        idx = find(distance_measure <= abc_tolerance);
        %     abc_tolerance = max(distance_measure(idx));
        fprintf('Set tolerance to %.3f\n', abc_tolerance)
    end
    
    if isempty(idx) || length(idx) < 1000
        continue
    end
    
    samples = candidates(idx);
    model_at_abc_samples = model_at_candidates(idx,:);
    
    %     fprintf('Acceptance rate: %.2f%%\n', length(idx)/N_cand*100)
    
    
    
    %% plot distance measure
    %     if bool_plot
    figure();
    semilogy(candidates, distance_measure,'o','Color',[0.7 0.7 0.7])
    hold on
    semilogy(samples, distance_measure(idx), 'x','Color',col_abc)
    
    semilogy(point_estimate_cand, point_estimate_val, 'd','Color',col_pe,'LineWidth',2)
    h_l = legend('Candidates','Samples', 'Point estimate') ;
    set(h_l, 'Interpreter','latex')
    
    xlabel(xlab, 'Interpreter','latex')
    ylabel('Distance measure', 'Interpreter','latex')
    set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
    set(gca,'FontSize',14)
    set(h_l, 'Location','NorthWest')
    ylim([0 1])
    %     end
    
    %% histogram of samples
    
    %     fprintf('Mean of samples: %.3f\n', mean(samples))
    %     fprintf('Var of samples: %.3f\n', var(samples))
    %     fprintf('Acceptance rate: %.3f\n\n', length(idx)/N_cand)
    
    
    %     fprintf('Tolerance & Acceptance rate & Mean of posterior & Standard deviation of posterior \\\\ \\hline\n')
    % for tolerance = [1.0 0.5 0.3 0.1]
    if bool_plot
        tolerance = abc_tolerance;
        
        idx = find(distance_measure <= tolerance);
        samples = candidates(idx);
        
        figure()
        histogram(samples,'Normalization','pdf','FaceColor',col_abc)
        hold on
        plot(point_estimate_cand*ones(2,1), get(gca,'ylim'),'-.','LineWidth',2,'Color',col_pe)
        h_l = legend('Posterior samples','Point estimate') ;
        set(h_l, 'Interpreter','latex')
        
        xlabel(xlab, 'Interpreter','latex')
        set(gca,'FontSize',14)
        set(h_l, 'Location','NorthWest')
        %     fprintf('%.3f \t & %.3f \t & %.3f \t & %.3f\\\\ \n\n', tolerance, length(idx)/N_cand*100, mean(samples), std(samples))
        set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
    end
    
    %% Propagation of all samples
    
    % choose 1000 samples to propagate
    idx_propagation = round(rand(1000,1)*(length(samples)-1)+1) ;
    samples_propgation = samples(idx_propagation);
    
    model_at_abc_samples = fun5d(samples_propgation);
    N_abc = length(samples_propgation);
    model_at_pe = fun5d(repmat(point_estimate_cand,N_abc,1));
    
    figure();
    for i = 1:length(widths)
        subplot(length(widths),1,i)
        histogram(model_at_abc_samples(:,i),'Normalization','pdf','FaceColor',col_abc)
        hold on
        histogram(model_at_pe(:,i),'Normalization','pdf','FaceColor',col_pe)
        set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
        % plot([min(model_at_pe(:,i)) max(model_at_pe(:,i))],get(gca,'ylim'),'Color',col_pe)
    end
    h_l = legend('ABC Posterior','Point estimate');
    set(h_l, 'Interpreter','latex')
    xlabel(ylab, 'Interpreter','latex')
    
    %% box plots
    if bool_plot
        figure()
        subplot(1,2,1)
        boxplot(model_at_abc_samples, widths,'Color',col_abc)
        ylimits = get(gca,'ylim');
        title('ABC','Interpreter','latex')
        xlabel('Bottleneck width','Interpreter','latex')
        ylabel('Flow','Interpreter','latex')
        set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
        hold on
        subplot(1,2,2)
        boxplot(model_at_pe, widths, 'Color',col_pe)
        title('PE','Interpreter','latex')
        ylim(ylimits)
        xlabel('Bottleneck width','Interpreter','latex')
        ylabel('Flow','Interpreter','latex')
        set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
    end
    
    %% Confidence intervals of linear fit
    % model_at_samples_t = model_at_samples';
    % model_at_pe_t = model_at_pe';
    % [coeff_data,ci_data,r_data] = regress(reshape(repmat(widths,N_abc,1), [], 1), reshape(repmat(data, N_abc,1),[],1));
    % [coeff_abc,ci_abc,r_abc] = regress(reshape(repmat(widths,N_abc,1), 1, [])', model_at_samples_t(:));
    % [coeff_pe,ci_pe,r_pe] = regress(reshape(repmat(widths,N_abc,1), 1, [])', model_at_pe_t(:));
    %
    % diff(ci_abc)
    % diff(ci_pe)
    % diff(ci_data)
    
    %% regression with just 1 data set at once
    % size_abc_ci = 0;
    % size_abc_pe = 0;
    % N_averages = 200;
    %
    % for i = 1:N_averages
    %     idx_abc = round(rand(1,5)*(N_abc-1)+1);
    %     idx_pe  = round(rand(1,5)*(N_abc-1)+1);
    %     [~,ci_abc_tmp,~] = regress(widths', diag(model_at_samples(idx_abc,[1:5])));
    %     [~,ci_pe_tmp,~] = regress(widths', diag(model_at_pe(idx_pe,[1:5])));
    %
    %     size_abc_ci = size_abc_ci + diff(ci_abc_tmp);
    %     size_abc_pe = size_abc_pe + diff(ci_pe_tmp);
    %
    %     mdl_abc = fitglm(widths, diag(model_at_samples(idx_abc,1:5)), 'Intercept', false);
    %     tmp_abc = coefCI(mdl_abc);
    %     ci_abc_glm(i,:) = tmp_abc(1,:) ;
    %
    %     slope_abc(i) = mdl_abc.Coefficients.Estimate(1);
    %     %     y_abc(i) = mdl_abc.Coefficients.Estimate(1);
    %     y_abc(i) = 0;
    %
    %     mdl_pe = fitglm(widths, diag(model_at_pe(idx_pe,1:5)), 'Intercept', false);
    %     tmp_pe = coefCI(mdl_pe);
    %     ci_pe_glm(i,:)  = tmp_pe(1,:);
    %     slope_pe(i) = mdl_pe.Coefficients.Estimate(1);
    %     %     y_pe(i) = mdl_pe.Coefficients.Estimate(1);
    %     y_pe(i) = 0;
    %
    % end
    %
    % mdl_exp = fitglm(widths, data, 'Intercept', false);
    % tmp_exp = coefCI(mdl_exp);
    % ci_data_glm  = tmp_exp(1,:);
    % slope_exp = mdl_exp.Coefficients.Estimate(1);
    % % y_exp = mdl_exp.Coefficients.Estimate(1);
    % y_exp = 0;
    %
    % % regress
    % % size_abc_ci = size_abc_ci / N_averages;
    % % size_abc_pe = size_abc_pe / N_averages;
    % % size_abc_ci
    % % size_abc_pe
    %
    % [coeff_abc,ci_abc,r_abc] = regress(reshape(repmat(widths,N_abc,1), 1, [])', model_at_samples_t(:));
    % [coeff_pe,ci_pe,r_pe] = regress(reshape(repmat(widths,N_abc,1), 1, [])', model_at_pe_t(:));
    % [coeff_data,ci_data,r_data] = regress(reshape(repmat(widths,N_abc,1), [], 1), reshape(repmat(data, N_abc,1),[],1));
    %
    % diff(ci_data)
    %
    % disp('Using fitglm')
    % mean_abc_ci = [mean(ci_abc_glm(:,1)) mean(ci_abc_glm(:,2))];
    % mean_pe_ci = [mean(ci_pe_glm(:,1)) mean(ci_pe_glm(:,2))];
    
    
    %% -> check_confidence_intervals.m - using fit and confint
    
    level = 0.85;
    N = N_cand;
    
    
    fit_data = fit(widths', data', 'poly1');
    ci_data = confint(fit_data,level);
    size_ci_data = diff(ci_data(:,1));
    N_averages = 200;
    
    size_ci_pe = zeros(1, N_averages);
    size_ci_pe_mixed = zeros(1, N_averages);
    ci_pe_mixed_save = zeros(N_averages,2);
    for i = 1:N_averages
        % fit through 1 sample
        
        idx_5 = round(rand(1,5)*(N_abc-1)+1);
        fit_abc_mixed = fit(widths', diag(model_at_abc_samples(idx_5,1:5)), 'poly1');
        ci_abc_mixed = confint(fit_abc_mixed,level);
        ci_abc_mixed_save(i,:) = ci_abc_mixed(:,1);
        size_ci_abc_mixed(i) = diff(ci_abc_mixed(:,1));
        
        idx_5 = round(rand(1,5)*(N_abc-1)+1);
        fit_pe_mixed = fit(widths', diag(model_at_pe(idx_5,1:5)), 'poly1');
        ci_pe_mixed = confint(fit_pe_mixed,level);
        ci_pe_mixed_save(i,:) = ci_pe_mixed(:,1);
        size_ci_pe_mixed(i) = diff(ci_pe_mixed(:,1));
        
    end
    
    %% Print results
    
    % fprintf('Tolerance & Acceptance rate & Data CI Size & Size CI PE & Size CI ABC \\\\ \\hline\n')
    
    fprintf('%.3e & %.3f & %.3f & %.3f & %.3f \\\\ \n', abc_tolerance, ...
        length(idx)/N_cand*100, size_ci_data, mean(size_ci_pe_mixed), mean(size_ci_abc_mixed))
    
    %     fprintf('Data Size CI: \t\t%f\n', size_ci_data)
    %     fprintf('Size CI PE (%dx): \t%f\n', N_averages, mean(size_ci_pe_mixed))
    %     fprintf('Mean CI PE: \t\t [%.5f, %.5f]\n', mean(ci_pe_mixed_save))
    %     fprintf('Size CI ABC (%dx): \t%.5f\n', N_averages, mean(size_ci_abc_mixed))
    %     fprintf('Mean CI ABC: \t\t [%.5f, %.5f]\n', mean(ci_abc_mixed_save))
    %
    %
    %% Plot fits
    % figure
    % % all datapoints
    % plot(reshape(repmat(widths,N_abc,1), 1, []), model_at_samples(:), 'x', 'Color',col_abc, 'HandleVisibility','off')
    % hold on
    % plot(reshape(repmat(widths,N_abc,1), 1, []), model_at_pe(:)', 'o','Color',col_pe, 'HandleVisibility','off')
    %
    %
    % plot(widths, mean(model_at_samples), 'x', 'Color',col_abc, 'MarkerSize',13,'LineWidth',2)
    % hold on
    % plot(widths, mean(model_at_pe), 'o', 'Color',col_pe, 'LineWidth',2, 'MarkerSize',13)
    % plot(widths,data,'d','Color',col_data, 'LineWidth',2, 'MarkerSize',13)
    %
    %
    %
    % xlimits = get(gca,'xlim');
    % x_vals = [linspace(xlimits(1), xlimits(2), 100)];
    % plot(x_vals, x_vals*mean(slope_abc) + mean(y_abc),'--','Color',col_abc, 'LineWidth',2)
    % plot(x_vals, x_vals*mean(slope_pe) + mean(y_pe),'.-','Color',col_pe, 'LineWidth',2)
    % plot(x_vals, x_vals*slope_exp + y_exp,'.-','Color',col_data, 'LineWidth',2)
    %
    %
    %
    %
    % h_l = legend('ABC','PE','Exp.', sprintf('fitglm(ABC), Slope=%.2f, CI=%.2f',mean(slope_abc),diff(mean_abc_ci)),...
    %     sprintf('fitglm(PE), Slope=%.2f, CI=%.2f',mean(slope_pe),diff(mean_pe_ci)),...
    %     sprintf('fitglm(Exp.), Slope=%.2f, CI=%.2f',slope_exp, diff(ci_data_glm)));
    % xlabel('Bottleneck width','Interpreter','latex')
    % ylabel('Flow','Interpreter','latex')
    % set(h_l, 'Interpreter','latex')
    % set(h_l, 'Location','Best')
    % set(gca,'xtick',widths')
    % set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
    
    %%
    summary_size_ci_pe(count) = mean(size_ci_pe_mixed);
    summary_size_ci_abc(count) = mean(size_ci_abc_mixed);
    n_samples(count) = length(samples);
    std_abc(count, :)  = std(model_at_abc_samples);
    std_pe(count,:) = std(model_at_pe);
    count = count +1;
    
    
    
end


%% Plot size CI
figure()
semilogx(tolerance_vec(1:length(summary_size_ci_pe)), summary_size_ci_pe,'o-')
hold on
semilogx(tolerance_vec(1:length(summary_size_ci_abc)), summary_size_ci_abc, 'd--')
h_l = legend('PE','ABC');
set(h_l, 'Interpreter','latex')
set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
set(h_l, 'Location','NorthWest')

xlabel('Tolerance $\epsilon$', 'Interpreter','latex')
ylabel('Size of CI for linear regression', 'Interpreter','latex')
set(gca,'FontSize',16)
ylim([0.1 0.8])
xlim([10^-6 10^0])

%% number of samples
figure()
loglog(tolerance_vec(1:length(n_samples)), n_samples,'o-')
ylabel('Number of samples', 'Interpreter','latex')
xlabel('Tolerance $\epsilon$', 'Interpreter','latex')
set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
set(gca,'FontSize',16)

%% standard deviation
figure()
semilogx(tolerance_vec(1:length(n_samples)), std_abc, '.--')
hold on
semilogx(tolerance_vec(1:length(n_samples)), std_pe, 'k-.')

semilogx(tolerance_vec(1:length(summary_size_ci_abc)), summary_size_ci_abc/10, 'd--', 'LineWidth',2)

semilogx(tolerance_vec(1:length(summary_size_ci_pe)), summary_size_ci_pe/10,'o-', 'LineWidth',2)
set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
set(gca,'FontSize',16)


