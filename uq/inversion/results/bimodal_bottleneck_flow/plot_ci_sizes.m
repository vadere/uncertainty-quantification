%% clean up 
clear 
close all 
clc


%% data

results_1e4 = [0.100 , 26.100 , 0.000 , 0.115 , 0.725 ; 
0.010 , 10.720 , 0.000 , 0.114 , 0.324 ; 
0.001 , 2.710 , 0.000 , 0.095 , 0.194 ]; 


results_1e5 = [0.100 , 25.957 , 0.000 , 0.119 , 0.711 ;
    0.050 , 24.092 , 0.000 , 0.110 , 0.580 ;
    0.010 , 11.133 , 0.000 , 0.113 , 0.328 ;
    0.005 , 6.129 , 0.000 , 0.111 , 0.272 ;
    0.001 , 3.222 , 0.000 , 0.113 , 0.178 ;
    0.0001 , 0.864 , 0.000 , 0.111 , 0.168 ;
    0.00001 , 0.312 , 0.000 , 0.109 , 0.166 ];

results_1e6 = [1e-1 , 26.541 , 0.000 , 0.115 , 0.749;
    1e-2 , 11.009 , 0.000 , 0.113 , 0.328 ;
    1e-3 , 2.825 , 0.000 , 0.115 , 0.185 ;
    1e-4 , 0.892 , 0.000 , 0.114 , 0.172 ;
    1e-5 , 0.259 , 0.000 , 0.116 , 0.164 ;
    1e-6 , 0.077 , 0.000 , 0.118 , 0.172 ]; 
    % 1e-9 ,  0.003 ,  0.000 ,  0.104 ,  0.177 ]; 

   

%% visualize 
figure()
% % semilogx(results_1e4(:,1), results_1e4(:,end-1),'o-')
% hold on

% semilogx(results_1e4(:,1), results_1e4(:,end),'d:')
% semilogx(results_1e5(:,1), results_1e5(:,end-1),'o-')
% semilogx(results_1e5(:,1), results_1e5(:,end),'d:')
semilogx(results_1e6(:,1), results_1e6(:,end-1),'o-')
hold on 
semilogx(results_1e6(:,1), results_1e6(:,end),'d:')
h_l = legend('PE ($10^4$)','ABC ($10^4$)','PE ($10^5$)','ABC ($10^5$)','PE ($10^6$)','ABC ($10^6$)');
set(h_l,  'Interpreter','latex')
ylabel('Size of CI for slope (linear fit)', 'Interpreter','latex')
xlabel('ABC tolerance $\epsilon$', 'Interpreter','latex')
set(gca,'FontSize',14)
set(get(gcf,'CurrentAxes'),'TickLabelInterpreter', 'latex')
