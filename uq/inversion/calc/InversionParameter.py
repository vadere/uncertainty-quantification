import logging
from typing import Union

import numpy as np

from uq.utils.calc.Parameter import Parameter
from uq.utils.prior.Prior import Prior


class InversionParameter(Parameter):

    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, bool_surrogate: bool, bool_load_surrogate: bool, surrogate_path: str,
                 limits_surrogate: np.ndarray, nr_points_surrogate: int, surrogate_fit_type: str,
                 nr_points_averaged_surrogate: int, bool_surrogate_data_misfit: bool, nr_points_surrogate_error: int,
                 method: str, bool_write_data: bool, n_samples: int, data: Union[float, np.ndarray],
                 true_parameter_value: Union[float, np.ndarray], bool_noise: bool, meas_noise: Union[float, np.ndarray],
                 legal_limits_parameters: np.ndarray, bool_plot: bool, run_local: bool):

        run_local = self.check_inputs(bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                                      bool_surrogate=bool_surrogate,
                                      nr_points_surrogate=nr_points_surrogate, surrogate_fit_type=surrogate_fit_type,
                                      nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                                      bool_load_surrogate=bool_load_surrogate, run_local=run_local)

        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results)

        # todo encapsulate surrogate parameters?
        self.__bool_surrogate = bool_surrogate
        self.__bool_load_surrogate = bool_load_surrogate
        self.__surrogate_path = surrogate_path
        self.__limits_surrogate = limits_surrogate
        self.__nr_points_surrogate = nr_points_surrogate
        self.__surrogate_fit_type = surrogate_fit_type
        self.__nr_points_averaged_surrogate = nr_points_averaged_surrogate
        self.__bool_surrogate_data_misfit = bool_surrogate_data_misfit
        # number of points at which surrogate error is evaluated
        self.__nr_points_surrogate_error = nr_points_surrogate_error

        self.__method = method  # todo enum instead?
        self.__bool_write_data = bool_write_data
        self.__n_samples = n_samples

        self.__data = data
        self.__true_parameter_value = true_parameter_value  # only for artifical data
        self.__bool_noise_data = bool_noise
        if meas_noise is None:
            self.__meas_noise = 0
        else:
            self.__meas_noise = meas_noise
        self.__legal_limits_parameters = legal_limits_parameters

        self.__bool_plot = bool_plot

        self.__run_local = run_local  # todo check where this is normally stored

    # GETTERS

    def is_surrogate(self) -> bool:
        return self.__bool_surrogate

    def is_surrogate_data_misfit(self) -> bool:
        return self.__bool_surrogate_data_misfit

    def is_load_surrogate(self) -> bool:
        return self.__bool_load_surrogate

    def get_surrogate_path(self) -> str:
        return self.__surrogate_path

    def get_nr_points_surrogate(self) -> int:
        return self.__nr_points_surrogate

    def get_limits_surrogate(self) -> np.ndarray:
        return self.__limits_surrogate

    def get_surrogate_fit_type(self) -> str:
        return self.__surrogate_fit_type

    def get_method(self) -> str:
        return self.__method

    def is_write_data(self) -> bool:
        return self.__bool_write_data

    def get_nr_points_averaged_surrogate(self) -> int:
        return self.__nr_points_averaged_surrogate

    def get_n_samples(self) -> int:
        return self.__n_samples

    def get_data(self) -> Union[float, np.ndarray]:
        return self.__data

    def get_meas_noise(self) -> Union[float, np.ndarray]:
        return self.__meas_noise

    def get_legal_limits_parameters(self) -> np.ndarray:
        return self.__legal_limits_parameters

    def get_true_parameter_value(self) -> Union[float, np.ndarray]:
        return self.__true_parameter_value

    def is_noisy_data(self):
        return self.__bool_noise_data

    def is_plot(self):
        return self.__bool_plot

    def get_nr_points_surrogate_error(self) -> int:
        return self.__nr_points_surrogate_error

    def get_run_local(self) -> bool:
        return self.__run_local

    # Check inputs

    # todo move logic in repective Parameter constructors
    def check_inputs(self, bool_surrogate_data_misfit: bool, bool_surrogate: bool, nr_points_surrogate: int,
                     surrogate_fit_type: str, nr_points_averaged_surrogate: int, bool_load_surrogate: bool,
                     run_local: bool) -> bool:
        if bool_surrogate_data_misfit and bool_surrogate:
            raise Exception("Surrogate can be constructed either for Vadere (bool_surrogate = True) or for the misfit "
                            "function (bool_surrogate_data_misfit = True) but not for both at the same time")

        if (bool_surrogate_data_misfit or bool_surrogate) \
                and surrogate_fit_type is "spline" and nr_points_surrogate <= 3:
            raise Exception("At least 4 data points are needed for spline interpolation")

        if nr_points_averaged_surrogate is not None and nr_points_averaged_surrogate < 1:
            raise Exception("Nr Points Averaged Surrogate must be at least 1 (no averaging)")

        if bool_surrogate_data_misfit and bool_load_surrogate and not run_local:
            run_local = True
            logger = logging.getLogger("check_inputs")
            logger.info(
                "Surrogate is loaded from file, local execution is probably faster since it's only necessary for "
                "the (synthetical) data point")
            logger.info("Run_local is set to True -> Vadere runs are performed locally")

        return run_local
