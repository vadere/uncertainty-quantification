from typing import Union

import numpy as np

from uq.inversion.calc.InversionParameter import InversionParameter
from uq.utils.prior.Prior import Prior


class InversionParameterRejection(InversionParameter):
    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, bool_surrogate: bool, bool_load_surrogate: bool, surrogate_path: str,
                 limits_surrogate: np.ndarray, nr_points_surrogate: int, surrogate_fit_type: str,
                 nr_points_averaged_surrogate: int, bool_surrogate_data_misfit: bool, nr_points_surrogate_error: int,
                 bool_write_data: bool, n_samples: int, data: Union[float, np.ndarray],
                 true_parameter_value: Union[float, np.ndarray], bool_noise: bool, meas_noise: Union[float, np.ndarray],
                 legal_limits_parameters: np.ndarray, bool_plot: bool, run_local: bool, abc_threshold_relative: float,
                 surrogate_noise_std: float):

        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results,
                         bool_surrogate=bool_surrogate, bool_load_surrogate=bool_load_surrogate,
                         surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                         nr_points_surrogate=nr_points_surrogate, surrogate_fit_type=surrogate_fit_type,
                         nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                         bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                         nr_points_surrogate_error=nr_points_surrogate_error, method="ABC",
                         bool_write_data=bool_write_data, n_samples=n_samples, data=data,
                         true_parameter_value=true_parameter_value, bool_noise=bool_noise, meas_noise=meas_noise,
                         legal_limits_parameters=legal_limits_parameters, bool_plot=bool_plot, run_local=run_local)

        self.__abc_threshold_relative = abc_threshold_relative
        self.__surrogate_noise_std = surrogate_noise_std

    def get_abc_threshold_relative(self) -> float:
        return self.__abc_threshold_relative

    def get_surrogate_noise_std(self) -> float:
        return self.__surrogate_noise_std
