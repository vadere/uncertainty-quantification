import logging
import os
import pickle
import time

from uq.inversion.calc import SamplingStrategy
from uq.inversion.calc.InversionParameter import InversionParameter
from uq.inversion.calc.InversionResult import InversionResult
from uq.utils.calc.Calculator import Calculator
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Model import Model
from uq.utils.model.Surrogate1DFactory import Surrogate1DFactory
from uq.utils.model.Surrogate1DFactoryDataMisfit import Surrogate1DFactoryDataMisfit


class InversionCalculator(Calculator):

    def __init__(self, param: InversionParameter, result: InversionResult = None):
        super().__init__(param=param, result=result)
        self.__surrogate_model = None
        self.__sampling_strategy = None

    # GETTERS

    def get_surrogate_model(self) -> Model:  # either model or surrogate
        return self.__surrogate_model

    def get_data_saver(self) -> DataSaver:
        return self.get_param().get_data_saver()

    def get_param(self) -> InversionParameter:
        return super().get_param()

    def get_result(self) -> InversionResult:
        return super().get_result()

    def get_sampling_config(self) -> SamplingStrategy:
        return self.__sampling_strategy

    # SETTERS

    def set_surrogate_model(self, surrogate_model: Model) -> None:
        self.__surrogate_model = surrogate_model

    def set_sampling_strategy(self, sampling_config: SamplingStrategy) -> None:
        self.__sampling_strategy = sampling_config

    def set_result(self, result: "InversionResult") -> None:
        self.__result = result

    # OTHERS

    def inversion(self):
        main_logger = logging.getLogger("InversionCalculator.inversion")
        param = self.get_param()
        vadere = param.get_model()

        if self.get_data_saver() is None:
            data_saver = DataSaver(os.getcwd())
            self.get_param().set_data_saver(data_saver=data_saver)
        else:
            data_saver = self.get_data_saver()

        result = self.get_result()

        if param.is_surrogate():
            # Construct surrogate
            if param.is_load_surrogate():  # Load existing surrogate
                main_logger.info("Load surrogate model for simulator.")

                # todo make sure that the file version is identical, otherwise pickle object cannot be loaded
                vadere_model = pickle.load(open(os.path.abspath(param.get_surrogate_path()), 'rb'))
                r_squared = vadere_model.r_squared
                main_logger.info("Loaded surrogate model for simulator.")
            else:
                # Generate new surrogate
                main_logger.info("Construct surrogate model for simulator.")
                # vadere_model = Surrogate1D(model=vadere, limits=param.get_limits_surrogate(),
                #                           nr_points=param.get_nr_points_surrogate(),
                #                           in_n_keys=vadere.get_dimension())
                # vadere_model.set_data_saver(data_saver)
                # vadere_model.construct_surrogate(function_type=param.get_surrogate_fit_type())

                if vadere.get_qoi_dim() == 1:
                    surrogate_factory = Surrogate1DFactory()
                    vadere_model = \
                        surrogate_factory.create_surrogate_from_model(model=vadere,
                                                                      function_type=param.get_surrogate_fit_type(),
                                                                      limits=param.get_limits_surrogate(),
                                                                      nr_points=param.get_nr_points_surrogate(),
                                                                      n_keys=vadere.get_dimension(),
                                                                      data_saver=data_saver)

                    # Surrogate for data misfit
                    data_misfit_surrogate = None
                    main_logger.info("Constructed surrogate model for simulator.")
                else:
                    raise ValueError('Surrogate is only implemented for 1d output (qoi_dim: %d)' % vadere.get_qoi_dim())

            self.set_surrogate_model(surrogate_model=vadere_model)
            self.get_sampling_config().set_surrogate(surrogate=vadere_model)
        else:
            vadere_model = vadere

        # Surrogate for data misfit function
        bool_surrogate = False
        start = time.time()
        if param.is_surrogate_data_misfit():
            if param.is_load_surrogate():
                main_logger.info("Load surrogate model for data misfit.")
                data_misfit_surrogate = pickle.load(open(os.path.abspath(param.get_surrogate_path()), 'rb'))
                main_logger.info("Loaded surrogate model for data misfit.")

            else:
                main_logger.info("Construct surrogate model for data misfit.")

                surrogate_factory = Surrogate1DFactoryDataMisfit(dim=param.get_model().get_dimension(),  # nr params
                                                                 random_state=self.get_random_state())
                data_misfit_surrogate = \
                    surrogate_factory.create_surrogate(model=vadere,
                                                       function_type=param.get_surrogate_fit_type(),
                                                       sampling_strategy=self.get_sampling_config(),
                                                       limits=param.get_limits_surrogate(),
                                                       nr_points=param.get_nr_points_surrogate(),
                                                       n_keys=param.get_model().get_dimension(),
                                                       nr_points_averaged=param.get_nr_points_averaged_surrogate())

                # data_misfit_surrogate = Surrogate1DDataMisfit(model=vadere,
                #                                              limits=param.get_limits_surrogate(),
                #                                              nr_points=param.get_nr_points_surrogate(),
                #                                              in_n_keys=param.get_model().get_dimension(),
                #                                              in_sampling_config=self.get_sampling_config(),
                #                                              nr_points_averaged_surrogate=param.get_nr_points_averaged_surrogate())
                # data_misfit_surrogate.set_data_saver(data_saver)
                # in_n_keys=dim), in_metropolis_config=metropolis_config)
                # data_misfit_surrogate.construct_surrogate(param.get_surrogate_fit_type())
                main_logger.info("Constructed surrogate model for data misfit.")

            if param.is_write_data():
                # Save surrogate to file
                data_saver.save_to_pickle(None, data_misfit_surrogate, name='data_misfit_surrogate')
                bool_surrogate = True
        else:
            data_misfit_surrogate = None

        self.get_sampling_config().set_surrogate_misfit(data_misfit_surrogate)
        computation_time_surrogate = (time.time() - start) / 60.0

        main_logger.info("Started sampling.")

        result = self.get_sampling_config().run_sampling(result=result)
        main_logger.info("Finished sampling.")

        if param.is_write_data():
            # Save data to file
            main_logger.info("Save data to file")
            writer = self.get_sampling_config().get_file_writer(data_saver=data_saver)
            writer.set_result(result)

            writer.save_results_to_file(sampling_config=self.get_sampling_config(), vadere_model=vadere_model)

            if not param.is_surrogate() and 'scenario_file_hash' in vadere_model.get_map_results().keys():
                vadere_model.get_map_results().pop('scenario_file_hash')

            writer.write_parameters(run_local=param.get_run_local(),
                                    computation_time_surrogate=computation_time_surrogate,
                                    sampling_config=self.get_sampling_config())

            main_logger.info("Saved data to file")
            writer.print_results(computation_time_surrogate=computation_time_surrogate)
            writer.write_result(burn_in=0)

        # Plot results
        if param.is_plot():
            main_logger.info("Plot results")
            plotter = self.get_sampling_config().get_plotter(data_saver=data_saver)
            plotter.set_result(result=result)

            plotter.plot_results(vadere_model=self.get_surrogate_model(),
                                 sampling_config=self.get_sampling_config())

            main_logger.info("Plotted results")
