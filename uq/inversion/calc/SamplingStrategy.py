import warnings
from typing import Union

import numpy as np
import scipy
from numpy.random import RandomState
from scipy.stats import wasserstein_distance

from uq.inversion.calc.InversionParameter import InversionParameter
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.ioput.FileWriterInversion import FileWriterInversion
from uq.inversion.ioput.InversionPlotter import InversionPlotter
from uq.utils.datatype import unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Surrogate import Surrogate
from uq.utils.model.Surrogate1D import Surrogate1D


class SamplingStrategy:

    def __init__(self, param: InversionParameter, nr_runs_averaged: int = 1):
        self.__param = param
        self.__random_state = RandomState(param.get_seed())

        self.__bool_surrogate = False
        self.__surrogate_misfit = None
        self.__surrogate = None
        self.__nr_runs_averaged = nr_runs_averaged

    # GETTERS

    def get_file_writer(self, data_saver: DataSaver = None) -> FileWriterInversion:
        filewriter = FileWriterInversion(param=self.get_param(), data_saver=data_saver)
        return filewriter

    def get_plotter(self, data_saver: DataSaver = None) -> InversionPlotter:
        return InversionPlotter(param=self.get_param(), data_saver=data_saver)

    def get_param(self) -> InversionParameter:
        return self.__param

    def get_dim(self) -> int:
        return self.get_param().get_model().get_dimension()

    def get_random_state(self) -> RandomState:
        return self.__random_state

    def get_nr_runs_averaged(self) -> int:
        return self.__nr_runs_averaged

    def get_surrogate_misfit(self) -> Surrogate1D:
        return self.__surrogate_misfit

    def is_surrogate(self) -> bool:
        return self.__bool_surrogate

    def get_surrogate(self) -> Surrogate:
        return self.__surrogate

    # SETTERS

    def set_surrogate(self, surrogate: Surrogate) -> None:
        if surrogate is not None:
            self.set_bool_surrogate(True)
            self.__surrogate = surrogate

    def set_surrogate_misfit(self, surrogate_data_misfit: Surrogate1D) -> None:
        if surrogate_data_misfit is not None:
            self.set_bool_surrogate(True)
            self.__surrogate_misfit = surrogate_data_misfit

    def set_bool_surrogate(self, bool_surrogate: bool) -> None:
        self.__bool_surrogate = bool_surrogate

    # OTHERS

    def run_sampling(self, result: InversionResult) -> InversionResult:
        pass

    def generate_new_candidate(self) -> Union[float, np.ndarray]:
        pass

    def evaluate_data_misfit_candidate(self, candidate: Union[float, np.ndarray]) -> float:
        pass

    def evaluate_data_misfit_surrogate(self, candidate: Union[float, np.ndarray]) -> float:
        pass

    @staticmethod
    def evaluate_likelihood_from_misfit(data_misfit: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        likelihood = np.exp(-data_misfit)
        return likelihood

    def evaluate_data_misfit(self, model_eval: Union[float, np.ndarray], eps_noise: float = 0.0) \
            -> Union[float, np.ndarray]:
        if self.get_param().get_meas_noise() > 0:
            factor = 1 / 2 * np.power(self.get_param().get_meas_noise(), -1 / 2)
        else:
            factor = 1.0

        if np.size(self.get_param().get_data()) == 1:  # single qoi
            inflated_data = np.ones(shape=np.shape(model_eval)) * self.get_param().get_data()
            difference = np.expand_dims(inflated_data - model_eval, axis=0)
            f_d = np.power(np.linalg.norm(factor * difference, axis=0), 2)

        elif np.ndim(self.get_param().get_data() == 2):
            inflated_data = np.ones(shape=np.shape(model_eval)) * self.get_param().get_data()
            difference = inflated_data - model_eval
            if np.ndim(difference) == 1:  # if single data point
                axis = 0
            else:
                axis = 1
            tmp_f_d = np.linalg.norm(factor * difference, axis=axis)
            f_d = np.power(tmp_f_d, 2)
        else:
            # todo: handle more dimensional data (expand in the right dimension)
            raise UserWarning("Higher-dimensional data (dim>=3) not yet supported")

        return f_d

    @staticmethod
    def kladek(density: Union[float, np.ndarray], free_flow: Union[float, np.ndarray], gamma: Union[float, np.ndarray],
               rho_max: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        velocity = free_flow * (1 - np.exp(-gamma * (1 / density - 1 / rho_max)))
        return velocity

    def evaluate_data_misfit_distribution(self, model_eval: Union[float, np.ndarray]) -> np.ndarray:
        # todo multi-dimensional comparison
        metric = -1 * np.ones(len(model_eval))
        for i in range(0, len(model_eval)):
            metric[i] = wasserstein_distance(model_eval[i, :], unbox(self.get_param().get_data()))
        return metric

    def evaluate_data_misfit_fundamental_diagram(self, model_eval: np.ndarray) -> np.ndarray:
        # curve fit to kladek curve
        # find the number of runs that were performed
        n_runs = len(model_eval.index.levels[0])
        f_d = np.ones(n_runs) * np.nan
        for i in range(0, n_runs):
            # todo change labels (to a labelname without ID)
            velocity = model_eval.reset_index().loc[model_eval.index.get_level_values(0) == i, 'velocity-PID16']
            density = model_eval.reset_index().loc[model_eval.index.get_level_values(0) == i, 'density-PID16']

            if (np.isnan(density).any() or np.isnan(velocity)).any():
                warnings.warn("evaluate_data_misfit_fundamental_diagram: density and /or velocity have inf entries ")

            # get rid of the zero density entries - assure that resulting vectors have equal length
            non_zero_entries = np.logical_and(density.values > 0, velocity.values > 0)
            density_nonzero = density[non_zero_entries]
            velocity_nonzero = velocity[non_zero_entries]
            try:
                popt, pcov = scipy.optimize.curve_fit(self.kladek, density_nonzero, velocity_nonzero)
            except RuntimeError:
                warnings.warn("Fitting of Kladek curve did not work: %s" % RuntimeError)

            residuals = velocity_nonzero - self.kladek(density_nonzero, *popt)
            ss_res = np.sum(residuals ** 2)
            ss_tot = np.sum((velocity_nonzero - np.mean(velocity_nonzero)) ** 2)
            r_squared = 1 - (ss_res / ss_tot)
            # print(r_squared)

            # plt.figure(3333)
            # plt.plot(density_nonzero, velocity_nonzero, 'x', label='Simulated data')
            # plt.hold
            # plt.plot(np.sort(density_nonzero),  self.kladek(np.sort(density_nonzero), *popt), ':',
            # label='Fit to simulated data')
            # plt.plot(np.sort(density_nonzero),  self.kladek(np.sort(density_nonzero), *self.get_data()), ':',
            # label='Fit to experimental data')
            # plt.legend()
            # plt.show()

            # compare norm of fitted optimal parameters (to data and to simulation)
            f_d[i] = np.linalg.norm(self.get_param().get_data() - popt)

        return f_d
