from typing import Union

import numpy as np

from uq.inversion.calc.InversionParameter import InversionParameter
from uq.utils.prior.Prior import Prior


class InversionParameterMetropolis(InversionParameter):

    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, bool_surrogate: bool, bool_load_surrogate: bool, surrogate_path: str,
                 limits_surrogate: np.ndarray, nr_points_surrogate: int, surrogate_fit_type: str,
                 nr_points_averaged_surrogate: int, bool_surrogate_data_misfit: bool, nr_points_surrogate_error: int,
                 bool_write_data: bool, n_samples: int, data: Union[float, np.ndarray],
                 true_parameter_value: Union[float, np.ndarray], bool_noise: bool, meas_noise: Union[float, np.ndarray],
                 legal_limits_parameters: np.ndarray, bool_plot: bool, run_local: bool,
                 initial_point: Union[float, np.ndarray], jump_width: Union[float, np.ndarray],
                 bool_adaptive_jump_width: bool, batch_jump_width: int, acceptance_rate_limits: np.ndarray,
                 burn_in: int):

        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results,
                         bool_surrogate=bool_surrogate, bool_load_surrogate=bool_load_surrogate,
                         surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                         nr_points_surrogate=nr_points_surrogate, surrogate_fit_type=surrogate_fit_type,
                         nr_points_averaged_surrogate=nr_points_averaged_surrogate,
                         bool_surrogate_data_misfit=bool_surrogate_data_misfit,
                         nr_points_surrogate_error=nr_points_surrogate_error, method="Bayesian inversion",
                         bool_write_data=bool_write_data, n_samples=n_samples, data=data,
                         true_parameter_value=true_parameter_value, bool_noise=bool_noise, meas_noise=meas_noise,
                         legal_limits_parameters=legal_limits_parameters, bool_plot=bool_plot, run_local=run_local)

        if initial_point is None:
            self.__initial_point = super().get_prior().get_mean()
        else:
            self.__initial_point = initial_point

        self.__jump_width = jump_width

        # for adaptive jump with regulation
        self.__bool_acceptance_rate = bool_adaptive_jump_width
        self.__batch_jump_width = batch_jump_width  # batch over which jump width is constant
        self.__acceptance_rate_limits = acceptance_rate_limits
        if burn_in is not None:
            self.__burn_in = burn_in
        else:
            UserWarning('Burn-in cannot be None. It is set to 0.')
            self.__burn_in = 0

        self.check_parameters()

    def get_initial_point(self) -> Union[float, np.ndarray]:
        return self.__initial_point

    def get_jump_width(self) -> Union[float, np.ndarray]:
        return self.__jump_width

    def get_batch_size_jump_width(self) -> int:
        return self.__batch_jump_width

    def get_acceptance_rate_limits(self) -> np.ndarray:
        return self.__acceptance_rate_limits

    def is_adaptive_jump_width(self) -> bool:
        return self.__bool_acceptance_rate

    def get_burn_in(self) -> int:
        return self.__burn_in

    def set_jump_width(self, jump_width: Union[float, np.ndarray]) -> None:
        self.__jump_width = jump_width

    def check_parameters(self) -> None:
        if np.size(self.get_initial_point()) != self.get_model().get_dimension():
            raise ValueError('Size of initial point (%d) does not match input dimension of model (%d)' % (
            np.size(self.get_initial_point()), self.get_model().get_dimension()))

        if self.get_true_parameter_value() is not None:
            if np.size(self.get_true_parameter_value()) != self.get_model().get_dimension():
                raise ValueError('Size of true parameter value (%d) does not match input dimension of model (%d)' % (
                    np.size(self.get_true_parameter_value()), self.get_model().get_dimension()))