import logging
import time
import warnings
from typing import Union, Tuple

import numpy as np

from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.calc.SamplingStrategy import SamplingStrategy
from uq.utils.datatype import box, unbox


class RejectionSampling(SamplingStrategy):

    def __init__(self, param: InversionParameterRejection):

        super().__init__(param=param)

        logger = logging.getLogger("ABC.init")
        logger.info("Threshold for ABC: %f" % self.get_param().get_abc_threshold_relative())
        logger.info("Data: " + str(self.get_param().get_data()))

    # GETTERS

    def get_param(self) -> InversionParameterRejection:
        return super().get_param()

    # OTHERS

    def run_sampling(self, result: InversionResult, no_runs_averaged: int = 1):

        start = time.time()

        nr_accepted = -np.ones(shape=(self.get_param().get_n_samples() + 2,))
        nr_accepted[0] = 0

        # noise_surr: add noise to surrogate samples
        samples, candidates, acceptance_rate, data_misfit, data_misfit_accepted = \
            self.rejection()

        result.set_samples(samples=samples)
        result.set_candidates(candidates=candidates)
        result.set_acceptance_ratio(acceptance_ratio=acceptance_rate)
        result.set_data_misfit_accepted(data_misfit_accepted=data_misfit_accepted)
        result.set_data_misfit(data_misfit=data_misfit)
        result.set_computation_time(computation_time=time.time() - start)

        return result

    def rejection_step(self) -> Tuple[bool, np.ndarray, np.ndarray, float]:

        new_candidate = None
        data_misfit = np.inf
        max_iter = 1000
        iterations = 0
        candidates = np.zeros(max_iter)
        while np.abs(data_misfit) > self.get_param().get_abc_threshold_relative() and iterations < max_iter:
            # generate new candidate
            new_candidate = self.generate_new_candidate()
            candidates[iterations] = new_candidate

            assert not any(np.isnan(box(new_candidate)))

            # data misfit is used as distance function between the data and the evaluation
            data_misfit = self.evaluate_data_misfit_candidate(candidate=new_candidate)

            iterations = iterations + 1

        if np.abs(data_misfit) <= self.get_param().get_abc_threshold_relative():
            # accepted
            # logger.info('** accepted ({}, {})'.format(candidates[iterations+1], model_eval))
            bool_accepted = True
            new_sample = new_candidate
            # candidates = candidates[0:iterations]
        else:
            raise Warning("Out of 100 samples not one candidate was found. Please check your settings!")

        return bool_accepted, new_sample, new_candidate, data_misfit

    def rejection(self):
        # Rejection algorithm for n_samples, evaluates all samples and returns which are accepted and acceptance rate

        n_samples = self.get_param().get_n_samples()
        random_state = self.get_random_state()

        if random_state is None:
            warnings.warn("ABC.rejection(): No random_state object is passed.")

        # generate new candidate
        new_candidates = self.generate_multiple_new_candidates(n_samples=n_samples)
        data_misfit = self.evaluate_data_misfit_candidate(candidate=new_candidates)

        # plot_data_misfit_accepted_rejected(new_candidates, data_misfit, self.get_folder_results())

        tolerance = self.get_param().get_abc_threshold_relative()
        tolerance = np.sort(data_misfit)[int(0.01*len(data_misfit))-1]

        if self.get_dim() == 1:
            candidates_accepted = new_candidates[data_misfit <= tolerance]
        else:
            candidates_accepted = \
                new_candidates[:, np.abs(data_misfit) <= tolerance]
        # else:
        #    raise Warning(' Not yet implemented for higher dimension')

        data_misfit_accepted = data_misfit[np.abs(data_misfit) <= tolerance]

        # todo works probably only for single parameter correct
        acceptance_rate = candidates_accepted.size / n_samples

        return candidates_accepted, new_candidates, acceptance_rate, data_misfit, data_misfit_accepted

    def generate_new_candidate(self) -> np.ndarray:

        # Sample a candidate parameter (vector) from some proposal distribution
        new_candidate = self.generate_multiple_new_candidates(n_samples=1)

        # Assure that candidates are in the limits
        if self.get_param().get_legal_limits_parameters() is not None:
            assert (np.all(new_candidate >= self.get_param().get_x_lower()))
            assert (np.all(new_candidate <= self.get_param().get_x_upper()))

        return new_candidate

    def generate_multiple_new_candidates(self, n_samples: int) -> np.ndarray:
        random_state = self.get_random_state()
        if random_state is None:
            warnings.warn("ABC.generate_new_candidate(): random_state object is None.")

        new_sample = self.get_param().get_prior().sample(n=n_samples, random_state=random_state)
        if self.get_dim() == 1:
            samples_in_limits_min = new_sample[new_sample >= self.get_param().get_x_lower()]
            samples_in_limits_max = samples_in_limits_min[samples_in_limits_min <= self.get_param().get_x_upper()]
            assert (len(samples_in_limits_max) == len(new_sample))
        else:
            # Make sure that the candidates are within the limits
            for i_sample in range(0, n_samples):
                assert np.all(self.get_param().get_x_lower() <= new_sample[:, i_sample]) and \
                       np.all(self.get_param().get_x_upper() >= new_sample[:, i_sample])
        return new_sample

    def evaluate_data_misfit_candidate(self, candidate: Union[float, np.ndarray]) -> Union[float, np.ndarray]:

        random_state = self.get_random_state()

        logger = logging.getLogger("ABC.evaluate_data_misfit_candidate")

        # Evaluate data misfit function at a candidate
        model = self.get_param().get_model()
        data_saver = model.get_data_saver()
        if not self.is_surrogate():
            if self.get_nr_runs_averaged() is not None and self.get_nr_runs_averaged() > 1:
                model_eval, _, _ = model.eval_model_averaged(parameter_value=candidate,
                                                             nr_runs_averaged=self.get_nr_runs_averaged(),
                                                             random_state=random_state)
            else:
                model_eval = model.eval_model(parameter_value=candidate, random_state=random_state)
            if data_saver is not None:
                data_saver.write_var_to_file(variable=candidate, name="candidates")
                data_saver.write_var_to_file(variable=model_eval, name="mean-model_eval")

            # todo do not discriminate by qoi but define types for comparison
            if "fundamentalDiagram" in model.get_qoi():
                f_d = self.evaluate_data_misfit_fundamental_diagram(model_eval)
            elif "egress" in model.get_qoi():
                # Compare distributions of egress times
                f_d = self.evaluate_data_misfit_distribution(model_eval)
            else:
                f_d = self.evaluate_data_misfit(model_eval)
        else:  # surrogate for data misfit present
            f_d = self.evaluate_data_misfit_surrogate(candidate)
            # noisy data misfit
            if self.get_param().get_surrogate_noise_std() > 0:
                noise = random_state.normal(loc=0, scale=self.get_param().get_surrogate_noise_std(), size=len(f_d))
                f_d = f_d + noise
                logger.info("Add noise to surrogate evaluations")

        if np.ndim(f_d) > 1:
            f_d = unbox(f_d)

        if data_saver is not None:
            data_saver.write_var_to_file(variable=f_d, name="data_misfit")

        np.testing.assert_equal(np.size(f_d), self.get_param().get_n_samples())
        return f_d

    def evaluate_data_misfit(self, model_eval: Union[float, np.ndarray], eps_noise: float = 0.0) \
            -> Union[float, np.ndarray]:

        if np.size(self.get_param().get_data()) == 1:  # single qoi
            inflated_data = np.ones(shape=np.shape(model_eval)) * self.get_param().get_data()
            difference = np.expand_dims(inflated_data - model_eval, axis=0)
            f_d = np.linalg.norm(difference, axis=0) ** 2

        elif np.ndim(self.get_param().get_data() == 2):  # multiple qois
            inflated_data = np.ones(shape=np.shape(model_eval)) * self.get_param().get_data()
            difference = inflated_data - model_eval
            f_d = np.linalg.norm(difference, axis=1) ** 2
        else:
            # todo: handle more dimensional data (expand in the right dimension) # when would this happen?
            raise UserWarning("Higher-dimensional data (dim>=3) not yet supported")

        return f_d

    def evaluate_data_misfit_surrogate(self, candidates: np.ndarray) -> np.ndarray:
        # logger.info(candidates)
        surrogate_misfit = self.get_surrogate_misfit()
        results = surrogate_misfit.eval_model(parameter_value=candidates, random_state=self.get_random_state())
        return results
