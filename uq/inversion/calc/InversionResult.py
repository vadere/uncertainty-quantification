import numpy as np

from uq.utils.calc.Result import Result


class InversionResult(Result):
    def __init__(self):
        super().__init__()
        self.__candidates = None  # candidates
        self.__samples = None  # posterior samples
        self.__posterior_samples = None  # posterior evaluated at samples
        self.__posterior_candidates = None  # posterior evaluated at candidates
        self.__acceptance_ratio = None
        self.__model_eval = None
        self.__data_misfit_accepted = None
        self.__data_misfit = None

    def equals(self, other: "InversionResult") -> bool:
        bool_equals = False
        if isinstance(other, InversionResult):
            if np.all(self.get_candidates() == other.get_candidates()) and \
                    np.all(self.get_samples() == other.get_samples()) and \
                    np.all(self.get_posterior_samples() == other.get_posterior_samples()) and \
                    np.all(self.get_posterior_candidates() == other.get_posterior_candidates()) and \
                    np.all(self.get_acceptance_ratio() == other.get_acceptance_ratio()) and \
                    np.all(self.get_data_misfit() == other.get_data_misfit()) and \
                    np.all(self.get_data_misfit_accepted() == other.get_data_misfit_accepted()):
                bool_equals = True
        return bool_equals

        # GETTERS

    def get_candidates(self) -> np.ndarray:
        return self.__candidates

    def get_samples(self) -> np.ndarray:
        return self.__samples

    def get_posterior_samples(self) -> np.ndarray:
        return self.__posterior_samples

    def get_posterior_candidates(self) -> np.ndarray:
        return self.__posterior_candidates

    def get_acceptance_ratio(self) -> np.ndarray:
        return self.__acceptance_ratio

    def get_data_misfit(self) -> np.ndarray:
        return self.__data_misfit

    def get_data_misfit_accepted(self) -> np.ndarray:
        return self.__data_misfit_accepted

    def get_total_acceptance_ratio(self) -> np.ndarray:
        acceptance_ratio = self.get_acceptance_ratio()
        if np.size(self.get_acceptance_ratio()) > 1:
            acceptance_ratio = acceptance_ratio[-1]
        return acceptance_ratio

    # SETTERS

    def set_candidates(self, candidates: np.ndarray) -> None:
        self.__candidates = candidates

    def set_samples(self, samples: np.ndarray) -> None:
        self.__samples = samples

    def set_posterior_samples(self, posterior_samples: np.ndarray) -> None:
        self.__posterior_samples = posterior_samples

    def set_posterior_candidates(self, posterior_candidates: np.ndarray) -> None:
        self.__posterior_candidates = posterior_candidates

    def set_acceptance_ratio(self, acceptance_ratio: np.ndarray) -> None:
        self.__acceptance_ratio = acceptance_ratio

    def set_data_misfit(self, data_misfit: np.ndarray) -> None:
        self.__data_misfit = data_misfit

    def set_data_misfit_accepted(self, data_misfit_accepted: np.ndarray) -> None:
        self.__data_misfit_accepted = data_misfit_accepted
