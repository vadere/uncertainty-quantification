import numpy as np

from uq.inversion.calc.InversionResult import InversionResult


class InversionResultMetropolis(InversionResult):

    def __init__(self):
        super().__init__()
        self.__effective_sample_size = None
        self.__acf_of_samples = None  # autocorrelation of samples
        self.__jump_width = None  # actual jump_width over time (if adaptive jump with)

    # GETTERS

    def get_effective_sample_size(self) -> float:
        return self.__effective_sample_size

    def get_acf_of_samples(self) -> np.ndarray:
        return self.__acf_of_samples

    def get_jump_width(self) -> np.ndarray:
        return self.__jump_width

    # SETTERS

    def set_jump_width(self, jump_width: np.ndarray) -> None:
        self.__jump_width = jump_width

    def set_effective_sample_size(self, ess: float) -> None:
        self.__effective_sample_size = ess

    def set_acf_of_samples(self, acf: np.ndarray) -> None:
        self.__acf_of_samples = acf
