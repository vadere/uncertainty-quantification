import logging
import time
from typing import Union, Tuple

import numpy as np
from statsmodels.tsa.stattools import acf

from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.SamplingStrategy import SamplingStrategy
from uq.inversion.ioput.FileWriterInversionMetropolis import FileWriterInversionMetropolis
from uq.inversion.ioput.InversionPlotterMetropolis import InversionPlotterMetropolis
from uq.utils.datatype import box, unbox
from uq.utils.ioput.DataSaver import DataSaver


class MetropolisSampling(SamplingStrategy):
    max_jump_width_factor = 50

    def __init__(self, param: InversionParameterMetropolis):
        super().__init__(param=param)

    # GETTERS

    def get_param(self) -> InversionParameterMetropolis:
        return super().get_param()

    def get_file_writer(self, data_saver: DataSaver = None) -> FileWriterInversionMetropolis:
        return FileWriterInversionMetropolis(param=self.get_param(), data_saver=data_saver)

    def get_plotter(self, data_saver: DataSaver = None) -> InversionPlotterMetropolis:
        return InversionPlotterMetropolis(param=self.get_param(), data_saver=data_saver)

    # OTHERS

    def generate_proposal_function(self, jump_width: Union[float, np.ndarray], limits: np.ndarray = None,
                                   last_sample: Union[float, np.ndarray] = None) \
            -> Tuple["Prior", np.ndarray, np.ndarray]:

        from uq.utils.prior.GaussianGenTrunc import GaussianGenTrunc
        from uq.utils.prior.GaussianGenTruncMult import GaussianGenTruncMult
        from uq.utils.prior.GaussianGen import GaussianGen
        from uq.utils.prior.GaussianGenMult import GaussianGenMult
        from uq.utils.prior.PriorFactory import PriorFactory

        prior_params = {"Type": "Normal", "Mean": last_sample, "Variance": jump_width, "Limits": limits,
                        "Dim": self.get_dim()}
        rho = PriorFactory().create_prior_by_type(prior_params)

        if limits is None:
            if self.get_dim() == 1:
                # no parameter limits
                proposal_function = GaussianGen(mean=last_sample, variance=jump_width)  # full normal distribution
                lower_limit = None
                upper_limit = None
            else:
                proposal_function = GaussianGenMult(mean=last_sample, variance=jump_width,
                                                    dim=self.get_dim())  # full normal distribution
                lower_limit = None
                upper_limit = None
        else:
            if self.get_dim() == 1:
                lower_limit = limits[0]
                upper_limit = limits[1]
            else:
                lower_limit = limits[0, :]
                upper_limit = limits[1, :]

            if self.get_dim() == 1:
                proposal_function = GaussianGenTrunc(mean=last_sample, variance=jump_width,
                                                     limits=np.array([lower_limit, upper_limit]))
            else:
                proposal_function = GaussianGenTruncMult(mean=last_sample, variance=np.eye(self.get_dim()) * jump_width,
                                                         limits=np.array([lower_limit, upper_limit]),
                                                         dim=self.get_dim())
        assert proposal_function.equals(rho)

        return rho, lower_limit, upper_limit

    def generate_new_candidate(self, last_sample: Union[float, np.ndarray], jump_width: Union[float, np.ndarray]) \
            -> np.ndarray:
        random_state = self.get_random_state()
        limits = self.get_param().get_legal_limits_parameters()

        # New candidate
        proposal_function, lower_limit, upper_limit = \
            self.generate_proposal_function(jump_width=jump_width, limits=limits, last_sample=last_sample)

        new_candidate = proposal_function.sample(n=1, random_state=random_state)  # centered around last_sample

        assert not any(np.isnan(box(new_candidate)))

        if limits is not None:
            if self.get_dim() == 1:
                assert (np.all(new_candidate >= lower_limit))
                assert (np.all(new_candidate <= upper_limit))
            else:
                assert (np.all(new_candidate >= np.expand_dims(lower_limit, axis=1)))
                assert (np.all(new_candidate <= np.expand_dims(upper_limit, axis=1)))

        return new_candidate

    def metropolis_step(self, last_sample: Union[float, np.ndarray], posterior_last_sample: float,
                        jump_width: Union[np.ndarray, float]):

        assert not any(np.isnan(box(last_sample)))
        assert not any(np.isnan(box(posterior_last_sample)))

        # generate new candidate
        new_candidate = self.generate_new_candidate(last_sample=last_sample, jump_width=jump_width)

        # evaluate model
        data_misfit = self.evaluate_data_misfit_candidate(new_candidate)

        # evaluate posterior
        # posterior_candidate = self.get_prior_dist().eval_prior(new_candidate) * self.evaluate_likelihood(model_eval)
        posterior_candidate = self.evaluate_posterior(self.get_param().get_prior().eval_prior(new_candidate),
                                                      self.evaluate_likelihood_from_data_misfit(data_misfit))

        if posterior_candidate >= posterior_last_sample:
            # accepted
            # logger.info('** accepted ({}, {})'.format(candidates[iterations+1], model_eval))
            bool_accepted = True
            new_sample = new_candidate
            posterior_sample = posterior_candidate
        else:
            u = self.get_random_state().uniform(0, 1, 1)
            acceptance_criterion = posterior_candidate / posterior_last_sample
            if u <= acceptance_criterion:
                bool_accepted = True
                new_sample = new_candidate
                posterior_sample = posterior_candidate
            else:
                bool_accepted = False
                new_sample = last_sample
                posterior_sample = posterior_last_sample

            assert not any(np.isnan(box(new_sample)))

        return bool_accepted, new_sample, posterior_sample, new_candidate, posterior_candidate, data_misfit

    def adapt_jump_width(self, iterations: int, nr_accepted: np.ndarray, jump_width: Union[float, np.ndarray]) -> float:
        logger = logging.getLogger("Metropolis.adapt_jump_width")
        if self.get_param().is_adaptive_jump_width():

            # adaptive jump width
            if (np.mod(iterations, self.get_param().get_batch_size_jump_width()) == 0) & (iterations > 0):
                # acceptance_rate = (np.sum(np.diff(np.transpose(nr_accepted[iterations-self.get_batch_jump_width(): \
                # iterations])))) / self.get_batch_jump_width()

                # Evaluate acceptance rate over all data (inertia)
                acceptance_rate = nr_accepted[iterations] / iterations

                # Only evaluate last batch
                # acceptance_rate acceptance_rate = (nr_accepted[iterations]- nr_accepted[iterations- \
                # self.get_batch_jump_width()+1])/self.get_batch_jump_width()

                if acceptance_rate > 1:
                    raise ValueError("Problems in calculation of acceptance rate: acceptance_rate > 1!")

                if acceptance_rate > self.get_param().get_acceptance_rate_limits()[1]:
                    jump_width = jump_width / acceptance_rate
                    logger.info("Jump width was adapted to {}".format(jump_width))

                elif acceptance_rate < self.get_param().get_acceptance_rate_limits()[0]:
                    jump_width = jump_width * acceptance_rate
                    logger.info("Jump width was adapted to {}".format(jump_width))

                if np.any(jump_width > self.max_jump_width_factor * self.get_param().get_jump_width()):
                    jump_width = self.max_jump_width_factor * self.get_param().get_jump_width()
                    logger.warning("Jump width seems to be exploding - set to %d* initial jump width" %
                                   self.max_jump_width_factor)

        return jump_width

    def evaluate_data_misfit_candidate(self, candidate: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        # Evaluate data misfit function at a candidate

        if not self.get_param().is_surrogate() and not self.get_param().is_surrogate_data_misfit():
            model = self.get_param().get_model()
            model_eval, _, _ = model.eval_model_averaged(parameter_value=candidate,
                                                           nr_runs_averaged=self.get_nr_runs_averaged(),
                                                           random_state=self.get_random_state())

            f_d = self.evaluate_data_misfit(model_eval)
        elif self.get_param().is_surrogate_data_misfit():  # surrogate for data misfit present
            f_d = self.evaluate_data_misfit_surrogate(candidate)
        elif self.get_param().is_surrogate():
            surrogate = self.get_surrogate()
            surrogate_eval = surrogate.eval_model(parameter_value=candidate, random_state=self.get_random_state())
            f_d = self.evaluate_data_misfit(surrogate_eval)

        return f_d

    def evaluate_likelihood(self, model_eval) -> float:
        f_d = self.evaluate_data_misfit(model_eval)
        likelihood = np.exp(-f_d)
        return likelihood

    @staticmethod
    def evaluate_likelihood_from_data_misfit(data_misfit: float) -> float:
        likelihood = np.exp(-data_misfit)
        return likelihood

    @staticmethod
    def evaluate_posterior(prior_value: float, likelihood_value: float) -> float:
        return prior_value * likelihood_value

    def evaluate_data_misfit_surrogate(self, candidates: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        # logger.info(candidates)
        surrogate_misfit = self.get_surrogate_misfit()
        return surrogate_misfit.eval_model(parameter_value=candidates, random_state=self.get_random_state())

    def run_sampling(self, result: InversionResultMetropolis) -> InversionResultMetropolis:
        logger = logging.getLogger("Metropolis.run_sampling")

        start = time.time()

        nr_iterations = self.get_param().get_n_samples()

        # Allocation
        if self.get_dim() == 1:
            candidates = np.nan * np.ones(shape=(nr_iterations + 1,))
            samples = np.nan * np.ones(shape=(nr_iterations + 1,))
            jump_width = np.nan * np.ones(shape=(nr_iterations + 1,))
        else:
            candidates = np.nan * np.ones(shape=(self.get_dim(), nr_iterations + 1))
            samples = np.nan * np.ones(shape=(self.get_dim(), nr_iterations + 1))
            jump_width = np.nan * np.ones(shape=(self.get_dim(), nr_iterations + 1,))

        posterior_samples = np.nan * np.ones(shape=(nr_iterations + 1,))
        posterior_candidates = np.nan * np.ones(shape=(nr_iterations + 1,))
        nr_accepted = np.nan * np.ones(shape=(nr_iterations + 1,))
        data_misfit = np.nan * np.ones(shape=(nr_iterations + 1,))
        data_misfit_accepted = np.nan*np.ones(shape=(nr_iterations + 1,))

        # Initialization
        count_accepted = 0
        count_rejected = 0
        iterations = 0

        if self.get_dim() == 1:
            candidates[0] = self.get_param().get_initial_point()
            samples[0] = self.get_param().get_initial_point()
            jump_width[0] = self.get_param().get_jump_width()

        else:
            candidates[:, 0] = self.get_param().get_initial_point()
            samples[:, 0] = self.get_param().get_initial_point()
            jump_width[:, 0] = self.get_param().get_jump_width()

        nr_accepted[0] = 0
        new_candidate = self.get_param().get_initial_point()

        data_misfit[0] = self.evaluate_data_misfit_candidate(candidate=new_candidate)
        data_misfit_accepted[0] = data_misfit[0]
        posterior_samples[0] = \
            self.evaluate_posterior(prior_value=self.get_param().get_prior().eval_prior(value=new_candidate),
                                    likelihood_value=self.evaluate_likelihood_from_data_misfit(
                                        data_misfit=data_misfit[0]))

        posterior_candidates[0] = posterior_samples[0]

        # Iterative MCMC sampling

        while iterations < nr_iterations:
            percent_iterations = (iterations / nr_iterations) * 100
            if np.mod(percent_iterations, 10) < 10 ** -5:
                logger.info("%.2f of Sampling (%d of %d)" % (percent_iterations, iterations, nr_iterations))

            if self.get_dim() == 1:
                jump_width[iterations + 1] = self.adapt_jump_width(iterations=iterations, nr_accepted=nr_accepted,
                                                                   jump_width=jump_width[iterations])

            else:
                jump_width[:, iterations + 1] = self.adapt_jump_width(iterations=iterations, nr_accepted=nr_accepted,
                                                                      jump_width=jump_width[:, iterations])
            bool_accepted = None
            if self.get_dim() == 1:

                bool_accepted, samples[iterations + 1], posterior_samples[iterations + 1], \
                candidates[iterations + 1], posterior_candidates[iterations + 1], data_misfit[iterations + 1] = \
                    self.metropolis_step(last_sample=samples[iterations],
                                         posterior_last_sample=posterior_samples[iterations],
                                         jump_width=jump_width[iterations + 1])

            elif self.get_dim() >= 1:
                tmp_jump_width = jump_width[:, iterations + 1]

                bool_accepted, tmp_sample, tmp_posterior_sample, tmp_candidate, tmp_posterior_candidate, \
                tmp_model_eval = self.metropolis_step(last_sample=samples[:, iterations],
                                                      posterior_last_sample=posterior_samples[iterations],
                                                      jump_width=tmp_jump_width)

                samples[:, iterations + 1] = unbox(tmp_sample)
                posterior_samples[iterations + 1] = unbox(tmp_posterior_sample)
                candidates[:, iterations + 1] = unbox(tmp_candidate)
                posterior_candidates[iterations + 1] = unbox(tmp_posterior_candidate)
                data_misfit[iterations + 1] = unbox(tmp_model_eval)

            if bool_accepted:
                count_accepted = count_accepted + 1
                data_misfit_accepted[iterations + 1] = data_misfit[iterations + 1]
            else:
                count_rejected = count_rejected + 1
                data_misfit_accepted[iterations + 1] = data_misfit_accepted[iterations]

            nr_accepted[iterations + 1] = count_accepted

            iterations = iterations + 1

        acceptance_ratio = np.transpose(
            np.divide(np.transpose(nr_accepted[1:]), np.arange(1, nr_iterations + 1)))

        result.set_samples(samples=samples)
        result.set_candidates(candidates=candidates)
        result.set_posterior_samples(posterior_samples=posterior_samples)
        result.set_posterior_candidates(posterior_candidates=posterior_candidates)
        result.set_jump_width(jump_width=jump_width)
        result.set_acceptance_ratio(acceptance_ratio=acceptance_ratio)
        result.set_data_misfit(data_misfit=data_misfit)
        result.set_data_misfit_accepted(data_misfit_accepted=data_misfit_accepted)

        result.set_effective_sample_size(ess=self.effective_sample_size(samples=samples))
        result.set_acf_of_samples(acf=self.calc_acf_samples(samples=samples))

        result.set_computation_time(computation_time=time.time() - start)

        return result

    def effective_sample_size(self, samples: np.ndarray) -> float:

        dim = self.get_dim()
        burn_in = self.get_param().get_burn_in()

        if dim == 1 and len(samples) > 1:
            nr_steps = self.get_param().get_n_samples()
            # from kruschke-2015, p. 184
            acf_samples = self.calc_acf_samples(samples=samples)
            # % ACF
            # % acf = autocorr(tau_save, min(n_steps - 1, 200));
            idx_acf = np.argmax(acf_samples <= 0.05)

            if idx_acf > 0:  # entry was found
                effective_size = (nr_steps - burn_in) / (1 + 2 * np.sum(acf_samples[1:idx_acf]))
            else:
                effective_size = 0

        else:
            # todo: effective sample size is calculated for each dimension independently - is that the common approach?
            effective_size = np.nan * np.ones(dim)
            nr_steps = np.size(samples, axis=1)
            acf_samples = self.calc_acf_samples(samples=samples)

            for i_dim in range(0, dim):
                acf_samples_i = acf_samples[i_dim, :]
                idx_acf = np.argmax(acf_samples_i <= 0.05)

                if idx_acf > 0:  # entry was found
                    effective_size[i_dim] = (nr_steps - burn_in) / (1 + 2 * np.sum(acf_samples_i[1:idx_acf]))
                else:
                    effective_size[i_dim] = 0


        return effective_size

    def calc_acf_samples(self, samples: np.ndarray) -> np.ndarray:
        burn_in = self.get_param().get_burn_in()
        dim = self.get_param().get_model().get_dimension()

        acf_values = None
        if samples is not None:
            if dim == 1 and len(samples) > 1:
                n_samples = len(samples)
                samples_wo_burn_in = samples[burn_in:n_samples]
                acf_values = acf(samples_wo_burn_in, fft=True, nlags=n_samples - burn_in)

            else:
                n_samples = np.size(samples, 1)
                acf_values = np.nan * np.ones((dim, n_samples - burn_in))
                for i_dim in range(0, dim):
                    acf_values[i_dim, :] = acf(samples[i_dim, burn_in:n_samples], fft=True, nlags=n_samples - burn_in)

        return acf_values

    def autocorrelation_samples(self, result: InversionResultMetropolis) -> np.ndarray:
        samples = result.get_samples()
        burn_in = self.get_param().get_burn_in()

        # from https://ipython-books.github.io/103-computing-the-autocorrelation-of-a-time-series/
        result = np.correlate(samples[burn_in:len(samples)], samples[burn_in:len(samples)], mode='full')
        return result[result.size // 2:]  # normalized
