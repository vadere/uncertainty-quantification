import os

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from uq.sensitivity_analysis.calc.SobolIndexCalculatorChaospy import SobolIndexCalculatorChaospy
from uq.sensitivity_analysis.calc.SobolIndexParameterChaospy import SobolIndexParameterChaospy
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.UniformGenMult import UniformGenMult

bool_averaged = True

no_runs_averaged = 2  # fixed seed !

step_size = 0.001  # for approximation of gradients
step_size = 0.025

#  -> with bottleneck_width
x_lower = np.array([0.5, 0.0, 160, 1.0, 1.6, 0.1])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 1.0])  # upper bounds for parameters

x_lower = np.array([1.0, 0.5, 0.0, 160])  # lower bounds for parameters
x_upper = np.array([5.0, 2.2, 1.0, 200])  # upper bounds for parameters

density_type = "uniform"  # input parameter density

# Parameters
# Uncertain parameters

key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==-1].spawnNumber", "sources.[id==-1].distributionParameters", "obstacles.[id==1].y",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters

key = ["searchRadius", "attributesPedestrian.speedDistributionMean",
       "attributesPedestrian.speedDistributionStandardDeviation", "sources.[id==3].spawnNumber"]

# key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
#       "sources.[id==-1].spawnNumber", "sources.[id==-1].distributionParameters", "bottleneck_width",
#       "obstacleRepulsionMaxWeight"]  # uncertain parameters

key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

qoi = "mean_density.txt"  # quantity of interest
# qoi = "waitingtime.txt"
run_local = False  # run on local machine (vs. run on server)

# Model
current_dir = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.abspath(os.path.join(current_dir, "../../scenarios", "vadere-console.jar"))
path2scenario = os.path.abspath(os.path.join(current_dir, "../../scenarios", "Liddle_bhm_v3_fixed_seed.scenario"))
# path2scenario = os.path.join(current_dir, "../scenarios", "bottleneck_OSM_all_in_one_N60.scenario")

test_model = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                         n_jobs=-1, log_lvl="OFF")

#  parameters
M = 10 ** 2  # number of samples
M = 10
order = 3  # order of polynomial
n_runs = 10
n_runs = 3

seed = 13674963

bool_load_approx_model = False
bool_load_samples = True
bool_load_samples = False
path_to_approx_model = "results/2019-10-09_17-45-25_019820/approx_model.pickle"
path_to_samples = "results/2019-10-10_17-16-01_126906/sensitivity_data_sobol.pickle"

bool_plot = True

if __name__ == '__main__':

    rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=test_model.get_dimension())
    params = SobolIndexParameterChaospy(model=test_model, prior=rho, M=M, order=order,
                                        bool_load_approx_model=bool_load_approx_model,
                                        path_to_approx_model=path_to_approx_model, bool_load_samples=bool_load_samples,
                                        path_to_samples=path_to_samples, seed=seed, bool_averaged=bool_averaged,
                                        no_runs_averaged=no_runs_averaged)

    data_saver = DataSaver(os.getcwd(), 'summary')

    if bool_plot:
        h1 = plt.figure()
        color_vec = sns.color_palette("hls", test_model.get_dimension())

    for i_run in range(0, n_runs):
        str_run = "_run" + str(i_run)
        print("** ........................ RUN %d **" % i_run)

        params.set_path2results(data_saver.get_path_to_files())
        calc = SobolIndexCalculatorChaospy(params)
        calc.calc_sobol_indices()
        result = calc.get_result()

        # Plot results
        if bool_plot:
            plt.plot(result.get_total_indices(), ':', color='black')
            for i in range(0, test_model.get_dimension()):
                if i_run == 1:
                    plt.plot(i, result.get_total_indices()[i], 'd', label=key_str_plot[i], color=color_vec[i])
                else:
                    plt.plot(i, result.get_total_indices()[i], 'd', color=color_vec[i])

            plt.ylabel("Sensitivity indices")
            if i_run == 1:
                plt.legend()

    print('*** FINISHED *** ')
    # save all curves in one file
    if bool_plot:
        data_saver.save_figure(h1, 'sensitivity_indices')
        plt.show()
    data_saver.save_to_pickle(data=test_model, name='vadere_config')  # does not change over the runs
