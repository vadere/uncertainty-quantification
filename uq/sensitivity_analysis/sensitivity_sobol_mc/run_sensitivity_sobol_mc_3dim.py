import os
import time

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import RandomState

from uq.sensitivity_analysis.calc.SobolIndexCalculatorMC import SobolIndexCalculatorMC
from uq.sensitivity_analysis.calc.SobolIndexParameterMC import SobolIndexParameterMC
from uq.sensitivity_analysis.ioput.SobolIndexFileWriterChaospy import SobolIndexFileWriterChaospy
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.prior.UniformGenMult import UniformGenMult

bool_averaged = True
no_runs_averaged = 1  # average over multiple runs?

M = 100
M = 3
run_local = False

scenario_name = "Liddle_bhm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
scenario_name = "Liddle_osm_v3_free_seed.scenario"  # Optimal steps model
scenario_name = "Liddle_osm_v4_short.scenario"  # Optimal steps model

# parameter limits
x_lower = np.array([0.5, 0.1, 160])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200])  # upper bounds for parameters

# parameters
key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==3].spawnNumber"]  # uncertain parameters
key_str_plot = ["free-flow mean", "free-flow dev", "spawn number"]
qoi = "mean_density.txt"  # quantity of interest

dim = len(key)

rho = UniformGenMult(x_lower, x_upper, dim)

if __name__ == "__main__":  # main required by Windows to run in parallel

    cur_dir = os.path.dirname(os.path.realpath(__file__))
    data_saver = DataSaver(cur_dir)
    n = 3

    # ----------------------------------------------------------------  allocation

    seeds_vec = np.round(np.random.rand(n) * (2 ** 31 - 1)).astype(np.int)

    sobol_indices_mc = -1 * np.ones(shape=(n, dim))
    normalized_indices_mc = -1 * np.ones(shape=(n, dim))
    # ----------------------------------------------------------------  perform calculations

    count = 0
    for idx in range(0, n):
        start = time.time()
        seed = seeds_vec[idx]  # use the same seeds for all configs
        # print(seed)

        # configure setup (make sure no old infos are stored)
        vadere_model, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)

        params = SobolIndexParameterMC(model=vadere_model, prior=rho, M=M, seed=seed, bool_averaged=bool_averaged,
                                       no_runs_averaged=no_runs_averaged, path2results=None, qoi_dim=1)

        # calculate Sobol' indices
        sobol_calc = SobolIndexCalculatorMC(param=params)
        sobol_calc.calc_sobol_indices()
        results = sobol_calc.get_result()

        sobol_indices_mc[idx, :] = results.get_total_indices_jansen()
        computation_time_samples = results.get_eval_time()

        normalized_indices_mc[idx, :] = sobol_indices_mc[idx, :] / np.linalg.norm(sobol_indices_mc[idx, :])
        computation_time = ((time.time() - start) / 60)

        count = count + 1

        # Save results
        writer = SobolIndexFileWriterChaospy(data_saver=data_saver)
        writer.write_result(total_sens=sobol_indices_mc[idx, :], computation_time=computation_time,
                            M=M, order=None, computation_time_samples=computation_time_samples,
                            key=key, approx_model=None, no_evals=vadere_model.get_n_evals())

    # --------------------------------------------------------------- Save results

    data_saver.write_var_to_file(sobol_indices_mc, 'sobol_indices_mc')

    # --------------------------------------------------------------- Evaluation

    print("Finished calculations - Start of evaluation of results")

    plt.figure()
    for i in range(0, n):
        plt.plot(sobol_indices_mc[i, :], 'o:', label=key[i])

    plt.xlabel('Parameter index')
    plt.ylabel('Sobol indices')
    plt.xticks(np.arange(0, dim))
    plt.show()
    data_saver.save_figure(plt.gcf(), 'sobol_indices')

    plt.figure()
    for i in range(0, n):
        plt.plot(normalized_indices_mc[i, :], 'o:', label=key[i])

    plt.xlabel('Parameter index')
    plt.ylabel('Normalized Sobol indices')
    plt.xticks(np.arange(0, dim))
    plt.show()
    data_saver.save_figure(plt.gcf(), 'normalized_sobol_indices')
