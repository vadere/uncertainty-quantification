import time

import numpy as np
from numpy.random import RandomState

from uq.sensitivity_analysis.calc.SobolIndexCalculatorMC import SobolIndexCalculatorMC
from uq.sensitivity_analysis.calc.SobolIndexParameterMC import SobolIndexParameterMC
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.prior.UniformGenMult import UniformGenMult

bool_averaged = True
no_runs_averaged = 10  # average over multiple runs?

M_vec = [50, 100, 500, 1000, 2000]

n_runs = 3  # number of runs for each number of samples

run_local = False

scenario_name = "bottleneck_OSM_all_in_one_N60_defaults.scenario"
scenario_name = "bottleneck_OSM_all_in_one_N60.scenario"  # Liddle_bhm_v3_fixed_seed,

# parameter limits
x_lower = np.array([1.0, 0.5, 0.1, 20, 40])  # lower bounds for parameters
x_upper = np.array([5.0, 2.2, 0.5, 60, 60])  # upper bounds for parameters

x_lower = np.array([1.0, 0.5, 0.1, 40, 2.0, 5.0, 0.0])  # lower bounds for parameters
x_upper = np.array([5.0, 2.2, 1.0, 80, 10.0, 50.0, 0.15])  # upper bounds for parameters

# parameters
key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
       "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[*].spawnNumber", "pedPotentialHeight"]  # uncertain parameters

key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
       "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[*].spawnNumber", "obstPotentialHeight", "pedPotentialHeight",
       "minStepLength"]  # uncertain parameters

key_str_plot = ["control parameter", "free-flow mean", "free-flow dev", "spawn number", "obstacle repulsion"]
key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "obstacle repulsion",
                "personal space", "minimum step length"]

qoi = "flow.txt"  # quantity of interest
qoi_dim = 5

dim = len(key)

general_seed = 267396767
general_random_state = RandomState(seed=general_seed)  # initialize Random State with the current time

rho = UniformGenMult(x_lower, x_upper, dim)
_, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)

if __name__ == "__main__":  # main required by Windows to run in parallel

    data_saver = DataSaver(path2tutorial, 'summary')

    # ----------------------------------------------------------------  allocation

    seeds_vec = np.round(general_random_state.rand(n_runs) * (2 ** 31)).astype(np.int)

    total_indices_mc = -1 * np.ones(shape=(len(M_vec), n_runs, m))
    first_indices_mc = -1 * np.ones(shape=(len(M_vec), n_runs, m))
    normalized_indices_mc = -1 * np.ones(shape=(len(M_vec), n_runs, m))

    # ----------------------------------------------------------------  perform calculations

    count = 0
    for iM in range(0, len(M_vec)):
        for idx in range(0, n_runs):
            start = time.time()
            seed = seeds_vec[idx]  # use the same seeds for all configs
            # print(seed)

            # configure setup (make sure no old infos are stored)
            vadere_model, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)
            vadere_model.qoi_dim = qoi_dim

            params = SobolIndexParameterMC(model=vadere_model, prior=rho, M=M_vec[iM], seed=seed,
                                           bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                           path2results=data_saver.get_path_to_files(), qoi_dim=qoi_dim)

            sobol_calc = SobolIndexCalculatorMC(param=params)
            sobol_calc.calc_sobol_indices()
            results = sobol_calc.get_result()

            # if total_indices_jansen is not None:
            #    total_indices_mc[iM, idx, :] = total_indices_jansen
            #    first_indices_mc[iM, idx, :] = first_incides_saltelli_2010
            # else:
            #    total_indices_mc[iM, idx, :] = total_indices_constantine

            count = count + 1

    # --------------------------------------------------------------- Save results

    # data_saver.write_var_to_file(total_indices_mc, 'sobol_indices_mc')
    data_saver.write_var_to_file(general_seed, 'general_seed')
    data_saver.write_var_to_file(results, 'SobolIndicesResults')

    print("Finished calculations")
