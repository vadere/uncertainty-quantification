import os

from uq.sensitivity_analysis.calc.SobolIndexParameterMC import SobolIndexParameterMC
from uq.sensitivity_analysis.calc.SobolIndexResultMC import SobolIndexResultMC
from uq.sensitivity_analysis.ioput.SobolIndexFileWriter import SobolIndexFileWriter
from uq.utils.find_software_versions import get_current_uq_state
from uq.utils.ioput.DataSaver import DataSaver


class SobolIndexFileWriterMC(SobolIndexFileWriter):

    def __init__(self, param: SobolIndexParameterMC, result: SobolIndexResultMC, data_saver: DataSaver):
        super().__init__(param=param, result=result, data_saver=data_saver)

    def get_param(self) -> SobolIndexParameterMC:
        return super().get_param()

    def get_result(self) -> SobolIndexResultMC:
        return super().get_result()

    def write_result(self) -> None:
        param = self.get_param()
        result = self.get_result()
        data_saver = self.get_datasaver()

        model = param.get_model()
        no_runs_averaged = param.get_no_runs_averaged()
        x_upper = param.get_prior().get_upper()
        x_lower = param.get_prior().get_upper()
        n_samples = param.get_M()

        data_saver.write_var_to_file(result.get_total_indices_constantine(), "total_indices_constantine")
        if result.get_first_indices_jansen() is not None:
            data_saver.write_var_to_file(result.get_total_indices_jansen(), "total_indices_jansen")
            data_saver.write_var_to_file(result.get_first_indices_jansen(), "first_indices_jansen")
            data_saver.write_var_to_file(result.get_first_incides_saltelli_2010(), "first_incides_saltelli_2010")

        with open(os.path.join(data_saver.get_path_to_files(), "results.txt"), 'a+') as file:
            file.write('** Parameters **: \n')
            file.write('Number of samples (N): \t\t\t\t\t {} \n'.format(n_samples))
            file.write("Number of runs averaged:  \t\t\t {} \n".format(no_runs_averaged))

            file.write('Parameters investigated: \t\t\t {} \n'.format(model.get_key()))
            file.write('Lower limits (parameters): \t\t\t {} \n'.format(x_lower))
            file.write('Upper limits (parameters): \t\t\t {} \n'.format(x_upper))

            file.write('Quantity of interest: \t\t\t\t {} \n'.format(model.get_qoi()))

            file.write('\n** Software versions **: \n')
            # file.write('SUQ Controller Commit (suqc): \t\t {} \n'.format(get_current_suqc_state()["git_hash"]))

            file.write('UQ Software (git_commit_hash): \t\t {} \n'.format(get_current_uq_state()["git_hash"]))

            file.write('UQ Software (uncommited_changes):\t {} \n'.format(
                str.replace(get_current_uq_state()["uncommited_changes"], '\n M', ';')))

            if self.get_param().get_model().get_version_str() is not None:
                file.write(self.get_param().get_model().get_version_str())

            file.write('\n** Results **: \n')

            # varianceAB is only available if bool_first_order is True
            if param.get_bool_first_order():
                file.write('Variance of A + B V[A,B] ~ V[Y]: \t\t\t\t {} \n'.format(result.get_variance_AB()))
            file.write('Variance of A V[A] ~ V[Y]: \t\t\t\t {} \n'.format(result.get_variance_A()))

            file.write(
                'Sobol total indices (Constantine): \t\t\t\t {} \n'.format(result.get_total_indices_constantine()))
            file.write('Sobol total indices (Jansen): \t\t\t\t {} \n'.format(result.get_total_indices_jansen()))

            file.write('Sobol first order indices (Jansen): \t\t\t {} \n'.format(result.get_first_indices_jansen()))
            file.write('Sobol first order indices (Saltelli-2010): \t\t\t {} \n'.format(
                result.get_first_incides_saltelli_2010()))

            file.write('Number of runs performed: \t\t\t {} \n'.format(model.get_n_evals()))

            file.write('\nComputation time: \t\t\t\t\t {} min \n'.format(result.get_eval_time()))

            file.write('\n')
            file.close()
