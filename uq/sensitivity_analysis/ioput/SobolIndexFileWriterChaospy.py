import os

from uq.sensitivity_analysis.calc import SobolIndexParameterChaospy
from uq.sensitivity_analysis.calc.SobolIndexResultChaospy import SobolIndexResultChaospy
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.ioput.FileWriter import FileWriter


class SobolIndexFileWriterChaospy(FileWriter):

    def __init__(self, param: SobolIndexParameterChaospy, result: SobolIndexResultChaospy, data_saver: DataSaver):
        super().__init__(param=param, result=result, data_saver=data_saver)

    def get_param(self) -> SobolIndexParameterChaospy:
        return super().get_param()

    def get_result(self) -> SobolIndexResultChaospy:
        return super().get_result()

    def write_result(self):
        no_evals = 0
        with open(os.path.join(self.get_datasaver().get_path_to_files(), "results.txt"), 'a+') as file:
            file.write('** Parameters **: \n')
            file.write('Number of samples: \t\t\t {} \n'.format(self.get_param().get_M()))
            if self.get_param().get_order() is not None:
                file.write('Order of polynomial: \t\t {} \n'.format(self.get_param().get_order()))
            file.write('Parameters investigated: \t {} \n'.format(self.get_param().get_model().get_key()))
            if no_evals is not None:
                file.write('Number of model evaluations (samples): \t {} \n'.format(no_evals))

            file.write('** Results **: \n')
            if self.get_result().get_approx_model() is not None:
                file.write('Approximation PC expansion: \t {} \n'.format(str(self.get_result().get_approx_model())))
            if self.get_result().get_eval_time() is not None:
                file.write('Computation time (samples): \t {} min \n'.format(self.get_result().get_eval_time()))

            file.write("Sobol' total indices indices: \t\t\t {} \n".format(self.get_result().get_total_indices()))
            file.write("Sobol' first order indices: \t\t\t {} \n".format(self.get_result().get_first_order_indices()))

            file.write('Computation time (total): \t {} min \n'.format(self.get_result().get_computation_time()))
            file.write('Computation time (total indices): \t {} min \n'.format(
                self.get_result().get_computation_time_total_indices()))
            file.write('Computation time (first order indices): \t {} min \n'.format(
                self.get_result().get_computation_time_total_indices()))

            file.write('\n')

            file.close()
