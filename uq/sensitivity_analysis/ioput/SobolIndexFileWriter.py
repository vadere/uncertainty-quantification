from uq.sensitivity_analysis.calc.SobolIndexParameter import SobolIndexParameter
from uq.sensitivity_analysis.calc.SobolIndexResult import SobolIndexResult
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.ioput.FileWriter import FileWriter


class SobolIndexFileWriter(FileWriter):

    def __init__(self, param: SobolIndexResult, result: SobolIndexParameter, data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    def get_result(self) -> SobolIndexResult:
        return super().get_result()

    def get_param(self) -> SobolIndexParameter:
        return super().get_param()
