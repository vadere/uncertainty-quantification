import os

from uq.sensitivity_analysis.calc.SobolIndexParameterSALib import SobolIndexParameterSALib
from uq.sensitivity_analysis.calc.SobolIndexResultSALib import SobolIndexResultSALib
from uq.sensitivity_analysis.ioput.SobolIndexFileWriter import SobolIndexFileWriter
from uq.utils.find_software_versions import get_current_uq_state


class SobolIndexFileWriterSALib(SobolIndexFileWriter):

    def __init__(self, param: SobolIndexParameterSALib, result: SobolIndexResultSALib):
        super().__init__(param=param, result=result)

    def get_result(self) -> SobolIndexResultSALib:
        return super().get_result()

    def get_param(self) -> SobolIndexParameterSALib:
        return super().get_param()

    def write_result(self) -> None:
        model = self.get_param().get_model()
        sobol_indices = self.get_result().get_sobol_indices()

        self.get_datasaver().write_var_to_file(sobol_indices, "sobol_indices")

        self.get_datasaver().write_var_to_file(self.get_result().get_total_sobol_indices(), "sobol_total_indices")

        with open(os.path.join(self.get_datasaver().get_path_to_files(), "results.txt"), 'a+') as file:
            file.write('** Parameters **: \n')
            file.write('Number of samples (N): \t\t\t\t\t {} \n'.format(self.get_param().get_N()))
            file.write("Number of runs averaged:  \t\t\t {} \n".format(self.get_param().get_no_runs_averaged()))

            file.write('Parameters investigated: \t\t\t {} \n'.format(model.get_key()))
            file.write('Lower limits (parameters): \t\t\t {} \n'.format(self.get_param().get_prior().get_lower()))
            file.write('Upper limits (parameters): \t\t\t {} \n'.format(self.get_param().get_prior().get_upper()))

            file.write('Quantity of interest: \t\t\t\t {} \n'.format(model.get_qoi()))

            file.write('\n** Software versions **: \n')
            file.write('UQ Software (git_commit_hash): \t\t {} \n'.format(get_current_uq_state()["git_hash"]))
            file.write('UQ Software (uncommited_changes):\t {} \n'.format(
                str.replace(get_current_uq_state()["uncommited_changes"], '\n M', ';')))

            if self.get_param().get_model().get_version_str() is not None:
                file.write(self.get_param().get_model().get_version_str())

            file.write('\n** Results **: \n')

            file.write('Sobol total indices: \t\t\t\t {} \n'.format(self.get_result().get_total_sobol_indices()))
            if self.get_param().get_bool_first_order():
                file.write(
                    'Sobol first order indices: \t\t\t {} \n'.format(self.get_result().get_first_order_indices()))

            file.write('Number of runs performed: \t\t\t {} \n'.format(model.get_n_evals()))

            file.write('\nComputation time: \t\t\t\t\t {} min \n'.format(self.get_result().get_computation_time()))

            file.write('\n')
            file.close()
