import os
import time

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import RandomState

from uq.sensitivity_analysis.calc.SobolIndexCalculatorSALib import SobolIndexCalculatorSALib
from uq.sensitivity_analysis.calc.SobolIndexParameterSALib import SobolIndexParameterSALib
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.prior.UniformGenMult import UniformGenMult

# todo assure that results are saved to file with  data_saver.write_model_eval_to_file() -> should be fixed


bool_gradient = False  # is an analytical gradient available? -> True
bool_averaged = True
no_runs_averaged = 10  # average over multiple runs?

run_local = False

# parameter limits

x_lower = np.array([0.5, 0.1, 160, 1.0, 1.6, 40])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 60])  # upper bounds for parameters

scenario_name = "Liddle_bhm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
scenario_name = "Liddle_osm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
scenario_name = "Liddle_osm_v4.scenario"  # Liddle_bhm_v3_fixed_seed,

# parameters
key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==3].spawnNumber",
       "sources.[id==3].distributionParameters", "bottleneck_width",
       "pedPotentialHeight"]  # uncertain parameters
key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

test_input = np.array([[1.34], [0.26], [180], [1], [1.6], [0.5]])  # legal test input

qoi = "max_density.txt"

if __name__ == "__main__":

    path2results = os.path.dirname(os.path.realpath(__file__))
    data_saver = DataSaver(path2results, 'summary')

    run_local = False

    # N = 20
    iN_vec = [5, 10, 20, 50]
    n_runs_per_config = 5

    in_no_runs_averaged = 10

    h = plt.figure()

    for iN in iN_vec:
        for i_runs in range(0, n_runs_per_config):
            # configure setup (make sure no old infos are stored)
            test_model, _, _ = configure_vadere_sa(run_local, scenario_name, key, qoi)
            test_model.set_run_local(False)

            seed = int(np.random.rand(1) * (2 ** 32 - 1))
            start = time.time()

            rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=len(key))

            params = SobolIndexParameterSALib(model=test_model, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                              no_runs_averaged=in_no_runs_averaged, N=iN,
                                              path2results=data_saver.get_path_to_files())

            calc = SobolIndexCalculatorSALib(params)
            results = calc.calc_sobol_indices()

            plt.plot(results.get_total_sobol_indices(), '.:', label='N=%d, Run: %d' % (iN, i_runs))

            print()

    plt.xlabel('Parameter index')
    plt.ylabel('Sobol indices (SALib)')
    plt.xticks(np.arange(0, len(key)))
    plt.legend()
    data_saver.save_figure(h, 'sobol_indices_all_runs')
