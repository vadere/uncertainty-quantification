import matplotlib.pyplot as plt
from numpy.random import RandomState

from uq.sensitivity_analysis.calc.SobolIndexCalculatorSALib import SobolIndexCalculatorSALib
from uq.sensitivity_analysis.calc.SobolIndexParameterSALib import SobolIndexParameterSALib
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.prior.UniformGenMult import UniformGenMult

if __name__ == "__main__":
    # load vadere config
    from uq.sensitivity_analysis.sensitivity_chaospy.config_vadere_SA_3dim import *

    path2results = os.path.dirname(os.path.realpath(__file__))

    data_saver = DataSaver(path2results, 'summary')

    run_local = False

    # N = 20
    iN_vec = [1, 5, 10, 20]
    iN_vec = [1, 2, 3]
    n_runs_per_config = 2

    in_no_runs_averaged = 10
    test_model.set_run_local(False)

    plt.figure()

    for iN in iN_vec:
        for i_runs in range(0, n_runs_per_config):
            seed = int(np.random.rand(1) * (2 ** 32 - 1))

            rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=len(key))

            params = SobolIndexParameterSALib(model=test_model, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                              no_runs_averaged=in_no_runs_averaged, N=iN,
                                              path2results=data_saver.get_path_to_files())
            calc = SobolIndexCalculatorSALib(params)
            results = calc.calc_sobol_indices()

            plt.plot(results.get_total_sobol_indices(), '.:', label='N=%d, Run: %d' % (iN, i_runs))

            print()

    plt.xlabel('Parameter index')
    plt.ylabel('Sobol indices (SALib)')
    plt.xticks(np.arange(0, len(key)))
    plt.legend()
    plt.show()

    print()
