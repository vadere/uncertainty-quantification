import os

import numpy as np

from uq.utils.model.VadereModel import VadereModel


def config_vadere_liddle_osm_6_dim(run_local: bool, scenario_name: str, key, qoi):
    # parameter limits
    x_lower = np.array([0.5, 0.1, 160, 1.0, 1.6, 30])  # lower bounds for parameters
    x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 80])  # upper bounds for parameters

    key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                    "obstacle repulsion"]

    cur_dir = os.path.dirname(os.path.realpath(__file__))
    scenario_path = os.path.join(cur_dir, "../../scenarios/")
    path2model = os.path.join(scenario_path, "vadere-console.jar")
    path2scenario = os.path.join(scenario_path, scenario_name)

    m = len(key)

    vaderemodel = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")

    return vaderemodel, m, path2tutorial
