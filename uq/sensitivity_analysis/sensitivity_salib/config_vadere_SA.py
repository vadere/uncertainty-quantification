import os

import numpy as np

from uq.utils.datatype import get_dimension
from uq.utils.model.VadereModel import VadereModel

bool_gradient = False  # gradient available
bool_averaged = True

no_runs_averaged = 10  # fixed seed !

step_size = 0.025  # for approximation of gradients
k = 3 + 1  # desired dimension of subspace +1

#  -> with bottleneck_width
x_lower = np.array([0.5, 0.0, 160, 1.0, 1.6, 0.1])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 1.0])  # upper bounds for parameters

# bool_vec = np.array([False, True, True, False])
density_type = "uniform"  # input parameter density

test_input = np.array([[1.34], [0.26], [180], [1], [1.6], [0.5]])  # legal test input -> with bottleneck_width

# parameters
key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==-1].spawnNumber", "sources.[id==-1].distributionParameters", "bottleneck_width",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters

key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

qoi = "mean_density.txt"  # quantity of interest
# qoi = "waitingtime.txt"
run_local = False  # run on local machine (vs. run on server)

M_boot = 10

# model
cur_dir = os.path.dirname(os.path.realpath(__file__))
path2tutorial = os.path.abspath(os.path.join(cur_dir, "../../inversion"))
print(path2tutorial)

path2model = os.path.abspath(os.path.join(cur_dir, "../../scenarios", "vadere-console.jar"))
print(path2model)

# path2scenario = os.path.join(path2tutorial, "cur_dir", "../../scenarios/Liddle_bhm_v3_free_seed.scenario")
# path2scenario = os.path.join(path2tutorial, "scenarios", "Liddle_bhm_v3_fixed_seed.scenario")
path2scenario = os.path.abspath(
    os.path.join(path2tutorial, "cur_dir", "../../scenarios/Liddle_bhm_v3_free_seed.scenario"))
path2scenario = os.path.abspath(os.path.join(path2tutorial, "cur_dir", "../../scenarios/Liddle_osm_v4.scenario"))

# test_model = TestModel()
test_model = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")

m = get_dimension(key)

# Defining the Model Inputs
problem = {
    'num_vars': len(key),
    'names': key,
    'bounds': np.column_stack((x_lower, x_upper)).tolist()
}
