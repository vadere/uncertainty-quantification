import numpy as np
from numpy.random import RandomState

# load config!
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa

if __name__ == "__main__":  # main required by Windows to run in parallel

    no_runs_averaged = 1  # average over multiple runs?

    run_local = True

    # parameter limits

    x_lower = np.array([0.5, 0.1, 160, 1.0, 1.6, 30])  # lower bounds for parameters
    x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 80])  # upper bounds for parameters
    x_default = np.array([1.34, 0.26, 180, 1, 2.0, 50])

    scenario_name = "Liddle_bhm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
    scenario_name = "Liddle_osm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
    scenario_name = "Liddle_osm_v4.scenario"

    # parameters
    key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
           "sources.[id==3].spawnNumber",
           "sources.[id==3].distributionParameters", "bottleneck_width",
           "pedPotentialHeight"]  # uncertain parameters

    test_input = np.array([[1.34], [0.26], [180], [1], [2], [0.5]])  # legal test input

    qoi = "max_density.txt"  # quantity of interest

    # configure setup
    test_model, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)

    seed = int(np.random.rand(1) * (2 ** 32 - 1))
    random_state = RandomState(seed)

    test_model.set_run_local(True)  # to obtain scenario files

    lower_samples = np.zeros(shape=(m, m))
    upper_samples = np.zeros(shape=(m, m))

    for i in range(0, m):
        tmp_lower = x_default.copy()
        tmp_lower[i] = x_lower[i]
        tmp_upper = x_default.copy()
        tmp_upper[i] = x_upper[i]
        lower_samples[:, i] = tmp_lower
        upper_samples[:, i] = tmp_upper

    samples = np.concatenate((np.expand_dims(x_default, axis=1), np.expand_dims(x_lower, axis=1), lower_samples,
                              upper_samples, np.expand_dims(x_upper, axis=1)), axis=1)

    values_f, _, _ = test_model.eval_model_averaged(parameter_value=samples, nr_runs_averaged=no_runs_averaged,
                                                    random_state=random_state)
