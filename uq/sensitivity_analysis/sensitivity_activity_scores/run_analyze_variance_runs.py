import matplotlib.pyplot as plt
import numpy as np
from numpy.random import RandomState

from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa

# parameter limits
x_lower = np.array([0.5, 0.1, 160, 1.0, 1.6, 0.1])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 1.0])  # upper bounds for parameters

scenario_name = "Liddle_bhm_v4_fixed_seed.scenario"  # Liddle_bhm_v3_fixed_seed, Liddle_bhm_v3_free_seed.scenario

density_type = "uniform"  # input parameter density

# parameters
key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==-1].spawnNumber",
       "sources.[id==-1].distributionParameters", "bottleneck_width",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters
key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

test_input = np.array([[1.34], [0.26], [180], [1], [2], [0.5]])  # legal test input

qoi = "mean_density.txt"  # quantity of interest
run_local = False

test_model, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)

n_runs = 20
no_runs_averaged = 10  # average over multiple runs?

dim = len(key)
if __name__ == "__main__":
    values_f = np.zeros(shape=n_runs)

    seed_list = np.ndarray([])
    plt.figure()

    for i in range(0, n_runs):
        seed = int(np.random.rand(1) * (2 ** 32 - 1))
        random_state = RandomState(seed)
        values_f[i], value_input_duplicated, result = test_model.eval_model_averaged(parameter_value=test_input,
                                                                                     nr_runs_averaged=no_runs_averaged,
                                                                                     random_state=random_state)
        # find outlier and associated seed
        idx = result.values > np.median(result.values) + np.std(result.values) * 3
        if (idx is True).any():
            print("%.10f, %d" % (result.values[idx], value_input_duplicated[-1, :][idx]))
        plt.plot(result.values, '.:')

    plt.figure()
    plt.plot(np.arange(0, n_runs), values_f, 'o:')
    plt.plot(np.arange(0, n_runs), np.mean(values_f * 1.05) * np.ones(n_runs), 'k--')
    plt.plot(np.arange(0, n_runs), np.mean(values_f * 0.95) * np.ones(n_runs), 'k--')

    plt.title("No runs averaged: %d" % no_runs_averaged)
