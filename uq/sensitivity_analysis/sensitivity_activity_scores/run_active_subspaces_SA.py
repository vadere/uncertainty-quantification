import time
import warnings

import matplotlib.pyplot as plt
import numpy as np

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult
from uq.active_subspace.ioput.ActiveSubspaceFileWriter import ActiveSubspaceFileWriter
from uq.active_subspace.ioput.ActiveSubspacePlotterGradients import ActiveSubspacePlotterGradients
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa

bool_gradient = False  # is an analytical gradient available? -> True
bool_averaged = True
no_runs_averaged = 10  # average over multiple runs?
no_runs_averaged = 10

step_size = 10 ** -2  # for (finite-difference) approximation of gradients
step_size = 0.025  # Number of pedestrians wird um 1 erhöht

factor_N = None  # for the local linear model (instead of finite difference approximation)
alpha = 2  # oversampling factor
alpha = 10
k = 2 + 1  # desired dimension of subspace +1
# M_boot = 10  # for bootstrapping
M_boot = 1

case = None

run_local = False

bool_save_data = True
bool_print = True
bool_plot = True

# parameter limits

x_lower = np.array([0.5, 0.1, 160, 1.0, 1.6, 40])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 60])  # upper bounds for parameters

scenario_name = "Liddle_bhm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
scenario_name = "Liddle_osm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed,
scenario_name = "Liddle_osm_v4.scenario"  # Liddle_bhm_v3_fixed_seed,

density_type = "uniform"  # input parameter density

# parameters
key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==3].spawnNumber",
       "sources.[id==3].distributionParameters", "bottleneck_width",
       "pedPotentialHeight"]  # uncertain parameters
key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

test_input = np.array([[1.34], [0.26], [180], [1], [1.6], [0.5]])  # legal test input

qoi = "mean_density.txt"  # quantity of interest
qoi = "max_density.txt"
# qoi = "evac_time.txt"

# configure setup
test_model, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)

if __name__ == "__main__":  # main required by Windows to run in parallel

    n = 3
    n_runs = 1

    test_input = x_lower

    # %% allocation
    count = 0
    mean_error_eig_gradients = np.zeros(n)
    mean_error_eig_finite_diff = np.zeros(n)
    mean_error_eig_local = np.zeros(n)

    alpha_vec = [2, 5, 10, 20]  # np.linspace(2, 10, n)
    seeds_vec = np.round(np.random.rand(n_runs) * (2 ** 31)).astype(np.int)

    result_finite_diff = -1 * np.ones(shape=(n, n_runs, m))
    result_gradients = -1 * np.ones(shape=(n, n_runs, m))
    error_c_gradients = -1 * np.ones(shape=(n, n_runs))
    activity_scores = -1 * np.ones(shape=(n, n_runs, m))
    true_activity_scores = -1 * np.ones(shape=(n, n_runs, m))
    size_subspace = np.zeros(shape=(n, n_runs))

    step_size_relative = step_size * (x_upper - x_lower)

    params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                             bool_gradient=bool_gradient, M_boot=M_boot,
                                             step_size_relative=step_size_relative, step_size=step_size, case=case,
                                             seed=None, bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                             bool_save_data=bool_save_data, bool_print=bool_print, bool_plot=bool_plot,
                                             path2results=path2tutorial)

    # %% perform calculations

    for idx in range(0, n):

        if len(alpha_vec) > 1:
            if len(alpha_vec) is not n:
                n = len(alpha_vec)
                warnings.warn('alpha_vec has as different dimension than the number of planned runs n. Was set to n.')
            else:
                alpha = alpha_vec[idx]
        else:
            alpha = alpha_vec[0]

        tmp_gradients = 0
        tmp_finite_diff = 0
        tmp_local_linear = 0

        for irun in range(0, n_runs):  # average over several runs
            start = time.time()
            seed = seeds_vec[irun]  # use the same seeds for all configs
            # print(seed)

            # configure setup (make sure no old infos are stored)
            test_model, _, _ = configure_vadere_sa(run_local, scenario_name, key, qoi)

            # True gradients
            params.set_seed(seed=seed)
            params.set_model(model=test_model)
            params.set_alpha(alpha=alpha)

            as_calculator = ActiveSubspaceCalculatorGradient(param=params)
            as_calculator.identify_active_subspace()
            results = as_calculator.get_result()

            result_gradients[idx, irun, :] = results.get_error_lambda_hat()
            error_c_gradients[idx, irun] = results.get_error_c_hat()
            activity_scores[idx, irun, :] = results.get_activity_scores()
            true_activity_scores[idx, irun, :] = results.get_activity_scores_true()
            size_subspace[idx, irun] = results.get_size_subspace()
            path_to_files = results.get_path2results()
            n_samples = results.get_n_samples()
            lambda_eig = results.get_lambda_hat()
            w_active = results.get_W_active()

            tmp_gradients = tmp_gradients + result_gradients[idx, irun]

            computation_time = ((time.time() - start) / 60)

            writer = ActiveSubspaceFileWriter(param=params, result=results)
            writer.write_result()

        count = count + 1

    print("Finished calculations - Start of evaluation of results")

    # Accumulation of results
    all_results = ActiveSubspaceResult()
    all_results.set_error_lambda_hat(result_gradients)
    all_results.set_error_c_hat(error_c_gradients)
    all_results.set_activity_scores(activity_scores)
    all_results.set_activity_scores_true(true_activity_scores)
    all_results.set_size_subspace(size_subspace)

    # Plot results
    plotter = ActiveSubspacePlotterGradients(params=params, results=all_results, datasaver=test_model.get_data_saver())
    plotter.plot_results_all_runs(alpha_vec=alpha_vec)

    plt.figure()
    for i in range(0, n):
        plt.plot(activity_scores[idx, n_runs - 1, :], 'o:', label=key[i])

    plt.xlabel('Parameter index')
    plt.ylabel('Activity score')
    plt.xticks(np.arange(0, m))
    plt.legend()
    plt.show()
    test_model.get_data_saver().save_figure(plt.gcf(), 'activity_scores')

    plt.figure()
    for i in range(0, n):
        plt.plot(activity_scores[idx, n_runs - 1, :] / np.linalg.norm(activity_scores[idx, n_runs - 1, :]), 'o:',
                 label=key[i])

    plt.xlabel('Parameter index')
    plt.ylabel('Normalized Activity score')
    plt.xticks(np.arange(0, m))
    plt.legend()
    plt.show()
    test_model.get_data_saver().save_figure(plt.gcf(), 'normalized_activity_scores')
