import time
import warnings

import numpy as np

from uq.sensitivity_analysis.calc.SobolIndexCalculator import SobolIndexCalculator
from uq.sensitivity_analysis.calc.SobolIndexParameterMC import SobolIndexParameterMC
from uq.sensitivity_analysis.calc.SobolIndexResultMC import SobolIndexResultMC
from uq.sensitivity_analysis.ioput.SobolIndexFileWriterMC import SobolIndexFileWriterMC
from uq.utils.datatype import unbox, box_dim
from uq.utils.ioput.DataSaver import DataSaver


class SobolIndexCalculatorMC(SobolIndexCalculator):

    def __init__(self, param: SobolIndexParameterMC):
        result = SobolIndexResultMC()
        super().__init__(param=param, result=result)

    def get_param(self) -> SobolIndexParameterMC:
        return super().get_param()

    def get_result(self) -> SobolIndexResultMC:
        return super().get_result()

    # m: number of input parameters
    # M: number of Monte Carlo points
    # from constantine-2017 (Matlab code)
    def calc_sobol_indices(self) -> SobolIndexResultMC:
        start = time.time()

        params = self.get_param()

        model = params.get_model()
        if params.get_path2results() is not None:
            data_saver = DataSaver(params.get_path2results())
            model.set_data_saver(data_saver)
        else:
            data_saver = None
            warnings.warn('No path2results input for DataSaver! ')

        N = int(np.floor(params.get_M() / (1 + params.get_m())))
        if N <= 2:  # otherwise the variance is 0, leading to infinity indices
            N = 2
            warnings.warn('Number of samples N resulted in 0 so it was set to 2. ')

        self.sobol_indices(N=N, bool_first_order=params.get_bool_first_order())
        self.get_result().set_computation_time((time.time() - start / 60.0))

        # Save results
        if data_saver is not None:
            writer = SobolIndexFileWriterMC(param=params, result=self.get_result(), data_saver=data_saver)
            writer.write_result()

        return self.get_result()

    def sobol_indices(self, N: int, bool_first_order: bool) -> SobolIndexResultMC:

        params = self.get_param()
        model = params.get_model()

        # generate random samples
        samples = params.get_prior().sample(int(2 * N), random_state=self.get_random_state())

        # Split up samples in two parts: A samples
        A = samples[:, 0:N]
        start_A = time.time()
        eval_A, _, _ = model.eval_model_averaged(parameter_value=A, random_state=self.get_random_state(),
                                                   nr_runs_averaged=params.get_no_runs_averaged())
        eval_time_A = time.time() - start_A

        variance_A = box_dim(np.var(eval_A, axis=0), 1)

        # Split up samples in two parts: B samples
        B = samples[:, range(N, 2 * N)]
        # for first order indices
        if bool_first_order:
            start_B = time.time()
            eval_B, _, _ = model.eval_model_averaged(parameter_value=B, random_state=self.get_random_state(),
                                                       nr_runs_averaged=params.get_no_runs_averaged())
            eval_time_B = time.time() - start_B
            variance_AB = box_dim(np.var(np.concatenate((eval_A, eval_B), axis=0), axis=0), 1)
        else:
            eval_time_B = 0

        total_order_constantine = np.zeros(shape=(params.get_qoi_dim(), params.get_m()))

        if bool_first_order:
            first_order_jansen = np.zeros(shape=(params.get_qoi_dim(), params.get_m()))
            first_order_saltelli_2010 = np.zeros(shape=(params.get_qoi_dim(), params.get_m()))
            total_order_jansen = np.zeros(shape=(params.get_qoi_dim(), params.get_m()))
        else:
            first_order_jansen = None
            first_order_saltelli_2010 = None
            total_order_jansen = None

        for i in range(0, params.get_m()):
            # it is important to make deep copies here!
            ABi = A.copy()
            ABi[i, :] = B.copy()[i, :]
            start_ABi = time.time()
            eval_ABi, _, _ = model.eval_model_averaged(parameter_value=ABi, random_state=self.get_random_state(),
                                                         nr_runs_averaged=params.get_no_runs_averaged())
            eval_time_ABi = (time.time() - start_ABi)
            tmp_total_jansen = box_dim(eval_A - eval_ABi, 2)
            for j in range(0, params.get_qoi_dim()):
                total_order_constantine[j, i] = np.dot(tmp_total_jansen[:, j], tmp_total_jansen[:, j]) / (
                        2 * N * variance_A[j])  # code from constantine-2017

                if bool_first_order:
                    total_order_jansen[j, i] = np.dot(tmp_total_jansen[:, j], tmp_total_jansen[:, j]) / (
                            2 * N * variance_AB[j])  # formula from Jansen-1999 (Saltelli-2010)
                    tmp_first_jansen = box_dim(eval_B - eval_ABi, 2)
                    first_order_jansen[j, i] = 1 - (np.dot(tmp_first_jansen[:, j], tmp_first_jansen[:, j]) / (2 * N)
                                                    / variance_AB[j])  # formula from Jansen-1999 (Saltelli-2010)
                    first_order_saltelli_2010[j, i] = np.dot(box_dim(eval_B, 2)[:, j], -tmp_total_jansen[:, j]) \
                                                      / (N * variance_AB[j])  # formula from Saltelli-2010

        if params.get_qoi_dim() == 1:
            total_order_constantine = unbox(total_order_constantine)
            total_order_jansen = unbox(total_order_jansen)
            first_order_jansen = unbox(first_order_jansen)
            first_order_saltelli_2010 = unbox(first_order_saltelli_2010)

        self.get_result().set_total_indices_constantine(total_order_constantine)
        self.get_result().set_total_indices_jansen(total_order_jansen)
        self.get_result().set_first_indices_jansen(first_order_jansen)
        self.get_result().set_first_incides_saltelli_2010(first_order_saltelli_2010)
        self.get_result().set_eval_time((eval_time_A + eval_time_B + eval_time_ABi) / 60.0)
        self.get_result().set_variance_A(variance_A)
        if bool_first_order:
            self.get_result().set_variance_AB(variance_AB)

        return self.get_result()
