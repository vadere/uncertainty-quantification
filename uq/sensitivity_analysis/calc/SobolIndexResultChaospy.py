import numpy as np

from uq.sensitivity_analysis.calc.SobolIndexResult import SobolIndexResult
from uq.utils.model import ApproxModel


class SobolIndexResultChaospy(SobolIndexResult):

    def __init__(self, eval_time: float = None):
        super().__init__()
        self.__first_order_indices = None
        self.__total_indices = None
        self.__computation_time_first_order = None
        self.__computation_time_total_indices = None
        self.__approx_model = None

    def equals(self, other: "SobolIndexResultChaospy") -> bool:
        bool_equals = False
        if isinstance(other, SobolIndexResultChaospy):
            if np.all(self.get_first_order_indices() == other.get_first_order_indices()) and \
                    np.all(self.get_total_indices() == other.get_total_indices()) and \
                    self.get_approx_model().__eq__(other.get_approx_model()):
                bool_equals = True
        return bool_equals

    # GETTERS

    def get_first_order_indices(self) -> np.ndarray:
        return self.__first_order_indices

    def get_total_indices(self) -> np.ndarray:
        return self.__total_indices

    def get_computation_time_first_order(self) -> float:
        return self.__computation_time_first_order

    def get_computation_time_total_indices(self) -> float:
        return self.__computation_time_total_indices

    def get_approx_model(self) -> ApproxModel:
        return self.__approx_model

    # SETTERS

    def set_first_order_indices(self, first_order_indices: np.ndarray) -> None:
        self.__first_order_indices = first_order_indices

    def set_total_indices(self, total_indices: np.ndarray) -> None:
        self.__total_indices = total_indices

    def set_computation_time_first_order(self, computation_time: float) -> None:
        self.__computation_time_first_order = computation_time

    def set_computation_time_total_indices(self, computation_time: float) -> None:
        self.__computation_time_total_indices = computation_time

    def set_approx_model(self, approx_model: ApproxModel) -> None:
        self.__approx_model = approx_model
