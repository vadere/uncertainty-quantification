import numpy as np

from uq.sensitivity_analysis.calc.SobolIndexResult import SobolIndexResult


class SobolIndexResultSALib(SobolIndexResult):

    def __init__(self, sobol_indices: dict = None, param_values: np.ndarray = None, n_runs: int = None):
        super().__init__()
        self.__sobol_indices = sobol_indices
        self.__param_values = param_values
        self.__n_runs = n_runs

    def get_sobol_indices(self) -> dict:
        return self.__sobol_indices

    def get_total_sobol_indices(self) -> np.ndarray:
        return np.asarray(self.get_sobol_indices()['ST'])

    def get_first_order_indices(self) -> np.ndarray:
        return np.asarray(self.get_sobol_indices()['S1'])

    def get_second_order_indices(self) -> np.ndarray:
        # todo make sure field exists (only  if bool_second_order_indices in SobolIndexParametersSALib)
        return np.asarray(self.get_sobol_indices()['S2'])

    def get_param_values(self) -> np.ndarray:
        return self.__param_values

    def get_n_runs(self) -> int:
        return self.__n_runs

    def set_sobol_indices(self, sobol_indices: dict) -> None:
        self.__sobol_indices = sobol_indices

    def set_param_values(self, param_values: np.ndarray) -> None:
        self.__param_values = param_values

    def set_n_runs(self, n_runs: int) -> None:
        self.__n_runs = n_runs
