import os
import pickle
import time
from datetime import datetime

import chaospy
import numpy as np
from numpy.random import RandomState

from uq.sensitivity_analysis.calc.SensitivityParametersChaospy import SensitivityParametersChaospy
from uq.sensitivity_analysis.calc.SobolIndexCalculator import SobolIndexCalculator
from uq.sensitivity_analysis.calc.SobolIndexParameterChaospy import SobolIndexParameterChaospy
from uq.sensitivity_analysis.calc.SobolIndexResultChaospy import SobolIndexResultChaospy
from uq.sensitivity_analysis.ioput.SobolIndexFileWriterChaospy import SobolIndexFileWriterChaospy
from uq.utils.ioput.DataSaver import DataSaver


# todo: tests
class SobolIndexCalculatorChaospy(SobolIndexCalculator):

    def __init__(self, param: SobolIndexParameterChaospy):
        result = SobolIndexResultChaospy()
        super().__init__(param=param, result=result)

    def get_param(self) -> SobolIndexParameterChaospy:
        return super().get_param()

    def get_result(self) -> SobolIndexResultChaospy:
        return super().get_result()

    def calc_sobol_indices(self) -> None:
        # Sensitivity analysis with polynomial chaos expansion
        # from: https://github.com/jonathf/chaospy/issues/81

        # Sensitivity analysis with polynomial chaos
        # from: https://github.com/jonathf/chaospy/issues/81

        bool_print = True

        total_start = time.time()

        param = self.get_param()
        test_model = param.get_model()

        # Initialize random state
        random_state = RandomState(param.get_seed())
        if bool_print:
            print("Seed: %d" % param.get_seed())

        # DataSaver
        data_saver = DataSaver(os.getcwd())
        if bool_print:
            print(data_saver.get_path_to_files())

        # Setup for polynomial chaos - todo make flexible for any size

        if len(np.unique(param.get_x_upper())) == 1 and len(np.unique(param.get_x_lower())) == 1:
            x_lower_unique = np.unique(param.get_x_lower())
            x_upper_unique = np.unique(param.get_x_upper())
            distributions = chaospy.Iid(chaospy.Uniform(x_lower_unique, x_upper_unique),
                                        length=test_model.get_dimension())  # -> for IID variables
        else:
            distribution_per_parameter = list()
            for i in range(0, test_model.get_dimension()):
                distribution_per_parameter.append(chaospy.Uniform(param.get_x_lower()[i], param.get_x_upper()[i]))

            distributions = chaospy.J(*distribution_per_parameter)

        # Create orthogonal polynomial basis (depending on distribution type)
        polynomials = chaospy.orth_ttr(param.get_order(), distributions)

        if not param.is_load_approx_model():  # generate new model
            if not param.is_load_samples():  # generate new samples

                # Density of uncertain parameters
                # todo: why not sample from distributions ?
                rho = self.get_param().get_prior()
                samples = rho.sample(n=param.get_M(), random_state=random_state)  # sample input parameter distributions

                # Evaluate model at samples
                start = datetime.now()
                values_f, _, _ = test_model.eval_model_averaged(parameter_value=samples,
                                                                nr_runs_averaged=param.get_no_runs_averaged(),
                                                                random_state=random_state)
                stop = datetime.now() - start
                computation_time_samples = (stop.seconds / 60)
                self.get_result().set_eval_time(computation_time_samples)

                if bool_print:
                    print("** Model evaluations finished")
                    print("** Computation time for model evaluations: %f [min]" % computation_time_samples)

                    # write samples to readable file
                if np.size(samples, axis=1) == self.get_param().get_M():
                    if np.ndim(values_f) > 1:
                        if np.size(values_f, axis=1) == 1:
                            combination = np.vstack((samples, np.transpose(values_f)))
                    else:
                        combination = np.vstack((samples, values_f))

                data_saver.write_nparray_to_file(variable=combination,
                                                 name="samples_results", key=test_model.get_key(),
                                                 qoi=test_model.get_qoi())

                dist_samples = samples
                QoI = values_f
            else:  # bool_load_samples
                with open(param.get_path_to_samples(), 'rb') as file:
                    data = pickle.load(file)
                    m_sample_in_data = np.size(data.posterior_samples, axis=1)

                    if m_sample_in_data < param.get_M():
                        M = m_sample_in_data
                        raise Warning("Number of samples is set to %d " % m_sample_in_data)
                    elif m_sample_in_data >= 3 * param.get_M():
                        print("Random sampling from existing data")
                        # load random samples out of the available samples
                        idx_samples = np.round(random_state.rand(param.get_M()) * (m_sample_in_data - 1)).astype(int)
                        print(idx_samples[0:10])
                    else:
                        # load first M samples
                        idx_samples = np.arange(0, param.get_M())

                    if np.size(data.posterior_samples, axis=0) > 0:  # more dimensional input
                        dist_samples = data.posterior_samples[:, idx_samples]
                    else:
                        dist_samples = data.posterior_samples[idx_samples]

                    #  QoI has to be scalar for sensitivity analysis (todo: check if that is true for chaospy)
                    QoI = data.QoI[idx_samples]
                    computation_time_samples = None

            approx_model = chaospy.fit_regression(polynomials, dist_samples, QoI)
            self.get_result().set_approx_model(approx_model)
            data_saver.save_to_pickle(None, approx_model, "approx_model")
        else:  # load model
            with open(param.get_path_to_approx_model(), 'rb') as pickle_file:
                approx_model = pickle.load(pickle_file)
            dist_samples = None
            QoI = None
            computation_time_samples = None

        # Store configuration for indices
        sensitivity_data = SensitivityParametersChaospy(M=param.get_M(), order=param.get_order(), model=test_model,
                                                        dist_samples=dist_samples, distributions=distributions,
                                                        polynomials=polynomials, qoi=QoI, approx_model=approx_model)

        # Calculate first order Sobol' indices
        if bool_print:
            print("** Calculate Sobol first order indices")

        start = datetime.now()
        first_order_indices = chaospy.Sens_m(poly=approx_model, dist=distributions)
        stop = datetime.now() - start
        computation_time_first_order = (stop.seconds / 60)
        sensitivity_data.set_first_order_indices(first_order_indices)
        self.get_result().set_first_order_indices(first_order_indices)
        self.get_result().set_computation_time_first_order(computation_time_first_order)

        if bool_print:
            print("** Computation time (1st order Sobol indices only) %f [min]" % computation_time_first_order)

        # Calculate Sobol total indices
        if bool_print:
            print("** Calculate Sobol' total indices")

        start = datetime.now()
        total_sens = chaospy.Sens_t(approx_model, distributions)
        stop = datetime.now() - start
        computation_time_total_order = (stop.seconds / 60)

        # Store Sobol indices
        sensitivity_data.set_indices(total_sens)
        self.get_result().set_total_indices(total_sens)
        self.get_result().set_computation_time_total_indices(computation_time_total_order)

        # Evaluation
        if bool_print:  # Print results
            print('** Total Sobol indices: %s' % np.array2string(total_sens))
            print("** Computation time (Sobol total indices only) %f [min]" % computation_time_total_order)

        self.get_result().set_computation_time((time.time() - total_start) / 60.0)

        # Save data to file
        writer = SobolIndexFileWriterChaospy(param=param, result=self.get_result(), data_saver=data_saver)
        writer.write_result()
        data_saver.save_to_pickle(path=None, data=sensitivity_data, name='sensitivity_data_sobol')
