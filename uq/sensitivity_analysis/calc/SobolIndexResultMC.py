import numpy as np

from uq.sensitivity_analysis.calc.SobolIndexResult import SobolIndexResult


class SobolIndexResultMC(SobolIndexResult):

    def __init__(self, total_indices_constantine: np.ndarray = None, total_indices_jansen: np.ndarray = None,
                 first_indices_jansen: np.ndarray = None, first_incides_saltelli_2010: np.ndarray = None,
                 eval_time: float = None, variance_AB: float = None, variance_A: float = None):
        super().__init__(eval_time=eval_time)
        self.__total_indices_constantine = total_indices_constantine
        self.__total_indices_jansen = total_indices_jansen
        self.__first_indices_jansen = first_indices_jansen
        self.__first_incides_saltelli_2010 = first_incides_saltelli_2010
        self.__variance_A = variance_A
        self.__variance_AB = variance_AB

    def equals(self, other: "SobolIndexResultMC") -> bool:
        bool_equals = False
        if isinstance(other, SobolIndexResultMC):
            if np.all(self.get_total_indices_constantine() == other.get_total_indices_constantine()) and \
                    np.all(self.get_total_indices_jansen() == other.get_total_indices_jansen()) and \
                    np.all(self.get_first_indices_jansen() == other.get_first_indices_jansen()) and \
                    np.all(self.get_first_incides_saltelli_2010() == other.get_first_incides_saltelli_2010()) and \
                    self.get_variance_A() == other.get_variance_A() and \
                    self.get_variance_AB() == other.get_variance_AB():
                bool_equals = True

        return bool_equals

    def get_total_indices_constantine(self) -> np.ndarray:
        return self.__total_indices_constantine

    def get_total_indices_jansen(self) -> np.ndarray:
        return self.__total_indices_jansen

    def get_first_indices_jansen(self) -> np.ndarray:
        return self.__first_indices_jansen

    def get_first_incides_saltelli_2010(self) -> np.ndarray:
        return self.__first_incides_saltelli_2010

    def get_variance_AB(self) -> float:
        # only available if bool_first_order in Parameters is True
        return self.__variance_AB

    def get_variance_A(self) -> float:
        return self.__variance_A

    # SETTER

    def set_total_indices_constantine(self, total_indices_constantine: np.ndarray) -> None:
        self.__total_indices_constantine = total_indices_constantine

    def set_total_indices_jansen(self, total_indices_jansen: np.ndarray) -> None:
        self.__total_indices_jansen = total_indices_jansen

    def set_first_indices_jansen(self, first_indices_jansen: np.ndarray) -> None:
        self.__first_indices_jansen = first_indices_jansen

    def set_first_incides_saltelli_2010(self, first_incides_saltelli_2010: np.ndarray) -> None:
        self.__first_incides_saltelli_2010 = first_incides_saltelli_2010

    def set_variance_A(self, variance_A: float) -> None:
        self.__variance_A = variance_A

    def set_variance_AB(self, variance_AB: float) -> None:
        self.__variance_AB = variance_AB
