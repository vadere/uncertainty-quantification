from uq.sensitivity_analysis.calc.SobolIndexParameter import SobolIndexParameter
from uq.utils.model.Model import Model
from uq.utils.prior.UniformGenMult import UniformGenMult


class SobolIndexParameterMC(SobolIndexParameter):

    def __init__(self, model: Model, prior: UniformGenMult, M: int, seed: int = None, bool_averaged: bool = False,
                 no_runs_averaged: int = 1, path2results: str = None, qoi_dim: int = 1, bool_first_order: bool = True):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged,
                         bool_first_order=bool_first_order, bool_second_order=False, path2results=path2results)

        self.__M = M  # number of samples for sampling
        self.__qoi_dim = qoi_dim  # dimension of quantity of interest

    def get_m(self) -> int:
        return self.get_model().get_dimension()

    def get_M(self) -> int:
        return self.__M

    def get_qoi_dim(self) -> int:
        return self.__qoi_dim

    # SETTERS

    def set_M(self, M: int) -> None:
        self.__M = M
