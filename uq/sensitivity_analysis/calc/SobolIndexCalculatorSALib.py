import time

import numpy as np
from SALib.analyze import sobol
from SALib.sample import saltelli
from numpy.random import RandomState

from uq.sensitivity_analysis.calc.SobolIndexCalculator import SobolIndexCalculator
from uq.sensitivity_analysis.calc.SobolIndexParameterSALib import SobolIndexParameterSALib
from uq.sensitivity_analysis.calc.SobolIndexResultSALib import SobolIndexResultSALib
from uq.sensitivity_analysis.ioput.SobolIndexFileWriterSALib import SobolIndexFileWriterSALib
from uq.utils.ioput.DataSaver import DataSaver


class SobolIndexCalculatorSALib(SobolIndexCalculator):

    def __init__(self, param: SobolIndexParameterSALib):
        result = SobolIndexResultSALib()
        super().__init__(param=param, result=result)

    def get_param(self) -> SobolIndexParameterSALib:
        return super().get_param()

    def get_result(self) -> SobolIndexResultSALib:
        return super().get_result()

    def calc_sobol_indices(self) -> SobolIndexResultSALib:

        start = time.time()

        problem = self.get_param().get_problem()
        seed = self.get_param().get_seed()
        path2results = self.get_param().get_path2results()
        no_runs_averaged = self.get_param().get_no_runs_averaged()
        test_model = self.get_param().get_model()
        N = self.get_param().get_N()

        if path2results is not None:
            data_saver = DataSaver(path2results)
            test_model.set_data_saver(data_saver)
        else:
            data_saver = None

        random_state = RandomState(seed)

        # Generate Samples
        param_values = saltelli.sample(problem=problem, N=N,
                                       calc_second_order=self.get_param().get_bool_second_order(), seed=seed)
        n_runs = len(param_values)

        print("Run SA - Number of samples: %d" % n_runs)

        start_eval = time.time()

        values_f, _, _ = test_model.eval_model_averaged(parameter_value=np.transpose(param_values),
                                                        nr_runs_averaged=no_runs_averaged,
                                                        random_state=random_state)  # evaluate model at samples
        self.get_result().set_eval_time(time.time() - start_eval)

        # Perform Analysis
        Si = sobol.analyze(problem, values_f, calc_second_order=self.get_param().get_bool_second_order(),
                           seed=seed)

        if self.get_param().get_bool_print():
            print(Si['S1'])
            print(Si['ST'])
            if self.get_param().get_bool_second_order():
                print("x1-x2:", Si['S2'][0, 1])

        computation_time = (time.time() - start) / 60
        print("Elapsed time: %.2f min" % computation_time)

        self.get_result().set_sobol_indices(sobol_indices=Si)
        self.get_result().set_param_values(param_values=param_values)
        self.get_result().set_n_runs(n_runs=n_runs)
        self.get_result().set_computation_time(computation_time)

        if data_saver is not None:  # for tests
            writer = SobolIndexFileWriterSALib(param=self.get_param(), result=self.get_result())
            writer.write_result()

        return self.get_result()
