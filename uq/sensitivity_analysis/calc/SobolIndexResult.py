from uq.utils.calc.Result import Result


class SobolIndexResult(Result):

    def __init__(self, eval_time: float = None):
        super().__init__()
        self.__eval_time = eval_time

    def get_eval_time(self) -> float:
        return self.__eval_time

    def set_eval_time(self, eval_time: float) -> None:
        self.__eval_time = eval_time

    def equals(self, other: "SobolIndexResult") -> bool:
        pass
