from uq.sensitivity_analysis.calc.SobolIndexParameter import SobolIndexParameter
from uq.sensitivity_analysis.calc.SobolIndexResult import SobolIndexResult
from uq.utils.calc.Calculator import Calculator


class SobolIndexCalculator(Calculator):

    def get_param(self) -> SobolIndexParameter:
        return super().get_param()

    def get_result(self) -> SobolIndexResult:
        return super().get_result()

    def calc_sobol_indices(self) -> SobolIndexResult:
        pass
