from uq.utils.model.Model import Model


# todo unify with SobolIndexResult / SobolIndexParameter (currently it's a mix between both)

class SensitivityParametersChaospy:

    def __init__(self, M: int, order: int, model: Model, dist_samples, distributions, polynomials, qoi: str,
                 approx_model: Model):
        self.__samples = dist_samples
        self.__model = model
        self.__M = M
        self.__order = order
        self.__distributions = distributions
        self.__polynomials = polynomials
        self.__qoi = qoi
        self.__approx_model = approx_model
        self.__indices = None
        self.__first_order_indices = None

    def set_indices(self, indices) -> None:
        self.__indices = indices

    def set_first_order_indices(self, indices) -> None:
        self.__first_order_indices = indices

    def get_approx_model(self) -> Model:
        return self.__approx_model

    def get_distributions(self):
        return self.__distributions

    def get_QoI(self):
        return self.__qoi
