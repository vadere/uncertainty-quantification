from uq.sensitivity_analysis.calc.SobolIndexParameter import SobolIndexParameter
from uq.utils.model import Model
from uq.utils.prior.UniformGenMult import UniformGenMult


class SobolIndexParameterChaospy(SobolIndexParameter):

    def __init__(self, model: Model, prior: UniformGenMult, M: int, order: int, bool_load_approx_model: bool,
                 path_to_approx_model: str, bool_load_samples: bool, path_to_samples: str, seed: int = None,
                 bool_averaged: bool = False, no_runs_averaged: int = 1, path2results: str = None, ):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged,
                         bool_first_order=True, bool_second_order=False, path2results=path2results)
        self.__M = M
        self.__order = order
        self.__bool_load_approx_model = bool_load_approx_model
        self.__path_to_approx_model = path_to_approx_model
        self.__bool_load_samples = bool_load_samples
        self.__path_to_samples = path_to_samples

    def get_M(self) -> int:
        return self.__M

    def get_order(self) -> int:
        return self.__order

    def is_load_approx_model(self) -> bool:
        return self.__bool_load_approx_model

    def get_path_to_approx_model(self) -> str:
        return self.__path_to_approx_model

    def is_load_samples(self) -> bool:
        return self.__bool_load_samples

    def get_path_to_samples(self) -> str:
        return self.__path_to_samples
