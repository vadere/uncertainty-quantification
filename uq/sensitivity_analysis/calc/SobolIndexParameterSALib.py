import numpy as np

from uq.sensitivity_analysis.calc.SobolIndexParameter import SobolIndexParameter
from uq.utils.model.Model import Model
from uq.utils.prior.UniformGenMult import UniformGenMult


class SobolIndexParameterSALib(SobolIndexParameter):

    def __init__(self, model: Model, prior: UniformGenMult, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 N: int, path2results: str, bool_second_order: bool = False, bool_print: bool = False):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, bool_first_order=True, bool_second_order=bool_second_order,
                         path2results=path2results)

        self.__N = N
        self.__bool_print = bool_print
        self.__problem = self.build_problem()  # make sure this is done last

    def get_problem(self) -> dict:
        return self.__problem

    def get_bool_print(self) -> bool:
        return self.__bool_print

    def get_N(self) -> int:
        return self.__N

    def set_N(self, N: int) -> None:
        self.__N = N

    # Necessary input for SALib
    def build_problem(self) -> dict:
        problem = {
            'num_vars': len(self.get_model().get_key()),
            'names': self.get_model().get_key(),
            'bounds': np.column_stack((self.get_x_lower(), self.get_x_upper())).tolist()
        }
        return problem
