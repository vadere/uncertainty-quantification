from uq.utils.calc.Parameter import Parameter
from uq.utils.model.Model import Model
from uq.utils.prior.Prior import Prior
from uq.utils.prior.UniformGenMult import UniformGenMult


class SobolIndexParameter(Parameter):

    def __init__(self, model: Model, prior: Prior, seed: int = None, bool_averaged: bool = False,
                 no_runs_averaged: int = 1, bool_first_order: bool = True, bool_second_order: bool = False,
                 path2results: str = None):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results)

        self.check_prior(prior=prior)

        self.__bool_first_order = bool_first_order
        self.__bool_second_order = bool_second_order

    def check_prior(self, prior: Prior):
        if not isinstance(prior, UniformGenMult):
            raise UserWarning('Sensitivity analysis currently only implemented for uniform distribution!')

    def get_bool_first_order(self) -> bool:
        return self.__bool_first_order

    def get_bool_second_order(self) -> bool:
        return self.__bool_second_order
