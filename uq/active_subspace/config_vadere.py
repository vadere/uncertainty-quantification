import os

import numpy as np

from uq.utils.datatype import get_dimension
from uq.utils.model.VadereModel import VadereModel

bool_gradient = False  # gradient available
bool_averaged = True

no_runs_averaged = 1  # fixed seed !

step_size = 0.001  # for approximation of gradients
step_size = 0.025
k = 1 + 1  # desired dimension of subspace +1
# alpha = 10  # oversampling factor -> in main file

# x_lower = np.array([0.5, 1.0, 1.0])  # lower bounds for parameters
# x_upper = np.array([2.2, 5.0, 5.0])  # upper bounds for parameters

# x_lower = np.array([0.5, 0.0, 160, 1.0, 7.5, 0.1])  # lower bounds for parameters
# x_upper = np.array([2.2, 1.0, 200, 5.0, 9.5, 1.0])  # upper bounds for parameters

#  -> with bottleneck_width
x_lower = np.array([0.5, 0.0, 160, 1.0, 1.6, 0.1])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 1.0])  # upper bounds for parameters

# bool_vec = np.array([False, True, True, False])
density_type = "uniform"  # input parameter density

# test_input = np.array([[1.34], [0.26], [180], [1], [8.5], [0.5]])  # legal test input
test_input = np.array([[1.34], [0.26], [180], [1], [1.6], [0.5]])  # legal test input -> with bottleneck_width

# x_lower = np.array([1.0, 1.0, -2147483648, 0.0])
# x_upper = np.array([3.0, 50.0, 2147483647, 10.0])
# test_input = np.array([[1.34], [18], [2147483647], [2.0]])


# Parameters

key = ["attributesPedestrian.speedDistributionMean", "sources.[id==-1].spawnNumber",
       "sources.[id==-1].distributionParameters"]  # uncertain parameters

key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==-1].spawnNumber", "sources.[id==-1].distributionParameters", "obstacles.[id==1].y",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters

# key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
#       "sources.[id==-1].spawnNumber", "sources.[id==-1].distributionParameters", "bottleneck_width",
#       "obstacleRepulsionMaxWeight"]  # uncertain parameters

key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

# standard deviation


qoi = "mean_density.txt"  # quantity of interest
# qoi = "waitingtime.txt"
run_local = False  # run on local machine (vs. run on server)

# Model
current_dir = os.path.dirname(os.path.realpath(__file__))
path2tutorial = os.path.join(current_dir, "../inversion")

print(path2tutorial)
# path2tutorial = os.path.abspath("D:/marion/Arbeit/repo_checkout/uncertainty-quantification-private/uq/inversion")

path2model = os.path.join(current_dir, "../scenarios", "vadere-console.jar")
print(path2model)
path2scenario = os.path.join(current_dir, "../scenarios", "rimea_01_pathway_discrete.scenario")
path2scenario = os.path.join(current_dir, "../scenarios", "bridge_coordinates_kai_origin_0_UNIT.scenario")
path2scenario = os.path.join(current_dir, "../scenarios", "bridge_coordinates_kai_origin_0_UNIT.scenario")
path2scenario = os.path.join(current_dir, "../scenarios", "Liddle_bhm_v3_free_seed.scenario")
path2scenario = os.path.join(current_dir, "../scenarios", "Liddle_bhm_v3_fixed_seed.scenario")

# test_model = TestModel()
test_model = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                         n_jobs=-1, log_lvl="OFF")

m = get_dimension(key)
M_boot = 10
