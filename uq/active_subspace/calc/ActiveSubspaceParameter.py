import warnings
from typing import Union

import numpy as np

from uq.utils.calc.Parameter import Parameter
from uq.utils.prior.UniformGenMult import UniformGenMult


class ActiveSubspaceParameter(Parameter):

    def __init__(self, model: "Model" = None, x_lower: Union[float, np.ndarray] = None,
                 x_upper: Union[float, np.ndarray] = None, alpha: int = None, k: int = None, case: int = None,
                 seed: int = None, bool_averaged: bool = None, no_runs_averaged: int = None,
                 bool_save_data: bool = None, bool_print: bool = None, bool_plot: bool = None, path2results: str = None,
                 force_idx_gap: int = None):

        if model is not None:
            rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=model.get_dimension())
        else:
            rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=np.size(x_lower))

        super().__init__(model=model, prior=rho, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results)

        self.__alpha = alpha  # oversampling factor
        self.__k = k  # dimension of desired subspace + 1
        self.__case = case  # for test problems
        self.__bool_save_data = bool_save_data
        self.__bool_print = bool_print
        self.__bool_plot = bool_plot
        self.__force_idx_gap = force_idx_gap  # force a certain spectral gap (for tests)

        self.check_parameter_dimensions()

    def check_parameter_dimensions(self):

        if self.get_x_lower() is not None:
            if np.size(self.get_x_lower()) is not np.size(self.get_x_upper()):
                raise UserWarning('Dimensions of x_lower (%d) and x_upper (%d) are not consistent' % (
                    np.size(self.get_x_lower()), np.size(self.get_x_upper())))

            if np.any(self.get_x_lower() >= self.get_x_upper()):
                raise UserWarning('Lower parameter limits (%s) are not all smaller than upper limits (%s)' % (
                    np.array2string(self.get_x_lower()), np.array2string(self.get_x_upper())))

            if self.get_model() is not None and np.size(self.get_x_lower()) is not self.get_model().get_dimension():
                raise UserWarning('Dimensions of parameter bounds (%d) and model dimension (%d) are not consistent' % (
                    np.size(self.get_x_lower()), self.get_model().get_dimension()))

        if self.get_model() is not None and self.get_k() >= self.get_model().get_dimension():
            warnings.warn(
                'Size of desired subspace (k: %d) is larger or equal to number of model parameters (%d).' % (
                    self.get_k(), self.get_model().get_dimension()) +
                ' This may result in a larger sample size than recommended.')

    def get_alpha(self) -> int:
        return self.__alpha

    def get_k(self) -> int:
        return self.__k

    def get_case(self):
        return self.__case

    def is_save_data(self) -> bool:
        return self.__bool_save_data

    def is_print_data(self) -> bool:
        return self.__bool_print

    def is_plot_data(self) -> bool:
        return self.__bool_plot

    def get_force_idx_gap(self) -> int:
        return self.__force_idx_gap

    # SETTERS

    def set_alpha(self, alpha: int) -> None:
        self.__alpha = alpha
