import os
import time
import warnings
from typing import Tuple

import numpy as np

from uq.active_subspace.calc.ActiveSubspaceCalculator import ActiveSubspaceCalculator
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.calc.ActiveSubspaceResultGradient import ActiveSubspaceResultGradient
from uq.active_subspace.ioput.ActiveSubspaceFileWriterGradient import ActiveSubspaceFileWriterGradient
from uq.active_subspace.ioput.ActiveSubspacePlotterGradients import ActiveSubspacePlotterGradients
from uq.active_subspace.utils import transform_coordinates_to_unit, transform_coordinates_from_unit
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.model.ApproxModel import ApproxModel
from uq.utils.prior.UniformGenMult import UniformGenMult


class ActiveSubspaceCalculatorGradient(ActiveSubspaceCalculator):

    def __init__(self, param: ActiveSubspaceParameterGradient = None):
        results = ActiveSubspaceResultGradient()
        super().__init__(param=param, result=results)

    def get_param(self) -> ActiveSubspaceParameterGradient:
        return super().get_param()

    def get_result(self) -> ActiveSubspaceResultGradient:
        return super().get_result()

    def is_bootstrapping(self) -> bool:
        return self.get_param().get_M_boot() > 0

    def find_largest_gap_log(self, lambda_eig: np.ndarray, step_size: float) -> Tuple[int, bool]:

        if not self.get_param().is_gradient_available():  # gradients are approximated
            eps = np.finfo(float).eps
            lambda_eig = lambda_eig[lambda_eig > (step_size - eps)]  # only choose a gap that is above the step_size

        idx, bool_plot_log = super().find_largest_gap_log(lambda_eig=lambda_eig)

        return idx, bool_plot_log

    def calc_activity_scores_from_C(self, C_hat: np.ndarray, prior: "Prior") -> None:
        params = self.get_param()
        test_model = params.get_model()
        results = self.get_result()

        self.calc_eigenvalues(C_hat=C_hat, prior=prior)

        # Find largest gap of eigenvalues
        if params.get_force_idx_gap() is None:  # standard case
            idx_gap, _ = self.find_largest_gap_log(lambda_eig=results.get_lambda_hat(),
                                                   step_size=params.get_step_size())
        else:  # if the idx gap is given (for examples only)
            warnings.warn('idx_gap is forced to a certain idx - should only be done in tests.')
            idx_gap = params.get_force_idx_gap()
        results.set_idx_gap(idx_gap)

        # Find true eigenvalue gap if information is available from this model (mainly for tests)
        # todo move to TestCalculator
        if isinstance(test_model, AnalyticalModel) and test_model.is_analytic_solution_available():
            idx_gap_true, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=results.get_lambda_true())
            results.set_idx_gap_true(idx_gap_true)

        # Divide into active and inactive subspaces
        n = results.get_idx_gap() + 1
        results.set_size_subspace(size_subspace=n)
        w_active, w_inactive = self.divide_active_inactive_subspace(w_vec=results.get_W_hat(),
                                                                    subspace_dim=results.get_idx_gap() + 1)
        results.set_W_active(w_active)

        # Calculate activity scores
        activity_scores = self.calc_activity_scores(lambda_eig=results.get_lambda_hat(), w_vec=results.get_W_hat(),
                                                    n=results.get_size_subspace())
        results.set_activity_scores(activity_scores)

        # Distance between subspaces of different dimensions (measure for the subspace)
        self.calc_distance_active_subspace(w_vec=results.get_W_hat(), prior=prior, lambda_eig=results.get_lambda_hat(),
                                           idx_gap=results.get_idx_gap())

        # todo move to TestCalculator
        # Get true activity scores if available (mainly for tests)
        if isinstance(test_model, AnalyticalModel) and test_model.is_analytic_solution_available():
            true_activity_scores = self.calc_activity_scores(lambda_eig=results.get_lambda_true(),
                                                             w_vec=results.get_W_true(),
                                                             n=results.get_size_subspace())
            results.set_activity_scores_true(true_activity_scores=true_activity_scores)

        # return activity_scores, true_activity_scores, w_active, w_vec, dist_as, dist_as_vec, true_distance, idx_gap, \
        #       idx_gap_true, lambda_eig, lambda_eig_true, n

    def calc_distance_subspace(self) -> float:
        max_distance = None
        distance_subspace = self.get_result().get_distance_subspace()
        true_distance = self.get_result().get_distance_subspace_true()
        if self.get_param().get_M_boot() > 1:  # Bootstrapping is activated
            # error in subspace distance
            max_distance = np.max(
                np.abs(distance_subspace - true_distance))
            if self.get_param().is_print_data():
                print("** Max error in subspace distance")
                print(max_distance)
        return max_distance

    # todo move to TestCalculator
    def check_C_hat(self, C_hat: np.ndarray, prior: "Prior") -> float:
        test_model = self.get_param().get_model()

        error_c_hat = None
        if isinstance(test_model, AnalyticalModel) and test_model.is_analytic_solution_available():
            error_c_hat = np.linalg.norm(C_hat - test_model.get_C_matrix(prior=prior))
            if self.get_param().is_print_data():
                print("Error in C_hat:")
                print(error_c_hat)
        return error_c_hat

    def calc_eigenvalues(self, C_hat: np.ndarray, prior: "Prior"):
        test_model = self.get_param().get_model()

        # Calculate eigenvectors and eigenvalues
        w_vec, lambda_eig, wh = self.eigendecomposition(C_hat)
        self.get_result().set_lambda_hat(lambda_eig)
        self.get_result().set_W_hat(w_vec)

        # todo move to TestCalculator
        if test_model is not None and \
                isinstance(test_model, AnalyticalModel) and test_model.is_analytic_solution_available():
            lambda_eig_true = test_model.get_eigenvalues_C(prior)
            self.get_result().set_lambda_true(lambda_eig_true=lambda_eig_true)
            if self.get_param().is_print_data():
                print(np.linalg.norm(lambda_eig - lambda_eig_true))
        else:
            lambda_eig_true = None

        return lambda_eig, lambda_eig_true, w_vec, wh

    #  Algorithm 1.1 from constantine-2015 (p. 4)
    def identify_active_subspace(self) -> ActiveSubspaceResultGradient:
        start = time.time()

        param = self.get_param()

        if param.is_print_data():
            print("Random seed: %d" % param.get_seed())
        random_state = self.get_random_state()

        if not param.is_averaged():
            param.set_no_runs_averaged(no_runs_averaged=1)

        model = param.get_model()
        if param.get_path2results() is not None:
            data_saver = DataSaver(param.get_path2results())
            model.set_data_saver(data_saver)
        else:
            data_saver = None

        dim = model.get_dimension()

        # Density of uncertain parameters
        if isinstance(param.get_prior(), UniformGenMult):
            lower_transformed = transform_coordinates_to_unit(x_lower=param.get_x_lower(),
                                                              x_upper=param.get_x_upper(),
                                                              value=param.get_x_lower())
            upper_transformed = transform_coordinates_to_unit(x_lower=param.get_x_lower(),
                                                              x_upper=param.get_x_upper(),
                                                              value=param.get_x_upper())
            rho = UniformGenMult(
                lower=lower_transformed, upper=upper_transformed, dim=dim)
        else:
            raise UserWarning("Density type not (yet) implemented!")

        # Generate samples according to density of uncertain parameters
        n_samples = np.ceil(param.get_alpha() * param.get_k() * np.log(dim)).astype(np.int)  # number of samples
        if param.is_print_data():
            print("Number of samples to be simulated: %d" % n_samples)

        # samples = np.random.rand(m,M)*2-1
        samples = rho.sample(n=n_samples, random_state=random_state)

        samples_transformed = transform_coordinates_from_unit(x_lower=param.get_x_lower(),
                                                              x_upper=param.get_x_upper(),
                                                              value=samples)

        # Construct C matrix (approximation) with gradient (approximations)
        C_hat = self.evaluate_gradients_and_construct_C(samples_transformed=samples_transformed, n_samples=n_samples)

        error_c_hat = self.check_C_hat(C_hat=C_hat, prior=rho)

        # Calculate activity scores (includes separation of active and inactive subspace)
        self.calc_activity_scores_from_C(C_hat=C_hat, prior=rho)

        approx_model = ApproxModel(W1=self.get_result().get_W_active(), model=model)

        # Perform Bootstrapping
        self.bootstrapping(values_grad_f=self.get_result().get_gradients_transformed(), M=n_samples)

        max_rel_error_eig = None
        # todo move to TestCalculator
        if isinstance(model, AnalyticalModel) and model.is_analytic_solution_available():
            # compare eigenvalues of \hat C with true eigenvalue of C
            true_eig_C = model.get_eigenvalues_C(rho=rho)
            max_rel_error_eig = self.relative_error(true_value=true_eig_C,
                                                    approx_value=self.get_result().get_lambda_hat())
            if param.is_print_data():
                print("** Max relative error in eigenvalues")
                print(max_rel_error_eig)

            self.calc_distance_subspace()

        # Save results

        if param.is_save_data() and not param.is_gradient_available():
            file_gradient = os.path.join(data_saver.get_path_to_files(), 'gradient_approximation.txt')
            open(file_gradient, 'w+')

        path2results = param.get_path2results()
        if data_saver is not None:
            path2results = data_saver.get_path_to_files()

        self.get_result().set_error_lambda_hat(max_rel_error_eig=max_rel_error_eig)
        self.get_result().set_error_c_hat(error_c_hat=error_c_hat)
        self.get_result().set_path2results(path2results=path2results)
        self.get_result().set_n_samples(n_samples=n_samples)
        self.get_result().set_C_hat(C_hat=C_hat)
        self.get_result().set_computation_time(((time.time() - start) / 60))

        if model.get_data_saver() is not None:
            print("Output directory: \n %s" % model.get_data_saver().get_path_to_files())
            writer = ActiveSubspaceFileWriterGradient(param=param, result=self.get_result())
            writer.write_result()

        if param.is_plot_data() and data_saver is not None:
            plotter = ActiveSubspacePlotterGradients(datasaver=data_saver, params=param, results=self.get_result())
            plotter.plot_results(prior=rho)
            # plot_results(data_saver=data_saver, rho=rho, dist_as_vec=dist_as_vec, distance_e=distance_bootstrapping)

        return self.get_result()

    def evaluate_gradients_and_construct_C(self, samples_transformed: np.ndarray, n_samples: int) -> np.ndarray:

        params = self.get_param()
        model = params.get_model()
        random_state = self.get_random_state()

        if params.is_gradient_available():
            # Evaluate gradient at samples
            values_grad_f_original = model.eval_gradient(parameter_value=samples_transformed)


        else:
            # Approximate gradients by finite differences
            values_grad_f_original = model.approximate_gradient(input_vector=samples_transformed,
                                                                step_size=params.get_step_size_relative(),
                                                                model_eval_vec=None,
                                                                n_runs_av=params.get_no_runs_averaged(),
                                                                random_state=random_state)

        self.get_result().set_gradients_original(gradients=values_grad_f_original)

        if model.get_qoi_dim() > 1:
            values_grad_f = np.zeros(np.shape(values_grad_f_original))
            for i_qoi_dim in range(0, model.get_qoi_dim()):
                print(np.shape(values_grad_f_original))
                values_grad_f[:, :, i_qoi_dim] = self.scale_gradient(gradient=values_grad_f_original[:, :, i_qoi_dim])
        else:
            values_grad_f = self.scale_gradient(gradient=values_grad_f_original)

        self.get_result().set_gradients_transformed(values_grad_f)

        # todo move to TestCalculator
        if isinstance(model, AnalyticalModel):

            values_grad_f_real = model.eval_gradient(samples_transformed)
            if params.is_print_data():
                print("Max diff between gradient and approximation")
                print(np.max(np.abs(values_grad_f - values_grad_f_real)))

        # Compute \hat C (approximation of C)
        C_hat = self.construct_C_matrix(values_grad_f=values_grad_f, M=n_samples)

        return C_hat

    def construct_C_from_gradients(self, values_grad_f: np.ndarray, M: int) -> np.ndarray:

        m = self.get_param().get_model().get_dimension()
        random_state = self.get_random_state()

        C_hat = np.zeros(shape=(m, m))
        for i in range(0, M):
            if self.is_bootstrapping():  # bootstrapping
                # draw a random integer j_k between 1 and M
                j_k = int(np.round(random_state.rand(1) * M - 0.5))
            else:
                j_k = i

            tmp = np.outer(values_grad_f[:, j_k], np.transpose(values_grad_f[:, j_k]))
            C_hat = C_hat + tmp

        C_hat = C_hat * 1 / M
        C_hat_tmp = 1 / M * np.matmul(values_grad_f, np.transpose(values_grad_f))

        if self.is_bootstrapping():  # bootstrapping
            C_hat_tmp = C_hat
        # todo: check why differences occur - even without bootstrap
        # todo: move to tests
        np.testing.assert_array_almost_equal(C_hat, C_hat_tmp)

        return C_hat_tmp

    def construct_C_matrix(self, values_grad_f: np.ndarray, M: int) -> np.ndarray:
        if self.get_param().get_model().get_qoi_dim() > 1:  # multidim qoi
            C_hat_tmp = None
            for i in range(0, self.get_param().get_model().get_qoi_dim()):
                C_hat_i = self.construct_C_from_gradients(values_grad_f=values_grad_f, M=M)

                if C_hat_tmp is None:
                    C_hat_tmp = C_hat_i
                else:
                    C_hat_tmp.v_stack([C_hat_tmp, C_hat_i])

        else:  # onedimensional qoi
            C_hat_tmp = self.construct_C_from_gradients(values_grad_f=values_grad_f, M=M)

        return C_hat_tmp

    def bootstrapping(self, values_grad_f: np.ndarray, M: int) -> None:
        M_boot = self.get_param().get_M_boot()
        w_vec = self.get_result().get_W_hat()

        distance_e = None
        distance_subspace = None
        lambda_eig_i_save = None

        if M_boot > 0:

            m = np.size(values_grad_f, axis=0)

            distance_e = np.zeros(shape=(m - 1, M_boot))
            lambda_eig_i_save = np.zeros(shape=(m, M_boot))

            for i in range(0, M_boot):  # could be parallelized

                C_hat_i = self.construct_C_matrix(values_grad_f=values_grad_f, M=M)

                # Compute the eigendecomposition

                w_vec_i, lambda_eig_i, wh_i = self.eigendecomposition(C_hat_i)
                lambda_eig_i_save[:, i] = lambda_eig_i

                # For a particular choice of the active subspace dimension n [constantine-2015]
                for subspace_idx in range(0, m - 1):
                    w_active_i, w_inactive_i = self.divide_active_inactive_subspace(w_vec=w_vec_i,
                                                                                    subspace_dim=subspace_idx + 1)

                    w_active, w_inactive = self.divide_active_inactive_subspace(w_vec=w_vec,
                                                                                subspace_dim=subspace_idx + 1)

                    # distance between subspaces according to 3.73
                    distance_e[subspace_idx, i] = self.distance_subspace_eq_349(W1=w_active, W2_hat=w_inactive_i)
                    # np.linalg.norm(np.matmul(np.transpose(w_active), w_inactive_i))

            distance_subspace = np.mean(distance_e, axis=1)

        self.get_result().set_distance_subspace(distance_subspace=distance_subspace)
        self.get_result().set_distance_bootstrapping(distance_bootstrapping=distance_e)
        self.get_result().set_lambda_bootstrapping(lambda_bootstrapping=lambda_eig_i_save)

    def scale_gradient(self, gradient: np.ndarray) -> np.ndarray:
        x_lower = self.get_param().get_x_lower()
        x_upper = self.get_param().get_x_upper()
        if np.ndim(x_lower) == 0:
            interval_size = np.array([x_upper - x_lower])
        else:
            interval_size = x_upper - x_lower
        # Scaling of gradient (according to quadrature rule factor - compensation of coordinate transformation
        scaled_gradient = np.matmul(0.5 * np.diag(interval_size), gradient)
        return scaled_gradient
