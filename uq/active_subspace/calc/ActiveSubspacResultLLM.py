import numpy as np

from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult


class ActiveSubspacResultLLM(ActiveSubspaceResult):

    def __init__(self, activity_scores: np.ndarray, idx_gap: int, w_active: np.ndarray, lambda_eig: np.ndarray,
                 error_lambda_hat: np.ndarray):
        super().__init__(error_lambda_hat=error_lambda_hat, activity_scores=activity_scores, lambda_eig=lambda_eig,
                         w_active=w_active, idx_gap=idx_gap)
