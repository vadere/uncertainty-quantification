from typing import Union

import numpy as np

from uq.utils.calc.Result import Result


class ActiveSubspaceResult(Result):
    def __init__(self, error_lambda_hat: np.ndarray = None, error_c_hat: float = None,
                 activity_scores: np.ndarray = None, true_activity_scores=None, size_subspace: int = None,
                 path2results: str = None, lambda_eig: np.ndarray = None, w_active: np.ndarray = None,
                 lambda_eig_true: np.ndarray = None, idx_gap: int = None, idx_gap_true: int = None,
                 distance_subspace: Union[np.ndarray, float] = None, true_distance: Union[np.ndarray, float] = None,
                 C_hat: np.ndarray = None, W: np.ndarray = None, true_W: np.ndarray = None,
                 dist_as_vec: np.ndarray = None, computation_time: int = None):
        super().__init__(computation_time=computation_time)

        self.__path2results = path2results

        # Activity scores
        self.__activity_scores = activity_scores
        self.__activity_scores_true = true_activity_scores

        # Uncentered covariance matrix C
        self.__C_hat = C_hat  # approximation
        self.__error_C_hat = error_c_hat

        # Eigenvalues of C
        self.__lambda_hat = lambda_eig  # Approximation, eigenvalues of C_hat
        self.__lambda_true = lambda_eig_true
        self.__error_lambda_hat = error_lambda_hat  # (=max_rel_error_eig)

        # Spectral gap in eigenvalues
        self.__idx_gap = idx_gap
        self.__idx_gap_true = idx_gap_true
        self.__size_subspace = size_subspace

        # C = W \Lambda W^T (Eigenvectors of C)
        self.__W_hat = W  # approximation, eigenvectors of C_hat
        self.__W_true = true_W
        self.__W_active = w_active  # active subspace (W_1)

        # Quality of subspace
        self.__distance_subspace = distance_subspace
        self.__distance_subspace_true = true_distance
        self.__dist_as_vec = dist_as_vec  # distance between subspaces after corollar 4.10

    def equals(self, other: "ActiveSubspaceResult") -> bool:
        bool_equals = False
        if isinstance(other, ActiveSubspaceResult):
            if np.all(self.get_activity_scores() == other.get_activity_scores()) and \
                    np.all(self.get_activity_scores_true() == other.get_activity_scores_true()) and \
                    np.all(self.get_C_hat() == other.get_C_hat()) and \
                    np.all(self.get_error_c_hat() == other.get_error_c_hat()) and \
                    np.all(self.get_lambda_hat() == other.get_lambda_hat()) and \
                    np.all(self.get_lambda_hat() == other.get_lambda_hat()) and \
                    np.all(self.get_lambda_true() == other.get_lambda_true()) and \
                    np.all(self.get_error_lambda_hat() == other.get_error_lambda_hat()) and \
                    self.get_idx_gap() == other.get_idx_gap() and \
                    self.get_idx_gap_true() == other.get_idx_gap_true() and \
                    self.get_size_subspace() == other.get_size_subspace() and \
                    np.all(self.get_W_hat() == other.get_W_hat()) and \
                    np.all(self.get_W_true() == other.get_W_true()) and \
                    np.all(self.get_W_active() == other.get_W_active()) and \
                    np.all(self.get_distance_subspace() == other.get_distance_subspace()) and \
                    np.all(self.get_distance_subspace_true() == other.get_distance_subspace_true()) and \
                    np.all(self.get_dist_as_vec() == other.get_dist_as_vec()):
                bool_equals = True

        return bool_equals

    # GETTERS

    def get_error_lambda_hat(self) -> np.ndarray:
        return self.__error_lambda_hat

    def get_error_c_hat(self) -> float:
        return self.__error_C_hat  # error in C_hat approximation if C is known

    def get_activity_scores(self) -> np.ndarray:
        return self.__activity_scores

    def get_activity_scores_true(self) -> np.ndarray:
        return self.__activity_scores_true

    def get_size_subspace(self) -> int:
        return self.__size_subspace

    def get_path2results(self) -> str:
        return self.__path2results

    def get_lambda_hat(self) -> np.ndarray:
        return self.__lambda_hat  # eigenvalues of C

    def get_lambda_true(self) -> np.ndarray:
        return self.__lambda_true

    def get_W_hat(self) -> np.ndarray:
        return self.__W_hat

    def get_W_active(self) -> np.ndarray:
        return self.__W_active  # W_1

    def get_idx_gap(self) -> int:
        return self.__idx_gap

    def get_idx_gap_true(self) -> int:
        return self.__idx_gap_true

    def get_distance_subspace(self) -> np.ndarray:
        return self.__distance_subspace

    def get_distance_subspace_true(self) -> np.ndarray:
        return self.__distance_subspace_true

    def get_W_true(self) -> np.ndarray:
        return self.__W_true

    def get_dist_as_vec(self) -> np.ndarray:
        return self.__dist_as_vec

    def get_C_hat(self) -> np.ndarray:
        return self.__C_hat  # uncentered covariance matrix of gradients

    def get_eigenvectors(self) -> np.ndarray:
        return self.get_W_hat()

    # SETTERS

    def set_error_lambda_hat(self, max_rel_error_eig) -> None:
        self.__error_lambda_hat = max_rel_error_eig

    def set_error_c_hat(self, error_c_hat: float) -> None:
        self.__error_C_hat = error_c_hat

    def set_activity_scores(self, activity_scores: np.ndarray) -> None:
        self.__activity_scores = activity_scores

    def set_activity_scores_true(self, true_activity_scores: np.ndarray) -> None:
        self.__activity_scores_true = true_activity_scores

    def set_size_subspace(self, size_subspace: Union[int, np.ndarray]) -> None:
        self.__size_subspace = size_subspace

    def set_path2results(self, path2results: str) -> None:
        self.__path2results = path2results

    def set_lambda_hat(self, lambda_eig: np.ndarray) -> None:
        self.__lambda_hat = lambda_eig

    def set_W_hat(self, W: np.ndarray) -> None:  # C = W \Lambda W^T
        self.__W_hat = W

    def set_lambda_true(self, lambda_eig_true: np.ndarray) -> None:
        self.__lambda_true = lambda_eig_true

    def set_W_active(self, w_active: np.ndarray) -> None:
        self.__W_active = w_active

    def set_idx_gap(self, idx_gap: int) -> None:
        self.__idx_gap = idx_gap

    def set_idx_gap_true(self, idx_gap_true: int) -> None:
        self.__idx_gap_true = idx_gap_true

    def set_distance_subspace(self, distance_subspace: np.ndarray) -> None:
        self.__distance_subspace = distance_subspace

    def set_distance_subspace_true(self, true_distance: np.ndarray) -> None:
        self.__distance_subspace_true = true_distance

    def set_W_true(self, true_W: np.ndarray) -> None:
        self.__W_true = true_W

    def set_dist_as_vec(self, dist_as_vec: np.ndarray) -> None:
        # distance between two subspaces (corollary 3.10 in constantine-2017)
        self.__dist_as_vec = dist_as_vec

    def set_C_hat(self, C_hat: np.ndarray) -> None:
        self.__C_hat = C_hat
