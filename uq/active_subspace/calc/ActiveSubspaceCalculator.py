import warnings
from typing import Tuple, Union

import numpy as np

from uq.active_subspace.calc.ActiveSubspaceParameter import ActiveSubspaceParameter
from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult
from uq.utils.calc.Calculator import Calculator
from uq.utils.datatype import is_row_vector
from uq.utils.model.AnalyticalModel import AnalyticalModel


class ActiveSubspaceCalculator(Calculator):

    def __init__(self, param: ActiveSubspaceParameter = None, result: ActiveSubspaceResult = None):
        super().__init__(param=param, result=result)

    def get_param(self) -> ActiveSubspaceParameter:
        return super().get_param()

    def get_result(self) -> ActiveSubspaceResult:
        return super().get_result()

    def identify_active_subspace(self) -> ActiveSubspaceResult:
        pass

    def construct_C_matrix(self) -> np.ndarray:
        pass

    def calc_distance_active_subspace(self, w_vec: np.ndarray, prior: "Prior", lambda_eig: np.ndarray, idx_gap: int) \
            -> np.ndarray:
        test_model = self.get_param().get_model()
        dim = test_model.get_dimension()

        #  true distance
        true_W = None
        if isinstance(test_model, AnalyticalModel) and test_model.is_analytic_solution_available():
            true_distance = np.zeros(dim - 1)
            dist_as_vec = np.zeros(dim - 1)
            for subspace_idx in range(0, dim - 1):
                # subspace estimated with M samples
                _, w_inactive_tmp = self.divide_active_inactive_subspace(w_vec=w_vec, subspace_dim=subspace_idx + 1)

                # true C matrix
                true_C = test_model.get_C_matrix(prior=prior)
                true_W, _, _ = self.eigendecomposition(C_hat=true_C)

                # true k-dimensional subspace
                true_W_active_k, _ = self.divide_active_inactive_subspace(w_vec=true_W, subspace_dim=subspace_idx + 1)

                # distance between true k-dimensional subspace and subspace estimated with M samples
                true_distance[subspace_idx] = self.distance_subspace_eq_349(true_W_active_k, w_inactive_tmp)

                if subspace_idx < dim:
                    dist_as_vec[subspace_idx] = self.distance_subspace_corollary310(lambda_eig=lambda_eig,
                                                                                    idx_gap=subspace_idx, n_dim=dim)
        else:
            true_distance = None
            dist_as_vec = None

        dist_as = self.distance_subspace_corollary310(lambda_eig, idx_gap, n_dim=dim)
        if self.get_param().is_print_data():
            print("** quality of active subspace (corollary 3.10)")
            print(dist_as)  # bounded by 1? (constantine-2014, p. 37)
            print("** approx **")

        self.get_result().set_distance_subspace_true(true_distance)
        self.get_result().set_W_true(true_W)
        self.get_result().set_dist_as_vec(dist_as_vec)

        return dist_as

    @classmethod
    def find_largest_gap_log(cls, lambda_eig: np.ndarray) -> Tuple[int, bool]:

        # make sure that the vector is sorted
        if any(np.diff(lambda_eig) > 0):
            raise ValueError("find_largest_gap_log: Input vector must be descending!")

        # lambda_eig should always be >= 0 since C is positive semi-definite, abs is for numerical instabilities
        if (lambda_eig <= 0).any():
            idx = np.argmax(np.abs(np.diff(lambda_eig)))  # find largest gap in normal representation
            bool_plot_log = False
        else:
            if len(lambda_eig) > 1:
                idx = np.argmax(
                    np.abs(np.diff(np.log10(np.abs(lambda_eig)))))  # find largest gap in logarithmic representation
            else:
                idx = 0

            bool_plot_log = True

        return idx, bool_plot_log

    @classmethod
    def eigendecomposition(cls, C_hat: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        # if linalg.eig is used, the eigenvalues (and vectors!) have to be sorted afterwards in descending order
        w_vec, lambda_eig, w_h = np.linalg.svd(C_hat)
        w_h = np.transpose(w_vec)

        threshold = 1e-9
        max_diff = np.max(np.matmul(np.matmul(w_vec, np.diag(lambda_eig)), w_h) - C_hat)
        if max_diff > threshold:  # np.finfo(float).eps:
            warnings.warn("Eigendecomposition: Errors are greater than %.3e (%.3e)" % (threshold, max_diff))

        return w_vec, lambda_eig, w_h

    @classmethod
    def distance_subspace_corollary310(cls, lambda_eig: np.ndarray, idx_gap: int, n_dim: int) -> float:
        if idx_gap == n_dim - 1:
            warnings.warn('Distance cannot be calculated if the active subspace is the whole space.')
            dist_as = None
        else:
            eps = ((lambda_eig[idx_gap] - lambda_eig[idx_gap + 1]) / (5 * lambda_eig[0]))
            dist_as = (4 * lambda_eig[0] * eps) / (lambda_eig[idx_gap] - lambda_eig[idx_gap + 1])  # upper bound
        return dist_as

    @classmethod
    def distance_subspace_eq_349(cls, W1: np.ndarray, W2_hat: np.ndarray) -> float:
        if is_row_vector(W1):  # W1 is row vector
            W1 = np.expand_dims(W1, axis=1)
        dist = np.linalg.norm(np.matmul(np.transpose(W1), W2_hat))
        return dist

    def divide_active_inactive_subspace(self, w_vec: np.ndarray, subspace_dim: int) -> Tuple[np.ndarray, np.ndarray]:
        m = self.get_param().get_model().get_dimension()
        w_active_tmp = w_vec[:, 0:subspace_dim]
        w_inactive_tmp = w_vec[:, subspace_dim:m]
        return w_active_tmp, w_inactive_tmp

    def calc_activity_scores(self, lambda_eig: np.array, w_vec: np.array, n: int) -> np.ndarray:
        m = self.get_param().get_model().get_dimension()
        alpha = np.zeros(shape=m)
        for i in range(0, m):
            tmp = 0
            for j in range(0, n):
                tmp = tmp + (lambda_eig[j] * np.square(w_vec[i, j]))
            alpha[i] = tmp
        return alpha

    @classmethod
    def relative_error(cls, true_value: Union[float, np.ndarray], approx_value: Union[float, np.ndarray]) -> float:
        return (approx_value - true_value) / true_value
