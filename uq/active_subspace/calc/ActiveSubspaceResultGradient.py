from typing import Union

import numpy as np

from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult


class ActiveSubspaceResultGradient(ActiveSubspaceResult):

    def __init__(self, error_lambda_hat=None, error_c_hat: float = None, activity_scores: np.ndarray = None,
                 true_activity_scores=None, size_subspace: int = None, path2results: str = None, n_samples: int = None,
                 lambda_eig: np.ndarray = None, w_active: np.ndarray = None, lambda_eig_true: np.ndarray = None,
                 idx_gap: int = None, idx_gap_true: int = None, distance_subspace: Union[np.ndarray, float] = None,
                 true_distance: Union[np.ndarray, float] = None, C_hat: np.ndarray = None, gradients: np.ndarray = None,
                 lambda_bootstrapping: np.ndarray = None, distance_bootstrapping: np.ndarray = None,
                 transformed_gradients: np.ndarray = None, computation_time: int = None):
        self.__n_samples = n_samples
        self.__gradients_original = gradients
        self.__gradients_transformed = transformed_gradients
        self.__lambda_bootstrapping = lambda_bootstrapping
        self.__distance_bootstrapping = distance_bootstrapping
        super().__init__(error_lambda_hat=error_lambda_hat, error_c_hat=error_c_hat, activity_scores=activity_scores,
                         true_activity_scores=true_activity_scores, size_subspace=size_subspace,
                         path2results=path2results, lambda_eig=lambda_eig, w_active=w_active,
                         lambda_eig_true=lambda_eig_true, idx_gap=idx_gap, idx_gap_true=idx_gap_true,
                         distance_subspace=distance_subspace, true_distance=true_distance, C_hat=C_hat,
                         computation_time=computation_time)

    def equals(self, other: "ActiveSubspaceResultGradient") -> bool:
        bool_equals = super().equals(other)

        if isinstance(other, ActiveSubspaceResultGradient):
            if self.get_n_samples() == other.get_n_samples() and \
                    np.all(self.get_gradients_original() == other.get_gradients_original()) and \
                    np.all(self.get_gradients_transformed() == other.get_gradients_transformed()) and \
                    np.all(self.get_lambda_bootstrapping() == other.get_lambda_bootstrapping()) and \
                    np.all(self.get_distance_bootstrapping() == other.get_distance_bootstrapping()):
                bool_equals = True
        return bool_equals

    # GETTERS

    def get_n_samples(self) -> int:
        return self.__n_samples

    def get_gradients_original(self) -> np.ndarray:
        return self.__gradients_original

    def get_gradients_transformed(self) -> np.ndarray:
        return self.__gradients_transformed

    # lambda_i_eig
    def get_lambda_bootstrapping(self) -> np.ndarray:
        return self.__lambda_bootstrapping

    def get_distance_bootstrapping(self) -> np.ndarray:
        return self.__distance_bootstrapping

    # SETTERS

    def set_gradients_original(self, gradients: np.ndarray) -> None:
        self.__gradients_original = gradients

    def set_gradients_transformed(self, transformed_gradients: np.ndarray) -> None:
        self.__gradients_transformed = transformed_gradients

    def set_lambda_bootstrapping(self, lambda_bootstrapping: np.ndarray) -> None:
        self.__lambda_bootstrapping = lambda_bootstrapping

    def set_n_samples(self, n_samples: int) -> None:
        self.__n_samples = n_samples

    def set_distance_bootstrapping(self, distance_bootstrapping: np.ndarray) -> None:
        self.__distance_bootstrapping = distance_bootstrapping
