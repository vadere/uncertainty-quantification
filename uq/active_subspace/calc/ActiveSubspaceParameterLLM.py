import numpy as np

from uq.active_subspace.calc.ActiveSubspaceParameter import ActiveSubspaceParameter


class ActiveSubspaceParameterLLM(ActiveSubspaceParameter):

    def __init__(self, model: "Model", x_lower: np.ndarray, x_upper: np.ndarray, alpha: int, k: int, case: int,
                 seed: int, factor_N: int):
        self.__factor_N = factor_N
        super().__init__(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k, case=case, seed=seed)

    def get_factor_N(self) -> int:
        return self.__factor_N
