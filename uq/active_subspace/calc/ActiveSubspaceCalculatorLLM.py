import time

import matplotlib as plt
import numpy as np
from numpy.random import RandomState

from uq.active_subspace.calc.ActiveSubspaceCalculator import ActiveSubspaceCalculator
from uq.active_subspace.calc.ActiveSubspaceParameterLLM import ActiveSubspaceParameterLLM
from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult
from uq.active_subspace.ioput.ActiveSubspacePlotterLLM import ActiveSubspacePlotterLLM
from uq.active_subspace.utils import transform_coordinates_to_unit, transform_coordinates_from_unit
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.model.ApproxModel import ApproxModel
from uq.utils.prior.UniformGenMult import UniformGenMult

bool_plot = False


# LLM = local linear model
class ActiveSubspaceCalculatorLLM(ActiveSubspaceCalculator):

    def __init__(self, param: ActiveSubspaceParameterLLM):
        result = ActiveSubspaceResult()
        super().__init__(param=param, result=result)

    def get_param(self) -> ActiveSubspaceParameterLLM:
        return super().get_param()

    #  Algorithm 1.2 from constantine-2015 (p. 4)
    def identify_active_subspace(self) -> ActiveSubspaceResult:
        start = time.time()

        param = self.get_param()
        model = param.get_model()
        dim = model.get_dimension()
        factor_N = param.get_factor_N()

        if param.get_path2results() is not None:
            datasaver = DataSaver(param.get_path2results())
            model.set_data_saver(datasaver)
        else:
            datasaver = None

        random_state = RandomState(param.get_seed())

        # Create prior distribution

        if isinstance(param.get_prior(), UniformGenMult):
            lower_transformed = transform_coordinates_to_unit(x_lower=param.get_x_lower(),
                                                              x_upper=param.get_x_upper(), value=param.get_x_lower())
            upper_transformed = transform_coordinates_to_unit(x_lower=param.get_x_lower(),
                                                              x_upper=param.get_x_upper(), value=param.get_x_upper())
            rho = UniformGenMult(lower=lower_transformed, upper=upper_transformed, dim=dim)
        else:
            raise UserWarning("Density type not (yet) implemented!")

        # Step 1
        n_samples = np.round(param.get_alpha() * dim * factor_N).astype(np.int)
        M = np.ceil(param.get_alpha() * param.get_k() * np.log(dim)).astype(np.int)
        p = np.round(random_state.rand(1)[0] * (n_samples - (dim + 1) + 1) + (dim + 1 - 0.5)).astype(
            int)  # m+1 <= p <= N

        # Step 2: Draw N samples according to the density

        samples = rho.sample(n=n_samples, random_state=random_state)

        # Compute q_j = f(x_j) for each sample
        samples_transformed = transform_coordinates_from_unit(x_lower=param.get_x_lower(),
                                                              x_upper=param.get_x_upper(), value=samples)

        # Construction of C differs from active_subspace_with_gradients
        C_hat = self.construct_C_matrix(samples_j=samples, samples_transformed_j=samples_transformed, rho=rho,
                                        M=M, m=dim, p=p)

        # Once we have C the procedure is identical to active subspace with gradients
        w_mat_hat, lambda_hat, w_h = self.eigendecomposition(C_hat=C_hat)

        # Find eigenvalue gap
        idx_gap, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=lambda_hat)
        self.get_result().set_idx_gap(idx_gap)

        # Calc activity scores
        activity_scores = self.calc_activity_scores(lambda_eig=lambda_hat, w_vec=w_mat_hat, n=idx_gap + 1)

        # Copied from ActiveSubspaceCalculatorGradient
        if isinstance(model, AnalyticalModel):
            lambda_eig_true = model.get_eigenvalues_C(rho=rho)
            self.get_result().set_lambda_true(lambda_eig_true)
            idx_gap_true, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=lambda_eig_true)
            self.get_result().set_idx_gap_true(idx_gap_true)

        # Copied from ActiveSubspaceCalculatorGradient
        # Distance between subspaces of different dimensions (measure for the subspace)
        self.calc_distance_active_subspace(w_vec=w_mat_hat, prior=rho, lambda_eig=lambda_hat, idx_gap=idx_gap)

        # Copied from ActiveSubspaceCalculatorGradient
        true_activity_scores = None
        if isinstance(model, AnalyticalModel):
            true_activity_scores = self.calc_activity_scores(lambda_eig=self.get_result().get_lambda_true(),
                                                             w_vec=self.get_result().get_W_true(), n=idx_gap + 1)

        # divide into active and inactive subspaces
        w_active, w_inactive = self.divide_active_inactive_subspace(w_vec=w_mat_hat, subspace_dim=idx_gap + 1)

        approx_model = ApproxModel(W1=w_active, model=model)

        # true distance
        ''' if model.is_analytic_solution_available():
            true_distance = np.zeros(m-1)
            dist_as_vec = np.zeros(m-1)
            for subspace_idx in range(0, m-1):
                # subspace estimated with M samples
                _, w_inactive_tmp = divide_active_inactive_subspace(w_mat_hat, subspace_idx+1, m)
    
                # true C matrix
                true_C = test_model.get_C_matrix(rho)
                true_W, true_eig, true_Wh = eigendecomposition(true_C)
    
                # true k-dimensional subspace
                true_W_active_k, _ = divide_active_inactive_subspace(true_W, subspace_idx+1, m)
    
    
                # distance between true k-dimensional subspace and subspace estimated with M samples
                true_distance[subspace_idx] = distance_subspace_eq_349(true_W_active_k, w_inactive_tmp)
    
                if subspace_idx < m:
                    dist_as_vec[subspace_idx] = distance_subspace_corollary310(lambda_hat, subspace_idx)
    
        print("** quality of active subspace (corollary 3.10)")
        dist_as = distance_subspace_corollary310(lambda_hat, idx_gap)
        print(dist_as)      # bounded by 1? (constantine-2014, p. 37) '''

        # --------------------------------------------------------------------- evaluation

        true_eig_C = None
        max_rel_error_eig = None
        if isinstance(model, AnalyticalModel):
            # print results
            true_eig_C = model.get_eigenvalues_C(rho=rho)
            max_rel_error_eig = np.max(
                ActiveSubspaceCalculator.relative_error(true_value=true_eig_C, approx_value=lambda_hat))

        # Save results

        self.get_result().set_activity_scores(activity_scores=activity_scores)
        self.get_result().set_activity_scores_true(true_activity_scores=true_activity_scores)
        self.get_result().set_idx_gap(idx_gap=idx_gap)
        self.get_result().set_idx_gap_true(idx_gap_true=idx_gap_true)
        self.get_result().set_W_active(w_active=w_active)
        self.get_result().set_lambda_hat(lambda_eig=lambda_hat)
        self.get_result().set_lambda_true(lambda_eig_true=lambda_eig_true)
        self.get_result().set_error_lambda_hat(max_rel_error_eig=max_rel_error_eig)
        self.get_result().set_C_hat(C_hat=C_hat)
        #        self.get_results().set_distance_subspace_true(true_distance=true_distance)
        print(self.get_result().get_distance_subspace())
        self.get_result().set_computation_time(((time.time() - start) / 60))

        # Plot results

        plotter = ActiveSubspacePlotterLLM(datasaver=datasaver, params=self.get_param(), results=self.get_result())
        plotter.plot_results_local_linear_model(prior=rho)

        return self.get_result()
        # return activity_scores, max_rel_error_eig, w_active, idx_gap, lambda_hat

    def construct_C_matrix(self, samples_j: np.ndarray, samples_transformed_j: np.ndarray, rho, M: int, m: int,
                           p: int) -> np.ndarray:

        params = self.get_param()
        model = params.get_model()
        random_state = self.get_random_state()

        q_j = model.eval_model(parameter_value=samples_transformed_j)  # evaluate model at samples

        # Step 3: Draw M samples according to the density
        samples_i = rho.sample(M, random_state=random_state)

        # Step 4: For each sample x_i^' find the p points of x_j nearest to x_i^'
        C_hat = np.zeros(shape=(m, m))

        for i in range(0, M):
            # distance between x_i^' and x_j
            dist = np.linalg.norm(samples_j - np.expand_dims(samples_i[:, i], axis=1), axis=0)

            # sort
            idx_sorted = np.argsort(dist)

            # p nearest points
            idx_near = idx_sorted[1:p + 1]
            q_j_near = q_j[idx_near]  # model evals
            samples_j_near = samples_j[:, idx_near]  # x-values

            # Step 5: fit local linear regression model (least squares)
            # Psi_X = np.ones(shape=(m+1, N))
            # Psi_X[1:m+1, :] = samples_j

            Psi_X = np.ones(shape=(p, m + 1))
            Psi_X[:, 1:p] = np.transpose(samples_j_near)

            beta_approx = self.least_squares(matrix_x=Psi_X, y=q_j_near)
            c_i = beta_approx[0]
            b_i = beta_approx[1:m + 1]

            # Step 6: Compute matrix C hat and its eigenvalue decomposition
            C_hat = C_hat + np.outer(b_i, np.transpose(b_i))

            # Evaluate fit
            residual_i = c_i + np.matmul(b_i, samples_j_near) - q_j_near
            residual_i_rel = residual_i / q_j_near

            if params.is_plot_data():
                plt.figure(2)
                plt.plot(residual_i_rel, '.:')
                plt.xlabel('Sample #')
                plt.ylabel('Relative residual')

                plt.figure(3)
                plt.plot(b_i, '.:')
                plt.ylabel('Coefficients b_i')
                plt.xlabel('Index (dim)')

        C_hat = C_hat * 1 / M

        return C_hat

    @classmethod
    def least_squares(cls, matrix_x: np.ndarray, y: np.ndarray) -> np.ndarray:
        tmp = np.linalg.inv(np.matmul(np.transpose(matrix_x), matrix_x))
        beta_hat = np.matmul(np.matmul(tmp, np.transpose(matrix_x)), y)
        return beta_hat
