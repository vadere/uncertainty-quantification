from typing import Union

import numpy as np

from uq.active_subspace.calc.ActiveSubspaceParameter import ActiveSubspaceParameter
from uq.utils.model.VadereModel import VadereModel


class ActiveSubspaceParameterGradient(ActiveSubspaceParameter):

    def __init__(self, model: "Model" = None, x_lower: Union[float, np.ndarray] = None,
                 x_upper: Union[float, np.ndarray] = None, alpha: int = None, k: int = None, bool_gradient: bool = None,
                 M_boot: int = None, step_size_relative: np.ndarray = None, step_size: float = None, case: int = None,
                 seed: int = None, bool_averaged: bool = None, no_runs_averaged: int = None,
                 bool_save_data: bool = None, bool_print: bool = None, bool_plot: bool = None, path2results: str = None,
                 force_idx_gap: int = None):

        super().__init__(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k, case=case, seed=seed,
                         bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged, bool_save_data=bool_save_data,
                         bool_print=bool_print, bool_plot=bool_plot, path2results=path2results,
                         force_idx_gap=force_idx_gap)

        self.__bool_gradient = bool_gradient  # is analytical gradient available
        self.__M_boot = M_boot  # M for bootstrapping of eigenvalues

        if not bool_gradient:
            if step_size is not None and step_size_relative is None:
                self.__step_size_relative = step_size * (x_upper - x_lower)
            else:
                # for gradient approximation (finite differences, per dim)
                self.__step_size_relative = step_size_relative

            self.__step_size = step_size  # for gradient approximation (finite differences)
        else:
            self.__step_size = None
            self.__step_size_relative = None

        self.check_step_size_for_int_parameters()

    def is_gradient_available(self) -> bool:
        return self.__bool_gradient

    def get_M_boot(self) -> int:
        return self.__M_boot

    def get_step_size_relative(self):
        return self.__step_size_relative

    def get_step_size(self):
        return self.__step_size

    def check_step_size_for_int_parameters(self) -> None:  # check step size for integer parameters - todo as function
        model = self.get_model()
        x_lower = self.get_x_lower()
        x_upper = self.get_x_upper()
        step_size = self.get_step_size()

        if isinstance(model, VadereModel):
            # split keys since some may have several parts to resemble structure and / or id of element
            idx = [index for index, val in enumerate(model.get_key()) if
                   str.split(val, '.')[-1] in VadereModel.INTEGER_PARAMETERS]

            if len(idx) > 0:
                diff_in_int_params = x_upper[idx] - x_lower[idx]
                increment_in_int_params = diff_in_int_params * step_size
                idx_increment_too_small = [index for index, val in enumerate(increment_in_int_params) if val < 1.0]

                if len(idx_increment_too_small) > 0:
                    param_str = list()
                    for i in range(0, len(idx_increment_too_small)):
                        param_idx = np.asarray(idx)[idx_increment_too_small[i]]
                        param_str.append(model.get_key()[param_idx])
                    raise UserWarning('For parameter(s) %s the step size is too small: Increment is 0. ' % param_str
                                      + 'This will lead to an undefined gradient. Choose a larger step size!')
