import numpy as np

from uq.utils.model.ExponentialModel import ExponentialModel

bool_gradient = True  # gradient available
step_size = 0.001  # for approximation of gradients
factor_N = 10  # local linear model
k = 2 + 1  # desired dimension of subspace +1
# alpha = 1000           # oversampling factor in [2,10]
alpha = 2

M_boot = 1000  # bootstrapping

test_model = ExponentialModel()
m = test_model.get_dimension()

x_lower = -1.0 * np.ones(shape=m)  # lower bounds for parameters
x_upper = np.ones(shape=m)  # upper bounds for parameters

# bool_vec = np.array([False, True, True, False])
density_type = "uniform"

test_input = x_lower
