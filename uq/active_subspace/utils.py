from typing import Union

import numpy as np

MACHINE_PRECISION = np.finfo(float).eps


def relative_error_constantine_2017(estimation: np.ndarray, reference: np.ndarray) -> np.ndarray:
    return np.abs(estimation - reference) / np.max(np.abs(reference))


def transform_coordinates_to_unit(x_lower: Union[float, np.ndarray], x_upper: Union[float, np.ndarray],
                                  value: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
    if np.ndim(value) == 1 and len(value) > 1:  # row vector
        value = np.expand_dims(value, axis=1)  # shape to column vector
    transformed_value = np.matmul(np.diag(1 / (x_upper - x_lower)),
                                  (2 * value) - np.expand_dims(x_upper + x_lower, axis=1))
    return transformed_value


# from constantine-2015, p. 2, eq. (1.1)
def transform_coordinates_from_unit(x_lower: Union[float, np.ndarray], x_upper: Union[float, np.ndarray],
                                    value: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
    if np.ndim(value) == 1 and len(value) > 1:  # row vector
        value = np.expand_dims(value, axis=1)  # shape to column vector
    if np.size(value) > 1:
        transformed_value = 1 / 2 * (
                np.matmul(np.diag(x_upper - x_lower), value) + np.expand_dims(x_upper + x_lower, axis=1))
    else:
        transformed_value = 1 / 2 * ((np.diag(x_upper - x_lower) * value) + np.expand_dims(x_upper + x_lower, axis=1))

    value_datatype = transformed_value

    return value_datatype


def abs_relative_error(true_value: Union[float, np.ndarray], approx_value: Union[float, np.ndarray]) -> float:
    return np.abs(approx_value - true_value) / true_value


# provided by Daniel Lehmberg
def assert_allclose_eigenvectors(eigvec1: np.ndarray, eigvec2: np.ndarray, tol: float = 1e-14) -> None:
    norms1 = np.linalg.norm(eigvec1, axis=0)
    norms2 = np.linalg.norm(eigvec2, axis=0)
    eigvec_test = (eigvec1.T @ eigvec2) * np.reciprocal(np.outer(norms1, norms2))

    actual = np.abs(np.diag(eigvec_test))  # -1 is also allowed for same direction
    expected = np.ones(actual.shape[0])

    np.testing.assert_allclose(expected, actual, atol=tol, rtol=0)


def is_smaller_equal_than_machine_precision(a: float) -> bool:
    return abs(a) <= np.finfo(float).eps
