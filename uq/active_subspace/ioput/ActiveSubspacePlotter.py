from uq.active_subspace.calc.ActiveSubspaceParameter import ActiveSubspaceParameter
from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult
from uq.utils.ioput.DataSaver import DataSaver


class ActiveSubspacePlotter:
    def __init__(self, datasaver: DataSaver, params: ActiveSubspaceParameter, results: ActiveSubspaceResult):
        self.__data_saver = datasaver
        self.__params = params
        self.__results = results

    def get_datasaver(self) -> DataSaver:
        return self.__data_saver

    def get_results(self) -> ActiveSubspaceResult:
        return self.__results

    def get_params(self) -> ActiveSubspaceParameter:
        return self.__params
