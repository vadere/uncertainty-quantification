import os

import numpy as np

from uq.active_subspace.calc.ActiveSubspaceParameter import ActiveSubspaceParameter
from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult
from uq.utils.find_software_versions import get_current_uq_state
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.ioput.FileWriter import FileWriter


class ActiveSubspaceFileWriter(FileWriter):

    def __init__(self, param: ActiveSubspaceParameter, result: ActiveSubspaceResult, data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    def get_param(self) -> ActiveSubspaceParameter:
        return super().get_param()

    def get_result(self) -> ActiveSubspaceResult:
        return super().get_result()

    def write_result(self):
        data_saver = self.get_datasaver()
        results = self.get_result()

        print("** RESULTS **")
        print("** Size of subspace:", np.unique(results.get_size_subspace()))
        print('** Computation time: %f min' % results.get_computation_time())
        print("Activity scores {}".format(results.get_activity_scores()))

        data_saver.write_var_to_file(results.get_activity_scores(), "activity_scores")
        data_saver.write_var_to_file(results.get_lambda_hat(), "eigenvalues")

        data_saver.write_var_to_file(results.get_distance_subspace(), "distance_subspace")
        data_saver.write_var_to_file(results.get_W_hat(), "eigenvectors")
        data_saver.write_var_to_file(np.diag(results.get_C_hat()), "derivative_based_metric")

        self.write_result_file()

    def write_result_file(self, n_samples: int = None, step_size: float = None) -> None:
        path = self.get_datasaver().get_path_to_files()
        params = self.get_param()
        results = self.get_result()

        print(path)
        print(results.get_path2results())

        model = params.get_model()

        output_file = os.path.join(path, "results.txt")

        with open(output_file, 'a+') as file:
            file.write('** Parameters **: \n')
            file.write('Seed: \t\t\t\t\t\t\t\t {} \n'.format(params.get_seed()))
            if n_samples is not None:
                file.write('Number of samples: \t\t\t\t\t {} \n'.format(n_samples))
            file.write("Number of runs averaged:  \t\t\t {} \n".format(params.get_no_runs_averaged()))
            file.write("Oversampling factor (alpha): \t\t {} \n".format(params.get_alpha()))
            file.write("Desired subspace size (k-1): \t\t {} \n".format(params.get_k() - 1))
            if step_size is not None:
                file.write("Step size (gradient approx.): \t\t {} \n".format(step_size))

            file.write('Parameters investigated: \t\t\t {} \n'.format(model.get_key()))
            file.write('Lower limits (parameters): \t\t\t {} \n'.format(params.get_x_lower()))
            file.write('Upper limits (parameters): \t\t\t {} \n'.format(params.get_x_upper()))

            file.write('Quantity of interest: \t\t\t\t {} \n'.format(model.get_qoi()))

            file.write('\n** Software versions **: \n')
            file.write('UQ Software (git_commit_hash): \t\t {} \n'.format(get_current_uq_state()["git_hash"]))
            file.write('UQ Software (uncommited_changes):\t {} \n'.format(
                str.replace(get_current_uq_state()["uncommited_changes"], '\n M', ';')))

            if model.get_version_str() is not None:
                file.write(model.get_version_str())

            file.write('\n** Results **: \n')
            file.write('Eigenvalues: \t\t\t\t\t\t {} \n'.format(results.get_lambda_hat()))
            file.write('Eigenvectors: \t\t\t\t\t\t {} \n'.format(np.transpose(results.get_W_active())))

            file.write('Activity scores: \t\t\t\t\t {} \n'.format(results.get_activity_scores()))
            file.write('Derivative-based metric: \t\t {} \n'.format(np.diag(results.get_C_hat())))
            file.write('Size of identified subspace: \t\t {} \n'.format(results.get_size_subspace()))
            file.write('Number of runs performed: \t\t\t {} \n'.format(model.get_n_evals()))

            file.write('\nComputation time: \t\t\t\t\t {} min \n'.format(results.get_computation_time()))

            file.write('\n')
            file.close()

            print("*** Results written to: %s ***" % file)
