from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.calc.ActiveSubspaceResultGradient import ActiveSubspaceResultGradient
from uq.active_subspace.ioput.ActiveSubspaceFileWriter import ActiveSubspaceFileWriter


class ActiveSubspaceFileWriterGradient(ActiveSubspaceFileWriter):

    def __init__(self, param: ActiveSubspaceParameterGradient, result: ActiveSubspaceResultGradient):
        super().__init__(param=param, result=result)

    def get_result(self) -> ActiveSubspaceResultGradient:
        return super().get_result()

    def get_param(self) -> ActiveSubspaceParameterGradient:
        return super().get_param()

    def write_result(self):
        super().write_result()

        data_saver = self.get_datasaver()
        results = self.get_result()
        data_saver.write_var_to_file(results.get_lambda_bootstrapping(), "eigenvalues_bootstrapping")
        data_saver.write_var_to_file(results.get_gradients_original(), "model_gradients_original")

    def write_result_file(self) -> None:
        results = self.get_result()
        params = self.get_param()

        super().write_result_file(n_samples=results.get_n_samples(),
                                  step_size=params.get_step_size())
