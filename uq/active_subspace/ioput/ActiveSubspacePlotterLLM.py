import matplotlib.pyplot as plt
import numpy as np

from uq.active_subspace.calc.ActiveSubspaceCalculator import ActiveSubspaceCalculator
from uq.active_subspace.calc.ActiveSubspaceParameterLLM import ActiveSubspaceParameterLLM
from uq.active_subspace.calc.ActiveSubspaceResult import ActiveSubspaceResult
from uq.active_subspace.ioput.ActiveSubspacePlotterGradients import ActiveSubspacePlotter
from uq.active_subspace.utils import is_smaller_equal_than_machine_precision
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.model.MatrixModel import MatrixModel


class ActiveSubspacePlotterLLM(ActiveSubspacePlotter):
    # coverage: 43%

    def __init__(self, datasaver: DataSaver, params: ActiveSubspaceParameterLLM, results: ActiveSubspaceResult):
        super().__init__(datasaver=datasaver, params=params, results=results)

    def plot_results_local_linear_model(self, prior: "Prior") -> None:

        bool_plot = self.get_params().is_plot_data()
        model = self.get_params().get_model()
        case = self.get_params().get_case()

        lambda_hat = self.get_results().get_lambda_hat()
        idx_gap = self.get_results().get_idx_gap()

        if bool_plot:

            # ---------------------------- plot eigenvalues
            fig, ax = plt.subplots()
            bool_log_plot = False
            true_eig_C = None
            idx_true_gap = None
            if isinstance(self.get_params().get_model(), AnalyticalModel):
                true_eig_C = model.get_eigenvalues_C(prior)
                _, bool_log_plot = ActiveSubspaceCalculator().find_largest_gap_log(lambda_eig=true_eig_C)
                idx_true_gap = self.get_results().get_idx_gap_true()

                plt.semilogy(np.arange(1, len(true_eig_C) + 1), true_eig_C, marker='o', color='royalblue',
                             label='True eigenvalues')

            plt.semilogy(np.arange(1, len(lambda_hat) + 1), lambda_hat, marker="x", color="red", linestyle=":",
                         label='Estimated eigenvalues')

            # lambda_i_eig

            ylim = plt.gca().get_ylim()
            if isinstance(self.get_params().get_model(), AnalyticalModel):
                plt.semilogy((idx_true_gap + 1.5) * np.ones(shape=(2, 1)), ylim, linestyle="-", color='royalblue',
                             label='True spectral gap')

            plt.semilogy((idx_gap + 1.5) * np.ones(shape=(2, 1)), ylim, linestyle=":", color='coral',
                         label='Spectral gap')

            # decide for linear y-axis if eigenvalues are close to zero
            if isinstance(self.get_params().get_model(), AnalyticalModel):
                if not bool_log_plot or is_smaller_equal_than_machine_precision(np.min(true_eig_C)):
                    ax.set_yscale('linear')
                    plt.xticks(np.arange(1, np.size(true_eig_C, axis=0) + 1))

            plt.grid()
            # if type(test_model) == MatrixModel:
            #    plt.ylim(10**-8, 10**4)
            #    plt.xlim(1, 6)

            plt.ylabel("Eigenvalues")
            plt.xlabel("Index")
            plt.legend()
            if type(model) == MatrixModel:
                plt.title("Case " + str(case) + ": Eigenvalues of C")
            else:
                plt.title("Eigenvalues of C")

            # fig.savefig(os.path.join(path_to_files, "eigenvalues_C.png"))  # save the figure to file
