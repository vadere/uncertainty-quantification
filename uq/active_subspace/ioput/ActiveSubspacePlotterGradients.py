import matplotlib.pyplot as plt
import numpy as np

from uq.active_subspace.calc.ActiveSubspaceCalculator import ActiveSubspaceCalculator
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.calc.ActiveSubspaceResultGradient import ActiveSubspaceResultGradient
from uq.active_subspace.ioput.ActiveSubspacePlotter import ActiveSubspacePlotter
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.model.MatrixModel import MatrixModel


class ActiveSubspacePlotterGradients(ActiveSubspacePlotter):
    # coverage: 10%

    def __init__(self, datasaver: DataSaver, params: ActiveSubspaceParameterGradient,
                 results: ActiveSubspaceResultGradient):
        super().__init__(datasaver=datasaver, params=params, results=results)

    def get_datasaver(self) -> DataSaver:
        return super().get_datasaver()

    def get_results(self) -> ActiveSubspaceResultGradient:
        return super().get_results()

    def get_params(self) -> ActiveSubspaceParameterGradient:
        return super().get_params()

    def plot_error_in_eigenvalue(self, result_gradients) -> None:

        m = self.get_params().get_model().get_dimension()
        # result_gradients = self.get_results()

        ax = plt.gca()
        ax.fill_between(x=np.arange(0, m), y1=np.min(result_gradients[0, :, :], axis=0),
                        y2=np.max(result_gradients[0, :, :], axis=0),
                        facecolor='lightcoral', alpha=0.3)
        plt.plot(np.arange(0, m), np.transpose(result_gradients[0, :, :]), ':2', color='lightcoral')
        plt.plot(np.arange(0, m), np.mean(result_gradients[0, :, :], axis=0), 'o:', label='Mean Gradients',
                 color='royalblue')

        # plt.semilogx(step_size_vec, result_finite_diff, '1:', color='lightgreen')
        # plt.semilogx(step_size_vec, mean_error_eig_finite_diff, 'x:', label='Mean Gradients', color='seagreen')

        # plt.plot(alpha_vec, mean_error_eig_local, 'x:', label='Local linear model')
        plt.xlabel(r'Oversampling factor $\alpha$')
        plt.xlabel(r'Index of eigenvalue')
        # plt.xlabel(r'Step size (finite differences)')

        plt.ylabel('Relative error in eigenvalues')
        plt.legend()

        fig = plt.gcf()
        self.get_datasaver().save_figure(fig, "error_in_eigenvalue")
        # plt.interactive(True)

        # plt.ylim([0, 1])
        # sns.palplot(sns.color_palette("Paired"))

        # plt.draw()

    def plot_error_in_C_matrix(self, alpha_vec: np.ndarray) -> None:
        error_c_gradients = self.get_results().get_error_c_hat()
        if not np.isnan(error_c_gradients).any():
            fig = plt.figure()
            ax = plt.gca()
            plt.plot(alpha_vec, error_c_gradients, ':2', color='lightcoral')
            ax.fill_between(x=alpha_vec, y1=np.min(error_c_gradients, axis=1), y2=np.max(error_c_gradients, axis=1),
                            facecolor='lightcoral', alpha=0.3)
            plt.plot(alpha_vec, np.mean(error_c_gradients, axis=1), 'o:', label='Mean Gradients', color='royalblue')
            plt.xlabel(r'Oversampling factor $\alpha$')
            plt.ylabel('Norm of error in C matrix')

            self.get_datasaver().save_figure(fig, "error_in_C_matrix")

    def plot_activity_scores(self) -> None:
        # activity_scores, true_activity_scores, size_subspace: int, m: int):
        results = self.get_results()
        params = self.get_params()

        m = len(params.get_model().get_key())

        if np.ndim(results.get_activity_scores()) > 2:
            tmp_activity_scores = results.get_activity_scores()[0, :, :]
        else:
            tmp_activity_scores = results.get_activity_scores()

        fig = plt.figure()
        ax = plt.gca()

        # ax.fill_between(x=np.arange(0, m), y1=np.min(tmp_activity_scores, axis=0),
        # y2=np.max(tmp_activity_scores, axis=0), facecolor='lightskyblue', alpha=0.3)
        ylim = plt.gca().get_ylim()

        # for i in range(0, n_runs):
        #    if i == 0:
        #        plt.plot(size_subspace[0, i] * np.ones(shape=2), ylim, color='sandybrown', linestyle=':',
        #        label='Subspace dimension')
        #    else:
        #        plt.plot(size_subspace[0, i]*np.ones(shape=2), ylim, color='sandybrown', linestyle=':')
        plt.bar(np.arange(1, m + 1), tmp_activity_scores, color='steelblue')
        # plt.plot(np.arange(1, m + 1), np.transpose(tmp_activity_scores), ':2', color='midnightblue')

        # todo: correct
        # if np.ndim(activity_scores) > 2:
        #    plt.plot(np.arange(1, m + 1), np.mean(activity_scores, axis=0), 'o:', label='Mean activity scores',
        #         color='steelblue')
        if not (results.get_activity_scores_true() is None) and not np.isnan(results.get_activity_scores_true()).any():
            plt.plot(np.arange(1, m + 1), np.mean(results.get_activity_scores_true()[0, :, :], axis=0), 'x:',
                     label='True activity scores',
                     color='gold')

        plt.ylabel('Activity scores')
        plt.xlabel('Index of parameter')
        plt.legend(loc='upper left')
        plt.title("Dimension of subspace: %s" % str(np.sort(np.unique(results.get_size_subspace()))))
        ax.set_xticks(np.arange(1, m + 1))

        self.get_datasaver().save_figure(fig, "activity_scores")

    def plot_true_activity_scores(self) -> None:
        model = self.get_params().get_model()
        true_activity_scores = self.get_results().get_activity_scores_true()
        size_subspace = self.get_results().get_size_subspace()
        m = self.get_params().get_model().get_dimension()

        if isinstance(self.get_params().get_model(), AnalyticalModel):
            fig = plt.figure()
            ax = plt.gca()
            ax.fill_between(x=np.arange(1, m + 1), y1=np.min(true_activity_scores[0, :, :], axis=0),
                            y2=np.max(true_activity_scores[0, :, :], axis=0),
                            facecolor='lightskyblue', alpha=0.3)
            plt.plot(np.arange(1, m + 1), np.transpose(true_activity_scores[0, :, :]), ':2', color='lightskyblue')
            plt.plot(np.arange(1, m + 1), np.mean(true_activity_scores[0, :, :], axis=0), 'o:',
                     label='Mean true activity scores',
                     color='steelblue')
            ylim = plt.gca().get_ylim()

            # for i in range(0, n_runs):
            #    if i == 0:
            #        plt.plot(size_subspace[0, i] * np.ones(shape=2), ylim, color='sandybrown', linestyle=':',
            #        label='Subspace dimension')
            #    else:
            #        plt.plot(size_subspace[0, i]*np.ones(shape=2), ylim, color='sandybrown', linestyle=':')
            ax.set_xticks(np.arange(1, m + 1))

            plt.ylabel('True activity scores')
            plt.xlabel('Index of parameter')
            plt.legend()
            plt.title("Dimension of subspace: %s" % str(np.sort(np.unique(size_subspace))))

            self.get_datasaver().save_figure(fig, "true_activity_scores")

    def plot_results(self, prior: "Prior") -> None:

        params = self.get_params()
        results = self.get_results()

        if params.is_plot_data():

            if isinstance(params.get_model(), AnalyticalModel):

                true_eig_C = params.get_model().get_eigenvalues_C(rho=prior)
                _, bool_log_plot = ActiveSubspaceCalculator().find_largest_gap_log(lambda_eig=true_eig_C)
            else:
                bool_log_plot = True
                true_eig_C = None

            self.plot_eigenvalues_A()
            self.plot_eigenvalues(true_eig_C=true_eig_C, bool_log_plot=bool_log_plot)

            self.plot_true_error_eigenvalue(true_eig_C=true_eig_C)

            self.plot_eigenvectors(w_vec=results.get_W_hat())

            if params.get_M_boot() > 1:
                self.plot_distance_subspaces()
                self.plot_eigenvalues_bootstrapping(true_eig_C=true_eig_C, bool_log_plot=bool_log_plot)

    def create_output_dir(self) -> DataSaver:

        if self.get_params().is_save_data():
            # path_to_files = os.path.join(os.path.abspath("results/figures/"),
            #                             datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S-%f"))
            # if not(os.path.exists(path_to_files)):
            #    os.makedirs(path_to_files)
            #    print("Results at %s" % path_to_files)

            data_saver = DataSaver()
        else:
            print("Results are not saved")
            data_saver = None

        return data_saver

    def plot_eigenvalues_A(self) -> None:
        model = self.get_params().get_model()
        case = self.get_params().get_case()
        m = model.get_dimension()
        if type(model) == MatrixModel:
            # ---------------------------- plot eigenvalues of A
            hdl = plt.figure()
            eigenvalues_A = model.get_eigenvalues_C(rho=None)
            plt.semilogy(np.arange(1, len(eigenvalues_A) + 1), eigenvalues_A, marker='o')
            plt.xlabel('Index')
            plt.ylabel('Eigenvalues')
            plt.ylim(10 ** -6, 10 ** 2)
            plt.title("Case " + str(case) + ": Eigenvalues of A")
            plt.grid()
            plt.xlim(1, m)

            self.get_datasaver().save_figure(hdl, "eigenvalues_A")

    def plot_eigenvalues(self, true_eig_C: np.ndarray, bool_log_plot: bool) -> None:

        model = self.get_params().get_model()
        lambda_eig = self.get_results().get_lambda_hat()
        lambda_i_eig = self.get_results().get_lambda_bootstrapping()
        bool_gradient = self.get_params().is_gradient_available()
        case = self.get_params().get_case()
        step_size = self.get_params().get_step_size()
        idx_gap = self.get_results().get_idx_gap()
        M_bootstrap = self.get_params().get_M_boot()
        idx_true_gap = self.get_results().get_idx_gap_true()

        fig, ax = plt.subplots()
        if min(lambda_eig) <= 0:
            bool_log_plot = False

        if isinstance(self.get_params().get_model(), AnalyticalModel):
            plt.semilogy(np.arange(1, len(true_eig_C) + 1), true_eig_C, marker='o', color='royalblue',
                         label='True eigenvalues')

        plt.plot(np.arange(1, len(lambda_eig) + 1), lambda_eig, marker="x", color="red", linestyle=":",
                 label='Estimated eigenvalues')

        if M_bootstrap > 1:
            ax.fill_between(x=np.arange(1, len(lambda_eig) + 1), y1=np.min(lambda_i_eig, axis=1),
                            y2=np.max(lambda_i_eig, axis=1), facecolor='red', alpha=0.3, label='Bootstrap')

        # lambda_i_eig
        if not bool_gradient:
            plt.semilogy(np.array([1, len(lambda_eig)]), step_size * np.ones(shape=2), color='black', linestyle='-',
                         label='Step size')

        ylim = plt.gca().get_ylim()
        if isinstance(self.get_params().get_model(), AnalyticalModel):
            plt.semilogy((idx_true_gap + 1.5) * np.ones(shape=(2, 1)), ylim, linestyle="-", color='royalblue',
                         label='True spectral gap')
        plt.semilogy((idx_gap + 1.5) * np.ones(shape=(2, 1)), ylim, linestyle="-.", color='cornflowerblue',
                     label='Spectral gap')

        plt.grid()
        if not bool_log_plot:
            ax.set_yscale('linear')
        if isinstance(self.get_params().get_model(), AnalyticalModel):
            if not bool_log_plot or np.min(true_eig_C) < np.finfo(float).eps:
                plt.ylim([np.min(lambda_i_eig) * 0.9, np.max(lambda_i_eig) * 1.1])
                ax.set_yscale('linear')

            plt.xticks(np.arange(1, np.size(true_eig_C, axis=0) + 1))

        if type(model) == MatrixModel:
            plt.ylim(10 ** -8, 10 ** 4)
            plt.xlim(1, 6)

        plt.ylabel("Eigenvalues")
        plt.xlabel("Index")
        plt.xticks(np.arange(1, len(lambda_eig) + 1))
        plt.legend()
        if type(model) == MatrixModel:
            plt.title("Case " + str(case) + ": Eigenvalues of C")
        else:
            plt.title("Eigenvalues of C")

        self.get_datasaver().save_figure(fig, "eigenvalues_C")

    def plot_true_error_eigenvalue(self, true_eig_C: np.ndarray) -> None:

        # plot error in eigenvalue
        if isinstance(self.get_params().get_model(), AnalyticalModel):
            fig = plt.figure()
            plt.plot(ActiveSubspaceCalculator.relative_error(true_value=true_eig_C,
                                                             approx_value=self.get_results().get_lambda_hat()), 'o:')
            plt.ylabel('Relative error of eigenvalues')

            self.get_datasaver().save_figure(fig, "true_error_eigenvalue")

    def plot_eigenvectors(self, w_vec: np.ndarray) -> None:

        lambda_eig = self.get_results().get_lambda_hat()
        m = self.get_params().get_model().get_dimension()

        fig = plt.figure()
        for i in range(0, m):
            plt.plot(w_vec[:, i], marker="o", linestyle=':', label="Eigenvalue: %.3e" % lambda_eig[i])

        plt.legend()
        plt.ylabel("Eigenvector components")
        plt.xlabel("Index")

        self.get_datasaver().save_figure(fig, "eigenvectors")

    def plot_eigenvalues_bootstrapping(self, true_eig_C: np.ndarray, bool_log_plot: bool) -> None:

        lambda_i_eig = self.get_results().get_lambda_bootstrapping()

        fig = plt.figure()
        plt.semilogy(lambda_i_eig, '.:')
        plt.title('Eigenvalues (Bootstrap)')
        if isinstance(self.get_params().get_model(), AnalyticalModel):
            if not bool_log_plot or np.min(true_eig_C) < np.finfo(float).eps:
                ax = plt.gca()
                ax.set_yscale('linear')
        plt.xlabel('Index')
        plt.xticks(np.arange(0, np.size(lambda_i_eig, axis=0)))

        self.get_datasaver().save_figure(fig, "eigenvalues_bootstrap")

    def plot_distance_subspaces(self) -> None:

        model = self.get_params().get_model()
        m = model.get_dimension()
        bool_gradient = self.get_params().is_gradient_available()
        step_size = self.get_params().get_step_size()
        case = self.get_params().get_case()

        true_distance = self.get_results().get_distance_subspace_true()
        distance_e = self.get_results().get_distance_bootstrapping()
        distance_subspace = self.get_results().get_distance_subspace()
        lambda_eig = self.get_results().get_lambda_hat()
        dist_as_vec = self.get_results().get_dist_as_vec()

        fig = plt.figure()

        if isinstance(self.get_params().get_model(), AnalyticalModel):
            plt.semilogy(np.arange(1, m), true_distance, 'o-', color='royalblue', label='True distance (3.49)')

        plt.semilogy(np.arange(1, m), distance_subspace, ':xr', label='Estimated distance')

        ax = plt.gca()
        ax.fill_between(x=np.arange(1, m), y1=np.min(distance_e, axis=1), y2=np.max(distance_e, axis=1),
                        facecolor='red', alpha=0.3, label='Bootstrap Interval')

        if isinstance(self.get_params().get_model(), AnalyticalModel):
            plt.semilogy(np.arange(1, m), dist_as_vec, '--x', color='gray', label='Upper bound distance (3.10)')

        if not bool_gradient:
            plt.semilogy(np.array([0, len(lambda_eig)]), step_size * np.ones(shape=2), color='black', linestyle='-',
                         label='Step size')

        plt.title('Distance between subspaces')
        plt.xlabel('Subspace Dimension')
        plt.ylabel('Subspace Error')
        plt.legend()
        if type(model) == MatrixModel:
            plt.ylim(10 ** -6, 10 ** 0.25)
            plt.xlim(1, 6)
        if type(model) == MatrixModel:
            plt.title("Case " + str(case) + ": Distance between subspaces")

        plt.grid()

        self.get_datasaver().save_figure(fig, "distance_subspaces")

    def plot_all_activity_scores(self, alpha_vec: np.ndarray, n_runs, n: int) -> None:

        activity_scores = self.get_results().get_activity_scores()
        key = self.get_params().get_model().get_key()
        m = self.get_params().get_model().get_dimension()

        h = plt.figure()
        activity_scores_reshaped = np.reshape(activity_scores, newshape=(int(activity_scores.size / len(key)), -1))
        for i in range(0, len(key)):
            plt.plot(activity_scores_reshaped[:, i], 'o:', label=key[i])

        plt.xlabel('Number of run')
        plt.ylabel('Activity score')
        plt.xticks(np.arange(0, n * n_runs))
        plt.legend()
        self.get_datasaver().save_figure(h, 'activity_scores')

        h = plt.figure()
        for i in range(0, n * n_runs):
            plt.plot(activity_scores_reshaped[i, :], 'o:',
                     label="run %d, alpha = %d" % (i, np.repeat(alpha_vec, n_runs)[i]))

        plt.xlabel('Parameter index')
        plt.ylabel('Activity scores')
        plt.xticks(np.arange(0, m))
        plt.legend()
        self.get_datasaver().save_figure(h, 'normalized_activity_scores')

    def plot_results_all_runs(self, alpha_vec: np.ndarray) -> None:

        result_gradients = self.get_results().get_error_lambda_hat()
        activity_scores = self.get_results().get_activity_scores()

        self.plot_error_in_eigenvalue(result_gradients)
        self.plot_error_in_C_matrix(alpha_vec)
        # plot_activity_scores(data_saver, activity_scores, true_activity_scores, size_subspace, m)
        [n, n_runs, _] = np.shape(activity_scores)
        self.plot_all_activity_scores(alpha_vec, n_runs, n)
        self.plot_true_activity_scores()
        self.get_datasaver().save_to_pickle(self.get_datasaver().get_path_to_files(), activity_scores,
                                            "activity_scores")
