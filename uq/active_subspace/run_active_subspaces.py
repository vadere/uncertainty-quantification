import time

from numpy.random import RandomState

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.calc.ActiveSubspaceResultGradient import ActiveSubspaceResultGradient
from uq.active_subspace.ioput.ActiveSubspaceFileWriter import ActiveSubspaceFileWriter
from uq.active_subspace.ioput.ActiveSubspacePlotterGradients import *
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa

bool_gradient = False  # is an analytical gradient available? -> True
bool_averaged = True
no_runs_averaged = 3  # average over multiple runs?

step_size = 10 ** -2  # 10**-3    # for (finite-difference) approximation of gradients
factor_N = None  # 10             # for the local linear model (instead of finite difference approximation)
alpha = 1  # 10                   # oversampling factor
k = 3 + 1  # desired dimension of subspace +1
M_boot = 10  # for bootstrapping

case = None

run_local = False

bool_save_data = True
bool_print = True
bool_plot = True

# parameter limits
x_lower = np.array([0.5, 0.0, 160, 1.0, 1.6, 0.1])  # lower bounds for parameters
x_lower = np.array([0.5, 0.1, 160, 1.0, 1.6, 0.1])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 200, 5.0, 3.0, 1.0])  # upper bounds for parameters

scenario_name = "Liddle_bhm_v3_free_seed.scenario"  # Liddle_bhm_v3_fixed_seed

# bool_vec = np.array([False, True, True, False])
density_type = "uniform"  # input parameter density

# parameters
key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==-1].spawnNumber",
       "sources.[id==-1].distributionParameters", "bottleneck_width",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters
key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                "obstacle repulsion"]

test_input = np.array([[1.34], [0.26], [180], [1], [1.6], [0.5]])  # legal test input

qoi = "mean_density.txt"  # quantity of interest

# configure setup
test_model, m, path2tutorial = configure_vadere_sa(run_local, scenario_name, key, qoi)


general_seed = 67923

# ------------------------------------------------------------------ Run and evaluate

if __name__ == "__main__":  # main required by Windows to run in parallel

    # for i_runs in range(0,10):

    n = 1
    n_runs = 1

    test_input = x_lower

    # %% allocation
    count = 0
    mean_error_eig_gradients = np.zeros(n)
    mean_error_eig_finite_diff = np.zeros(n)
    mean_error_eig_local = np.zeros(n)

    alpha_vec = [alpha]  # np.linspace(2, 10, n)
    seeds_vec = np.round(RandomState(general_seed).rand(n_runs) * (2 ** 31)).astype(np.int)

    result_finite_diff = -1 * np.ones(shape=(n, n_runs, m))
    result_gradients = -1 * np.ones(shape=(n, n_runs, m))
    error_c_gradients = -1 * np.ones(shape=(n, n_runs))
    activity_scores = -1 * np.ones(shape=(n, n_runs, m))
    true_activity_scores = -1 * np.ones(shape=(n, n_runs, m))
    size_subspace = np.zeros(shape=(n, n_runs))

    step_size_relative = step_size * (x_upper - x_lower)

    # todo besser gleich oben zuordnen?
    params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                             bool_gradient=bool_gradient, M_boot=M_boot,
                                             step_size_relative=step_size_relative, step_size=step_size, case=case,
                                             seed=None, bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                             bool_save_data=bool_save_data, bool_print=bool_print, bool_plot=bool_plot,
                                             path2results=path2tutorial)

    # %% perform calculations

    for idx in range(0, n):

        # step_size = step_size_vec[idx]
        alpha = alpha_vec[idx]

        tmp_gradients = 0
        tmp_finite_diff = 0
        tmp_local_linear = 0

        for irun in range(0, n_runs):  # average over several runs
            start = time.time()
            seed = seeds_vec[irun]  # use the same seeds for all configs

            params.set_seed(seed)
            params.set_model(model=test_model)
            params.set_alpha(alpha=alpha)

            as_calculator = ActiveSubspaceCalculatorGradient(param=params)
            as_calculator.identify_active_subspace()
            results = as_calculator.get_result()

            result_gradients[idx, irun, :] = results.max_rel_error_eig()
            error_c_gradients[idx, irun] = results.get_error_c_hat()
            activity_scores[idx, irun, :] = results.get_activity_scores()
            true_activity_scores[idx, irun, :] = results.get_activity_scores_true()
            size_subspace[idx, irun] = results.get_size_subspace()
            path_to_files = results.get_path2results()
            n_samples = results.get_n_samples()
            lambda_eig = results.get_lambda_hat()
            w_active = results.get_W_active()

            tmp_gradients = tmp_gradients + result_gradients[idx, irun]

            computation_time = ((time.time() - start) / 60)

            writer = ActiveSubspaceFileWriter(param=params, result=results)
            writer.write_result()

            # plt.semilogx(step_size, result_gradients, '.', color='royalblue')

            # finite differences
            # result_finite_diff[idx, irun] = active_subspace_with_gradients(test_model, density_type, x_lower, x_upper,
            #                                                  test_input, alpha, k, False, M_boot,
            #                                                 step_size, case, seed)
            # tmp_finite_diff = tmp_finite_diff + result_finite_diff[idx, irun]

            # local linear model
            # llm_params = ActiveSubspaceParametersLLM(model=test_model, density_type=density_type, x_lower=x_lower,
            #                                         x_upper=x_upper, alpha=alpha, k=k, factor_N=factor_N,
            #                                         bool_gradient=bool_gradient, step_size=step_size, case=case,
            #                                         seed=seed)
            # llm_calc = ActiveSubspaceCalculatorLLM(params=llm_params)
            # result_local = llm_calc.local_linear_model(density_type=density_type)
            # tmp_local_linear = tmp_local_linear + result_local # todo adapt to results object

        # step_size_vec[count] = step_size
        count = count + 1

    print("Finished calculations - Start of evaluation of results")

    # Accumulation of results
    all_results = ActiveSubspaceResultGradient()
    all_results.set_error_lambda_hat(result_gradients)
    all_results.set_error_c_hat(error_c_gradients)
    all_results.set_activity_scores(activity_scores)
    all_results.set_activity_scores_true(true_activity_scores)
    all_results.set_size_subspace(size_subspace)

    # Plot results
    plotter = ActiveSubspacePlotter(datasaver=DataSaver(), params=params, results=all_results)
    plotter.plot_results_all_runs(alpha_vec=alpha_vec)
