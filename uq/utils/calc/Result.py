class Result:

    def __init__(self, computation_time: float = None):
        self.__computation_time = computation_time

    def get_computation_time(self) -> float:
        return self.__computation_time

    def set_computation_time(self, computation_time: float) -> None:
        self.__computation_time = computation_time

