import copy
import warnings

from numpy.random import RandomState

from uq.utils.calc.Parameter import Parameter
from uq.utils.calc.Result import Result


class Calculator:

    def __init__(self, param: Parameter = None, result: Result = None):
        self.__param = copy.deepcopy(param)
        self.__result = result

        if param is None:  # for tests
            seed = 0
            warnings.warn('No seed was defined by the user!')
        else:
            seed = param.get_seed()
        self.__random_state = RandomState(seed=seed)

    # GETTERS

    def get_result(self) -> Result:
        return self.__result

    def get_param(self) -> Parameter:
        return self.__param

    def get_random_state(self) -> RandomState:
        return self.__random_state
