import numpy as np

from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Model import Model
from uq.utils.prior.Prior import Prior


class Parameter:

    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, data_saver: DataSaver = None):
        self.__model = model  # computer model under study
        self.__prior = prior  # distribution of uncertain input parameters
        self.__seed = seed
        self.__bool_averaged = bool_averaged  # averaging of model evaluations
        self.__path2results = path2results
        self.__data_saver = data_saver

        if not bool_averaged:
            self.__no_runs_averaged = 1  # number of repeated model evaluations that are averaged
        else:
            self.__no_runs_averaged = no_runs_averaged

    # GETTER

    def get_data_saver(self) -> DataSaver:
        return self.__data_saver

    def get_path2results(self) -> str:
        return self.__path2results

    def get_model(self) -> "Model":
        return self.__model

    def get_prior(self) -> "Prior":
        return self.__prior

    def get_x_lower(self) -> np.ndarray:
        return self.get_prior().get_lower()

    def get_x_upper(self) -> np.ndarray:
        return self.get_prior().get_upper()

    def get_seed(self) -> int:
        return self.__seed

    def get_no_runs_averaged(self) -> int:
        return self.__no_runs_averaged

    def is_averaged(self):
        return self.__bool_averaged

    # SETTER

    def set_data_saver(self, data_saver: DataSaver) -> None:
        self.__data_saver = data_saver

    def set_no_runs_averaged(self, no_runs_averaged: int):
        self.__no_runs_averaged = no_runs_averaged

    def set_seed(self, seed: int) -> None:
        self.__seed = seed

    def set_model(self, model: "Model") -> None:
        self.__model = model

    def set_path2results(self, path2results: str) -> None:
        self.__path2results = path2results
