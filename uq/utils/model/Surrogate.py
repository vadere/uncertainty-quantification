from typing import List

import numpy as np

from uq.utils.model.Model import Model


class Surrogate(Model):

    def __init__(self, model: Model, model_input: np.ndarray, model_output: np.ndarray, dim: int = 1,
                 key: List[str] = None):
        if key is None:
            # inputs of Surrogate are identical to inputs of original model (except for ApproxModel from Active Subsp.)
            key = model.get_key()
        super().__init__(key=key, qoi=model.get_qoi(), dim=dim)

        self.__original_model = model
        self.__model_input = model_input  # multi-dimensional
        self.__model_output = model_output
        self.__model_response = None  # only for data misfit surrogate (model evaluations)

    # GETTERS

    def get_original_model(self) -> Model:
        return self.__original_model

    def get_model_input(self) -> np.ndarray:
        return self.__model_input

    def get_model_output(self) -> np.ndarray:
        return self.__model_output

    def get_model_response(self) -> np.ndarray:
        return self.__model_response

    # SETTERS

    def set_model_input(self, model_input: np.ndarray) -> None:
        self.__model_input = model_input

    def set_model_output(self, model_output: np.ndarray) -> None:
        self.__model_output = model_output

    def set_model_response(self, model_response: np.ndarray) -> None:
        self.__model_response = model_response

    # OTHERS

    #def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) \
    #        -> Union[float, np.ndarray]:
    #    pass
