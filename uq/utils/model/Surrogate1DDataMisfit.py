import logging
from typing import Tuple

import numpy as np
from numpy.random import RandomState

from uq.utils.model.Model import Model
from uq.utils.model.Surrogate1D import Surrogate1D


class Surrogate1DDataMisfit(Surrogate1D):
    """ Surrogate model for data misfit evaluations (instead of Vadere) - always 1D due to norm """

    def __init__(self, model: Model, limits: np.ndarray = None, nr_points: int = None, model_input=None,
                 model_output=None, n_keys: int = 1, sampling_config: "SamplingStrategy" = None,
                 nr_points_averaged_surrogate: int = 1):
        self.__sampling_config = sampling_config
        super().__init__(model=model, model_input=model_input, model_output=model_output,
                         n_keys=n_keys, nr_points_averaged=nr_points_averaged_surrogate)

    def get_sampling_strategy(self) -> "SamplingStrategy":
        return self.__sampling_config

    def generate_data(self, model: Model, limits: np.ndarray, nr_points: int) -> Tuple[np.ndarray, np.ndarray]:
        logger = logging.getLogger("Surrogate1DDataMisfit.generate_data")
        points_vec, model_output = super().generate_data(model, limits, nr_points)
        data_misfit_output = self.get_sampling_strategy().evaluate_data_misfit(model_output)
        logger.info("Generated data for surrogate")
        return points_vec, data_misfit_output

    def eval_model(self, parameter_value: float, random_state: RandomState = None):
        return super().eval_model(parameter_value=parameter_value, random_state=random_state)
