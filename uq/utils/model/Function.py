from typing import Union, List

import numpy as np


class Function:
    @classmethod
    def rational(cls, x: Union[float, List[float]], p: Union[float, List[float]],
                 q: Union[float, List[float]]) -> float:
        """
        The general rational function description.
        p is a list with the polynomial coefficients in the numerator
        q is a list with the polynomial coefficients (except the first one)
        in the denominator
        The zeroth order coefficient of the denominator polynomial is fixed at 1.
        Numpy stores coefficients in [x**2 + x + 1] order, so the fixed
        zeroth order denominator coefficient must comes last. (Edited.)
        """
        return np.polyval(p, x) / np.polyval(q, x)

    def rational3_3(self, x: float, p0: float, p1: float, p2: float, q1: float, q2: float) -> float:
        return Function.rational(x=x, p=[p0, p1, p2], q=[q1, q2])

    def rational1(self, x: float, p0: float, q1: float, q2: float) -> float:
        return Function.rational(x=x, p=[p0], q=[q1, q2])

    def polynomial(self, x: float, p: Union[float, List[float]]):
        return np.polyval(p, x)

    def polynomial0(self, x: float, p0: float):
        return Function.polynomial(self, x=x, p=p0)

    def polynomial1(self, x: float, p0: float, p1: float):
        return Function.polynomial(self, x=x, p=[p0, p1])

    def polynomial2(self, x: float, p0: float, p1: float, p2: float):
        return Function.polynomial(self, x=x, p=[p0, p1, p2])
