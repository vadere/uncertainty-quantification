from typing import Union

import numpy as np
from numpy.random import RandomState
from scipy.interpolate import interp1d
from scipy.stats import truncnorm

import matplotlib.pyplot as plt

from uq.utils.model.StochasticModel import StochasticModel


class SFMEmulatorBottleneckFlow(StochasticModel):

    def __init__(self, noise_level: float = 0.01):
        super().__init__(key="free-flow speed", qoi="flow", map_of_results=None, bool_stochastic_model=True,
                         bool_analytic_solution=False, qoi_dim=5)

        self.__fit = self.calc_fit_to_helbing_data()
        self.__widths = np.array([0.8, 0.9, 1.0, 1.1, 1.2])
        self.__noise_level = noise_level

    def get_fit(self):
        return self.__fit

    def get_noise_level(self) -> float:
        return self.__noise_level

    def get_widths(self) -> np.ndarray:
        return self.__widths

    def calc_fit_to_helbing_data(self):
        x = np.array([0.1, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0])
        y = np.array([0.96, 1.1, 1.22, 1.27, 1.3, 1.27, 1.1, 0.82, 0.6, 0.45, 0.35, 0.3, 0.22, 0.2])

        # my_fit = interp1d(x, y, kind='cubic')
        my_fit = interp1d(x, y)

        return my_fit

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) -> Union[
        float, np.ndarray]:
        n_evals = np.size(parameter_value)

        # generate noise
        my_mean = 0.0
        my_std = self.get_noise_level()
        my_clip_a = -0.5
        my_clip_b = 0.5
        a, b = (my_clip_a - my_mean) / my_std, (my_clip_b - my_mean) / my_std
        noise_unscaled = truncnorm.rvs(a, b, size=n_evals * self.get_qoi_dim(), random_state=random_state)
        noise_unshaped = noise_unscaled * my_std + my_mean
        noise = np.reshape(noise_unshaped, (n_evals, self.get_qoi_dim()))

        # noise = np.reshape(r, (np.size(y, 0), np.size(y, 1)))
        # noise = random_state.randn(np.size(y, 0), np.size(y, 1))

        y = np.outer(self.get_fit()(parameter_value), self.get_widths()) + noise

        return y

    def eval_model_averaged(self, parameter_value: Union[float, np.ndarray], nr_runs_averaged: int = 1,
                            random_state: RandomState = None) -> np.ndarray:
        n_evals = np.size(parameter_value)  # only one uncertain parameter here
        y_tmp = np.zeros((nr_runs_averaged, n_evals, self.get_qoi_dim()))
        for i in range(0, nr_runs_averaged):
            y_tmp[i, :, :] = self.eval_model(parameter_value, random_state)

        y_av = np.mean(y_tmp, axis=0)

        return y_av, parameter_value, y_tmp
