from typing import Union

import numpy as np
from numpy.random import RandomState

from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.model.Model import Model


class MultiplicativeModel(AnalyticalModel):
    def __init__(self):
        key = ["x1", "x2"]
        qoi = "f(x,y)=x*y"
        super().__init__(key=key, qoi=qoi, map_of_results=None, bool_analytic_solution=True,
                         bool_stochastic_model=False, dim=2)

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) \
            -> Union[float, np.ndarray]:
        if np.ndim(parameter_value) == 1:
            result = parameter_value[0] * parameter_value[1]
        elif np.ndim(parameter_value) > 1:
            if np.size(parameter_value, axis=0) == self.get_dimension():
                parameter_value.transpose()
            result = parameter_value[0, :] * parameter_value[1, :]
        else:
            raise ValueError('Input has to be numpy array!')

        return result
