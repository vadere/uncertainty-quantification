import copy
import json
import logging
import os
import warnings
from builtins import any as b_any
from datetime import datetime
from typing import Union, List, Tuple

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from numpy.random.mtrand import RandomState
from scipy import stats as stats
from statsmodels import api as sm
from suqc import VadereConsoleWrapper, DictVariation, PostScenarioChangesBase
from suqc.utils.general import get_current_suqc_state

from uq.utils.datatype import box_to_n_dim, get_dimension, box1d, box
from uq.utils.find_software_versions import get_current_vadere_version
from uq.utils.model.StochasticModel import StochasticModel


class VadereModel(StochasticModel):
    INTEGER_PARAMETERS = ["stepCircleResolution", "fixedSeed", "numberOfCircles", "spawnNumber"]
    LIST_PARAMETERS = ["distributionParameters"]
    GROUP_PARAMETERS = ["groupSizeDistribution"]
    TOPOGRAPHY_SYMMETRY_PARAMETERS = ["bottleneck_width"]
    SPAWN_NR_SEVERAL_SOURCES = "sources.[*].spawnNumber"
    DISTRIBUTION_SOURCES = "sources.[*].distributionParameters"

    def __init__(self, run_local: bool, path2scenario: str, path2model: str, key: Union[str, list],
                 qoi: Union[str, list], n_jobs: int, log_lvl: str, qoi_dim: int = 1, data_saver: "DataSaver" = None):

        self.__path2scenario = path2scenario
        scenario_file_hash = self.hash_scenario_file()
        map_of_results = dict({"scenario_file_hash": scenario_file_hash})
        super().__init__(key=key, qoi=qoi, map_of_results=map_of_results, bool_stochastic_model=True,
                         bool_analytic_solution=False, qoi_dim=qoi_dim, data_saver=data_saver)

        self.__run_local = run_local
        self.__path2model = path2model
        self.__n_jobs = n_jobs
        self.__log_lvl = log_lvl

        self.check_scenario_file()

    # GETTERS

    def get_version_str(self) -> str:
        suqc_state = get_current_suqc_state()
        suqc_str = ""
        if "suqc_version" in suqc_state:
            suqc_str = suqc_str + 'SUQ Controller Version (suqc): \t\t {} \n'.format(suqc_state["suqc_version"])
        if "git_hash" in suqc_state:
            suqc_str = suqc_str + 'SUQ Controller Commit Hash (suqc): \t\t {} \n'.format(suqc_state["git_hash"])

        # file.write('SUQ Controller Commit Hash (suqc): \t\t {} \n'.format(get_current_suqc_state()["git_hash"]))
        vadere_version_str = 'Vadere Software (version): \t\t\t {}'.format(
            get_current_vadere_version(self.get_path_2_model()))
        vadere_commit_str = 'Vadere Software (commit hash): \t\t {}'.format(
            get_current_vadere_version(self.get_path_2_model()))
        vadere_scenario_str = 'Vadere Scenario: \t\t\t\t\t {} \n\n'.format(self.get_path_2_scenario())

        return suqc_str + vadere_version_str + vadere_commit_str + vadere_scenario_str

    def get_run_local(self) -> bool:
        return self.__run_local

    def get_path_2_scenario(self) -> str:
        return self.__path2scenario

    def get_path_2_model(self) -> str:
        return self.__path2model

    def get_loglvl(self) -> str:
        return self.__log_lvl

    def get_n_jobs(self) -> int:
        return self.__n_jobs

    # SETTERS

    def set_n_jobs(self, in_n_jobs: int) -> None:
        self.__n_jobs = in_n_jobs

    def set_n_evals(self, n: int) -> None:
        self.__n_evals = n

    def set_run_local(self, run_local: bool) -> None:
        self.__run_local = run_local

    # OTHERS

    def check_scenario_file(self) -> None:  # todo checks depend on Vadere version! - structure of .scenario file
        f = open(self.get_path_2_scenario(), "r")
        json_scenario = json.load(f)  # deserialize
        f.close()

        f = open(self.get_path_2_scenario(), "r")
        scenario_str = f.read()  # just read as string
        f.close()

        # Assure reproducibility
        if not json_scenario["scenario"]["attributesSimulation"]["useFixedSeed"]:
            warnings.warn(
                "useFixedSeed has to be set to \"true\" in order to assert that the externally supplied seed is used")

        self.set_bool_fixed_seed(json_scenario["scenario"]["attributesSimulation"]["useFixedSeed"])

        # Make sure that qoi is set in the json file
        list_files = json_scenario["processWriters"]["files"]
        bool_found_qoi = False
        for idx, file in enumerate(list_files):
            if file["filename"] == self.get_qoi():
                bool_found_qoi = True
                break
        if not bool_found_qoi:
            raise UserWarning("User-defined qoi (%s) is not defined in scenario file (%s)" % (
                self.get_qoi(), self.get_path_2_scenario()))

        # Make sure that keys exist
        for idx, key in enumerate(self.get_key()):

            key_name = key.split(".")[-1]
            if scenario_str.find(key_name) == -1:
                raise UserWarning("User-defined parameter (%s) is not defined in scenario file (%s)" %
                                  (key, self.get_path_2_scenario()))

        # todo make sure that the scenario elements in keys exists - e.g. sources.[id == 3].spawnNumber

    def hash_scenario_file(self) -> int:
        with open(self.get_path_2_scenario(), 'rb') as file:
            return hash(file)

    def add_seed(self, n: int, value_input: np.ndarray, random_state: RandomState = None):
        key = self.get_key()
        if random_state is None:
            warnings.warn("VadereModel.add_seed(): random_state is empty!")
            random_state = RandomState()

        # Generate seeds
        seeds = np.array([random_state.rand(n) * (VadereModel.HIGHEST_SEED - VadereModel.LOWEST_SEED)
                          + VadereModel.LOWEST_SEED]).astype(int)
        if type(key) == str or type(key) == list:
            if type(value_input) == np.ndarray:
                ext_value_input = np.concatenate((box_to_n_dim(value_input, 2), seeds), axis=0)
                # Assure that self.key is not changed (key.append changes self.key)
                new_key = copy.deepcopy(key)
                new_key.append("fixedSeed")
                # new_key = key + list(["fixedSeed"])
            else:
                raise Exception("Unexpected type of value_input: %s" % type(value_input))
        else:
            raise Exception("Unexpected type of key: %s" % type(key))

        return ext_value_input, new_key, seeds

    def create_dict_list(self, n: int, value_input: np.ndarray) -> list:
        dict_list = self.create_dict_list_with_key(key=self.get_key(), n=n, value_input=value_input)

        return dict_list

    def create_dict_list_with_key(self, key: List[str], n: int, value_input: np.ndarray) -> list:

        dict_list = []
        for i in range(0, n):  # iterate over samples
            param_dict = dict()
            for j in range(0, get_dimension(key)):  # iterate over input parameters
                value = None

                bool_set_key = True

                # if key is nested key, extract just the last bit to find the type
                tmp_parts = str.split(key[j], '.')
                if len(tmp_parts) > 1:
                    key_name = tmp_parts[-1]  # last entry
                else:
                    key_name = key[j]

                if key[j] == VadereModel.SPAWN_NR_SEVERAL_SOURCES:
                    for s in range(0, 5):  # todo why fixed to 5? (probably specifically for bottleneck scenario)
                        my_key = "sources.[id==%d].spawnNumber" % (s + 1)
                        my_value = int(np.round(value_input[j, i]))
                        param_dict[my_key] = my_value
                    bool_set_key = False
                else:
                    if b_any(key_name in x for x in VadereModel.INTEGER_PARAMETERS):
                        value = int(np.round(value_input[j, i]))
                    elif b_any(key_name in x for x in VadereModel.LIST_PARAMETERS):
                        if "sources.[*]" in key[j]:  # for all sources
                            for s in range(0, 5):
                                my_key = "sources.[id==%d].%s" % (s + 1, key_name)
                                my_value = [value_input[j, i]]
                                param_dict[my_key] = my_value
                            bool_set_key = False

                        else:
                            value = [value_input[j, i]]  # convert to list
                    elif b_any(key_name in x for x in VadereModel.GROUP_PARAMETERS):
                        value = [1 - value_input[j, i],
                                 1 - value_input[j, i]]  # convert to list with percentage of singles and couples
                    elif b_any(key_name in x for x in VadereModel.TOPOGRAPHY_SYMMETRY_PARAMETERS):
                        # todo: special file or sth for this config that is only for one specific scenario file
                        # vertical center: 7.5
                        v_center_line = 7.5
                        obstacle_height = 6.0
                        my_key = "obstacles.[id==1].y"  # lower obstacle (Liddle_bhm_v3)
                        my_value = -value_input[j, i] / 2 + v_center_line - obstacle_height
                        param_dict[my_key] = my_value

                        my_key = "obstacles.[id==2].y"  # upper obstacle
                        my_value = value_input[j, i] / 2 + v_center_line
                        param_dict[my_key] = my_value

                        distance_to_obstacle = 0.3  # distance between intermediate target and obstacle
                        height_of_obstacle = 10

                        # for intermediate target
                        # my_key = "targets.[id==1].height"
                        # my_value = value_input[j, i]
                        # target_1_height = my_value - distance_to_obstacle * 2
                        # param_dict[my_key] = target_1_height
                        #
                        # my_key = "targets.[id==1].y"  # intermediate target
                        # my_value = v_center_line - target_1_height / 2
                        # param_dict[my_key] = my_value

                        my_key = "targets.[id==1].height"
                        target_2_height = value_input[j, i]
                        param_dict[my_key] = target_2_height

                        my_key = "targets.[id==1].y"  # target
                        my_value = v_center_line - target_2_height / 2
                        param_dict[my_key] = my_value

                        bool_set_key = False

                    else:
                        value = value_input[j, i]

                if bool_set_key:
                    param_dict[key[j]] = value

            dict_list.append(param_dict)
        return dict_list

    def evaluate_deviation_from_mean(self, results_reshaped: np.ndarray, result_averaged: np.ndarray,
                                     value_input: Union[float, np.ndarray], nr_points_averaged: int = 1) -> np.ndarray:
        dev = results_reshaped - np.tile(np.expand_dims(result_averaged, axis=1), (1, nr_points_averaged))
        if np.ndim(value_input) == 1:
            plt.figure()
            value_input_formatted = np.expand_dims(value_input, axis=1)
            plt.plot(np.tile(value_input_formatted, reps=(1, nr_points_averaged)), dev, marker='.')

            # n_param_values = np.size(dev, axis=0) + 1
            # ax1 = plt.subplot(n_param_values, 1, 1)
            for i in range(0, np.size(dev, axis=0)):
                h3 = plt.figure()
                # plt.subplot(n_param_values, 1, i + 1).\
                plt.hist(dev[i, :], stacked=False, label=str(value_input[i]), density=True)
                # plt.xlim([-0.1, 0.1])
                plt.title(str(value_input[i]))
                if self.get_data_saver() is not None:
                    self.get_data_saver().save_figure(h3, ("deviations_over_parameter%d" % i))

        # else:
        # value_input_formatted = value_input

        h1 = plt.figure()
        plt.hist(np.ravel(dev), 100, density='true')
        mean_est = np.mean(np.ravel(dev))
        std_est = np.std(np.ravel(dev))
        x = np.linspace(np.min(np.ravel(dev)), np.max(np.ravel(dev)))
        random_normal = stats.norm(mean_est, std_est).pdf(x)
        plt.plot(x, random_normal, '--r', label='Fitted normal distribution')
        plt.legend()
        if not self.get_data_saver() is None:
            self.get_data_saver().save_figure(h1, "histogram_deviations_vadere")
        # plt.savefig('histogram_deviations_vadere.png')

        sm.qqplot(np.ravel(dev), line='s')
        h2 = plt.gcf()
        # plt.savefig('qqplot_deviations_vadere.png')
        if not self.get_data_saver() is None:
            self.get_data_saver().save_figure(h2, "qqplot_deviations_vadere")

        plt.close(h2)

        vadere_logger = logging.getLogger("vaderemodel.evaluate_deviation_from_mean")
        vadere_logger.info("Vadere evaluations: Deviations from average")
        vadere_logger.info("Mean: %s, Std: %s" % (np.array2string(mean_est), np.array2string(std_est)))

        # skewtest needs at least 8 samples
        if len(np.ravel(dev)) >= 20:
            alpha = 0.01
            k2, p = stats.normaltest(np.ravel(dev))
            vadere_logger.info("p = {:g}".format(p))
            if p < alpha:  # null hypothesis: x comes from a normal distribution
                vadere_logger.info("The null hypothesis can be rejected")
            else:
                vadere_logger.info("The null hypothesis cannot be rejected")

        return dev

    def eval_model_multiple_times(self, parameter_value: Union[float, np.ndarray], no_runs: int = 1,
                                  random_state: RandomState = None) -> Tuple[np.ndarray, np.ndarray]:
        if np.mod(len(box1d(parameter_value)), self.get_dimension()) > 0:
            raise Warning("Shapes of value_input and size of key not compatible")
        if self.get_dimension() > 1 and np.size(parameter_value, axis=0) != self.get_dimension():
            raise Warning(
                'Shape of parameter_value does not match dimension (expected: [%d x N])' % self.get_dimension())

        if self.get_dimension() == 1 and no_runs == 1:
            number_of_values = np.size(box1d(parameter_value), axis=0)
        else:
            # todo: check if this works for multi-dim input
            number_of_values = np.size(box(parameter_value), axis=1)
            # if number_of_values == self.get_dimension() and np.size(box1d(value_input), axis=0):
            #     raise Warning("Dimension of value_input does not fit the expectations. Might need to be transposed")

        loglvl = self.get_loglvl()

        if no_runs > 1:
            value_input_duplicated = np.ravel(np.transpose(np.tile(parameter_value, reps=(no_runs, 1))))
        else:
            value_input_duplicated = parameter_value

        if type(value_input_duplicated) == float or type(value_input_duplicated) == int or type(
                value_input_duplicated) == np.float64:
            value_input_duplicated = np.ones(shape=(1, 1)) * value_input_duplicated
        elif type(value_input_duplicated) == np.ndarray:
            if np.ndim(value_input_duplicated) == 1:
                value_input_duplicated = np.expand_dims(value_input_duplicated, axis=0)
                value_input_duplicated = np.transpose(
                    np.reshape(value_input_duplicated, newshape=(-1, self.get_dimension())))
            elif no_runs > 1:
                raise Exception("More-dimensional version (with multiple runs) not yet implemented")

        # Assure reproducibility for stochastic models
        if self.is_stochastic_model() and random_state is not None:
            ext_value_input, new_key, seeds = self.add_seed(n=no_runs * number_of_values,
                                                            value_input=value_input_duplicated,
                                                            random_state=random_state)
        else:
            new_key = self.get_key()
            ext_value_input = value_input_duplicated

        dict_list = self.create_dict_list_with_key(key=new_key, n=no_runs * number_of_values,
                                                   value_input=ext_value_input)

        # Make sure that the first duplicated value is the first parameter set and the last is the last
        if self.get_dimension() > 1:  # more than one parameter
            if np.ndim(parameter_value) == 1:  # just one parameter set
                assert (np.asarray(list(dict_list[0].values()))[0:-1] == parameter_value).all()
                assert (np.asarray(list(dict_list[-1].values()))[0:-1] == parameter_value).all()
            elif np.ndim(parameter_value) > 1:  # multiple parameter sets
                # todo might not work if dict_list contains a list of multiple parameters
                if 'fixedSeed' in dict_list[0].keys():
                    # avoid last value
                    last_index = -1
                else:
                    last_index = len(dict_list[0].values())
                first_values = list(dict_list[0].values())[0:last_index]
                first_values_formatted = np.array([i if isinstance(i, list) else [i] for i in first_values]).ravel()
                last_values = list(dict_list[-1].values())[0:last_index]
                last_values_formatted = np.array([i if isinstance(i, list) else [i] for i in last_values]).ravel()

                assert (abs(first_values_formatted - parameter_value[:, 0]) <= 0.5).all() # some values are rounded
                assert (abs(last_values_formatted - parameter_value[:, -1]) <= 0.5).all()

        # dict_list = self.create_dict_list(n=no_runs, value_input=value_input_duplicated)  #without seed
        result = self.single_eval_model(value_input=dict_list, loglvl_in=loglvl,
                                        n=no_runs * number_of_values)  # eval model at list of dicts

        data_saver = self.get_data_saver()
        if data_saver is not None:
            self.get_data_saver().write_model_eval_to_file(key=new_key, qoi=self.get_qoi(), parameters=dict_list,
                                                           results=result)

        return result, ext_value_input

    # Evaluate an array of sample points and average for each point
    # the functionality of several runs in the suq controller is not used currently to assure reproducability
    # (providing the same seeds to the SUQ controller)
    def eval_model_averaged(self, parameter_value: Union[float, np.ndarray], nr_runs_averaged: int = 1,
                            random_state: RandomState = None, bool_plot: bool = False):

        result, value_input_duplicated = self.eval_model_multiple_times(parameter_value=parameter_value,
                                                                        no_runs=nr_runs_averaged,
                                                                        random_state=random_state)

        # average results
        if nr_runs_averaged > 1:

            if np.ndim(result) == 1:

                results_reshaped = np.reshape(np.array(result), (-1, nr_runs_averaged))
                result_averaged = np.mean(results_reshaped, axis=1)

                # evaluate deviation from mean
                if bool_plot:
                    self.evaluate_deviation_from_mean(results_reshaped=results_reshaped,
                                                      result_averaged=result_averaged,
                                                      value_input=parameter_value, nr_points_averaged=nr_runs_averaged)
            elif np.ndim(result) == 2:
                # todo: test for reshaping
                results_reshaped = np.reshape(np.array(np.transpose(result)),
                                              (-1, self.get_qoi_dim(), nr_runs_averaged))
                # group by no_runs_averaged (runs with the same parameter)
                results_reshaped = np.reshape(result, (-1, nr_runs_averaged, self.get_qoi_dim()))
                result_averaged = np.mean(results_reshaped, axis=1)

            else:
                raise UserWarning("Averaging not defined for higher dimension than 2")

        else:
            if type(result) is not dict and type(result) is not pd.DataFrame:
                result_averaged = np.array(result)
            else:
                result_averaged = result

        return result_averaged, value_input_duplicated, result

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) \
            -> Union[float, np.ndarray]:
        # Evaluate an array of sample points
        # model_eval, all_input, all_output = self.eval_model_averaged(value_input, no_points_averaged=1)

        # instead of overloading method
        model_eval, all_input, all_output = self.eval_model_averaged(parameter_value, nr_runs_averaged=1,
                                                                     random_state=random_state)
        return model_eval

    def single_eval_model(self, value_input: Union[float, np.ndarray, list], loglvl_in: str, n: int = 1,
                          bool_fixed_seed: bool = False, random_state: RandomState = None) -> Union[float, np.ndarray]:
        logger = logging.getLogger("VadereModel.single_eval_model")
        value = box1d(value_input)

        vadere_model = VadereConsoleWrapper(model_path=self.get_path_2_model(), loglvl=loglvl_in,
                                            jvm_flags=["-enableassertions"])

        if type(value) == np.ndarray:
            new_key = self.get_key()

            # Add seed to run
            if not bool_fixed_seed:
                value, new_key, seeds = self.add_seed(n=n, value_input=value, random_state=random_state)

            value = self.create_dict_list_with_key(key=new_key, n=n, value_input=value)
        else:
            if not (type(value) == list):
                raise Exception("Type ist not allowed here %s " % type(value))

        output_folder = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

        output_path = os.path.join(os.path.abspath(".."), "suqc_output")

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        setup_scenario = DictVariation(scenario_path=self.get_path_2_scenario(),
                                       parameter_dict_list=value,
                                       qoi=self.get_qoi(),
                                       scenario_runs=1,
                                       model=vadere_model,
                                       output_path=output_path,
                                       output_folder=output_folder,
                                       remove_output=True,
                                       post_changes=PostScenarioChangesBase(apply_default=True))

        if self.get_run_local():
            par_var, data = setup_scenario.run(self.get_n_jobs())
        else:
            par_var, data = setup_scenario.remote(self.get_n_jobs())

        self.add_n_evals(len(value))

        if data is None:
            logger.exception("VadereModel.single_eval_model: Resulting data of Simulation run is None")
            raise ValueError('Resulting data of Simulation run is None')

        if self.get_qoi() in ["evac_time.txt", "mean_density.txt", "waitingtime.txt", "flow.txt",
                              "cTimeStep.fundamentalDiagram", "max_density.txt", "egress.txt"]:

            if self.get_qoi() == "evac_time.txt":
                identifier = "evacuationTime"
            elif self.get_qoi() == "mean_density.txt":
                identifier = "mean_area_density_voronoi_processor"
            elif self.get_qoi() == "max_density.txt":
                identifier = "max_area_density_voronoi_processor"
            elif self.get_qoi() == "waitingtime.txt":
                identifier = "waitingTimeStart"
            elif self.get_qoi() == "flow.txt":
                identifier = "flow"
            elif self.get_qoi() == "cTimeStep.fundamentalDiagram":
                # todo: handle > 1 identifier
                identifier = ["density", "velocity"]
            elif self.get_qoi() == "egress.txt":
                identifier = "evacuationTime"
            else:
                raise Exception("Model.py: Identifier not yet defined")
        else:
            raise UserWarning("No identifier was found. QoI might not yet be matched to an identifier.")

        # figure out the processor id '-PIDx'
        if type(identifier) == str:
            identifier_list = [s for s in data.columns if identifier in s]

            if len(identifier_list) == 1:
                identifier = identifier_list[0]

                if type(value) == float:
                    result = data[identifier].values[0]
                else:
                    # todo tests for this case
                    df = data[identifier]
                    m = len(df.index.levels[0])
                    result = df.values.reshape(m, -1)

            else:  # multiple entries in the file fit the identifier
                if identifier == "flow":

                    # todo: adapt model averaging for this case!
                    if type(value) == list and self.get_qoi_dim() > 1:
                        result = data.values

                    elif type(value) == list and self.get_qoi_dim() == 1:
                        identifier = identifier_list[0]  # choose first one if only a one-dimensional qoi is selected
                        identifier = identifier_list[-1]  # choose last one -> for bottleneck study
                        print(identifier)
                        result = data[identifier].values
                    else:
                        logger.debug("model function: Warning: Identifier %s not found" % identifier)
                        raise UserWarning("Identifier %s not found" % identifier)

                else:
                    logger.debug("model function: Warning: Identifier %s not found" % identifier)
                    raise UserWarning("Identifier %s not found" % identifier)
        elif type(identifier) == list:  # multiple identifiers necessary for QOI eval
            result = dict()
            for j in range(0, len(identifier)):  # for each identifier
                identifier_list = [s for s in data.columns if
                                   identifier[j] in s]  # first entry with this identifier (to get the ID)
                result[identifier[j]] = data[identifier_list[0]]
            result = data

        else:
            logger.exception("Model.py:: QOI not yet defined")
            raise Exception("Model.py:: QOI not yet defined")

        # make sure that size of output matches qoi_dim
        if np.size(result) != n * self.get_qoi_dim():
            raise UserWarning(
                'Number of result values (%d) does not match number of evaluations (%d) and qoi dimension (%d)' % (
                    np.size(result), n, self.get_qoi_dim()))

        return result
