from typing import Union

import numpy as np
from numpy.random import RandomState

from uq.utils.datatype import is_vector, is_matrix
from uq.utils.model.AnalyticalModel import AnalyticalModel


class CircuitModel(AnalyticalModel):
    from uq.utils.prior.UniformGenMult import UniformGenMult

    def __init__(self):
        key = ["R_b1", "R_b2", "R_f", "R_c1", "R_c2", "beta"]
        qoi = "Response"
        super().__init__(key=key, qoi=qoi, map_of_results=None, bool_analytic_solution=True,
                         bool_stochastic_model=False, qoi_dim=1, dim=6)

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) -> np.ndarray:
        # example from constantine-2017: http://www.sfu.ca/~ssurjano/otlcircuit.html
        if is_vector(parameter_value):
            R_b1 = parameter_value[0]
            R_b2 = parameter_value[1]
            R_f = parameter_value[2]
            R_c1 = parameter_value[3]
            R_c2 = parameter_value[4]
            beta = parameter_value[5]
        elif is_matrix(parameter_value):
            R_b1 = parameter_value[0, :]
            R_b2 = parameter_value[1, :]
            R_f = parameter_value[2, :]
            R_c1 = parameter_value[3, :]
            R_c2 = parameter_value[4, :]
            beta = parameter_value[5, :]
        else:
            raise Warning('Dimension of parameter value does not fit the model')

        V_b1 = 12 * R_b2 / (R_b1 + R_b2)
        term1 = (V_b1 + 0.74) * beta * (R_c2 + 9) / (beta * (R_c2 + 9) + R_f)
        term2 = 11.35 * R_f / (beta * (R_c2 + 9) + R_f)
        term3 = 0.74 * R_f * beta * (R_c2 + 9) / ((beta * (R_c2 + 9) + R_f) * R_c1)
        V_m = term1 + term2 + term3

        return V_m

    def get_C_matrix(self, prior: "Prior") -> np.ndarray:
        # calculated with Gauss Quadrature (not exact truth) - from Matlab code of constantine-2017

        C = np.array([[2.4555075012e+00, -1.8840695197e+00, -7.5922850961e-01, 4.0379535157e-01, 4.8953133700e-04,
                       1.1616907862e-02], [
                          -1.8840695197e+00, 1.7038668418e+00, 6.7105606089e-01, -3.5661734586e-01, -4.6881645905e-04,
                          -1.1125308912e-02], [
                          -7.5922850961e-01, 6.7105606089e-01, 2.9015818078e-01, -1.6103873334e-01, -1.9146596624e-04,
                          -4.5593065768e-03], [
                          4.0379535157e-01, -3.5661734586e-01, -1.6103873334e-01, 1.0904998348e-01, 1.1583772359e-04,
                          2.7484719868e-03], [
                          4.8953133700e-04, -4.6881645905e-04, -1.9146596624e-04, 1.1583772359e-04, 2.0373109646e-07,
                          6.0499138189e-06], [
                          1.1616907862e-02, -1.1125308912e-02, -4.5593065768e-03, 2.7484719868e-03, 6.0499138189e-06,
                          2.1028328072e-04]])
        return C

    def get_eigenvalues_C(self, rho: UniformGenMult) -> np.ndarray:
        # calculated with Gauss Quadrature (not exact truth) - from Matlab code of constantine-2017
        lambda_C = np.array([4.3339297734, 0.1721545468, 0.0438372806, 0.0087407672, 0.0001306198, 0.0000000066])
        return lambda_C

    def eval_gradient(self, parameter_value: np.ndarray) -> np.ndarray:

        if is_vector(parameter_value):
            Rb1 = parameter_value[0]
            Rb2 = parameter_value[1]
            Rf = parameter_value[2]
            Rc1 = parameter_value[3]
            Rc2 = parameter_value[4]
            beta = parameter_value[5]
        elif is_matrix(parameter_value):
            Rb1 = parameter_value[0, :]
            Rb2 = parameter_value[1, :]
            Rf = parameter_value[2, :]
            Rc1 = parameter_value[3, :]
            Rc2 = parameter_value[4, :]
            beta = parameter_value[5, :]
        else:
            raise Warning('Dimension of value_input does not fit the model')
        n_values = int(np.size(parameter_value) / self.get_dimension())
        dV = np.zeros(shape=(self.get_dimension(), n_values))

        dV[0, :] = (-12 * Rb2 * beta * (Rc2 + 9)) / ((beta * (Rc2 + 9) + Rf) * np.square(Rb1 + Rb2))

        dV[1, :] = (12 * Rb1 * beta * (Rc2 + 9)) / ((beta * (Rc2 + 9) + Rf) * np.square(Rb1 + Rb2))

        dV[2, :] = (beta * (beta * (Rb1 + Rb2) * (59.94 + 13.32 * Rc2 + 0.74 * np.square(Rc2)) + Rc1 * (
                Rb2 * (-12.51 - 1.39 * Rc2) + Rb1 * (95.49 + 10.61 * Rc2)))) / (
                           (Rb1 + Rb2) * Rc1 * np.square(beta * (9 + Rc2) + Rf))

        dV[3, :] = -(0.74 * beta * (Rc2 + 9) * Rf) / (np.square(Rc1) * (beta * (Rc2 + 9) + Rf))

        dV[4, :] = (beta * Rf * (-10.61 * Rb1 * Rc1 + 1.39 * Rb2 * Rc1 + 0.74 * Rb1 * Rf + 0.74 * Rb2 * Rf)) / (
                (Rb1 + Rb2) * Rc1 * np.square(beta * (9 + Rc2) + Rf))

        dV[5, :] = (Rf * (Rb1 * (-95.49 * Rc1 - 10.61 * Rc1 * Rc2 + 6.66 * Rf + 0.74 * Rc2 * Rf) + Rb2 * (
                12.51 * Rc1 + 1.39 * Rc1 * Rc2 + 6.66 * Rf + 0.74 * Rc2 * Rf))) / (
                           (Rb1 + Rb2) * Rc1 * np.square(beta * (9 + Rc2) + Rf))

        return dV
