import logging
import warnings
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import symfit
from numpy.random import RandomState
from scipy import interpolate, optimize as scipy_opt
from sklearn import linear_model
from sklearn import pipeline
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor

from uq.utils.data_eval import averaging_simulation_data
from uq.utils.datatype import unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Model import Model
from uq.utils.model.StochasticModel import StochasticModel
from uq.utils.model.Surrogate1D import Surrogate1D, RationalSurrogate, SklearnSurrogate, SplineSurrogate, \
    PolynomialSurrogate
from uq.utils.model.SurrogateFactory import SurrogateFactory


class Surrogate1DFactory(SurrogateFactory):
    def __init__(self, model_input: np.ndarray = None, model_output: np.ndarray = None, dim: int = 1,
                 nr_points_averaged: int = 1, random_state: RandomState = None):
        super().__init__(random_state=random_state)
        self.__model_input = model_input
        self.__model_output = model_output
        self.__nr_points_averaged = nr_points_averaged
        self.__dimension = dim

    # GETTERS

    def get_dimension(self) -> int:
        return self.__dimension

    def get_model_input(self) -> np.ndarray:
        return self.__model_input

    def get_model_output(self) -> np.ndarray:
        return self.__model_output

    def get_nr_points_averaged(self) -> int:
        return self.__nr_points_averaged

    # SETTERS

    def set_model_input(self, all_x: np.ndarray) -> None:
        self.__model_input = all_x

    def set_model_output(self, all_y: np.ndarray) -> None:
        self.__model_output = all_y

    def set_nr_points_averaged(self, nr_poins_averaged: int) -> None:
        self.__nr_points_averaged = nr_poins_averaged

    def set_dim(self, dim: int) -> None:
        self.__dimension = dim

    # OTHERS

    def create_surrogate_from_model(self, model: "Model", function_type: str, limits: np.ndarray, nr_points: int,
                                    n_keys: int = 1, nr_points_averaged: int = 1,
                                    data_saver: DataSaver = None) -> Surrogate1D:
        self.set_nr_points_averaged(nr_points_averaged)

        model_input, model_output = self.generate_data(model=model, limits=limits, nr_points=nr_points)

        surrogate = self.create_surrogate(model=model, function_type=function_type, model_input=model_input,
                                          model_output=model_output, input_dimension=n_keys,
                                          nr_points_averaged=nr_points_averaged)

        return surrogate

    def create_surrogate(self, model: "Model", function_type: str, model_input: np.ndarray = None,
                         model_output: np.ndarray = None, input_dimension: int = 1,
                         nr_points_averaged: int = 1) -> Surrogate1D:

        self.set_model_input(model_input)
        self.set_model_output(model_output)
        self.set_nr_points_averaged(nr_points_averaged)
        self.set_dim(input_dimension)

        # todo enum for function types?
        if input_dimension == 1:
            if function_type == 'rational':
                fit = self.fit_rational()
                surrogate = RationalSurrogate(model=model, model_input=model_input, model_output=model_output,
                                              n_keys=input_dimension, nr_points_averaged=nr_points_averaged, fit=fit,
                                              surrogate_type=function_type)
            elif function_type == 'rf':
                fit = self.fit_random_forest()
                surrogate = SklearnSurrogate(model=model, model_input=model_input, model_output=model_output,
                                             n_keys=input_dimension, nr_points_averaged=nr_points_averaged, fit=fit,
                                             surrogate_type=function_type)
            elif function_type == 'spline':
                fit = self.fit_spline()
                surrogate = SplineSurrogate(model=model, model_input=model_input, model_output=model_output,
                                            n_keys=input_dimension, nr_points_averaged=nr_points_averaged, fit=fit,
                                            surrogate_type=function_type)
            elif function_type == 'poly':
                fit = self.fit_poly()
                surrogate = PolynomialSurrogate(model=model, model_input=model_input, model_output=model_output,
                                                n_keys=input_dimension, nr_points_averaged=nr_points_averaged, fit=fit,
                                                surrogate_type=function_type)
            else:
                raise ValueError('construct_surrogate: Type (%s) is not defined yet!' % function_type)
        elif input_dimension == 2:
            if function_type == 'spline':

                fit = self.fit_spline_2d()
                surrogate = SplineSurrogate(model=model, model_input=model_input, model_output=model_output,
                                            n_keys=input_dimension, nr_points_averaged=nr_points_averaged, fit=fit,
                                            surrogate_type=function_type)

            elif function_type == "poly":
                fit = self.fit_poly_nd()
                surrogate = SklearnSurrogate(model=model, model_input=model_input, model_output=model_output,
                                             n_keys=input_dimension, nr_points_averaged=nr_points_averaged, fit=fit,
                                             surrogate_type=function_type)
            else:
                raise ValueError('construct_surrogate: Type (%s) is not defined yet!' % function_type)
        else:
            raise Exception('Multidimensional fit not yet implemented')

        return surrogate

    def fit_spline_2d(self):
        fit = interpolate.interp2d(x=self.get_model_input()[0, :], y=self.get_model_input()[1, :],
                                   z=self.get_model_output())

        return fit

    def fit_spline(self):
        # for the spline fit, the runs for the same parameter value need to be averaged at first!
        # use median instead of mean for robustness (outliers have been observed in the past)

        x_av, y_av = averaging_simulation_data(x_data=self.get_model_input(), y_data=self.get_model_output(),
                                               nr_points_averaged=self.get_nr_points_averaged())

        assert (np.max(x_av - np.unique(self.get_model_input())) < 10 ** -5)

        spline_fit = interpolate.splrep(x_av, y_av, s=0)
        return spline_fit

    def fit_poly_nd(self):

        n_points = int(np.sqrt(int(np.size(self.get_model_input()) / self.get_dimension())))
        ex_variables = np.transpose(self.get_model_input())
        response = unbox(self.get_model_output())
        order = 2

        model = pipeline.Pipeline([('poly', preprocessing.PolynomialFeatures(order)),
                                   ('linear', linear_model.LinearRegression())])
        # model.fit(self.get_model_input(), self.get_model_output())
        model.fit(ex_variables, response)

        predictions = model.predict(ex_variables)

        if order == 2:
            x = np.reshape(self.get_model_input()[0, :], (n_points, n_points))
            y = np.reshape(self.get_model_input()[1, :], (n_points, n_points))
            z = np.reshape(self.get_model_output(), (n_points, n_points))
            z_pred = np.reshape(predictions, (n_points,n_points))

            fig, (ax1, ax2) = plt.subplots(1, 2)
            ax1.contour(x, y, z)
            ax1.set_title('Model evaluation')
            ax2.contour(x, y, z_pred)
            plt.title('Surrogate')
            plt.show()

            plt.figure()
            ax = plt.axes(projection='3d')
            ax.contour3D(x, y, z, 50, cmap='binary')
            plt.title('Model evaluation')
            plt.show()

            plt.figure()
            ax = plt.axes(projection='3d')
            ax.contour3D(x, y, z_pred, 50, cmap='binary')
            plt.title('Surrogate')
            plt.show()

        return model

    def fit_poly_2d(self):
        # from https://symfit.readthedocs.io/en/stable/examples/ex_poly_surface_fit.html
        # important: model_output needs to be averaged!

        n_points = int(np.sqrt(int(np.size(self.get_model_input()) / self.get_dimension())))
        x, y, z = symfit.variables('x, y, z')
        c1, c2 = symfit.parameters('c1, c2')
        # Make a polynomial. Note the `as_expr` to make it symfit friendly.
        model_dict = {
            z: symfit.Poly({(2, 0): c1, (0, 2): c1, (1, 1): c2}, x, y).as_expr()
        }
        model = symfit.Model(model_dict)
        xdata = np.reshape(self.get_model_input()[0, :], (n_points, n_points))
        ydata = np.reshape(self.get_model_input()[1, :], (n_points, n_points))
        zdata = np.reshape(unbox(self.get_model_output()), (n_points, n_points))
        fit = symfit.Fit(model, x=xdata, y=ydata, z=zdata)
        fit_result = fit.execute()

        zfit = model(x=xdata, y=ydata, **fit_result.params).z
        print(fit_result)

        r_squared = fit_result.gof_qualifiers['r_squared']

        fig, (ax1, ax2) = plt.subplots(1, 2)
        sns.heatmap(zdata, ax=ax1)
        sns.heatmap(zfit, ax=ax2)
        plt.show()

    def fit_poly(self):
        logger = logging.getLogger()
        deg_uneven = 3
        deg_even = 4
        p_uneven = np.polyfit(self.get_model_input(), self.get_model_output(), deg=deg_uneven)
        residuals_uneven = self.get_model_output() - np.polyval(p_uneven, self.get_model_input())
        n_coeff_uneven = len(p_uneven)
        p_even = np.polyfit(self.get_model_input(), self.get_model_output(), deg=deg_even)
        residuals_even = self.get_model_output() - np.polyval(p_even, self.get_model_input())
        n_coeff_even = len(p_even)

        if np.sum(np.square(residuals_uneven)) < np.sum(np.square(residuals_even)):
            # chose uneven fit
            logger.info('Polynomial of degree: %d' % deg_uneven)
            polynomial_fit = p_uneven
            n_coeff = n_coeff_uneven
        else:
            logger.info('Polynomial of degree: %d' % deg_even)
            polynomial_fit = p_even
            n_coeff = n_coeff_even

        if 10 * n_coeff >= np.size(self.get_model_input()) / self.get_dimension():
            warnings.warn('Increase number of data points (%d) to be at least 10 * number of coefficients (%d)' % (
                np.size(self.get_model_input()) / self.get_dimension(), n_coeff))

        return polynomial_fit

    def fit_rational(self) -> dict:
        from uq.utils.model.Function import Function
        # fit rational
        popt, pcov = scipy_opt.curve_fit(Function.rational1, self.get_model_input(), self.get_model_output())
        fit = dict({"popt": popt, "pcov": pcov})
        n_coeff = len(popt)

        if 10 * n_coeff >= np.size(self.get_model_input()) / self.get_dimension():
            warnings.warn('Increase number of data points (%d) to be at least 10 * number of coefficients (%d)' % (
                np.size(self.get_model_input()) / self.get_dimension(), n_coeff))

        return fit

    def fit_random_forest(self) -> RandomForestRegressor:
        regr = RandomForestRegressor()
        regr.fit(np.expand_dims(self.get_model_input(), axis=1), np.expand_dims(self.get_model_output(), axis=1))

        return regr

    def generate_data(self, model: "Model", limits: np.ndarray, nr_points: int) -> Tuple[np.ndarray, np.ndarray]:
        logger = logging.getLogger("Surrogate1D.generate_data")
        logger.info("Generate data for the construction of the surrogate model")
        points_vec = list()
        for i in range(0, self.get_dimension()):
            if self.get_dimension() == 1:
                points_vec.append(np.linspace(limits[0], limits[1], nr_points))
            else:
                points_vec.append(np.linspace(limits[0, i], limits[1, i], nr_points))

        points_array = np.meshgrid(*points_vec)

        """ translate into the right dimensions """
        points_vec = np.zeros(shape=(self.get_dimension(), nr_points ** self.get_dimension()))
        for i in range(0, self.get_dimension()):
            points_vec[i, :] = points_array[i].ravel()

        # model_output_av, all_x, all_y = model.eval_model_averaged(points_vec, self.get_nr_points_averaged())
        if isinstance(model, StochasticModel):
            model_output, model_input = model.eval_model_multiple_times(parameter_value=points_vec,
                                                                        no_runs=self.get_nr_points_averaged(),
                                                                        random_state=self.get_random_state())

            # remove seeds (otherwise surrogate fit will not work)
            model_input_wo_seeds = model_input[0:-1, :]

            model_output = np.array(model_output)
            model_input_wo_seeds = unbox(model_input_wo_seeds)
        else:
            model_output, model_input = model.eval_model(parameter_value=points_vec)
            model_input_wo_seeds = model_input

        if np.any(np.isnan(model_output)):
            warnings.warn("generate_data: Some runs resulted in Nans!")

        logger.info("Generated data for the construction of the surrogate model")
        return model_input_wo_seeds, model_output
