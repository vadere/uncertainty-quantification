from typing import Union, Tuple

import numpy as np
from numpy.random import RandomState

from uq.utils.model.Model import Model


class AnalyticalModel(Model):
    # Test Models

    def __init__(self, key: Union[str, list], qoi: str, map_of_results=None, bool_analytic_solution: bool = True,
                 bool_stochastic_model: bool = False, data_saver: "DataSaver" = None, qoi_dim: int = 1,
                 bool_sensitivity: bool = False, n_evals: int = None, dim: int = None):
        super().__init__(key=key, qoi=qoi, map_of_results=map_of_results, bool_stochastic_model=bool_stochastic_model,
                         data_saver=data_saver, bool_analytic_solution=bool_analytic_solution,
                         qoi_dim=qoi_dim, bool_sensitivity=bool_sensitivity, n_evals=n_evals, dim=dim)

    def eval_gradient(self, parameter_value: Union[float, np.ndarray]):
        pass

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) -> Union[
        float, np.ndarray]:
        pass

    def eval_model_averaged(self, parameter_value: Union[float, np.ndarray], nr_runs_averaged: int = 1,
                            random_state: RandomState = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        result = self.eval_model(parameter_value=parameter_value, random_state=random_state)
        return result, None, None
