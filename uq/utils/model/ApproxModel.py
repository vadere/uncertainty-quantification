from typing import Union

import numpy as np
from numpy.random import RandomState

from uq.utils.model.Model import Model
from uq.utils.model.Surrogate import Surrogate


class ApproxModel(Surrogate):

    def __init__(self, W1: Union[float, np.ndarray], model: Model):
        dim = np.size(W1, axis=1)
        key = ["x_a%d" % i for i in range(0, dim)]  # active variables
        super().__init__(model=model, model_input=None, model_output=None, dim=dim, key=key)

        self.__W1 = W1  # active subspace

    def equals(self, other: "ApproxModel"):
        bool_equals = False
        if isinstance(other, ApproxModel):
            if np.all(self.get_W1() == other.get_W1()) and \
                    self.get_original_model().equals(other.get_original_model()):
                bool_equals = True

        return bool_equals

    def get_W1(self) -> np.ndarray:
        return self.__W1

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) \
            -> Union[float, np.ndarray]:
        if np.size(parameter_value) == 1:
            input_value = self.get_W1() * parameter_value
        else:
            input_value = np.matmul(self.get_W1(), parameter_value)
        g = self.get_original_model().eval_model(input_value)
        return g
