import logging
from typing import Tuple

import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.SamplingStrategy import SamplingStrategy
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Model import Model
from uq.utils.model.Surrogate1D import Surrogate1D
from uq.utils.model.Surrogate1DFactory import Surrogate1DFactory


class Surrogate1DFactoryDataMisfit(Surrogate1DFactory):

    def __init__(self, model_input: np.ndarray = None, model_output: np.ndarray = None, dim: int = 1,
                 nr_points_averaged: int = 1, random_state: RandomState = None):
        super().__init__(model_input=model_input, model_output=model_output, dim=dim,
                         nr_points_averaged=nr_points_averaged, random_state=random_state)

        self.__model_response = None  # model evaluations (in model_output is the data misfit stored)

    def get_model_response(self) -> np.ndarray:
        return self.__model_response

    def set_model_response(self, model_response: np.ndarray) -> None:
        self.__model_response = model_response



    def create_surrogate(self, model: Model, function_type: str, sampling_strategy: SamplingStrategy,
                         limits: np.ndarray, nr_points: int, n_keys: int, nr_points_averaged: int = 1,
                         data_saver: DataSaver = None) -> Surrogate1D:
        self.set_nr_points_averaged(nr_points_averaged)

        model_input, model_output = self.generate_data(model=model, sampling_strategy=sampling_strategy,
                                                       limits=limits, nr_points=nr_points)

        surrogate = super().create_surrogate(model=model, function_type=function_type, model_input=model_input,
                                             model_output=model_output, input_dimension=n_keys,
                                             nr_points_averaged=nr_points_averaged)

        surrogate.set_model_response(self.get_model_response())

        return surrogate

    def generate_data(self, model: Model, sampling_strategy: SamplingStrategy, limits: np.ndarray,
                      nr_points: int) -> Tuple[np.ndarray, np.ndarray]:
        logger = logging.getLogger("Surrogate1DDataMisfit.generate_data")
        model_input, model_output = super().generate_data(model=model, limits=limits, nr_points=nr_points)
        # points_vec, model_output = self.generate_data(model=model, limits=limits, nr_points=nr_points)
        self.set_model_response(model_output)
        data_misfit_output = sampling_strategy.evaluate_data_misfit(model_output)
        logger.info("Generated data for surrogate")
        return model_input, data_misfit_output
