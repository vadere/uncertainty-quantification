from typing import Union

import numpy as np
from numpy.random import RandomState

from uq.utils.datatype import is_vector, is_matrix
from uq.utils.model.AnalyticalModel import AnalyticalModel


class IshigamiModel(AnalyticalModel):
    # https://uqworld.org/t/ishigami-function/55

    def __init__(self, a: float, b: float, qoi_dim: int = 1):
        key = ["x_1", "x_2", "x_3"]
        qoi = "Response"
        super().__init__(key=key, qoi=qoi, map_of_results=None, bool_stochastic_model=False,
                         bool_analytic_solution=False, bool_sensitivity=True, qoi_dim=qoi_dim, dim=3)
        self.__a = a
        self.__b = b

    def get_a(self) -> float:
        return self.__a

    def get_b(self) -> float:
        return self.__b

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) -> np.ndarray:
        if is_vector(parameter_value):
            x1 = parameter_value[0]
            x2 = parameter_value[1]
            x3 = parameter_value[2]
        elif is_matrix(parameter_value):
            x1 = parameter_value[0, :]
            x2 = parameter_value[1, :]
            x3 = parameter_value[2, :]
        else:
            raise UserWarning("Dimension of input parameter_value not applicable")

        y = np.sin(x1) + self.get_a() * np.square(np.sin(x2)) + self.get_b() * np.power(x3, 4) * np.sin(x1)

        return y

    def eval_gradient(self, parameter_value: Union[float, np.ndarray]) -> np.ndarray:
        if is_vector(parameter_value):
            x1 = parameter_value[0]
            x2 = parameter_value[1]
            x3 = parameter_value[2]
            N = 1
        elif is_matrix(parameter_value):
            x1 = parameter_value[0, :]
            x2 = parameter_value[1, :]
            x3 = parameter_value[2, :]
            N = np.size(parameter_value, axis=1)
        else:
            raise UserWarning("Dimension of input parameter_value not applicable")

        y = np.zeros(shape=(3, N))
        y[0, :] = self.get_b() * x3 ** 4 * np.cos(x1) + np.cos(x1)
        y[1, :] = 2 * self.get_a() * np.sin(x2) * np.cos(x2)
        y[2, :] = 4 * self.get_b() * x3 ** 3 * np.sin(x1)
        return y

    def get_true_first_order_indices(self) -> np.ndarray:
        v_1 = 0.5 * np.square(1 + (self.get_b() * np.power(np.pi, 4)) / 5)
        d_1 = 1 / 2 + self.get_b() * np.power(np.pi, 4) / 5 + np.square(self.get_b()) * np.power(np.pi, 8) / 50
        v_2 = np.square(self.get_a()) / 8
        v_3 = 0
        s1 = v_1 / self.get_true_vy()
        s2 = v_2 / self.get_true_vy()
        s3 = v_3 / self.get_true_vy()
        return np.array([s1, s2, s3])

    def get_true_vy(self):
        # sobol-1999
        v_y = 1 / 2 + np.square(self.get_a()) / 8 + self.get_b() * np.power(np.pi, 4) / 5 + np.square(
            self.get_b()) * np.power(np.pi, 8) / 18
        return v_y

    def get_true_total_effect_indices(self) -> np.ndarray:
        # d_1 = 1 / 2 + self.get_b() * np.power(np.pi, 4) / 5 + np.square(self.get_b()) * np.power(np.pi, 8) / 50
        # d_2 = np.square(self.get_a()) / 8
        # d_13 = (1 / 18 - 1 / 50) * np.square(self.get_b()) * np.power(np.pi, 8)

        # d_tot1 = (d_1 + d_13) / self.get_true_vy()
        # d_tot2 = d_2 / self.get_true_vy()
        # d_tot3 = d_13 / self.get_true_vy()

        tmp = (8 * np.square(self.get_b()) * np.power(np.pi, 8)) / 225
        v_t1 = 0.5 * np.square(1 + (self.get_b() * np.power(np.pi, 4) / 5)) + tmp
        v_t2 = np.square(self.get_a()) / 8
        v_t3 = tmp
        s_t1 = v_t1 / self.get_true_vy()
        s_t2 = v_t2 / self.get_true_vy()
        s_t3 = v_t3 / self.get_true_vy()

        return np.array([s_t1, s_t2, s_t3])


class IshigamiModel2DOutputTest(IshigamiModel):

    def __init__(self, a: float, b: float):
        super().__init__(a=a, b=b, qoi_dim=2)

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) -> np.ndarray:
        y = super().eval_model(parameter_value=parameter_value)
        return np.vstack([y, y])

    def eval_gradient(self, parameter_value: Union[float, np.ndarray]) -> np.ndarray:
        y = super().eval_gradient(parameter_value=parameter_value)
        return np.vstack([y, y])
