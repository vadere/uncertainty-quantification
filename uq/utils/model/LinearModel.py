import warnings
from typing import Union, Tuple

import numpy as np
from numpy.random import RandomState

from uq.utils.datatype import unbox
from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.model.StochasticModel import StochasticModel
from uq.utils.prior.GaussianGen import GaussianGen
from uq.utils.prior.Prior import Prior


class LinearModel(AnalyticalModel):
    def __init__(self, m: float, t: float):
        super(LinearModel, self).__init__(key="x", qoi="f", map_of_results=None, bool_stochastic_model=False,
                                          bool_analytic_solution=True, dim=1)
        if type(m) == np.ndarray:
            warnings.warn('m should be a float, not a numpy array (%s)' % np.array2string(m))
            m = unbox(m)
        if type(t) == np.ndarray:
            warnings.warn('t should be a float, not a numpy array (%s)' % np.array2string(t))
            t = unbox(t)
        self.__m = m
        self.__t = t

    def get_m(self) -> float:
        return self.__m

    def get_t(self) -> float:
        return self.__t

    def eval_model(self, parameter_value: float, random_state: RandomState = None) -> float:
        return self.get_m() * parameter_value + self.get_t()

    def eval_gradient(self, parameter_value: float) -> float:
        return self.get_m()


class NoisyLinearModel(LinearModel, StochasticModel):
    def __init__(self, m: float, t: float, noise_level: float):
        super().__init__(m=m, t=t)
        self.__noise_level = noise_level
        self.__noise_dist = GaussianGen(mean=0.0, variance=self.get_noise_level())

    def get_noise_level(self) -> float:
        return self.__noise_level

    def get_noise_dist(self) -> Prior:
        return self.__noise_dist

    def eval_model(self, parameter_value: float, random_state: RandomState = None) -> float:
        n = np.size(parameter_value)
        deterministic_part = super().eval_model(parameter_value=parameter_value)
        stochastic_part = self.get_noise_dist().sample(n=n, random_state=random_state)
        # additive noise
        return deterministic_part + stochastic_part

    def eval_model_averaged(self, parameter_value: Union[float, np.ndarray], nr_runs_averaged: int = 1,
                            random_state: RandomState = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        result = np.zeros((nr_runs_averaged, np.size(parameter_value)))
        if np.ndim(parameter_value) == 1:
            for i_rep in range(0, nr_runs_averaged):
                result[i_rep, :] = self.eval_model(parameter_value=parameter_value, random_state=random_state)
            mean_result = np.mean(result, axis=0)
            value_input_duplicated = np.repeat(np.expand_dims(parameter_value, axis=1), nr_runs_averaged, axis=1)

        elif np.ndim(parameter_value) == 0:
            value_input_duplicated = np.repeat(parameter_value, nr_runs_averaged)
            result = self.eval_model(parameter_value=value_input_duplicated, random_state=random_state)
            mean_result = np.mean(result)
        else:
            raise ValueError('Wrong format of parameter_value!')

        # todo test for scalar input and vector input
        return mean_result, value_input_duplicated, result

    def eval_model_multiple_times(self, parameter_value: Union[float, np.ndarray], no_runs: int = 1,
                                  random_state: RandomState = None) -> np.ndarray:
        value_input_duplicated = np.ravel(np.transpose(np.tile(parameter_value, reps=(no_runs, 1))))
        result = self.eval_model(parameter_value=value_input_duplicated, random_state=random_state)
        return result

    def eval_gradient(self, parameter_value: float) -> float:
        return self.get_m() * np.ones(shape=parameter_value)
