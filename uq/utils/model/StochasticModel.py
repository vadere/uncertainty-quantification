from typing import Union

import numpy as np
from numpy.random import RandomState

from uq.utils.model.Model import Model


class StochasticModel(Model):

    def eval_model_averaged(self, parameter_value: Union[float, np.ndarray], nr_runs_averaged: int = 1,
                            random_state: RandomState = None) -> np.ndarray:
        pass

    def eval_model_multiple_times(self, parameter_value: Union[float, np.ndarray], no_runs: int = 1,
                                  random_state: RandomState = None):
        pass
