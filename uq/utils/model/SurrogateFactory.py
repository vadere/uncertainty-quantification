from numpy.random import RandomState

from uq.utils.model import Surrogate


class SurrogateFactory:
    def __init__(self, random_state: RandomState = None):
        self.__random_state = random_state

    def create_surrogate(self) -> Surrogate:
        pass

    def get_random_state(self) -> RandomState:
        return self.__random_state
