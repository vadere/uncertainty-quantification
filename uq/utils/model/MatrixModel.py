from typing import List

import numpy as np
from numpy.random import RandomState

from uq.utils.datatype import is_scalar, is_row_vector
from uq.utils.model.AnalyticalModel import AnalyticalModel
from uq.utils.prior.UniformGenMult import UniformGenMult


class MatrixModel(AnalyticalModel):

    def __init__(self, A: np.ndarray, key: List[str] = None):

        if key is None:
            key = ["x%d" % i for i in range(0, np.size(A, 1))]
        super().__init__(key=key, qoi="MatrixModelOutput", map_of_results=None, bool_stochastic_model=False,
                         bool_analytic_solution=True, dim=np.size(A, axis=0))
        self.__A = A
        self.__eig = None

    def get_A(self) -> np.ndarray:
        return self.__A

    def set_eigenvalues(self, eig: np.ndarray) -> None:
        self.__eig = eig

    def get_eigenvalues(self) -> np.ndarray:
        return self.__eig

    def get_eigenvalues_C(self, rho: "UniformGenMult") -> np.ndarray:
        if isinstance(rho, UniformGenMult):
            eig = 1 / 3 * (self.get_eigenvalues() ** 2)
        else:
            print(type(rho))
            raise Warning('C-Matrix not implemented for this distribution')
        return eig

    def get_C_matrix(self, prior: UniformGenMult) -> np.ndarray:
        # only valid for uniform distribution [-1,1]^m
        # if type(rho) is UniformGenMult:
        C = 1 / 3 * np.matmul(self.get_A(), self.get_A())
        # else:
        #    raise Warning('C-Matrix not implemented for this distribution')
        return C

    def eval_model(self, parameter_value: np.ndarray, random_state: RandomState = None) -> float:
        if is_scalar(parameter_value):
            result = 1 / 2 * parameter_value * self.get_A() * parameter_value
        else:
            if is_row_vector(parameter_value):
                parameter_value = np.expand_dims(parameter_value, axis=1)

            m = np.size(parameter_value, axis=1)
            result = np.zeros(shape=m)
            for i_cols in range(0, m):
                result[i_cols] = 1 / 2 * np.matmul(np.matmul(np.transpose(parameter_value[:, i_cols]), self.get_A()),
                                                   parameter_value[:, i_cols])
        return result

    def eval_gradient(self, parameter_value: np.ndarray) -> np.ndarray:
        if is_scalar(parameter_value):
            gradient = self.get_A() * parameter_value
        else:
            gradient = np.matmul(self.get_A(), parameter_value)
        return gradient
