import warnings
from builtins import list
from typing import Union, Tuple

import numpy as np
from numpy.random import RandomState

from uq.utils.datatype import box
from uq.utils.datatype import is_scalar, is_row_vector


class Model:
    # coverage: 74%
    LOWEST_SEED = 0
    HIGHEST_SEED = 2 ** 31 - 1

    def __init__(self, key: Union[str, list], qoi: str, map_of_results=None, bool_stochastic_model: bool = False,
                 data_saver: "DataSaver" = None, bool_analytic_solution: bool = False, qoi_dim: int = 1,
                 bool_sensitivity: bool = False, n_evals: int = None, dim: int = None):
        if type(key) == str:  # otherwise, length of str is dimension
            key = [key]
        self.__key = key
        self.__qoi = qoi
        self.__qoi_dim = qoi_dim  # multi-dim qoi

        self.__bool_analytic_solution = bool_analytic_solution
        self.__data_saver = data_saver
        self.__stochastic_model = bool_stochastic_model
        self.__bool_fixed_seed = False
        self.__bool_sensitivity = bool_sensitivity

        if dim is not None and dim is not len(self.get_key()):
            raise UserWarning('Dimension argument (%d) does not match length of key (%d)' % (dim, len(self.get_key())))

        # todo move these attributes to a results class or similar (it is not fixed)

        if n_evals is None:
            self.__n_evals = 0
        else:
            self.__n_evals = n_evals

        if map_of_results is None:
            self.__map_of_results = dict()
        else:
            self.__map_of_results = map_of_results

        self.__computation_time = list()

    # EQUALS

    def equals(self, other: "Model") -> bool:
        # compare all fields except computation times and datasaver (paths)
        bool_equals = False
        if isinstance(other, Model):
            if self.get_key() == other.get_key() and \
                    self.get_qoi() == other.get_qoi() and \
                    self.get_qoi_dim() == other.get_qoi_dim() and \
                    self.get_map_results() == other.get_map_results() and \
                    self.is_stochastic_model() == other.is_stochastic_model() and \
                    self.is_fixed_seed() == other.is_fixed_seed() and \
                    self.is_sensitivity_indices_available() == other.is_sensitivity_indices_available() and \
                    self.get_n_evals() == other.get_n_evals() and \
                    self.get_dimension() == other.get_dimension():
                # self.get_params().get_model().is_analytic_solution_available()
                bool_equals = True
        return bool_equals

    # GETTERS

    def is_fixed_seed(self) -> bool:
        return self.__bool_fixed_seed

    def is_sensitivity_indices_available(self) -> bool:
        return self.__bool_sensitivity

    def get_key(self) -> Union[str, list]:
        return self.__key

    def is_stochastic_model(self) -> bool:
        return self.__stochastic_model

    def get_qoi(self) -> Union[str, list]:
        return self.__qoi

    def is_analytic_solution_available(self) -> bool:
        return self.__bool_analytic_solution

    def get_computation_time(self) -> float:
        return self.__computation_time

    def get_map_results(self) -> dict:
        return self.__map_of_results

    def get_eigenvalues_C(self, rho):
        pass

    def get_version_str(self) -> str:
        return ""

    # number of uncertain parameters
    def get_dimension(self) -> int:
        return len(self.get_key())

    def get_qoi_dim(self) -> int:
        return self.__qoi_dim

    def get_data_saver(self) -> "DataSaver":
        return self.__data_saver

    def get_n_evals(self) -> int:
        return self.__n_evals

    def get_C_matrix(self, prior: "Prior") -> np.ndarray:
        pass

    # SETTERS

    def set_bool_fixed_seed(self, bool_fixed_seed: bool):
        self.__bool_fixed_seed = bool_fixed_seed

    def set_data_saver(self, data_saver: "DataSaver") -> None:
        self.__data_saver = data_saver

    def set_n_evals(self, n_evals: int) -> None:
        self.__n_evals = n_evals

    # OTHERS

    def add_n_evals(self, n: int) -> None:
        self.set_n_evals(self.get_n_evals() + n)

    def eval_model(self, parameter_value: Union[float, np.ndarray], random_state: RandomState = None) \
            -> Union[float, np.ndarray]:
        # logger = logging.getLogger("Model.eval_model")
        # logger.info(parameter_value) -> too much output
        pass

    # if not implemented by implementation
    def eval_model_averaged(self, parameter_value: Union[float, np.ndarray], nr_runs_averaged: int = 1,
                            random_state: RandomState = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        pass

    # Function to approximate the gradient with finite differences
    # model_evac: Already known model evaluations - used for forward / backward difference
    # at the moment, the central difference is used
    def approximate_gradient(self, input_vector, step_size, model_eval_vec=None, n_runs_av: int = 1,
                             random_state: RandomState = None) -> np.ndarray:
        if np.ndim(input_vector) == 0:
            input_vector = box(input_vector)
        if is_scalar(step_size):
            step_size = np.ones(self.get_dimension()) * step_size
        if is_row_vector(input_vector):
            if np.ndim(input_vector) == 1:
                input_vector = np.expand_dims(input_vector, axis=1)
            else:
                input_vector = input_vector.T
            warnings.warn('Input to approximate_gradient should be a column vector, not a row vector.')
        n_dim = np.size(input_vector, axis=0)

        n_samples = np.size(input_vector, axis=1)
        if self.get_qoi_dim() > 1:
            approx_gradient = np.ones(shape=(n_dim, n_samples, self.get_qoi_dim()))
        else:
            approx_gradient = np.ones(shape=(n_dim, n_samples))
        approx_gradient[:] = np.nan

        # todo: parallelize
        for j in range(0, n_samples):
            for i in range(0, n_dim):
                h_vec = np.zeros(shape=n_dim)
                h_vec[i] = step_size[i]
                value_plus_h = np.expand_dims(input_vector[:, j] + h_vec, axis=1)
                value_min_h = np.expand_dims(input_vector[:, j] - h_vec, axis=1)

                # todo collect config, run later
                model_eval_val_plus_h, _, _ = self.eval_model_averaged(parameter_value=value_plus_h,
                                                                       nr_runs_averaged=n_runs_av,
                                                                       random_state=random_state)
                model_eval_val_min_h, _, _ = self.eval_model_averaged(parameter_value=value_min_h,
                                                                      nr_runs_averaged=n_runs_av,
                                                                      random_state=random_state)

                # central difference
                tmp = (model_eval_val_plus_h - model_eval_val_min_h) / (2 * step_size[i])

                # forward difference
                # h_vec[i] = step_size
                # value_plus_h = np.expand_dims(input_vector[:, j]+h_vec, axis=1)
                # value = np.expand_dims(input_vector[:, j], axis=1)
                # model_eval_val = self.eval_model(value)
                # model_eval_val_plus_h = self.eval_model(value_plus_h)
                # tmp2 = (model_eval_val_plus_h - model_eval_vec[j]) / step_size (forward diff)
                if self.get_qoi_dim() == 1:
                    approx_gradient[i, j] = tmp
                else:
                    approx_gradient[i, j, :] = tmp
        return approx_gradient
