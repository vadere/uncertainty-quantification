import warnings

import numpy as np
from numpy.random import RandomState

from uq.utils.datatype import is_vector, unbox, is_matrix, box
from uq.utils.model.AnalyticalModel import AnalyticalModel


class ExponentialModel(AnalyticalModel):
    # from constantine-2015
    from uq.utils.prior.UniformGenMult import UniformGenMult

    def __init__(self):
        key = ["x1", "x2"]
        qoi = "f"
        super().__init__(key=key, qoi=qoi, map_of_results=None, bool_analytic_solution=True,
                         bool_stochastic_model=False, dim=2)

    def get_C_matrix(self, prior: UniformGenMult) -> np.ndarray:
        # only valid for uniform distribution [-1,1]^2
        factor = np.sum(prior.get_upper() - prior.get_lower())
        result = 1 / factor * 1 / 84 * np.array([[49, 21], [21, 9]]) * \
                 (self.eval_model(prior.get_upper()) ** 2
                  - self.eval_model(np.array([prior.get_lower()[0], prior.get_upper()[1]])) ** 2 -
                  self.eval_model(np.array([prior.get_upper()[0], prior.get_lower()[1]])) ** 2
                  + self.eval_model(prior.get_lower()) ** 2)
        # C_mat = np.array([[0.707222, 0.303095], [0.303095, 0.129898]])
        return result

    def get_eigenvalues_C(self, rho: UniformGenMult) -> np.ndarray:
        # factor = np.sum(rho.upper - rho.lower)
        C_mat = self.get_C_matrix(rho)
        eig_C = np.linalg.eigvals(C_mat)
        return eig_C

    def eval_model(self, parameter_value: np.ndarray, random_state: RandomState = None) -> float:
        if is_vector(parameter_value) and parameter_value.size == self.get_dimension():
            if np.ndim(parameter_value) > 1:
                parameter_value = unbox(parameter_value)
            # for i in range(0, value_input.shape[1]):
            # y = np.matmul(self.W1, np.transpose(self.W1) * value_input[:,i])
            f_value = np.exp(0.7 * parameter_value[0] + 0.3 * parameter_value[1])
        elif is_matrix(parameter_value) and np.mod(np.size(parameter_value), self.get_dimension()) == 0:
            if np.size(parameter_value, axis=1) is self.get_dimension() and not (
                    np.size(parameter_value, axis=0) == self.get_dimension()):
                warnings.warn("TestModel.eval_model(): Dimensions of input do not fit. Input was transposed.")
                np.transpose(parameter_value)
            f_value = np.exp(0.7 * parameter_value[0, :] + 0.3 * parameter_value[1, :])
        else:
            raise Warning("eval_model: Input does not match the model")
        return f_value

    def eval_gradient(self, parameter_value: np.ndarray) -> np.ndarray:
        if is_vector(parameter_value):
            n_values = int(parameter_value.size / self.get_dimension())
            parameter_value = box(parameter_value)
        elif is_matrix(parameter_value):
            n_values = np.size(parameter_value, 1)
        else:
            raise Warning("Eval gradient: Input does not have the expected dimensions")

        gradient = None
        for i in range(0, n_values):
            # partial derivatives
            f_x1 = np.exp(0.7 * parameter_value[0, i] + 0.3 * parameter_value[1, i]) * 0.7
            f_x2 = np.exp(0.7 * parameter_value[0, i] + 0.3 * parameter_value[1, i]) * 0.3
            f_value = np.array([[f_x1], [f_x2]])
            if i == 0:
                gradient = f_value
            else:
                gradient = np.append(gradient, f_value, axis=1)
        return gradient
