import numpy as np
from numpy.random import RandomState

from uq.utils.model.AnalyticalModel import AnalyticalModel


class QuadraticModel(AnalyticalModel):
    def __init__(self, a: float, b: float):
        super(QuadraticModel, self).__init__(key="x", qoi="f", map_of_results=None, bool_stochastic_model=False,
                                             bool_analytic_solution=True, dim=1)
        self.__a = a
        self.__b = b

    def get_a(self) -> float:
        return self.__a

    def get_b(self) -> float:
        return self.__b

    def eval_model(self, parameter_value: float, random_state: RandomState = None) -> float:
        return self.get_a() * np.square(parameter_value) + self.get_b()

    def eval_gradient(self, parameter_value: float) -> float:
        return 2 * self.get_a() * parameter_value
