import logging
import time
from typing import Union

import numpy as np
from numpy.random import RandomState
from scipy import interpolate

from uq.utils.datatype import box, unbox, is_scalar
from uq.utils.model.Function import Function
from uq.utils.model.Model import Model
from uq.utils.model.Surrogate import Surrogate


class Surrogate1D(Surrogate):

    def __init__(self, model: Model, model_input: np.ndarray = None, model_output: np.ndarray = None, n_keys: int = 1,
                 nr_points_averaged: int = 1, fit=None, r_squared: float = None, surrogate_type: str = None):

        super().__init__(model=model, model_input=model_input, model_output=model_output, dim=n_keys)

        self.__eval_time = dict()
        self.__nr_points_averaged = nr_points_averaged
        self.__fit = fit
        self.__surrogate_type = surrogate_type

        if r_squared is None:
            self.__r_squared = self.calc_r_squared()

    # GETTERS

    def get_surrogate_type(self) -> str:
        return self.__surrogate_type

    def get_r_squared(self) -> float:
        return self.__r_squared

    def get_nr_points_averaged(self) -> int:
        return self.__nr_points_averaged

    def get_fit(self):
        return self.__fit

    def get_eval_time(self) -> map:
        return self.__eval_time

    # SETTERS

    def set_r_squared(self, r_squared: float):
        self.__r_squared = r_squared

    def set_fit(self, fit):
        self.__fit = fit

    def set_surrogate_type(self, surrogate_type: str) -> None:
        self.__surrogate_type = surrogate_type

    # OTHERS

    def append_map_of_results(self, value: Union[float, np.ndarray], result: Union[float, np.ndarray],
                              start_time: float) -> None:
        # todo make sure this works
        self.get_eval_time()[value] = time.time() - start_time
        self.get_map_results()[value] = result

    def calc_r_squared(self) -> float:
        logger = logging.getLogger("Surrogate1D.calc_r_squared")
        residuals = self.get_model_output() - self.eval_model(self.get_model_input())

        ss_res = np.sum(residuals ** 2)
        n_samples = int(np.size(self.get_model_input()) / self.get_dimension())
        mean_output = np.mean(self.get_model_output())

        if self.get_nr_points_averaged() > 1:
            model_output = np.reshape(self.get_model_output(), (-1, self.get_nr_points_averaged()))
            mean_output = np.mean(model_output, axis=1)

        else:
            # todo: refactor
            if np.size(mean_output) != n_samples:
                mean_output = np.mean(self.get_model_output(), axis=0)
            if np.size(mean_output) != n_samples:
                mean_output = np.mean(self.get_model_output(), axis=1)
            if np.size(mean_output) != n_samples:
                raise ValueError('Dimension of output does not fit the number of samples')

            model_output = self.get_model_output()

        if np.ndim(mean_output) < np.ndim(model_output):
            mean_output = np.expand_dims(mean_output, axis=1)

        ss_tot = np.sum((model_output - np.mean(mean_output)) ** 2) # y_i - \bar y

        if ss_tot == 0:
            r_squared = 1
        else:
            r_squared = 1 - (ss_res / ss_tot)
        self.set_r_squared(r_squared)
        if ss_tot != 0 and r_squared < 0.8:
            raise UserWarning('Fit is not representative of the original data (r_squared %.2f< 0.8).' % r_squared
                              + 'Try another fit type!')
        logger.info("R^2 of fit : {}".format(self.get_r_squared()))

        return r_squared

    def eval_model(self, parameter_value: np.ndarray, random_state: RandomState) -> np.ndarray:
        pass

    def add_result_to_map(self, parameter_value: np.ndarray, result: np.ndarray, start_time) -> None:
        value_formatted = unbox(parameter_value)

        if is_scalar(value_formatted):
            self.append_map_of_results(value=value_formatted, result=result, start_time=start_time)


class SklearnSurrogate(Surrogate1D):

    def __init__(self, model: Model, model_input: np.ndarray = None, model_output: np.ndarray = None, n_keys: int = 1,
                 nr_points_averaged: int = 1, fit=None, r_squared: float = None, surrogate_type: str = None):
        super().__init__(model=model, model_input=model_input, model_output=model_output, n_keys=n_keys,
                         nr_points_averaged=nr_points_averaged, fit=fit, r_squared=r_squared,
                         surrogate_type=surrogate_type)

    def eval_model(self, parameter_value: Union[float, int, np.ndarray],
                   random_state: RandomState = None) -> np.ndarray:
        start_time = time.time()

        value_formatted = box(parameter_value)
        result = self.get_fit().predict(np.transpose(value_formatted))

        super().add_result_to_map(parameter_value=parameter_value, result=result, start_time=start_time)

        return result


class RationalSurrogate(Surrogate1D):
    def __init__(self, model: Model, model_input: np.ndarray = None, model_output: np.ndarray = None, n_keys: int = 1,
                 nr_points_averaged: int = 1, fit=None, r_squared: float = None, surrogate_type: str = None):
        super().__init__(model=model, model_input=model_input, model_output=model_output, n_keys=n_keys,
                         nr_points_averaged=nr_points_averaged, fit=fit, r_squared=r_squared,
                         surrogate_type=surrogate_type)

    def eval_model(self, parameter_value: Union[float, int, np.ndarray],
                   random_state: RandomState = None) -> np.ndarray:
        start_time = time.time()

        result = Function.rational1(parameter_value, *self.__fit["popt"])

        super().add_result_to_map(parameter_value=parameter_value, result=result, start_time=start_time)

        return result


class SplineSurrogate(Surrogate1D):
    def __init__(self, model: Model, model_input: np.ndarray = None, model_output: np.ndarray = None, n_keys: int = 1,
                 nr_points_averaged: int = 1, fit=None, r_squared: float = None, surrogate_type: str = None):
        super().__init__(model=model, model_input=model_input, model_output=model_output, n_keys=n_keys,
                         nr_points_averaged=nr_points_averaged, fit=fit, r_squared=r_squared,
                         surrogate_type=surrogate_type)

    def eval_model(self, parameter_value: Union[float, int, np.ndarray],
                   random_state: RandomState = None) -> np.ndarray:
        start_time = time.time()
        result = None
        if self.get_dimension() == 1:
            result = interpolate.splev(x=parameter_value, tck=self.get_fit(), der=0)
        elif self.get_dimension() == 2:
            func = self.get_fit()
            if np.ndim(parameter_value) == 1:
                result = func(parameter_value[0], parameter_value[1])
            else:
                result = func(parameter_value[:, 0], parameter_value[:, 1])
        super().add_result_to_map(parameter_value=parameter_value, result=result, start_time=start_time)

        return result


class PolynomialSurrogate(Surrogate1D):
    def __init__(self, model: Model, model_input: np.ndarray = None, model_output: np.ndarray = None, n_keys: int = 1,
                 nr_points_averaged: int = 1, fit=None, r_squared: float = None, surrogate_type: str = None):
        super().__init__(model=model, model_input=model_input, model_output=model_output, n_keys=n_keys,
                         nr_points_averaged=nr_points_averaged, fit=fit, r_squared=r_squared,
                         surrogate_type=surrogate_type)

    def eval_model(self, parameter_value: Union[float, int, np.ndarray],
                   random_state: RandomState = None) -> np.ndarray:
        start_time = time.time()

        result = np.polyval(p=self.get_fit(), x=parameter_value)

        super().add_result_to_map(parameter_value=parameter_value, result=result, start_time=start_time)

        return result
