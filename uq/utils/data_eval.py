import numpy as np

from uq.utils.datatype import unbox

""" ----------------------------------- sample mean / var  --------------------------------------------------- """


def sample_mean(samples: np.ndarray, burn_in: int = 0):
    if np.ndim(samples) == 1:
        sample_mean_val = np.mean(samples[burn_in:len(samples)], axis=0)
    else:
        n_samples = np.size(samples, axis=1)
        sample_mean_val = np.mean(samples[:, burn_in:n_samples], axis=1)

    return sample_mean_val


def sample_mode(samples: np.ndarray, burn_in: int = 0):
    if np.size(samples) > 0:
        # most common value (for asymmetric distribution)
        # stats.mode is only for discrete data
        if np.ndim(samples) == 1:  # 1-d key

            count, bins = np.histogram(samples[burn_in:len(samples)],
                                       np.max([int(len(samples[burn_in:len(samples)]) / 100), 10]))
            idx = np.argmax(count)
            mode = (bins[idx] + bins[idx + 1]) / 2
        else:  # multi-dim key
            mode = np.zeros(np.size(samples, axis=0))
            n_samples = np.size(samples, axis=1)
            for i in range(0, np.size(samples, axis=0)):
                count, bins = np.histogram(samples[i][burn_in:n_samples],
                                           np.max([int(len(samples[i][burn_in:n_samples]) / 100), 10]))
                idx = np.argmax(count)
                mode[i] = (bins[idx] + bins[idx + 1]) / 2

        mode = unbox(mode)
    else:  # samples empty
        mode = np.nan

    return mode


def sample_std(samples: np.ndarray, burn_in: int = 0):
    if np.ndim(samples) == 1:
        sample_stdev = np.std(samples[burn_in:len(samples)], axis=0)
    else:
        n_samples = np.size(samples, axis=1)
        sample_stdev = np.std(samples[:, burn_in:n_samples], axis=1)
    return sample_stdev


def sample_var(samples: np.ndarray, burn_in: int = 0):
    if np.ndim(samples) == 1:
        sample_variance = np.var(samples[burn_in:len(samples)], axis=0)
    else:
        n_samples = np.size(samples, axis=1)
        sample_variance = np.var(samples[:, burn_in:n_samples], axis=1)
    return sample_variance


""" ----------------------------------- averaging of samples --------------------------------------------------- """


def averaging_simulation_data(x_data: np.ndarray, y_data: np.ndarray, nr_points_averaged: int):
    x_reshaped = np.reshape(x_data, newshape=(-1, nr_points_averaged))
    y_reshaped = np.reshape(y_data, newshape=(-1, nr_points_averaged))

    if nr_points_averaged == 1:  # no averaging
        y_av = y_data
        x_av = x_data
    else:
        y_av = np.median(y_reshaped, axis=1)
        x_av = np.median(x_reshaped, axis=1)

    n_samples = int(np.size(x_data) / nr_points_averaged)
    assert len(x_av) == n_samples
    assert len(y_av) == n_samples

    return x_av, y_av
