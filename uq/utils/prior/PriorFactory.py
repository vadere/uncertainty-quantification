import numpy as np

from uq.utils.prior.GaussianGen import GaussianGen
from uq.utils.prior.GaussianGenMult import GaussianGenMult
from uq.utils.prior.GaussianGenTrunc import GaussianGenTrunc
from uq.utils.prior.GaussianGenTruncMult import GaussianGenTruncMult
from uq.utils.prior.UniformGen import UniformGen
from uq.utils.prior.UniformGenMult import UniformGenMult


class PriorFactory:

    def __init__(self):
        pass

    def create_prior_by_type(self, params: dict) -> "Prior":
        if params["Dim"] == 1:  # one-dim
            if params["Type"] == 'Normal':
                if "Limits" in params and params["Limits"] is not None:
                    distribution = GaussianGenTrunc(mean=params["Mean"], variance=params["Variance"],
                                                    limits=params["Limits"])
                else:
                    distribution = GaussianGen(mean=params["Mean"], variance=params["Variance"])
            elif params["Type"] == 'Uniform':
                distribution = UniformGen(lower=params["Low"], upper=params["High"])
            else:
                raise UserWarning('This type (%s) is not supported.' % params["Type"])

        else:  # multi-dim
            if params["Type"] == 'Uniform':
                distribution = UniformGenMult(lower=params["Low"], upper=params["High"],
                                              dim=np.size(params["Low"]))
            elif params["Type"] == 'Normal':
                if np.size(params["Mean"]) == 1:
                    if "Limits" in params:
                        distribution = GaussianGenTrunc(mean=params["Mean"],
                                                        variance=params["Variance"], limits=params["Limits"])
                    else:
                        distribution = GaussianGen(mean=params["Mean"],
                                                   variance=params["Variance"])
                else:
                    if "Limits" in params and params["Limits"] is not None:
                        distribution = GaussianGenTruncMult(mean=params["Mean"], variance=params["Variance"],
                                                            limits=params["Limits"], dim=params["Dim"])
                    else:
                        distribution = GaussianGenMult(mean=params["Mean"], variance=params["Variance"],
                                                       dim=params["Dim"])
            else:
                raise UserWarning('This type (%s) is not supported.' % params["Type"])

        return distribution
