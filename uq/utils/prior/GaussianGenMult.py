import warnings
from typing import Union

import numpy as np
from numpy.random.mtrand import RandomState
from scipy import stats as scipy_stats

from uq.utils.prior.Prior import Prior


class GaussianGenMult(Prior):
    """ Multivariate Normal Distribution (non-correlated variables)"""

    def __init__(self, mean: Union[float, np.ndarray], variance: Union[float, np.ndarray], dim: int = 1):
        super().__init__(dim=dim)
        self.check_inputs(mean=mean, variance=variance, dim=dim)
        self.__mean = mean
        self.__variance = variance

    # GETTERS

    def get_mean(self) -> Union[float, np.ndarray]:
        return self.__mean

    def get_cov(self) -> Union[float, np.ndarray]:
        return self.__variance

    # OTHERS

    def equals(self, other: "GaussianGenMult") -> bool:
        bool_equals = False
        if isinstance(other, GaussianGenMult):
            if np.all(self.get_mean() == other.get_mean()) and np.all(
                    self.get_cov() == other.get_cov()) and self.get_dim() == other.get_dim():
                bool_equals = True

        return bool_equals

    def check_inputs(self, mean: Union[float, np.ndarray], variance: Union[float, np.ndarray], dim: int = 1) -> None:
        if dim is not np.size(mean):
            raise ValueError('Dimension (%d) has to be consistent with mean vector (%s)' % (dim, np.array2string(mean)))
        if np.ndim(variance) == 1 and np.size(variance) > 1:  # vector
            raise ValueError('Covariance (%s) has to be a quadratic matrix!' % (np.array2string(variance)))
        if np.ndim(variance) == 2:
            if not np.ceil(np.sqrt(np.size(variance))) == np.sqrt(np.size(variance)):
                raise ValueError('Covariance (%s) has to be a quadratic matrix!' % (np.array2string(variance)))
            if not int(np.sqrt(np.size(variance))) == dim:
                raise ValueError(
                    'Dimension (%d) has to be consistent with covariance (%s)' % (dim, np.array2string(variance)))

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        """ Only works for non-correlated variables """

        if random_state is None:
            warnings.warn("Called without RandomState object. Consider adding a RandomState object.")
            random_state = RandomState()

        sample_vec = random_state.multivariate_normal(mean=self.get_mean(), cov=self.get_cov(), size=n).T

        return sample_vec

    def eval_prior(self, value: np.ndarray) -> float:
        tmp = scipy_stats.multivariate_normal.pdf(value, mean=self.get_mean(), cov=self.get_cov())
        # tmp should be one-dimensional
        return tmp
