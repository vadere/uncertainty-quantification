import warnings
from typing import Union

import numpy as np
from numpy.random.mtrand import RandomState
from scipy import stats as scipy_stats

from uq.utils.datatype import is_vector
from uq.utils.prior.GaussianGenMult import GaussianGenMult


# todo tests for this class
class GaussianGenTruncMult(GaussianGenMult):

    def __init__(self, mean: Union[float, np.ndarray], variance: [float, np.ndarray], limits: np.ndarray, dim: int):
        if is_vector(variance):
            variance = np.eye(dim) * variance
        super().__init__(mean=mean, variance=variance, dim=dim)
        self.check_inputs_truncated(mean=mean, variance=variance, limits=limits, dim=dim)

        self.__limits = limits

        if dim == 1 and np.ndim(variance) == 0:
            a = (self.get_lower_limit() - mean) / variance
            b = (self.get_upper_limit() - mean) / variance
        else:
            a = (self.get_lower_limit() - mean) / np.diag(variance)
            b = (self.get_upper_limit() - mean) / np.diag(variance)

        self.__a = a
        self.__b = b

    # GETTERS

    def get_a(self) -> Union[float, np.ndarray]:
        return self.__a

    def get_b(self) -> Union[float, np.ndarray]:
        return self.__b

    def get_limits(self) -> np.ndarray:
        return self.__limits

    def get_upper_limit(self) -> Union[float, np.ndarray]:
        upper_limit = None
        if self.get_dim() == 1:  # 1-dimensional
            upper_limit = self.get_limits()[1]
        elif self.get_dim() > 1:  # multi-dimensional
            upper_limit = self.get_limits()[1, :]
        return upper_limit

    def get_lower_limit(self) -> Union[float, np.ndarray]:
        lower_limit = None
        if self.get_dim() == 1:  # 1-dimensional
            lower_limit = self.get_limits()[0]
        elif self.get_dim() > 1:  # multi-dimensional
            lower_limit = self.get_limits()[0, :]
        return lower_limit

    # OTHERS

    def check_inputs_truncated(self, mean: Union[float, np.ndarray], variance: Union[float, np.ndarray],
                               limits: np.ndarray, dim: int) -> None:
        if np.any(variance < 0):
            raise ValueError('Deviation (%s) has to be >= 0.' % np.array2string(variance))
        if np.size(limits, axis=0) is not 2:
            raise ValueError('Limits (%s) has to be 2-dim np.ndarray.' % np.asarray(limits))

        if not np.size(mean) == dim:
            raise ValueError('Dimension of mean (%d) has to be consistent with dimension (%d).' % (np.size(mean), dim))

        if not np.size(variance) == dim and not np.size(variance) == dim * dim:
            raise ValueError(
                'Dimension of variance (%d) has to be consistent with dimension (%d).' % (np.size(variance), dim))

        if not np.size(limits) == 2 * dim:
            raise ValueError(
                'Dimension of limits (%d) has to be consistent with dimension (%d).' % (np.size(limits), dim))

        if not dim >= 1:
            raise ValueError('Dimension (%d) has to be >= 1.' % dim)

        lower_limit = None
        upper_limit = None
        if dim == 1:  # 1-dimensional
            lower_limit = limits[0]
            upper_limit = limits[1]
        elif dim > 1:  # multi-dimensional
            lower_limit = limits[0, :]
            upper_limit = limits[1, :]

        if np.any(lower_limit >= upper_limit):
            raise ValueError('Lower limit (%s) has to be smaller than upper limit (%s)' % (
            np.array2string(lower_limit), np.array2string(upper_limit)))
        if np.any(lower_limit > mean):
            raise ValueError(
                'Lower limit (%s) has to be <= mean (%s)' % (np.array2string(lower_limit), np.array2string(mean)))
        if np.any(upper_limit < mean):
            raise ValueError(
                'Upper limit (%s) has to be <= mean (%s)' % (np.array2string(upper_limit), np.array2string(mean)))
        if dim == 1:
            if variance == 0:
                raise ValueError('Variance for truncated distribution has to be larger than 0.')
        elif dim > 1:
            if np.any(np.diag(variance)) == 0:
                raise ValueError('Variance for truncated distribution has to be larger than 0.')

        if dim > 1:
            helper = np.ones((dim, dim)) - np.eye(dim)
            if not np.all(helper * variance) == 0:
                raise UserWarning('Multidimensional truncated Gaussian distribution currently only for uncorrelated'
                                  + ' random variables available.')

    def equals(self, other: "GaussianGenTruncMult") -> bool:
        bool_equals = False
        if super().equals(other):
            if isinstance(other, GaussianGenTruncMult):
                if np.all(self.get_lower_limit() == other.get_lower_limit()) \
                        and np.all(self.get_upper_limit() == other.get_upper_limit()):
                    bool_equals = True
        return bool_equals

    def eval_prior(self, value: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        pdf_at_x = None
        if self.get_dim() == 1:
            if is_vector(value):
                pdf_at_x = scipy_stats.truncnorm.pdf(x=value, a=self.get_a(), b=self.get_b(), loc=self.get_mean(),
                                                     scale=self.get_cov())
            else:
                raise ValueError('Dimension of value (%d) does not fit dimension of distribution (%d)' % (
                    np.size(value), self.get_dim()))

        elif self.get_dim() > 1:
            if np.size(value, axis=0) == self.get_dim():
                pdf_at_x = np.zeros(self.get_dim(), np.size(value, axis=1))
                for i in range(0, self.get_dim()):
                    pdf_at_x[i] = scipy_stats.truncnorm.pdf(x=value[i, :], a=self.get_a()[i], b=self.get_b()[i],
                                                            loc=self.get_mean()[i],
                                                            scale=self.get_cov()[i])

            else:
                raise ValueError('Dimension of value (%d) does not fit dimension of distribution (%d)' % (
                    np.size(value, axis=0), self.get_dim()))
        return pdf_at_x

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        if random_state is None:
            warnings.warn("GaussianGenTrunc.sample() is called without a RandomState object!")
        scaled_samples = None

        if self.get_dim() == 1:
            # todo call 1d distribution here

            samples = scipy_stats.truncnorm.rvs(self.get_a(), self.get_b(), size=n,
                                                random_state=random_state)  # standard-normal samples
            scaled_samples = samples * self.get_cov() + self.get_mean()
            assert np.all(scaled_samples >= self.get_lower_limit())
            assert np.all(scaled_samples <= self.get_upper_limit())
        elif self.get_dim() > 1:

            scaled_samples = np.zeros((self.get_dim(), n))

            for i in range(0, self.get_dim()):
                # todo call 1d distribution here
                tmp_samples = scipy_stats.truncnorm.rvs(self.get_a()[i], self.get_b()[i], size=n,
                                                        random_state=random_state)  # standard-normal samples
                tmp_scaled_samples = tmp_samples * self.get_cov()[i, i] + self.get_mean()[i]
                assert np.all(tmp_scaled_samples >= self.get_lower_limit()[i])
                assert np.all(tmp_scaled_samples <= self.get_upper_limit()[i])
                scaled_samples[i, :] = tmp_scaled_samples

        return scaled_samples
