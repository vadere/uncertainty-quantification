import warnings
from typing import Union

import numpy as np
from numpy.random.mtrand import RandomState
from scipy import stats as scipy_stats

from uq.utils.prior.GaussianGenTruncMult import GaussianGenTruncMult


class GaussianGenTrunc(GaussianGenTruncMult):

    def __init__(self, mean: Union[float, np.ndarray], variance: [float, np.ndarray], limits: np.ndarray):
        super().__init__(mean=mean, variance=variance, limits=limits, dim=1)
        self.check_inputs_truncated(mean=mean, variance=variance, limits=limits, dim=1)

        # a = (limits[0] - mean) / variance
        # b = (limits[1] - mean) / variance
        # self.__a = a
        # self.__b = b

    # GETTERS

    def get_lower_limit(self) -> float:
        return super().get_lower_limit()

    def get_a(self) -> float:
        return super().get_a()

    def get_b(self) -> float:
        return super().get_b()

    def get_upper_limit(self) -> float:
        return super().get_upper_limit()

    def get_variance(self) -> float:
        return super().get_cov()

    # OTHERS

    def equals(self, other: "GaussianGenTrunc") -> bool:
        bool_equals = False
        if isinstance(other, GaussianGenTrunc):
            if self.get_mean() == other.get_mean() and self.get_variance() == other.get_variance() \
                    and self.get_lower_limit() == other.get_lower_limit() \
                    and self.get_upper_limit() == other.get_upper_limit() \
                    and self.get_a() == other.get_a() and self.get_b() == other.get_b():
                bool_equals = True
        return bool_equals

    def check_inputs_truncated(self, mean: float, variance: float, limits: np.ndarray, dim: int = 1) -> None:
        if variance < 0:
            raise ValueError('Deviation (%f) has to be >= 0.' % variance)
        if np.size(limits) is not 2:
            raise ValueError('Limits (%s) has to be 2-dim np.ndarray.' % np.asarray(limits))
        lower_limit = limits[0]
        upper_limit = limits[1]
        if lower_limit >= upper_limit:
            raise ValueError('Lower limit (%f) has to be smaller than upper limit (%f)' % (lower_limit, upper_limit))
        if lower_limit > mean:
            raise ValueError('Lower limit (%f) has to be <= mean (%f)' % (lower_limit, mean))
        if upper_limit < mean:
            raise ValueError('Upper limit (%f) has to be <= mean (%f)' % (upper_limit, mean))
        if variance is 0:
            raise ValueError('Variance for truncated distribution has to be larger than 0.')

    def eval_prior(self, value: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        pdf_at_x = scipy_stats.truncnorm.pdf(x=value, a=self.get_a(), b=self.get_b(), loc=self.get_mean(),
                                             scale=self.get_variance())
        return pdf_at_x

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        if random_state is None:
            warnings.warn("GaussianGenTrunc.sample() is called without a RandomState object!")
        samples = scipy_stats.truncnorm.rvs(self.get_a(), self.get_b(), size=n,
                                            random_state=random_state)  # standard-normal samples
        scaled_samples = samples * self.get_variance() + self.get_mean()

        assert (np.all(scaled_samples >= self.get_lower_limit()))
        assert (np.all(scaled_samples <= self.get_upper_limit()))

        return scaled_samples
