import warnings
from typing import Union

import numpy as np
from numpy.random.mtrand import RandomState
from scipy import stats as scipy_stats

from uq.utils.datatype import box, nr_entries, is_scalar, unbox
from uq.utils.prior.Prior import Prior


class UniformGenMult(Prior):
    """Multivariate uniform distribution (non-correlated variables) """

    def __init__(self, lower: Union[float, np.ndarray] = -1,
                 upper: Union[float, np.ndarray] = 1, dim: int = 1):
        super().__init__(dim=dim)
        self.check_inputs(lower=lower, upper=upper, dim=dim)
        self.__lower = lower
        self.__upper = upper

    # GETTERS

    def get_upper(self) -> Union[float, np.ndarray]:
        return self.__upper

    def get_lower(self) -> Union[float, np.ndarray]:
        return self.__lower

    def get_mean(self) -> np.ndarray:
        tmp = np.mean([self.get_lower(), self.get_upper()], axis=1)
        return tmp

    # OTHERS

    def to_str(self) -> str:
        name_str = str.replace(str(type(self))[1:-1],'\'', '')
        distribution_str = "%s, dim: %d, lower: %s, upper: %s" % \
                           (name_str, self.get_dim(), np.array2string(np.asarray(self.get_lower())),
                            np.array2string(np.asarray(self.get_upper())))

        return distribution_str

    def check_inputs(self, lower: Union[float, np.ndarray], upper: Union[float, np.ndarray], dim: int = 1):
        if type(lower) == float:
            lower = np.asarray(lower)
            upper = np.asarray(upper)
        if lower is not None and np.any(lower > upper):
            raise ValueError("Lower bound (%s) must be smaller than upper bound (%s)" %
                             (np.array2string(lower), np.array2string(upper)))
        if lower is not None and np.any(lower == upper):
            warnings.warn('Element of lower bound (%s) is equal to element of upper bound (%s)' %
                             (np.array2string(lower), np.array2string(upper)))
        if lower is not None and (dim is not np.size(lower) or dim is not np.size(upper)):
            raise ValueError("Dimension (%d) has to be consistent with size of parameter bounds (%s,%s)!" %
                             (dim, np.array2string(lower), np.array2string(upper)))

    def equals(self, other: "UniformGenMult") -> bool:
        bool_equals = False
        if isinstance(other, UniformGenMult):
            if (self.get_lower() == other.get_lower()).all() and \
                    (self.get_upper() == other.get_upper()).all() and self.get_dim() == other.get_dim():
                bool_equals = True
        return bool_equals

    def eval_prior(self, value: np.ndarray) -> np.ndarray:
        # todo: test
        if self.get_dim() > 1:
            if np.ndim(value) == 1:
                value = box(value)
            m = np.size(value, axis=0)
            n = np.size(value, axis=1)
            if m == n:
                nr_values = m
            elif m == self.get_dim():
                nr_values = n
            elif n == self.get_dim():
                nr_values = m
                # first dimension is supposed to be the dimension of x, [:,i] should give all realizations for one dim
                value = np.transpose(value)
            else:
                raise Exception("In one dimension, x needs to be equals to the dimension of the distribution")
        else:
            nr_values = nr_entries(value)
            while np.ndim(value) < 2:
                value = box(value)

        if is_scalar(value):
            value = box(value)

        pdf_val = np.zeros(shape=(self.get_dim(), nr_values))
        for i in range(0, self.get_dim()):
            dist = scipy_stats.uniform(loc=box(self.get_lower())[i],
                                       scale=box(self.get_upper())[i] - box(self.get_lower())[i])
            if nr_values == 1:
                pdf_val[i] = unbox(dist.pdf(value[i]))
            else:
                pdf_val[i] = unbox(dist.pdf(value[:, i]))

            # pdf_val[i] = scipy_stats.uniform.pdf(box(x)[i], loc=box(self.get_lower())[i], \
            # scale=box(self.get_upper())[i] - box(self.get_lower())[i])

        # return scipy_stats.uniform.pdf(x, loc=self.lower, scale=self.upper-self.lower)

        if self.get_dim() > 1:
            # todo: test
            joint_pdf_val = np.prod(pdf_val)
        else:
            joint_pdf_val = pdf_val

        assert joint_pdf_val.all() >= 0  # and np.prod(pdf_val) <= 1

        return joint_pdf_val

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        from uq.utils.prior.UniformGen import UniformGen

        if random_state is None:
            warnings.warn("Called without RandomState object. Consider adding a RandomState object.")
        if self.get_dim() == 1:
            sample_vec = UniformGen(lower=self.get_lower(), upper=self.get_upper()). \
                sample(n=n, random_state=random_state)
        else:
            sample_vec = np.zeros(shape=(self.get_dim(), n))
            for i in range(0, self.get_dim()):
                sample_vec[i, :] = UniformGen(lower=self.get_lower()[i], upper=self.get_upper()[i]) \
                    .sample(n=n, random_state=random_state)

        return sample_vec
