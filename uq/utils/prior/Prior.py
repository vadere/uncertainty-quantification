from typing import Union

import numpy as np
from numpy.random import RandomState


# todo define to_str method for all classes

class Prior(object):

    def __init__(self, dim: int = 1):
        self.__dim = dim

    def get_dim(self) -> int:
        return self.__dim

    def get_mean(self) -> float:
        pass

    def eval_prior(self, value: Union[float, np.ndarray]):
        pass

    def to_str(self) -> str:
        pass

    def equals(self, other: "Prior") -> bool:
        pass

    def sample(self, n: int, random_state: RandomState) -> Union[float, np.ndarray]:
        pass



