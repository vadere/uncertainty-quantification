import warnings
from typing import Union

import numpy as np
from numpy.random.mtrand import RandomState
from scipy import stats as scipy_stats

from uq.utils.prior.UniformGenMult import UniformGenMult


class UniformGen(UniformGenMult):
    """Uniform distribution"""

    def __init__(self, lower: Union[float, np.ndarray] = -1, upper: Union[float, np.ndarray] = 1):
        super().__init__(lower=lower, upper=upper, dim=1)

    def get_mean(self) -> Union[float, np.ndarray]:
        return np.mean([self.get_lower(), self.get_upper()])

    def equals(self, other: "UniformGen") -> bool:
        bool_equals = False
        if isinstance(other, UniformGen):
            if self.get_lower() == other.get_lower() and self.get_upper() == other.get_upper():
                bool_equals = True
        return bool_equals

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        if random_state is None:
            warnings.warn("UniformGen.sample(): random_state is None. Consider passing a RandomState element.")
            random_state = RandomState()
        samples = scipy_stats.uniform.rvs(loc=self.get_lower(), scale=self.get_upper() - self.get_lower(), size=n,
                                          random_state=random_state)
        # random_state.rand(n)*(self.get_upper()-self.get_lower()) + self.get_lower()
        return samples

    def eval_prior(self, value: Union[float, np.ndarray]) -> np.ndarray:
        pdf_at_value = scipy_stats.uniform.pdf(value, loc=self.get_lower(), scale=self.get_upper() - self.get_lower())
        return pdf_at_value
