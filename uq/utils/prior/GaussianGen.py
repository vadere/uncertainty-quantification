import warnings
from typing import Union

import numpy as np
from numpy.random.mtrand import RandomState
from scipy import stats as scipy_stats

from uq.utils.prior.GaussianGenMult import GaussianGenMult


class GaussianGen(GaussianGenMult):

    def __init__(self, mean: Union[float, np.ndarray] = 0.0, variance: Union[float, np.ndarray] = 1.0):
        super().__init__(mean=mean, variance=variance, dim=1)

    # GETTERS

    def get_deviation(self) -> Union[float, np.ndarray]:
        return super().get_cov()

    # OTHERS

    def equals(self, other: "GaussianGen"):
        bool_equals = False
        if isinstance(other, GaussianGen):
            if self.get_mean() == other.get_mean() and self.get_deviation() == other.get_deviation():
                bool_equals = True
        return bool_equals

    def eval_prior(self, value) -> float:
        print("GaussianGen: get_pdf()")
        return scipy_stats.norm.pdf(value, self.get_mean(), self.get_deviation())

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        if random_state is None:
            warnings.warn("Called without RandomState object. Consider adding a RandomState object.")
        samples = scipy_stats.norm.rvs(loc=self.get_mean(), scale=self.get_deviation(), size=n,
                                       random_state=random_state)
        return samples
