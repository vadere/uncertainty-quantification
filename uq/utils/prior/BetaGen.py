import warnings
from typing import Union

import numpy as np
from numpy.random import RandomState
from scipy import stats as scipy_stats

from uq.utils.prior.Prior import Prior


class BetaGen(Prior):

    def __init__(self, alpha: float, beta: float):
        super().__init__(dim=1)
        self.alpha = alpha
        self.beta = beta

    def get_alpha(self) -> float:
        return self.alpha

    def get_beta(self) -> float:
        return self.beta

    def equals(self, other: "BetaGen") -> bool:
        bool_equals = False
        if isinstance(other, BetaGen):
            if self.get_alpha() == other.get_alpha() and self.get_beta() == other.get_beta():
                bool_equals = True
        return bool_equals

    def eval_prior(self, value) -> float:
        print("GaussianGen: get_pdf()")
        return scipy_stats.beta.pdf(value, self.get_alpha(), self.get_beta())

    def sample(self, n: int, random_state: RandomState = None) -> Union[float, np.ndarray]:
        if random_state is None:
            warnings.warn("Called without RandomState object. Consider adding a RandomState object.")
        samples = scipy_stats.beta.rvs(a=self.get_alpha(), b=self.get_beta(), size=n,
                                       random_state=random_state)
        return samples
