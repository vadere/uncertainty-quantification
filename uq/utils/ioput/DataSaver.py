import os
import pickle
from datetime import datetime
from typing import Union, List

import matplotlib.pyplot as plt
import numpy as np
from SALib.util.results import ResultDict
from matplotlib.figure import Figure
from pandas import DataFrame, Series

from uq.propagation.calc.PropagationResult import PropagationResult
from uq.utils.data_eval import sample_mean, sample_std, sample_mode
from uq.utils.datatype import is_vector, unbox


class DataSaver:

    def __init__(self, in_path_to_tutorial: str = None, type_str: str = None):
        self.__type_str = type_str
        self.__create_output_dir(path2tutorial=in_path_to_tutorial)

    def get_type_str(self) -> str:
        return self.__type_str

    def get_path_to_files(self) -> str:
        return self.__path_to_files

    def __set_path_to_files(self, in_path_to_files: str) -> None:
        self.__path_to_files = in_path_to_files

    def __set_path_to_folder(self, path_to_folder: str) -> None:
        self.__path_to_folder = path_to_folder

    def __create_output_dir(self, path2tutorial: str) -> None:
        if "results" not in path2tutorial:
            path_folder = os.path.abspath(os.path.join(path2tutorial, 'results'))
        else:
            path_folder = os.path.abspath(path2tutorial)
        date_str = datetime.now().strftime('%Y-%m-%d_%H-%M-%S_%f')
        if self.get_type_str() is not None:
            path_files = os.path.join(path_folder, date_str + "_" + self.get_type_str())
        else:
            path_files = os.path.join(path_folder, date_str)

        if not (os.path.isdir(path_files)):
            os.makedirs(path_files)

        self.__set_path_to_files(path_files)
        self.__set_path_to_folder(path_folder)

    def write_nparray_to_file(self, variable: np.ndarray, name: str, key: str, qoi: str) -> None:
        # write samples and results to file
        f = open(os.path.join(self.get_path_to_files(), name + '.data'), 'w')
        f.write(repr(key) + "\t" + qoi + "\n")
        np.savetxt(f, np.transpose(variable))
        # f.write(repr(variable))
        f.close()

    def write_var_to_file(self, variable, name: str) -> None:
        f = open(os.path.join(self.get_path_to_files(), name + '.data'), 'a+')
        if type(variable) == np.ndarray:
            while is_vector(variable) and np.ndim(variable) > 1:
                variable = unbox(variable)
            if np.ndim(variable) > 2:
                np.savetxt(f, variable.flatten())
            else:
                np.savetxt(f, variable)
        elif type(variable) == list:
            for item in variable:
                f.write("%s\n" % item)
        else:
            if type(variable) == ResultDict:
                for item in variable.to_df():
                    f.write("%s\n" % item)
            else:
                f.write("%s\n" % variable)
        f.close()

    def write_model_eval_to_file(self, key: Union[List[str], str], qoi: str, parameters: np.ndarray,
                                 results: np.ndarray, bool_write: bool = True) -> None:
        if bool_write:
            filepath = os.path.join(self.get_path_to_files(), 'model_evaluations.data')
            bool_first = not (os.path.isfile(filepath))

            f = open(filepath, 'a+')

            if bool_first:
                # write header
                f.write(repr(key) + "\t" + qoi + "\n")

            for i in range(0, len(results)):  # len(parameters) does not work if any parameter setting fails!
                parameter_values = list(parameters[i].values())
                if type(results) == np.ndarray or type(results) == list:
                    qoi_values = np.array(results[i])
                else:
                    if type(results) == dict:
                        qoi_values = np.array([])
                        for key in results.keys():
                            tmp = np.array(results[key])
                            qoi_values = np.append(qoi_values, tmp)
                    else:
                        if type(results) == DataFrame:
                            qoi_values = results.loc[i]
                        else:
                            if type(results) == Series:
                                qoi_values = results.values[i]
                            else:
                                raise Warning("Datatype of results is not known / implemented.")

                f.write(' '.join(map(str, parameter_values)))
                f.write("\t")
                # todo: adapt for multi-dim QoI
                if np.ndim(qoi_values) == 0:
                    qoi_values = [qoi_values]
                f.write(' '.join(map(str, qoi_values)))
                f.write("\n")
            f.flush()
            f.close()

    def write_data_misfit_to_file(self, key: Union[List[str], str], qoi: str, parameters: np.ndarray,
                                  data_misfit: np.ndarray) -> None:
        # write to readable file for post-processing with 3rd party software
        filepath = os.path.join(self.get_path_to_files(), 'data_misfit.data')
        f = open(filepath, 'a+')
        # write header
        f.write(repr(key) + "\t Data misfit (" + qoi + ") \n")
        array_to_file = np.transpose(np.vstack((parameters, data_misfit)))
        f.write(' \n'.join(map(str, array_to_file)))
        f.flush()
        f.close()

        # write to pickle
        results = dict()
        results["parameters"] = parameters
        results["data_misfit"] = data_misfit
        results["key"] = key
        results["qoi"] = qoi

        with open(os.path.join(self.get_path_to_files(), 'data_misfit.pickle'), 'wb') as file:
            pickle.dump(results, file)

    def save_figure(self, handle: Figure, name: str, bool_save_data: bool = True):

        if bool_save_data and self.get_path_to_files() is not None:
            plt.savefig(os.path.join(self.get_path_to_files(), "fig_png_" + name + ".png"))
            plt.savefig(os.path.join(self.get_path_to_files(), "fig_pdf_" + name + ".pdf"))
            with open(os.path.join(self.get_path_to_files(), "fig_pickle_" + name + '_fig.pickle'),
                      'wb') as file:  # wb = write binary
                pickle.dump(handle, file)

            plt.close(handle)

    def save_to_pickle(self, path: str = None, data=None, name: str = None):
        if path is None and not self.get_path_to_files() is None:
            path = self.get_path_to_files()
        with open(os.path.join(path, name + ".pickle"), 'wb') as file:
            pickle.dump(data, file)

    # Propagation + Inversion - todo move to FileWriter

    def write_results_inversion_postprocessing(self, point_estimate_value: float, samples: np.ndarray,
                                               acceptance_rate: float) -> None:
        with open(os.path.join(self.get_path_to_files(), "results.txt"), 'a+') as file:
            file.write('\n** Results (Inversion) ** \n')
            file.write('Point estimate value: \t\t\t\t {} \n'.format(point_estimate_value))
            file.write('ABC Posterior Mean: \t\t\t\t {} \n'.format(sample_mean(samples)))
            file.write('ABC Posterior Mode: \t\t\t\t {} \n'.format(sample_mode(samples)))
            file.write('ABC Posterior Std: \t\t\t\t\t {} \n'.format(sample_std(samples)))
            file.write('Acceptance rate: \t\t\t\t\t {} \n'.format(acceptance_rate))
            file.close()


    def dump_var_to_file(self, var, name: str) -> None:
        with open(os.path.join(self.get_path_to_files(), name + ".pickle"), 'wb') as file:
            pickle.dump(var, file)

    def dump_model_to_file(self, model: "Model", name: str) -> None:

        with open(os.path.join(self.get_path_to_files(), name + ".pickle"), 'wb') as file:
            pickle.dump(model, file)
        with open(os.path.join(self.get_path_to_files(), name + ".pickle"), 'wb') as file:
            pickle.dump(model.get_map_results(), file)
