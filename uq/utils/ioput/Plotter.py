from uq.utils.calc.Parameter import Parameter
from uq.utils.calc.Result import Result


class Plotter:

    def __init__(self, param: Parameter, result: Result):
        self.__result = result
        self.__param = param

    # GETTER

    def get_result(self) -> Result:
        return self.__result

    def get_param(self) -> Parameter:
        return self.__param

    # SETTER

    def set_result(self, result: Result) -> None:
        self.__result = result
