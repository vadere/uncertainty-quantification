from uq.utils.calc.Parameter import Parameter
from uq.utils.calc.Result import Result

from uq.utils.ioput.DataSaver import DataSaver


class FileWriter:

    def __init__(self, param: Parameter, result: Result, data_saver: DataSaver):
        self.__param = param
        self.__result = result
        if data_saver is None:
            data_saver = param.get_model().get_data_saver()
        if data_saver is None:
            raise UserWarning('DataSaver cannot be None for FileWriter.')
        self.__data_saver = data_saver

    # GETTERS

    def get_datasaver(self) -> DataSaver:
        return self.__data_saver

    def get_param(self) -> Parameter:
        return self.__param

    def get_result(self) -> Result:
        return self.__result

    # SETTERS

    def set_result(self, result: Result) -> None:
        self.__result = result

    # OTHERS

    def write_result(self):
        pass
