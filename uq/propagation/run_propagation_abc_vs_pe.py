import os
import pickle
import shutil
import time
import warnings
from os import path

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import RandomState

from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.propagation.calc.PropagationResult import PropagationResult
from uq.propagation.calc.postprocessing_inversion import calc_pe_from_inversion, get_dimension, \
    calc_posterior_samples
from uq.propagation.calc.propagation_utils import regression_sklearn
from uq.propagation.ioput.FileWriterPropagation import FileWriterPropagation
from uq.propagation.ioput.plotting import plot_regression_sklearn, plot_regression_statsmodels
from uq.propagation.calc.propagation_utils import regression_statsmodels
from uq.utils.data_eval import sample_mean, sample_std, sample_mode
from uq.utils.datatype import unbox
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel

BOOL_PROPAGATION = True  # False: evaluation of existing propagation

# SUQC path of results
suqc_output_path = path.abspath("D:\\repo_checkout\\uncertainty-quantification-private\\uq\\propagation\\suqc_output\\")

# path to data from inversion
cur_dir = os.path.dirname(os.path.realpath(__file__))
folder_path_inversion = path.abspath(path.join(cur_dir, "../inversion/results"))
# folder_inversion_results = "2019-11-04_11-19-14_689486"  # 10^4 samples
# folder_inversion_results = "2020-07-09_17-43-48_200336"  # 10^5 samples (OSM)
# folder_inversion_results = "2020-08-21_15-50-59_971248"  # 10^5 samples -> GNM!
# folder_inversion_results = "2020-08-24_14-33-52_161981"  # 10^4 samples -> GNM!
# folder_inversion_results = "2020-08-27_12-11-52_039142"  # 10^4 samples -> GNM!
# folder_inversion_results = "2020-08-28_15-02-25_075906"  # 10^5 samples -> GNM!
# folder_inversion_results = "2020-08-28_15-02-25_075906"  # 10^5 samples -> GNM!
# folder_inversion_results = "2020-10-05_17-42-30_679189"  # vadere-mean (10^3 samples, 10 rep, OSM)
# folder_inversion_results = "2020-07-09_17-43-48_200336"  # 10^5 samples (OSM)

# paper
folder_inversion_results = "2020-10-19_17-25-45_929408"  # 10^5 samples (OSM) sum of squares (paper - case 1)
# folder_inversion_results = "test_folder_bimodal"  # just for testing of bimodal (paper - case 2 )
# folder_inversion_results = "2020-10-17_22-32-46_392912"  # 10^5 samples (OSM), sum of squares, 3-dim (paper - case 3)


# diss- new version of inversion
folder_inversion_results = "2021-04-28_16-59-26_253712"

bool_intercept = True

# folder_inversion_results = "2020-07-22_13-03-17_704100"  # 10^3 samples für _defaults scenario

# tolerance_vec = [0.6, 0.5, 0.4, 0.3, 0.2, 0.15, 0.1]  # ABC threshold
# tolerance_vec = [0.2, 0.15, 0.1, 0.075, 0.05, 0.025, 0.01]
# tolerance_vec = [0.6, 0.4, 0.2, 0.1]
# tolerance_vec = [0.0685]
tolerance_vec = [0.3]

N_samples_propagation_abc = 1000
N_samples_propagation_pe = 1000


no_runs_averaged = 1
bool_averaged = False

seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 2396730

path_to_propagation_results = path.abspath(path.join(cur_dir, "../inversion/results/x"))
folder_propagation_results = 'propagation'
widths = np.array([0.8, 0.9, 1.0, 1.1, 1.2])

# attributes for eval
col_point_estimate = 'forestgreen'
col_data = 'darkorange'
col_inversion = 'cornflowerblue'
label_pe = 'Point estimate'
label_abc = 'ABC inversion'
label_data = 'Experimental data'

if __name__ == "__main__":  # main required by Windows to run in parallel

    bool_first_tol = True
    path_inversion_data = path.abspath(path.join(folder_path_inversion, folder_inversion_results))

    # Load parameters and results from inversion
    inv_param = pickle.load(
        open(path.join(folder_path_inversion, folder_inversion_results, "inv_param.pickle"), "rb"))

    inv_result = pickle.load(
        open(path.join(folder_path_inversion, folder_inversion_results, "inv_result.pickle"), "rb"))

    # Load model evaluations
    # todo put in results object?
    model_evals_tmp = \
        np.loadtxt(path.join(folder_path_inversion, folder_inversion_results, "model_evaluations.data"), skiprows=1)

    for tolerance in tolerance_vec:

        try:  # Remove results
            shutil.rmtree(suqc_output_path)
        except OSError as e:
            print("Error: %s : %s" % (suqc_output_path, e.strerror))

        # Parameter setup
        random_state = RandomState(seed)
        data_saver = DataSaver(path.join(folder_path_inversion, folder_inversion_results, folder_propagation_results))

        data = inv_param.get_data()
        dim = get_dimension(inv_param)
        candidates = inv_result.get_candidates()
        distance_measure_results = inv_result.get_data_misfit()

        # Get point estimate from inversion data
        point_estimate_value, point_estimate_flow = calc_pe_from_inversion(inv_result=inv_result,
                                                                           inv_param=inv_param,
                                                                           model_evals=model_evals_tmp)

        # ------ start test todo: remove (test bimodal distribution)
        """N = np.size(distance_measure_results)
        sigma = 0.1
        mu1 = 0.9
        mu2 = 1.8
        tmp_part1 = random_state.randn(int(N / 2)) * sigma + mu1
        tmp_part2 = random_state.randn(int(N / 2)) * sigma + mu2
        constructed_candidates = np.concatenate([tmp_part1, tmp_part2])
        distance_measure_results["parameters"] = constructed_candidates
        f2 = 1 / np.sqrt(2 * np.pi * np.square(sigma)) * np.exp(
            -np.power((constructed_candidates - mu2), 2) / (2 * np.square(sigma)))
        f1 = 1 / np.sqrt(2 * np.pi * np.square(sigma)) * np.exp(
            -np.power((constructed_candidates - mu1), 2) / (2 * np.square(sigma)))
        constructed_misfit = 1 - (f1 + f2) / np.max(f1 + f2)
        distance_measure_results["data_misfit"] = constructed_misfit """
        # ----- end test

        # Get posterior samples for chosen tolerance
        samples, N_samples_posterior = calc_posterior_samples(inv_param=inv_param, inv_result=inv_result,
                                                              tolerance=tolerance)

        if N_samples_posterior == 0:
            continue  # jump to next loop iteration

        # write results of postprocessed inversion with the defined tolerance
        data_saver.write_results_inversion_postprocessing(point_estimate_value, samples,
                                                          N_samples_posterior / np.size(distance_measure_results))

        # propagation part
        propagation_param_abc = PropagationParameter(model=inv_param.get_model(), prior=samples, seed=seed,
                                                     bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                                     path2results=data_saver.get_path_to_files(),
                                                     n_samples=N_samples_propagation_abc, name="abc")

        propagation_param_pe = PropagationParameter(model=inv_param.get_model(), prior=samples, seed=seed,
                                                    bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                                    path2results=data_saver.get_path_to_files(),
                                                    n_samples=N_samples_propagation_pe, name="pe")

        propagation_result_pe = PropagationResult()
        propagation_result_abc = PropagationResult()

        file_writer_abc = FileWriterPropagation(param=propagation_param_abc, result=propagation_result_abc,
                                                data_saver=data_saver)
        file_writer_abc.write_parameters_propagation_after_inversion(seed=seed, tolerance=tolerance)

        file_writer_pe = FileWriterPropagation(param=propagation_param_pe, result=propagation_result_pe,
                                               data_saver=data_saver)
        file_writer_abc.write_parameters_propagation_after_inversion(seed=seed)

        if BOOL_PROPAGATION:
            start_time = time.time()
            # select samples for propagation of ABC posterior
            if N_samples_posterior < N_samples_propagation_abc * 0.9:  # if we have less samples than we want to draw
                N_samples_propagation_abc = N_samples_posterior
                N_samples_propagation_pe = N_samples_posterior
                warnings.warn("Number of posterior samples (%d) is significantly lower than " % N_samples_posterior
                              + " number of samples for propagation_old (%d)." % N_samples_propagation_abc
                              + "\nInstead only %d random samples will be propagated" % N_samples_posterior,
                              UserWarning)

            # random selection of samples
            # todo: extend for multi-dimensional input !
            idx_random_samples = random_state.randint(0, N_samples_posterior, N_samples_propagation_abc)
            if dim == 1:
                random_selected_posterior_samples = samples[idx_random_samples]
            elif dim > 1:
                random_selected_posterior_samples = samples[:, idx_random_samples]
            else:
                raise UserWarning('Dimension has to be >= 1 (dim = %d).' % dim)

            # Perform propagation
            vadere_model = inv_param.get_model()
            if ~hasattr(vadere_model, 'bool_fixed_seed'):
                vadere_model.bool_fixed_seed = False
            vadere_model.set_n_jobs(40)  # todo only for GNM to avoid memory allocation error

            if N_samples_propagation_abc <= 20 and isinstance(vadere_model, VadereModel):
                # vadere_model.run_local = True  # switch to local run
                vadere_model.run_local = False
            propagation_abc, _, _ = vadere_model.eval_model_averaged(
                parameter_value=random_selected_posterior_samples, nr_runs_averaged=no_runs_averaged,
                random_state=RandomState(seed))
            if len(propagation_abc) < N_samples_propagation_abc:
                warnings.warn("ABC: Some simulations failed, only %d (of %d) were successful."
                              % (len(propagation_abc), N_samples_propagation_abc), UserWarning)
                N_samples_propagation_abc = len(propagation_abc)  # correct length (in case some simulations failed)

            # Remove outputs
            try:
                shutil.rmtree(suqc_output_path)
            except OSError as e:
                print("Error: %s : %s" % (suqc_output_path, e.strerror))

            if dim == 1:
                point_estimate_repeated = np.repeat(point_estimate_value, N_samples_propagation_pe)
            elif dim > 1:
                point_estimate_repeated = np.repeat(np.expand_dims(point_estimate_value, axis=1),
                                                    N_samples_propagation_pe, axis=1)
            else:
                raise UserWarning('Dimension has to be >= 1 (dim=%d).' % dim)

            propagation_pe, _, _ = vadere_model.eval_model_averaged(parameter_value=point_estimate_repeated,
                                                                    nr_runs_averaged=no_runs_averaged,
                                                                    random_state=RandomState(seed))
            if len(propagation_pe) < N_samples_propagation_pe:
                warnings.warn("PE: Some simulations failed, only %d (of %d) were successful." % (
                    len(propagation_pe), N_samples_propagation_pe), UserWarning)
                N_samples_propagation_pe = len(propagation_pe)  # correct length (in case some simulations failed)

            print("Performed forward propagation_old for %d samples" % N_samples_propagation_abc)
            computation_time = (time.time() - start_time)/60.0
            print("Time for propagation (%d + %d): %.3f minutes" % (
                N_samples_propagation_abc, N_samples_propagation_pe, computation_time))
        else:
            # pure evaluation of existing propagation
            propagation_abc = pickle.load(
                open(path.join(path_to_propagation_results, 'propagation_old.pickle'), 'rb'))
            random_selected_posterior_samples = pickle.load(
                open(path.join(path_to_propagation_results, 'selected_samples.pickle'), 'rb'))
            N_samples_propagation_abc = len(propagation_abc)  # todo: check for other dimensions
            propagation_pe = point_estimate_flow
            computation_time = 0
            vadere_model = None

        # ******************* Evaluation

        if len(samples) <= 5:
            raise UserWarning('tolerance is too low only %d samples. ' % len(samples))

        # Regression
        # todo encapsulate returns
        _, _, mod_pe, res_pe, confidence_interval_pe, mod_data, res_data, ci_data, mod_abc, res_abc, \
        confidence_interval_abc, sample_mean_abc, sample_mean_pe = \
            regression_statsmodels(widths, N_samples_propagation_abc, propagation_abc, propagation_pe=propagation_pe,
                                   data=data, random_state=RandomState(seed), data_saver=data_saver,
                                   bool_intercept=bool_intercept)

        # Plot regression (SKLEARN)
        tmp_model_eval_widths, tmp_model_eval_propagation, regression_abc, regression_pe, regression_data = \
            regression_sklearn(widths, N_samples_propagation_abc, propagation_abc, propagation_pe=propagation_pe,
                               data=data)

        # *******************  Save results to file
        # todo: check for multi-dim in- and output
        # results_mat = np.vstack((random_selected_posterior_samples, model_eval_propagation))

        if BOOL_PROPAGATION:
            data_saver.save_to_pickle(data=random_selected_posterior_samples, name="selected_samples")
            data_saver.save_to_pickle(data=propagation_abc, name="propagation_selected_samples")
            data_saver.write_var_to_file(variable=seed, name="seed")
            data_saver.write_var_to_file(variable=propagation_pe, name="propagation_pe")
            data_saver.write_var_to_file(variable=propagation_abc, name="propagation_abc")

            data_saver.write_results_propagation_after_inversion(confidence_interval_pe=confidence_interval_pe,
                                                                 ci_data=ci_data,
                                                                 confidence_interval_abc=confidence_interval_abc,
                                                                 sample_mean_pe=sample_mean_pe, res_data=res_data,
                                                                 sample_mean_abc=sample_mean_abc,
                                                                 computation_time=computation_time,
                                                                 propagation_pe=propagation_pe,
                                                                 propagation_abc=propagation_abc,
                                                                 bool_intercept=bool_intercept)

        # ******************* Plot results

        # Plot histogram of selected posterior samples to see if the distribution changes to all posterior samples
        h = plt.figure()
        if dim == 1:
            plt.hist(samples, label='All posterior samples', density=True, alpha=0.5)
            plt.hist(random_selected_posterior_samples, density=True, label='Random selected posterior samples',
                     alpha=0.5)
            plt.xlabel("Free-flow speed [m/s]")
            plt.legend()
        elif dim > 1:
            for i in range(0, dim):
                plt.subplot(dim, 1, i + 1)
                plt.hist(samples[i, :], density=True, label='All posterior samples', alpha=0.5)
                plt.hist(random_selected_posterior_samples[i, :], density=True,
                         label='Random selected posterior samples', alpha=0.5)
                plt.legend()
                plt.xlabel(vadere_model.get_key()[i])

        data_saver.save_figure(h, "hist_selected_samples")

        # Boxplot for propagation_old results (over corridor width)
        h = plt.figure()
        plot_abc = plt.boxplot(propagation_abc, patch_artist=False, labels=widths)
        plot_pe = plt.boxplot(propagation_pe, patch_artist=False, labels=widths)
        colors = ['green', 'orange', 'lightgreen']
        for _, line_list in plot_abc.items():
            for line in line_list:
                line.set_color(col_inversion)
        for _, line_list in plot_pe.items():
            for line in line_list:
                line.set_color(col_point_estimate)

        if np.ndim(data) > np.ndim(np.arange(1, len(widths) + 1)):
            data = unbox(data)

        plt.plot(np.arange(1, len(widths) + 1), data, '.:', label=label_data, color=col_data)
        plt.xlabel('Bottleneck width [m]')
        plt.ylabel('Flow [1/s]')
        plt.legend()
        data_saver.save_figure(h, "boxplot_propagation")

        # Plot regression (STATSMODELS)

        # todo encapsulate parameters
        plot_regression_statsmodels(data_saver, data, tmp_model_eval_widths, tmp_model_eval_propagation, res_abc, res_pe,
                                    res_data, confidence_interval_abc, confidence_interval_pe, ci_data, widths,
                                    propagation_pe, col_inversion, col_data, col_point_estimate, label_abc, label_pe,
                                    label_data, bool_intercept)

        # Plot regression (SKLEARN)
        # todo encapsulate parameters
        plot_regression_sklearn(widths, data, propagation_pe, tmp_model_eval_widths, tmp_model_eval_propagation,
                                regression_abc, regression_pe, regression_data, data_saver, label_abc, label_data,
                                label_pe, col_inversion, col_data, col_point_estimate)

        # Evaluate histograms for each width
        for i in range(0, len(widths)):
            h = plt.figure()
            plt.hist(propagation_abc[:, i], label=label_abc, alpha=0.5, color=col_inversion)
            plt.hist(propagation_pe[:, i], label=label_pe, alpha=0.5, color=col_point_estimate)
            # plt.plot(point_estimate_flow[i] * np.ones(2), np.array(plt.gca().get_ylim()), color=col_point_estimate,
            #                      label=label_pe, linestyle='--')
            plt.plot(data[i] * np.ones(2), np.array(plt.gca().get_ylim()), color=col_data, label=label_data)

            plt.xlabel('Flow [1/s]')
            plt.legend()
            data_saver.save_figure(h, 'histogram_propagation_width_%.1f.png' % widths[i])

        bool_first_tol = False

    print("*** FINISHED ***")
