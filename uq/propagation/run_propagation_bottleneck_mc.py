import os
import shutil
import time
from os import path

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import RandomState

from uq.propagation.calc.PropagationCalculator import PropagationCalculator
from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.propagation.ioput.FileWriterPropagation import FileWriterPropagation
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel

from uq.utils.prior.PriorFactory import PriorFactory

config = 4

# Config 1: before SA
if config == 1:
    # parameters
    key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
           "attributesPedestrian.speedDistributionStandardDeviation",
           "sources.[*].spawnNumber", "obstPotentialHeight", "pedPotentialHeight",
           "minStepLength"]  # uncertain parameters
    dim = len(key)

    # prior
    x_lower = np.array([1.0, 0.5, 0.1, 40, 2.0, 5.0, 0.0])  # lower bounds for parameters
    x_upper = np.array([5.0, 2.2, 1.0, 80, 10.0, 50.0, 0.15])  # upper bounds for parameters
    prior_params = {"Type": "Uniform", "Low": x_lower, "High": x_upper, "Dim": dim}

elif config == 2:
    # Config 2: after factor fixing

    # choose uncertain parameters (to be propagated)
    key = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
           "obstPotentialHeight", "pedPotentialHeight"]

    # prior
    x_lower = np.array([0.5, 0.1, 2.0, 5.0])  # lower bounds for parameters
    x_upper = np.array([2.2, 1.0, 10.0, 50.0])  # upper bounds for parameters
    prior_params = {"Type": "Uniform", "Low": x_lower, "High": x_upper, "Dim": 3}

elif config == 3:
    # Config 3: after calibration of all three influential parameters - use posterior from inversion
    print("Not yet implemented")

elif config == 4:
    # propagate single uncertain parameter
    key = ["attributesPedestrian.speedDistributionMean"]
    x_lower = np.array([0.5])
    x_upper = np.array([2.2])
    prior_params = {"Type": "Uniform", "Low": x_lower, "High": x_upper, "Dim": 1}


    dim = len(key)


# choose quantity of interest (scenario file must provide the output)
qoi = "flow.txt"
qoi_dim = 5

# SUQC path of results
suqc_output_path = path.abspath("D:\\repo_checkout\\uncertainty-quantification-private\\uq\\propagation\\suqc_output\\")

# path to data from inversion
cur_dir = os.path.dirname(os.path.realpath(__file__))

N_samples_propagation = 1000
N_samples_propagation = 100

rho = PriorFactory().create_prior_by_type(params=prior_params)

# model
run_local = False
path2tutorial = os.path.dirname(os.path.realpath(__file__))
path2model = os.path.join(path2tutorial, "../scenarios", "vadere-console.jar")
path2scenario = os.path.abspath("../scenarios/bottleneck_OSM_all_in_one_N60.scenario")

no_runs_averaged = 1
bool_averaged = False

seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 2396730

path_to_propagation_results = path.abspath(path.join(cur_dir, "../inversion/results/x"))
folder_propagation_results = 'propagation'
widths = np.array([0.8, 0.9, 1.0, 1.1, 1.2])

# attributes for eval
# col_point_estimate = 'forestgreen'
# col_data = 'darkorange'
# col_inversion = 'cornflowerblue'


if __name__ == "__main__":  # main required by Windows to run in parallel

    start_time = time.time()

    try:  # Remove results
        shutil.rmtree(suqc_output_path)
    except OSError as e:
        print("Error: %s : %s" % (suqc_output_path, e.strerror))

    # Parameter setup
    random_state = RandomState(seed)
    data_saver = DataSaver(os.getcwd())

    # configure model
    vadere = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                         n_jobs=-1, log_lvl="OFF", qoi_dim=qoi_dim)
    vadere.set_data_saver(data_saver)

    # propagation part
    propagation_param = PropagationParameter(model=vadere, prior=rho, seed=seed,
                                             bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                             path2results=data_saver.get_path_to_files(),
                                             n_samples=N_samples_propagation, name="propagation")

    propagation_calc = PropagationCalculator(param=propagation_param)
    propagation_calc.propagate()
    propagation_result = propagation_calc.get_result()

    file_writer = FileWriterPropagation(param=propagation_param, result=propagation_result,
                                        data_saver=data_saver)


    print("Performed forward propagation_old for %d samples" % N_samples_propagation)
    computation_time = (time.time() - start_time) / 60.0
    print("Time for propagation (%d): %.3f minutes" % (N_samples_propagation, computation_time))

    # *******************  Save results to file
    # todo: check for multi-dim in- and output
    # results_mat = np.vstack((random_selected_posterior_samples, model_eval_propagation))

    # todo: move to filewriter
    data_saver.save_to_pickle(data=propagation_result.get_prior_samples(), name="prior_samples")
    data_saver.save_to_pickle(data=propagation_result.get_qoi_samples(), name="qoi_samples")
    data_saver.write_var_to_file(variable=propagation_result.get_prior_samples(), name="prior_samples")
    data_saver.write_var_to_file(variable=propagation_result.get_qoi_samples(), name="qoi_samples")
    data_saver.write_var_to_file(variable=seed, name="seed")

    # ******************* Plot results

    # todo move to plotter
    # Plot histogram of selected posterior samples to see if the distribution changes to all posterior samples
    h = plt.figure()
    if dim == 1:
        plt.hist(propagation_result.get_prior_samples(), label='Prior samples', density=True, alpha=0.5)
        plt.xlabel("Free-flow speed [m/s]")
        plt.legend()
    elif dim > 1:
        for i in range(0, dim):
            plt.subplot(dim, 1, i + 1)
            plt.hist(propagation_result.get_prior_samples()[i, :], density=True, label='All posterior samples',
                     alpha=0.5)
            plt.legend()
            plt.xlabel(vadere.get_key()[i])

    data_saver.save_figure(h, "hist_selected_samples")

    # Boxplot for propagation_old results (over corridor width)
    h = plt.figure()
    plot_abc = plt.boxplot(propagation_result.get_qoi_samples(), patch_artist=False, labels=widths)

    plt.xlabel('Bottleneck width [m]')
    plt.ylabel('Flow [1/s]')
    plt.legend()
    data_saver.save_figure(h, "boxplot_propagation")

    # Plot regression (STATSMODELS)

    # Evaluate histograms for each width
    for i in range(0, len(widths)):
        h = plt.figure()
        plt.hist(propagation_result.get_qoi_samples()[:, i], alpha=0.5)
        plt.xlabel('Flow [1/s]')
        plt.legend()
        data_saver.save_figure(h, 'histogram_propagation_width_%.1f.png' % widths[i])

    plt.show()

    print("*** FINISHED ***")
