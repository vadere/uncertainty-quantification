import warnings
import time
import numpy as np
from numpy.random import RandomState
import chaospy as cp

from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.propagation.calc.PropagationResult import PropagationResult
from uq.propagation.ioput.FileWriterPropagation import FileWriterPropagation
from uq.utils.calc.Calculator import Calculator


class PropagationCalculator(Calculator):
    def __init__(self, param: PropagationParameter, result: PropagationResult = None):
        if result is None:
            result = PropagationResult()
        super().__init__(param=param, result=result)

    # GETTER
    def get_param(self) -> PropagationParameter:
        return super().get_param()

    def get_result(self) -> PropagationResult:
        return super().get_result()

    # OTHER

    def generate_samples(self, random_state: RandomState, distribution=None) -> np.ndarray:
        param = self.get_param()
        # Create samples from prior distribution
        # todo: offer various sampling methods e.g. LHS

        n_samples = param.get_n_samples()
        prior = param.get_prior()
        nodes = prior.sample(n=n_samples, random_state=random_state)  # Monte Carlo Sampling

        weights = None

        return nodes, weights

    def propagate(self, prior_samples: np.ndarray = None):
        start = time.time()
        param = self.get_param()
        random_state = RandomState(param.get_seed())

        if prior_samples is None:
            prior_samples = self.generate_samples(random_state=random_state)

        # Evaluate model at samples
        model = param.get_model()
        qoi_averaged, _, _ = model.eval_model_averaged(parameter_value=prior_samples,
                                                       nr_runs_averaged=param.get_no_runs_averaged(),
                                                       random_state=random_state)

        if len(qoi_averaged) < self.get_param().get_n_samples():
            warnings.warn("ABC: Some simulations failed, only %d (of %d) were successful."
                          % (len(qoi_averaged), self.get_param().get_n_samples()), UserWarning)

        n_samples_propagation = len(qoi_averaged)  # correct length (in case some simulations failed)

        # Save results

        result = self.get_result()
        result.set_computation_time(computation_time=(time.time() - start) / 60.0)
        result.set_qoi_samples(qoi_averaged)
        result.set_prior_samples(prior_samples)

        if param.is_write_data():
            writer = FileWriterPropagation(param, result)
            writer.save_results_to_file()

        return qoi_averaged, n_samples_propagation, prior_samples
