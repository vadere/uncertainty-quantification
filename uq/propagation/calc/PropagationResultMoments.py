from typing import Union

import chaospy as cp
import numpy as np

from uq.propagation.calc.PropagationResult import PropagationResult


class PropagationResultMoments(PropagationResult):
    def __init__(self, prior_samples: np.ndarray = None, qoi_samples: np.ndarray = None, n_samples: int = None):
        super().__init__(prior_samples=prior_samples, qoi_samples=qoi_samples, n_samples=n_samples)
        self.__expected_response = None
        self.__std_response = None
        self.__expansion = None
        self.__qoi_dist = None

    # GETTERS

    def get_expected_response(self) -> Union[np.ndarray, float]:
        return self.__expected_response

    def get_std_response(self) -> Union[np.ndarray, float]:
        return self.__std_response

    def get_expansion(self) -> cp.ndpoly:
        return self.__expansion

    def get_qoi_dist(self):
        return self.__qoi_dist

    # SETTERS

    def set_expected_response(self, expected_response: Union[np.ndarray, float]) -> None:
        self.__expected_response = expected_response

    def set_std_response(self, std_response: Union[np.ndarray, float]) -> None:
        self.__std_response = std_response

    def set_expansion(self, expansion: cp.ndpoly) -> None:
        self.__expansion = expansion

    def set_qoi_dist(self, qoi_dist) -> None:
        self.__qoi_dist = qoi_dist
