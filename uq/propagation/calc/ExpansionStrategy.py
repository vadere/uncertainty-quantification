from typing import Tuple

import numpy as np
from numpoly import ndpoly
from numpy.random import RandomState

from uq.propagation.calc.PropagationParameterGPCE import PropagationParameterGPCE


class ExpansionStrategy:

    def __init__(self, param: PropagationParameterGPCE):
        self.__param = param

    # GETTERS
    def get_param(self) -> PropagationParameterGPCE:
        return self.__param

    # OTHERS
    def calc_gpce(self, expansion: ndpoly, qoi_averaged: np.ndarray, nodes: np.ndarray,
                  weights: np.ndarray = None) -> ndpoly:
        pass

    def generate_samples(self, random_state: RandomState, distribution=None) -> Tuple[np.ndarray, np.ndarray]:
        pass
