from uq.utils.calc.Parameter import Parameter
from uq.utils.prior.Prior import Prior


class PropagationParameter(Parameter):

    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, n_samples: int, name: str = None, bool_write_data: bool = True):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results)
        self.__n_samples = n_samples
        self.__name = name
        self.__bool_write_data = bool_write_data

    # GETTERS
    def get_n_samples(self) -> int:
        return self.__n_samples

    def get_name(self) -> str:
        return self.__name

    def is_write_data(self) -> bool:
        return self.__bool_write_data
