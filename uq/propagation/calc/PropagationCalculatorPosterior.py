import warnings

import numpy as np
from numpy.random import RandomState

from uq.propagation.calc.PropagationCalculator import PropagationCalculator
from uq.propagation.calc.PropagationParameterPosterior import PropagationParameterPosterior
from uq.propagation.calc.PropagationResult import PropagationResult


class PropagationCalculatorPosterior(PropagationCalculator):
    # special case in which we already have posterior samples

    def __init__(self, param: PropagationParameterPosterior, result: PropagationResult = None):
        super().__init__(param=param, result=result)

    # GETTERS
    def get_param(self) -> PropagationParameterPosterior:
        return super().get_param()

    # OTHERS

    def generate_samples(self, random_state: RandomState) -> np.ndarray:
        # instead of generation of new samples, use posterior samples

        posterior_samples_input = self.get_param().get_posterior_samples()
        n_samples = self.get_param().get_n_samples()
        dim = self.get_param().get_model().get_dimension()
        n_samples_posterior = int(np.size(posterior_samples_input) / dim)
        if n_samples_posterior < n_samples * 0.9:  # if we have less samples than we want to draw
            n_samples = n_samples_posterior
            warnings.warn("Number of posterior samples (%d) is significantly lower than" % n_samples_posterior
                          + " number of samples for propagation_old (%d)" % n_samples
                          + "\n Instead only %d random samples will be propagated" % n_samples_posterior,
                          UserWarning)

        # random selection of samples
        # todo: extend for multi-dimensional input !
        idx_random_samples = random_state.randint(0, n_samples_posterior, n_samples)
        if dim == 1:
            posterior_samples = posterior_samples_input[idx_random_samples]
        elif dim > 1:
            posterior_samples = posterior_samples_input[:, idx_random_samples]
        else:
            raise UserWarning('Dimension has to be >= 1 (dim = %d).' % dim)

        return posterior_samples
