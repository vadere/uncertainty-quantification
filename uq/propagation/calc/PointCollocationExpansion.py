import chaospy as cp
import numpy as np
from numpoly import ndpoly
from numpy.random import RandomState

from uq.propagation.calc.ExpansionStrategy import ExpansionStrategy
from uq.propagation.calc.PropagationParameterGPCE import PropagationParameterGPCE


class PointCollocationExpansion(ExpansionStrategy):
    # PC: point collocation approach

    def __init__(self, param: PropagationParameterGPCE):
        super().__init__(param=param)

    # GETTERS
    def get_param(self) -> PropagationParameterGPCE:
        return super().get_param()

    # OTHERS
    def calc_gpce(self, expansion: ndpoly, qoi_averaged: np.ndarray, nodes: np.ndarray,
                  weights: np.ndarray = None) -> ndpoly:
        # fit regression
        qoi_gpce = cp.fit_regression(expansion, nodes, qoi_averaged)
        return qoi_gpce

    def generate_samples(self, random_state: RandomState, distribution=None) -> np.ndarray:
        param = self.get_param()
        # Create samples from prior distribution
        # todo: offer various sampling methods e.g. LHS

        n_samples = param.get_n_samples()
        prior = param.get_prior()
        nodes = prior.sample(n=n_samples, random_state=random_state)  # Monte Carlo Sampling

        weights = None

        return nodes, weights
    