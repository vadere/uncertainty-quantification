import numpy as np

from uq.utils.calc.Result import Result


class PropagationResult(Result):
    def __init__(self, prior_samples: np.ndarray = None, qoi_samples: np.ndarray = None, n_samples: int = None):
        super().__init__()
        self.__prior_samples = prior_samples  # input
        self.__qoi_samples = qoi_samples  # output of the model
        self.__n_samples = n_samples

    # GETTERS
    def get_qoi_samples(self) -> np.ndarray:
        return self.__qoi_samples

    def get_prior_samples(self) -> np.ndarray:
        return self.__prior_samples

    def get_n_samples(self) -> int:
        return self.__n_samples

    # SETTERS
    def set_qoi_samples(self, qoi_samples: np.ndarray) -> None:
        self.__qoi_samples = qoi_samples
        self.set_n_samples(len(qoi_samples))

    def set_prior_samples(self, prior_samples: np.ndarray) -> None:
        self.__prior_samples = prior_samples

    def set_n_samples(self, n_samples: int) -> None:
        self.__n_samples = n_samples
