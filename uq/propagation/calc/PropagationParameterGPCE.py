from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.utils.prior.Prior import Prior


class PropagationParameterGPCE(PropagationParameter):

    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, n_samples: int, expansion_order: int, name: str = None,
                 bool_write_data: bool = True):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results, n_samples=n_samples,
                         bool_write_data=bool_write_data, name=name)
        self.__expansion_order = expansion_order

    # GETTERS

    def get_expansion_order(self) -> int:
        return self.__expansion_order
