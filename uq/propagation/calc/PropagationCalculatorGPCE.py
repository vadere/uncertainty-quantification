import time

import chaospy as cp
import numpy as np
from numpoly import ndpoly
from numpy.random import RandomState

from uq.propagation.calc.PropagationCalculator import PropagationCalculator
from uq.propagation.calc.PropagationParameterGPCE import PropagationParameterGPCE
from uq.propagation.calc.PropagationResultGPCE import PropagationResultGPCE
from uq.propagation.ioput.FileWriterPropagationGPCE import FileWriterPropagationGPCE
from uq.utils.prior.GaussianGenMult import GaussianGenMult
from uq.utils.prior.GaussianGenTrunc import GaussianGenTrunc
from uq.utils.prior.UniformGenMult import UniformGenMult


class PropagationCalculatorGPCE(PropagationCalculator):
    # GPCE: generalized polynomial chaos expansion

    def __init__(self, param: PropagationParameterGPCE, result: PropagationResultGPCE = None):
        if result is None:
            result = PropagationResultGPCE()
        super().__init__(param=param, result=result)
        self.__gpce_strategy = None

    # GETTERS
    def get_param(self) -> PropagationParameterGPCE:
        return super().get_param()

    def get_result(self) -> PropagationResultGPCE:
        return super().get_result()

    def get_gpce_strategy(self):
        return self.__gpce_strategy

    # SETTERS
    def set_gpce_strategy(self, gpce_strategy) -> None:
        self.__gpce_strategy = gpce_strategy

    # OTHERS

    def calc_gpce(self, expansion, qoi_averaged: np.ndarray, nodes: np.ndarray, weights: np.ndarray = None) -> ndpoly:
        pass

    def generate_samples(self, random_state: RandomState, distribution) -> np.ndarray:
        pass

    def propagate(self, prior_samples: np.ndarray = None):

        start = time.time()
        param = self.get_param()
        random_state = RandomState(param.get_seed())

        # generate cp distribution from prior
        distribution = self.translate_prior_to_chaospy_distribution()

        nodes, weights = self.get_gpce_strategy().generate_samples(random_state, distribution)

        qoi_averaged, n_samples_propagation, prior_samples = super().propagate(prior_samples=nodes)

        # Generate expansion
        expansion = cp.generate_expansion(param.get_expansion_order(), distribution)

        # Generate the general polynomial chaos expansion polynomial
        qoi_gpce = self.get_gpce_strategy().calc_gpce(expansion=expansion, qoi_averaged=qoi_averaged, nodes=nodes, weights=weights)

        # Modes of response distribution
        expected = cp.E(qoi_gpce, distribution)
        std = cp.Std(qoi_gpce, distribution)

        # Generate qoi samples from gpce
        qoi_dist = cp.descriptives.QoI_Dist(qoi_gpce, distribution, sample=1000)

        # Save results

        result = self.get_result()
        result.set_computation_time(computation_time=(time.time() - start) / 60.0)
        result.set_qoi_samples(qoi_averaged)
        result.set_prior_samples(nodes)
        result.set_expected_response(expected)
        result.set_std_response(std)

        result.set_expansion(qoi_gpce)
        result.set_qoi_dist(qoi_dist)

        if param.is_write_data():
            writer = FileWriterPropagationGPCE(param, result)
            writer.write_parameters()
            writer.save_results_to_file()
            writer.write_result()

    def translate_prior_to_chaospy_distribution(self):
        prior = self.get_param().get_prior()
        dim = self.get_param().get_model().get_dimension()

        tmp_distribution = list()
        for i in range(0, dim):
            if isinstance(prior, UniformGenMult):
                lower = prior.get_lower()
                upper = prior.get_upper()
                tmp_distribution.append(cp.Uniform(lower=lower[i], upper=upper[i]))
            elif isinstance(prior, GaussianGenMult):
                mean = prior.get_mean()
                variance = prior.get_cov()
                tmp_distribution.append(cp.Normal(mu=mean[i], sigma=variance[i]))

            elif isinstance(prior, GaussianGenTrunc):
                mean = prior.get_mean()
                variance = prior.get_variance()
                lower = prior.get_lower_limit()
                upper = prior.get_upper_limit()
                tmp_distribution.append(cp.TruncNormal(lower=lower[i], upper=upper[i], mu=mean[i], sigma=variance[i]))

        distribution = cp.J(*tmp_distribution)

        return distribution
