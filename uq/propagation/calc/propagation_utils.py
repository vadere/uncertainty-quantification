import numpy as np
import scipy.stats
from matplotlib import pyplot as plt
from numpy.random import RandomState
from sklearn.linear_model import LinearRegression
from statsmodels import api as sm
from typing import Tuple

from statsmodels.base.wrapper import ResultsWrapper
from statsmodels.genmod.generalized_linear_model import GLM
from statsmodels.tools.sm_exceptions import PerfectSeparationError

from uq.utils.datatype import unbox


def regression_sklearn(widths: np.ndarray, n_samples_propagation: int, propagation_abc: np.ndarray,
                       propagation_pe: np.ndarray, data: np.ndarray):
    widths_reshaped = np.transpose(np.array([np.repeat(widths, n_samples_propagation)]))
    propagation_abc_reshaped = np.transpose(np.array([np.transpose(propagation_abc).flatten()]))
    propagation_pe_reshaped = np.transpose(np.array([np.transpose(propagation_pe).flatten()]))

    regression_abc = LinearRegression().fit(widths_reshaped, propagation_abc_reshaped)
    regression_pe = LinearRegression().fit(widths_reshaped, propagation_pe_reshaped)
    regression_data = LinearRegression().fit(np.transpose(np.array([widths])), np.transpose(data))

    return widths_reshaped, propagation_abc_reshaped, regression_abc, regression_pe, regression_data


def linear_regression_statsmodels(x: np.ndarray, y: np.ndarray, bool_intercept: bool = True) \
        -> Tuple[GLM, ResultsWrapper, np.ndarray]:
    if bool_intercept:
        model = sm.GLM(y, sm.add_constant(x))
        try:
            model.fit()
        except ValueError:
            print("ValueError")
        regression = model.fit()
        confidence_interval = regression.conf_int()[1]  # with y-intercept
    else:
        model = sm.GLM(y, x)  # without y-intercept
        regression = model.fit()
        confidence_interval = regression.conf_int()[0]  # without y-intercept

    # plot results
    # from statsmodels.graphics.api import abline_plot
    # fig, ax = plt.subplots()
    # ax.scatter(x, y)
    # abline_plot(model_results=regression, ax=ax)
    # plt.show()

    return model, regression, confidence_interval


def predict_regression_statsmodels(result: ResultsWrapper, x: np.ndarray, bool_intercept: bool = True) -> np.ndarray:
    if bool_intercept:
        y = result.predict(sm.add_constant(x))
    else:
        y = result.predict(x)
    return y


def mean_confidence_interval(data: np.ndarray, confidence: float = 0.95) -> Tuple[float, np.ndarray]:
    # https://stackoverflow.com/questions/15033511/compute-a-confidence-interval-from-sample-data
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
    confidence_interval = np.array([m - h, m + h])
    sample_mean = m
    return sample_mean, confidence_interval


def estimate_confidence_interval(x: np.ndarray, y: np.ndarray, N_averages: int, random_state: RandomState,
                                 data_saver: "DataSaver" = None, bool_intercept: bool = True) \
        -> Tuple[float, np.ndarray]:
    print("N_averages for estimation of CI: %d"%N_averages)
    # estimate confidence interval for regression with multiple datapoints so that is comparable to regression with only
    # a few datapoints
    slope = -1 * np.ones(N_averages)
    yintercept = -1 * np.ones(N_averages)
    ci_min = -1 * np.ones(N_averages)
    ci_max = -1 * np.ones(N_averages)
    N = len(x)

    for i in range(0, N_averages):
        idx_random_sample = random_state.randint(0, len(y), N)
        random_sample_flow = y[
            idx_random_sample, range(0, N)]  # choose results for one simulation run (5 different widths)
        model, regr, ci = linear_regression_statsmodels(x, random_sample_flow, bool_intercept)
        if bool_intercept:
            slope[i] = regr.params[1]
            yintercept[i] = regr.params[0]
        else:
            slope[i] = regr.params[0]
            yintercept[i] = 0
        ci_min[i] = ci[0]
        ci_max[i] = ci[1]

    sample_mean, confidence_interval = mean_confidence_interval(slope)
    mean_ci = np.array([np.mean(ci_min), np.mean(ci_max)])

    if data_saver is not None:
        h1 = plt.figure()
        plt.hist(slope)
        plt.xlabel("Slope (specific flow) [1/(m s)]")
        data_saver.save_figure(h1, "hist_slopes_regression")
        plt.close()

        h2 = plt.figure()
        plt.hist(ci_min, alpha=0.5, label='Minimum of confidence interval')
        plt.hist(ci_max, alpha=0.5, label='Maximum of confidence interval')
        plt.xlabel("Slope (specific flow) [1/(m s)]")
        data_saver.save_figure(h2, "hist_ci_slopes_regression")

        h3 = plt.figure()
        plt.hist(ci_max - ci_min, alpha=0.5,
                 label='Size of confidence interval\nMean %s \nSize mean interval %s' % (
                     np.array2string(np.mean(ci_max - ci_min)), np.array2string(np.diff(mean_ci))))
        plt.xlabel("Size of confidence interval for slope (specific flow) [1/(m s)]")
        plt.legend()
        data_saver.save_figure(h3, "hist_size_ci_slopes_regression")

    return sample_mean, mean_ci


def regression_statsmodels(widths: np.ndarray, n_samples_propagation: int, propagation_abc: np.ndarray,
                           propagation_pe: np.ndarray, data: np.ndarray, random_state: RandomState,
                           data_saver: "DataSaver" = None, bool_intercept: bool = True):
    n_averages = int(n_samples_propagation / 5)  # number of times the confidence interval is calculated and averaged

    widths_reshaped = np.transpose(np.array([np.repeat(widths, n_samples_propagation)]))
    propagation_abc_reshaped = np.transpose(np.array([np.transpose(propagation_abc).flatten()]))

    # 1) Calculate a confidence interval for ABC inversion data (slope of regression) that is comparable to data

    mod_abc, regr_abc, _ = linear_regression_statsmodels(widths_reshaped, propagation_abc_reshaped,
                                                         bool_intercept)

    slope_abc, ci_abc = estimate_confidence_interval(x=widths, y=propagation_abc, N_averages=int(
        n_samples_propagation / 5), random_state=random_state, data_saver=data_saver, bool_intercept=bool_intercept)

    if bool_intercept:
        print("* Slope parameter from single regression: %f " % regr_abc.params[1])
    else:
        print("* Slope parameter from single regression: %f " % regr_abc.params[0])

    print("* Sample mean of slope from multiple regressions: %f " % slope_abc)
    print("* Estimated confidence interval from multiple regressions: [%f, %f] " % (
        ci_abc[0], ci_abc[1]))

    # 2) Calculate a confidence interval for PE inversion data (slope of regression) that is comparable to data

    propagation_pe_reshaped = np.transpose(np.array([np.transpose(propagation_pe).flatten()]))

    mod_pe, regr_pe, _ = linear_regression_statsmodels(widths_reshaped, propagation_pe_reshaped,
                                                       bool_intercept)

    slope_pe, ci_pe = estimate_confidence_interval(x=widths, y=propagation_pe,
                                                   N_averages=n_averages,
                                                   random_state=random_state, data_saver=None,
                                                   bool_intercept=bool_intercept)

    # 3) Calculate a confidence interval for data (slope of regression)

    if not np.ndim(widths) == np.ndim(data):
        if np.ndim(widths) < np.ndim(data):
            data = unbox(data)

    regr_data = None
    ci_data = None
    slope_data = None
    try:
        mod_data, regr_data, ci_data = linear_regression_statsmodels(widths, data, bool_intercept)
        if bool_intercept:
            slope_data = regr_data.params[1]
        else:
            slope_data = regr_data.params[0]
    except PerfectSeparationError:
        print("Perfect separation - Data has linear relationship")

    # plot results
    # from statsmodels.graphics.api import abline_plot
    # fig, ax = plt.subplots()
    # ax.scatter(widths_reshaped, propagation_abc_reshaped, 'propagation of abc posterior samples')
    # abline_plot(model_results=regr_abc, ax=ax, color='red', label='single regression')
    # res_abc_cpy = deepcopy(regr_abc)
    # res_abc_cpy.params[1] = slope_abc
    # abline_plot(model_results=res_abc_cpy, ax=ax, color='green', label='slope_abc (slope)')
    # plt.legend()
    # plt.show()

    # todo: wrap in results object
    return widths_reshaped, propagation_abc_reshaped, regr_pe, ci_pe, regr_data, ci_data, regr_abc, ci_abc, slope_abc, slope_pe, slope_data
