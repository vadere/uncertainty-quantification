import numpy as np

from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.utils.prior.Prior import Prior


class PropagationParameterPosterior(PropagationParameter):

    def __init__(self, model: "Model", prior: Prior, seed: int, bool_averaged: bool, no_runs_averaged: int,
                 path2results: str, n_samples: int, posterior_samples: np.ndarray, name: str = None):
        super().__init__(model=model, prior=prior, seed=seed, bool_averaged=bool_averaged,
                         no_runs_averaged=no_runs_averaged, path2results=path2results, n_samples=n_samples, name=name)
        self.__posterior_samples = posterior_samples

    # GETTERS
    def get_posterior_samples(self) -> np.ndarray:
        return self.__posterior_samples
