import warnings

import numpy as np
from numpy.random import RandomState

from uq.propagation.calc.PropagationCalculator import PropagationCalculator
from uq.propagation.calc.PropagationParameterPosterior import PropagationParameterPosterior
from uq.propagation.calc.PropagationResult import PropagationResult


class PropagationCalculatorPointEstimate(PropagationCalculator):
    # special case in which we have one posterior sample that we want to propagate

    def __init__(self, param: PropagationParameterPosterior, result: PropagationResult = None):
        super().__init__(param=param, result=result)

    # GETTERS
    def get_param(self) -> PropagationParameterPosterior:
        return super().get_param()

    # OTHERS

    def generate_samples(self, random_state: RandomState) -> np.ndarray:
        # repeat point estimate a given number of times

        point_estimate = self.get_param().get_posterior_samples()
        n_samples_pe = int(np.size(point_estimate) / self.get_param().get_model().get_dimension())
        if n_samples_pe > 1:
            raise UserWarning('should only be 1 sample (%d samples passed).' % n_samples_pe)
        n_samples = self.get_param().get_n_samples()
        dim = self.get_param().get_model().get_dimension()

        if dim == 1:
            point_estimate_repeated = np.repeat(point_estimate, n_samples)
        elif dim > 1:
            point_estimate_repeated = np.repeat(np.expand_dims(point_estimate, axis=1),
                                                n_samples, axis=1)
        else:
            raise UserWarning('Dimension has to be >= 1 (dim=%d).' % dim)

        return point_estimate_repeated
