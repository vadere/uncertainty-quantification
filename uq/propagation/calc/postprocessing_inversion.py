import numpy as np
from numpy.random import RandomState

from uq.inversion.calc.InversionParameter import InversionParameter
from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult

import matplotlib.pyplot as plt


def calc_pe_from_inversion(inv_result: InversionResult, inv_param: InversionParameter,
                           model_evals: np.ndarray, model: "Model" = None, random_state: RandomState = None):
    # Define point estimate as sample with minimum distance
    distance_measure_results = inv_result.get_data_misfit()
    candidates = inv_result.get_candidates()
    idx = np.argmin(distance_measure_results)
    dim = inv_param.get_model().get_dimension()
    if dim > 1:
        point_estimate_value = candidates[:, idx]
    elif dim == 1:
        point_estimate_value = candidates[idx]
    else:
        raise UserWarning('Dimension has to be >=1. (dim= %d)' % dim)
    # point_estimate_value = 1.121085203984218

    # make sure the indexing aligns
    if model_evals is not None:
        if inv_param.get_no_runs_averaged() == 1:
            # np.testing.assert_equal(model_evals[idx, 0], point_estimate_value) # todo uncomment
            point_estimate_flow = model_evals[idx, 2:]
        else:
            x_candidates = model_evals[:, 0]
            y_flow = model_evals[:, 2:]
            point_estimate_flow = np.mean(y_flow[x_candidates == point_estimate_value], axis=0)
    else:
        point_estimate_flow, _, _ = model.eval_model_averaged(
            parameter_value=np.transpose(np.expand_dims(point_estimate_value, axis=0)),
            nr_runs_averaged=10,
            random_state=random_state)

    return point_estimate_value, point_estimate_flow


def get_dimension(inv_param: InversionParameter):
    return np.size(inv_param.get_true_parameter_value())


def calc_posterior_samples(inv_param: InversionParameter, inv_result: InversionResult, tolerance: float):
    # load posterior samples
    # works only for ABC

    distance_measure_results = inv_result.get_data_misfit()
    candidates = inv_result.get_candidates()
    dim = inv_param.get_model().get_dimension()

    if not inv_param.get_method() == "ABC" or not isinstance(inv_param, InversionParameterRejection):
        raise UserWarning(
            "Script is designed for results from ABC inversion, data ist from %s inversion. " % inv_param.get_method())
    # if tolerance == inv_param.get_abc_threshold_relative():  # check if tolerance is the same
    #    # carry on with samples
    #    samples = inv_result.get_samples()
    # else:
    # find samples from candidates based on distance measure + tolerance
    if dim > 1:
        _, idx = np.where([distance_measure_results < tolerance])
        samples = candidates[:, idx]
        distance_measure_results_samples = distance_measure_results[idx]
    else:
        samples = candidates[distance_measure_results < tolerance]
        distance_measure_results_samples = distance_measure_results[distance_measure_results < tolerance]

        plt.figure()
        plt.semilogy(candidates, distance_measure_results, 'o', label="Candidates")
        plt.semilogy(samples, distance_measure_results_samples, 'x', label="Samples")
        plt.ylim([0, 1])
        plt.legend()
        plt.show()

    n_samples_posterior = len(np.transpose(samples))
    print("Loaded / identified %d samples" % n_samples_posterior)

    return samples, n_samples_posterior
