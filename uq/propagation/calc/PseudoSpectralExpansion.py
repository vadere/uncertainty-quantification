import chaospy as cp
import numpy as np
from numpoly import ndpoly
from numpy.random import RandomState

from uq.propagation.calc.ExpansionStrategy import ExpansionStrategy
from uq.propagation.calc.PropagationParameterGPCE import PropagationParameterGPCE


class PseudoSpectralExpansion(ExpansionStrategy):
    # PS: Pseudo-spectral / discrete projection approach

    def __init__(self, param: PropagationParameterGPCE, quadrature_degree: int, quadrature_rule: str = "gaussian"):
        super().__init__(param=param)
        self.__quadrature_degree = quadrature_degree
        self.__quadrature_rule = quadrature_rule

    # GETTERS
    def get_param(self) -> PropagationParameterGPCE:
        return super().get_param()

    # GETTERS

    def get_quadrature_order(self) -> int:
        return self.__quadrature_degree

    def get_quadrature_rule(self) -> str:
        return self.__quadrature_rule

    # OTHERS

    def generate_samples(self, random_state: RandomState, distribution) -> np.ndarray:
        # generate quadrature points
        nodes, weights = cp.generate_quadrature(self.get_quadrature_order(), distribution,
                                                rule=self.get_quadrature_rule())

        return nodes, weights

    def calc_gpce(self, expansion, qoi_averaged: np.ndarray, nodes: np.ndarray, weights: np.ndarray = None) -> ndpoly:
        # Generate the general polynomial chaos expansion polynomial
        qoi_gpce = cp.fit_quadrature(expansion, nodes, weights, qoi_averaged)

        return qoi_gpce
