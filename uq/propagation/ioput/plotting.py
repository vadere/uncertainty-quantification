import numpy as np
from matplotlib import pyplot as plt
from statsmodels.base.wrapper import ResultsWrapper

from uq.propagation.calc.propagation_utils import predict_regression_statsmodels
from uq.utils.datatype import unbox
from uq.utils.ioput.DataSaver import DataSaver


def plot_regression_sklearn(widths, data, propagation_pe, tmp_model_eval_widths, tmp_model_eval_propagation,
                            regression_abc, regression_pe, regression_data, data_saver: "DataSaver", label_abc,
                            label_data: str, label_pe: str, col_abc: str, col_data: str, col_pe: str) -> None:
    h = plt.figure()
    x_value = np.array([np.linspace(min(widths), max(widths))])
    x_value_col = np.transpose(x_value)

    if np.ndim(data) > 1:
        data = unbox(data)

    plt.plot(x_value_col, regression_abc.predict(x_value_col),
             label=label_abc + '\nCoef %.2f\nScore %.2f' %
                   (regression_abc.coef_, regression_abc.score(tmp_model_eval_widths, tmp_model_eval_propagation)),
             color=col_abc, linestyle='-')
    plt.plot(x_value_col, regression_pe.predict(x_value_col),
             label=label_pe + '\nCoef %.2f\nScore %.2f' % (
                 regression_pe.coef_[0], regression_pe.score(tmp_model_eval_widths,
                                                             tmp_model_eval_propagation)), color=col_data,
             linestyle=':')
    plt.plot(x_value_col, regression_data.predict(x_value_col),
             label=label_data + '\nCoef %.2f\nScore %.2f' % (
                 regression_data.coef_[0], regression_data.score(tmp_model_eval_widths, tmp_model_eval_propagation)),
             color=col_pe, linestyle='--')

    plt.plot(tmp_model_eval_widths, tmp_model_eval_propagation, color=col_abc, marker='.', linestyle='none')
    plt.plot(widths, data, color=col_data, marker='o', linestyle='none')
    tmp_point_estimate_flow = np.transpose(np.array([np.transpose(propagation_pe).flatten()]))
    plt.plot(tmp_model_eval_widths, tmp_point_estimate_flow, color=col_pe, marker='x', linestyle='none')

    plt.legend()
    plt.xlabel('Bottleneck width [m]')
    plt.ylabel('Flow [1/s]')
    data_saver.save_figure(h, "regression_sklearn")


def create_label(confidence_interval: np.ndarray, result: ResultsWrapper, idx_slope: int) -> str:
    label_str = '\nCI [%.2f, %.2f]\nCoef %.2f' % (
        confidence_interval[0], confidence_interval[1], result.params[idx_slope])
    return label_str


def plot_regression_statsmodels(data_saver: "DataSaver", data: np.ndarray, tmp_model_eval_widths: np.ndarray,
                                tmp_model_eval_propagation: np.ndarray, regr_abc: ResultsWrapper,
                                regr_pe: ResultsWrapper, regr_data: ResultsWrapper, ci_abc: np.ndarray, ci_pe: np.ndarray,
                                ci_data: np.ndarray, widths: np.ndarray, propagation_pe: np.ndarray, col_abc: str,
                                col_data: str, col_pe: str, label_abc: str, label_pe: str, label_data: str,
                                bool_intercept: bool = True) -> None:
    # Plot results of regression
    x_value = np.linspace(np.min(widths), np.max(widths))
    x_value_col = np.transpose(np.array([x_value]))

    handle = plt.figure()
    if bool_intercept:
        idx_slope = 1
    else:
        idx_slope = 0

    if np.ndim(data) > 1:
        data = unbox(data)

    plt.plot(x_value, predict_regression_statsmodels(regr_abc, x_value_col, bool_intercept), color=col_abc,
             linestyle='-', label=label_abc + create_label(ci_abc, regr_abc, idx_slope))
    plt.plot(x_value, predict_regression_statsmodels(regr_pe, x_value_col, bool_intercept), color=col_pe,
             linestyle=':', label=label_pe + create_label(ci_pe, regr_pe, idx_slope))

    if regr_data is not None:
        plt.plot(x_value, predict_regression_statsmodels(regr_data, x_value_col, bool_intercept), color=col_data,
             linestyle='--', label=label_data + create_label(ci_data, regr_data, idx_slope))

    plt.plot(tmp_model_eval_widths, tmp_model_eval_propagation, color=col_abc, marker='.', linestyle='none',
             label=label_abc)
    plt.plot(widths, data, color=col_data, marker='o', linestyle='none', label=label_data)

    tmp_model_eval_pe_propagation = np.transpose(np.array([np.transpose(propagation_pe).flatten()]))
    plt.plot(tmp_model_eval_widths, tmp_model_eval_pe_propagation, color=col_pe, marker='x',
             linestyle='none', label=label_pe)

    plt.legend()
    plt.xticks(widths)
    plt.xlabel('Bottleneck width [m]')
    plt.ylabel('Flow [1/s]')
    data_saver.save_figure(handle, name="regression_statsmodels")


def plot_posteriors(samples: np.ndarray, random_selected_posterior_samples: np.ndarray, dim: int, vadere_model: "Model",
                    data_saver: DataSaver) -> None:
    # Plot histogram of selected posterior samples to see if the distribution changes to all posterior samples
    h = plt.figure()
    if dim == 1:
        plt.hist(samples, label='All posterior samples', density=True, alpha=0.5)
        plt.hist(random_selected_posterior_samples, density=True, label='Random selected posterior samples',
                 alpha=0.5)
        plt.xlabel("Free-flow speed [m/s]")
        plt.legend()
    elif dim > 1:
        for i in range(0, dim):
            plt.subplot(dim, 1, i + 1)
            plt.hist(samples[i, :], density=True, label='All posterior samples', alpha=0.5)
            plt.hist(random_selected_posterior_samples[i, :], density=True,
                     label='Random selected posterior samples', alpha=0.5)
            plt.legend()
            plt.xlabel(vadere_model.get_key()[i])

    data_saver.save_figure(h, "hist_selected_samples")


def plot_boxplot(propagation_abc: np.ndarray, propagation_pe: np.ndarray, widths: np.ndarray, data: np.ndarray,
                 col_abc: str, col_pe: str, col_data: str, label_data: str, data_saver: DataSaver) -> None:
    # Boxplot for propagation_old results (over corridor width)
    h = plt.figure()
    plot_abc = plt.boxplot(propagation_abc, patch_artist=False, labels=widths)
    plot_pe = plt.boxplot(propagation_pe, patch_artist=False, labels=widths)
    colors = ['green', 'orange', 'lightgreen']
    for _, line_list in plot_abc.items():
        for line in line_list:
            line.set_color(col_abc)
    for _, line_list in plot_pe.items():
        for line in line_list:
            line.set_color(col_pe)

    if np.ndim(data) > np.ndim(np.arange(1, len(widths) + 1)):
        data = unbox(data)

    plt.plot(np.arange(1, len(widths) + 1), data, '.:', label=label_data, color=col_data)
    plt.xlabel('Bottleneck width [m]')
    plt.ylabel('Flow [1/s]')
    plt.legend()
    data_saver.save_figure(h, "boxplot_propagation")


def plot_posterior_per_width(widths: np.ndarray, propagation_abc: np.ndarray, propagation_pe: np.ndarray,
                             data: np.ndarray, col_data: str, col_pe: str, col_abc: str, label_data: str, label_pe: str,
                             label_abc: str, data_saver: DataSaver) -> None:
    # Evaluate histograms for each width
    for i in range(0, len(widths)):
        h = plt.figure()
        plt.hist(propagation_abc[:, i], label=label_abc, alpha=0.5, color=col_abc)
        plt.hist(propagation_pe[:, i], label=label_pe, alpha=0.5, color=col_pe)
        # plt.plot(point_estimate_flow[i] * np.ones(2), np.array(plt.gca().get_ylim()), color=col_point_estimate,
        #                      label=label_pe, linestyle='--')
        if np.ndim(data) > 1:
            data = unbox(data)
        plt.plot(data[i] * np.ones(2), np.array(plt.gca().get_ylim()), color=col_data, label=label_data)

        plt.xlabel('Flow [1/s]')
        plt.legend()
        data_saver.save_figure(h, 'histogram_propagation_width_%.1f.png' % widths[i])

    bool_first_tol = False
