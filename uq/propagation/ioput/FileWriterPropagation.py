import os
import numpy as np

from uq.propagation.calc.PropagationResult import PropagationResult
from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.utils.data_eval import sample_mean, sample_var, sample_mode
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.ioput.FileWriter import FileWriter


class FileWriterPropagation(FileWriter):
    def __init__(self, param: PropagationParameter, result: PropagationResult = None, data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    # GETTERS

    def get_param(self) -> PropagationParameter:
        return super().get_param()

    def get_result(self) -> PropagationResult:
        return super().get_result()

    def write_parameters_propagation_after_inversion(self, seed: int, tolerance: float = None):

        result_file_name = "results.txt"
        with open(os.path.join(self.get_datasaver().get_path_to_files(), result_file_name), 'a+') as file:
            if self.get_param().get_name() is not None:
                file.write('\n** Parameters ** \n' % self.get_param().get_name())
            else:
                file.write('\n** Parameters ** \n')

            file.write('Seed: \t\t\t\t\t\t\t\t\t {} \n'.format(seed))
            if tolerance is not None:
                file.write('ABC Threshold (tolerance): \t\t\t\t {} \n'.format(tolerance))
            file.write('Number of samples for propagation: \t\t {} \n'.format(self.get_param().get_n_samples()))
            file.close()

    def write_parameters(self) -> None:
        result_file_name = "results.txt"
        param = self.get_param()
        model = param.get_model()
        with open(os.path.join(self.get_datasaver().get_path_to_files(), result_file_name), 'a+') as file:
            file.write('\n** %s Parameters ** \n' % self.get_param().get_name())

            # Parameters
            file.write('Key(s): \t\t\t\t\t\t\t {} \n'.format(model.get_key()))
            file.write('QoI: \t\t\t\t\t\t\t\t {} \n'.format(model.get_qoi()))
            file.write('Prior params: \t\t\t\t\t\t {} \n'.format(param.get_prior().to_str()))
            file.write('Seed: \t\t\t\t\t\t\t\t {} \n'.format(param.get_seed()))
            file.write('Number of steps / samples: \t\t\t {} \n'.format(param.get_n_samples()))
            file.write('Number of runs averaged: \t\t\t {} \n'.format(param.get_no_runs_averaged()))

    def write_result(self) -> None:
        result = self.get_result()
        folder = self.get_datasaver().get_path_to_files()
        qoi_samples = result.get_qoi_samples()

        with open(os.path.join(folder, "results.txt"), 'a') as file:
            file.write("\n\n** Results ** \n")
            file.write('Computation time (sampling):  \t\t{} min\n'.format(result.get_computation_time() / 60.0))
            if self.get_param().get_model().get_qoi_dim() == 1:
                file.write('Mean of samples (without burn-in): \t{}\n'.format(sample_mean(qoi_samples)))
                file.write('Mode of samples (without burn-in): \t{}\n'.format(sample_mode(qoi_samples)))
                file.write('Var of samples (without burn-in): \t{}\n'.format(sample_var(qoi_samples)))
            else:
                file.write('Mean of samples (without burn-in): \t{}\n'.format(sample_mean(np.transpose(qoi_samples))))
                file.write('Mode of samples (without burn-in): \t{}\n'.format(sample_mode(np.transpose(qoi_samples))))
                file.write('Var of samples (without burn-in): \t{}\n'.format(sample_var(np.transpose(qoi_samples))))
            file.close()

    def save_results_to_file(self):
        param = self.get_param()
        result = self.get_result()

        data_saver = self.get_datasaver()
        data_saver.save_to_pickle(data=result.get_prior_samples(), name="prior_samples")
        data_saver.write_var_to_file(variable=result.get_prior_samples(), name="prior_samples")

        data_saver.save_to_pickle(data=result.get_qoi_samples(), name="qoi_samples")
        data_saver.write_var_to_file(variable=result.get_qoi_samples(), name="qoi_samples")
        data_saver.write_var_to_file(variable=param.get_seed(), name="seed")
