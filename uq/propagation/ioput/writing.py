import numpy as np
import os

from uq.propagation.calc.PropagationResult import PropagationResult
from uq.utils.ioput.DataSaver import DataSaver


def write_results_propagation_after_inversion(data_saver: DataSaver, confidence_interval: np.ndarray, slope: float,
                                              result: PropagationResult, name_str: str) -> None:
    if result is not None:
        samples_qoi = result.get_qoi_samples()

    folder = data_saver.get_path_to_files()

    with open(os.path.join(folder, "results.txt"), 'a+') as file:
        file.write('\n** Results (Propagation): %s ** \n' % name_str)
        file.write('Confidence Interval: \t\t\t\t\t {} \n'.format(confidence_interval))
        file.write('Confidence Interval Size: \t\t\t\t {} \n'.format(np.diff(confidence_interval)))
        file.write('Mean slope from multiple regressions: \t {}  \n'.format(slope))
        if result is not None:
            file.write('Mean of qoi samples: \t\t\t\t\t {} \n'.format(np.mean(samples_qoi, axis=0)))
            file.write('Std of qoi samples: \t\t\t\t\t {} \n'.format(np.std(samples_qoi, axis=0)))
            file.write('Computation time: \t\t\t\t\t\t {} minutes \n'.format(result.get_computation_time()))

        file.close()


def write_all_results_propagation_after_inversion(data_saver: DataSaver, confidence_interval_pe: np.ndarray,
                                                  ci_data: np.ndarray,
                                                  confidence_interval_abc: np.ndarray, sample_mean_pe: float, res_data,
                                                  sample_mean_abc: float, result_pe: PropagationResult,
                                                  result_abc: PropagationResult, bool_intercept: bool = True) -> None:
    propagation_pe = result_pe.get_qoi_samples()
    propagation_abc = result_abc.get_qoi_samples()

    folder = data_saver.get_path_to_files()

    with open(os.path.join(folder, "results.txt"), 'a+') as file:
        file.write('\n** Results (Propagation) ** \n')
        file.write('Data Confidence Interval: \t\t\t\t\t {} \n'.format(ci_data))
        file.write('Data Confidence Interval Size: \t\t\t\t {} \n'.format(np.diff(ci_data)))

        file.write('Point Estimate Confidence Interval: \t\t {} \n'.format(confidence_interval_pe))
        file.write('Point Estimate Confidence Interval Size: \t {} \n'.format(np.diff(confidence_interval_pe)))

        file.write('ABC Confidence Interval: \t\t\t\t\t {} \n'.format(confidence_interval_abc))
        file.write('ABC Confidence Interval Size: \t\t\t\t {} \n\n'.format(np.diff(confidence_interval_abc)))

        file.write('PE mean slope from multiple regressions: \t {}  \n'.format(sample_mean_pe))
        file.write('ABC mean slope from multiple regressions: \t {} \n'.format(sample_mean_abc))
        if bool_intercept:
            file.write('Data slope from single regression: \t\t\t {} \n\n'.format(res_data.params[1]))
        else:
            file.write('Data slope from single regression: \t\t\t {} \n\n'.format(res_data.params[0]))
        file.write('Mean of propagation (PE): \t\t\t {} \n'.format(np.mean(propagation_pe, axis=0)))
        file.write('Mean of propagation (ABC): \t\t\t {} \n'.format(np.mean(propagation_abc, axis=0)))

        file.write('Std of propagation (PE): \t\t\t {} \n'.format(np.std(propagation_pe, axis=0)))
        file.write('Std of propagation (ABC): \t\t\t {} \n\n'.format(np.std(propagation_abc, axis=0)))

        file.write('Computation time: \t\t\t\t\t\t\t {} minutes \n'.format(
            result_pe.get_computation_time() + result_abc.get_computation_time()))

        file.close()
