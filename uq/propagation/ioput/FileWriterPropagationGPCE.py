import os

from uq.propagation.calc.PropagationParameterGPCE import PropagationParameterGPCE
from uq.propagation.calc.PropagationParameter import PropagationParameter
from uq.propagation.calc.PropagationResult import PropagationResult
from uq.propagation.calc.PropagationResultGPCE import PropagationResultGPCE
from uq.propagation.ioput.FileWriterPropagation import FileWriterPropagation
from uq.utils.data_eval import sample_mean, sample_var, sample_mode
from uq.utils.ioput.DataSaver import DataSaver


class FileWriterPropagationGPCE(FileWriterPropagation):
    def __init__(self, param: PropagationParameter, result: PropagationResult = None, data_saver: DataSaver = None):
        super().__init__(param=param, result=result, data_saver=data_saver)

    # GETTERS

    def get_param(self) -> PropagationParameterGPCE:
        return super().get_param()

    def get_result(self) -> PropagationResultGPCE:
        return super().get_result()

    def write_parameters(self) -> None:
        super().write_parameters()
        result_file_name = "results.txt"
        param = self.get_param()
        with open(os.path.join(self.get_datasaver().get_path_to_files(), result_file_name), 'a+') as file:
            # Parameters
            file.write('Expansion order: \t\t\t\t\t {} \n'.format(param.get_expansion_order()))
            file.close()

    def write_result(self) -> None:
        result = self.get_result()
        folder = self.get_datasaver().get_path_to_files()
        super().write_result()

        with open(os.path.join(folder, "results.txt"), 'a') as file:
            file.write('GPCE:  \t\t{} min\n'.format(result.get_expansion()))
            file.close()

    def save_results_to_file(self):
        param = self.get_param()
        result = self.get_result()

        data_saver = self.get_datasaver()
        data_saver.save_to_pickle(data=result.get_prior_samples(), name="prior_samples")
        data_saver.write_var_to_file(variable=result.get_prior_samples(), name="prior_samples")

        data_saver.save_to_pickle(data=result.get_qoi_samples(), name="qoi_samples")
        data_saver.write_var_to_file(variable=result.get_qoi_samples(), name="qoi_samples")
        data_saver.write_var_to_file(variable=param.get_seed(), name="seed")

