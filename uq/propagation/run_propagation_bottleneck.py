import os

import matplotlib.pylab as plt
import numpy as np
from numpy.random import RandomState

from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.UniformGenMult import UniformGenMult

N_samples_propagation = 1000
no_runs_averaged = 1  # no averaging
seed = 844562455

path2model = os.path.abspath("../scenarios/vadere-console.jar")
path2tutorial = os.path.dirname(os.path.realpath(__file__))
scenario_name = "bottleneck_OSM_all_in_one_N60.scenario"  # 5in1
path2scenario = os.path.abspath(os.path.join("../scenarios", scenario_name))

key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
       "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[*].spawnNumber", "obstPotentialHeight", "pedPotentialHeight",
       "minStepLength"]  # uncertain parametersqoi = "mean_density.txt"
qoi = "flow.txt"  # quantity of interest

width = [0.8, 0.9, 1.0, 1.1, 1.2]

run_local = False

qoi_dim = 5

data_saver = DataSaver(path2tutorial)

# Step 1:  Propagation of intervals before SA


dim = 7

x_lower = np.array([1.0, 0.5, 0.1, 40, 2.0, 5.0, 0.0])  # lower bounds for parameters
x_upper = np.array([5.0, 2.2, 1.0, 80, 10.0, 50.0, 0.15])  # upper bounds for parameters

rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=dim)

samples = rho.sample(n=N_samples_propagation, random_state=RandomState(seed))

vadere_model = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                           n_jobs=-1, log_lvl="OFF",
                           qoi_dim=qoi_dim)

propagation, _, _ = vadere_model.eval_model_averaged(parameter_value=samples, nr_runs_averaged=no_runs_averaged,
                                                       random_state=RandomState(seed))

data_saver.write_var_to_file(propagation, "propagation_sa_input")

# Step 2: Propagation of intervals after Factor Fixing setting

dim = 4

x_lower = np.array([0.5, 0.1, 2.0, 5.0])  # lower bounds for parameters
x_upper = np.array([2.2, 1.0, 10.0, 50.0])  # upper bounds for parameters

key = ["attributesPedestrian.speedDistributionMean",
       "attributesPedestrian.speedDistributionStandardDeviation",
       "obstPotentialHeight", "pedPotentialHeight", ]  # uncertain parameters

vadere_model = VadereModel(run_local=run_local, path2scenario=path2scenario, path2model=path2model, key=key, qoi=qoi,
                           n_jobs=-1, log_lvl="OFF",
                           qoi_dim=qoi_dim)

rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=dim)

samples = rho.sample(n=N_samples_propagation, random_state=RandomState(seed))

propagation_ff, _, _ = vadere_model.eval_model_averaged(parameter_value=samples, nr_runs_averaged=no_runs_averaged,
                                                          random_state=RandomState(seed))

data_saver.write_var_to_file(propagation_ff, "propagation_ff")

# Step 3: Propagation with calibrated free-flow speed mean


# Plot results
fig, axs = plt.subplots(qoi_dim, 1)
tmp_max_n = 0
for i in range(0, qoi_dim):
    n, bins, patches = axs[i].hist(propagation[:, i], density=True)
    max_n = np.max(n, tmp_max_n)
    axs[i].set_xlim([np.min(propagation), np.max(propagation)])
    axs[i].set_xlabel(qoi)
for i in range(0, qoi_dim):
    axs[i].set_ylim([0, max_n])
    axs[i].legend("%.1f m" % width[i])

fig, axs = plt.subplots(qoi_dim, 1)
tmp_max_n = 0
for i in range(0, qoi_dim):
    n, bins, patches = axs[i].hist(propagation_ff[:, i], density=True)
    max_n = np.max(n, tmp_max_n)
    axs[i].set_xlim([np.min(propagation_ff), np.max(propagation_ff)])
    axs[i].set_xlabel(qoi)
for i in range(0, qoi_dim):
    axs[i].set_ylim([0, max_n])
    axs[i].legend("%.1f m" % width[i])

fig.suptitle('Factor fixing setting')
plt.xlabel(qoi)

plt.show()

print("done.")
