##############################################################################
#
# This is a simple python program to show how to use chaospy
# to quantify the uncertainty of a simple evacuation scenario that is
# simulated with vadere using the SUQ controller.
#
# Uncertain parameter:
# - free_flow_speed_mean
#
# Quantity of interest:
# - meanVoronoiDensity
#
# Reference: https://chaospy.readthedocs.io/en/master/recipes/spectral.html
##############################################################################

import os

import chaospy as cp
import suqc

# Setup model
path2scenario = os.path.abspath("../scenarios/Liddle_bhm_v3_free_seed.scenario")
path2model = os.path.abspath("../scenarios/vadere-console.jar")
key = "speedDistributionMean"
qoi = "mean_density.txt"
run_local = False

# Setup uncertain parameter
free_flow_speed_mean = cp.Uniform(0.5, 2)

# Generate nodes and weights
nodes, weights = cp.generate_quadrature(3, free_flow_speed_mean, rule="G")
nodes = nodes[0]


def simulate_qu(path2scenario, key, values, qoi, path2model, run_local):
    setup = suqc.SingleKeyVaryScenario(scenario_path=path2scenario,  # -> path to the Vadere .scenario file to vary
                                       key=key,  # -> parameter key to change
                                       values=values,  # -> values to set for the parameter
                                       qoi=qoi,  # -> output file name to collect
                                       model=path2model)  # -> path to Vadere console jar file to use for simulation
    if run_local:
        par_var, data = setup.run(njobs=1)
    else:
        par_var, data = setup.remote(njobs=-1)

    return data.values


# Propagate the uncertainty
evacuation_times = simulate_qu(path2scenario, key, nodes, qoi, path2model, run_local)

# Generate orthogonal polynomials for the distribution
OP = cp.orth_ttr(3, free_flow_speed_mean)

# Generate the general polynomial chaos expansion polynomial
evacuation_times_GPCE = cp.fit_quadrature(OP, nodes, weights, evacuation_times)

# Calculate statistics
E_evacuation_time = cp.E(evacuation_times_GPCE, free_flow_speed_mean)
StdDev_evacuation_time = cp.Std(evacuation_times_GPCE, free_flow_speed_mean)

# Print the statistics
print("mean evacuation time: %f" % E_evacuation_time)
print("stddev evacuation time: %f" % StdDev_evacuation_time)
