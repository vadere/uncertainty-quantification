import os
import pickle
import shutil
import warnings
from os import path
import matplotlib.pyplot as plt

import numpy as np
from numpy.random import RandomState

from uq.propagation.calc.PropagationCalculatorPointEstimate import PropagationCalculatorPointEstimate
from uq.propagation.calc.PropagationCalculatorPosterior import PropagationCalculatorPosterior
from uq.propagation.calc.PropagationParameterPosterior import PropagationParameterPosterior
from uq.propagation.calc.PropagationResult import PropagationResult
from uq.propagation.calc.postprocessing_inversion import calc_pe_from_inversion, get_dimension, \
    calc_posterior_samples
from uq.propagation.calc.propagation_utils import regression_sklearn
from uq.propagation.calc.propagation_utils import regression_statsmodels
from uq.propagation.ioput.FileWriterPropagation import FileWriterPropagation
from uq.propagation.ioput.plotting import plot_regression_sklearn, plot_regression_statsmodels, plot_posteriors, \
    plot_boxplot, plot_posterior_per_width
from uq.propagation.ioput.writing import write_all_results_propagation_after_inversion, \
    write_results_propagation_after_inversion
from uq.utils.ioput.DataSaver import DataSaver

BOOL_PROPAGATION = True  # False: evaluation of existing propagation

# SUQC path of results
suqc_output_path = path.abspath("D:\\repo_checkout\\uncertainty-quantification-private\\uq\\propagation\\suqc_output\\")

# path to data from inversion
cur_dir = os.path.dirname(os.path.realpath(__file__))
folder_path_inversion = path.abspath(path.join(cur_dir, "../inversion/results"))
# paper
# folder_inversion_results = "test_folder_bimodal"  # just for testing of bimodal (paper - case 2 )


# diss- new version of inversion
folder_inversion_results = "2021-04-30_17-32-54_454797"  # 10^5 samples (paper calibration - case 1, eps = 0.0685)
folder_inversion_results = "2021-05-06_14-55-21_006747"  # 10^5 samples (paper calibration - case 2, eps = )
folder_inversion_results = "2021-05-07_16-12-51_582760"
folder_inversion_results = "2021-05-07_16-22-52_119620"
folder_inversion_results_test = "bimodal_bottleneck_flow//test_data_for_python"

bool_intercept = True

tolerance_vec = [1e-1, 1e-2, 7.5e-3, 5e-3, 2.5e-3, 1e-3, 7.5e-4, 5e-4, 2.5e-4, 1e-4]  # ABC threshold
tolerance_vec = [1e-1, 1e-2, 1e-3, 1e-4, 1e-5]  # ABC threshold

n_samples_propagation = 1000

no_runs_averaged = 1
bool_averaged = False

seed = int(np.random.uniform(0, 2 ** 32 - 1))
seed = 2396730

path_to_propagation_results = path.abspath(path.join(cur_dir, "../inversion/results/x"))
folder_propagation_results = 'propagation'
widths = np.array([0.8, 0.9, 1.0, 1.1, 1.2])

# attributes for eval

# colors
col_pe = 'forestgreen'
col_data = 'darkorange'
col_abc = 'cornflowerblue'

# labels
label_pe = 'Point estimate'
label_abc = 'ABC inversion'
label_data = 'Experimental data'

if __name__ == "__main__":  # main required by Windows to run in parallel

    bool_first_tol = True
    path_inversion_data = path.abspath(path.join(folder_path_inversion, folder_inversion_results))

    # Load parameters and results from inversion
    inv_param = pickle.load(
        open(path.join(folder_path_inversion, folder_inversion_results, "inv_param.pickle"), "rb"))

    inv_result = pickle.load(
        open(path.join(folder_path_inversion, folder_inversion_results, "inv_result.pickle"), "rb"))

    cand = np.loadtxt(path.join(folder_path_inversion, folder_inversion_results_test, "candidates.data"))
    samp = np.loadtxt(path.join(folder_path_inversion, folder_inversion_results_test, "samples.data"))
    dist = np.loadtxt(path.join(folder_path_inversion, folder_inversion_results_test, "data_misfit.data"))
    inv_result.set_candidates(cand)
    inv_result.set_samples(cand)
    inv_result.set_data_misfit(dist)


    # Load model evaluations
    # todo put in results object?
    if os.path.exists(path.join(folder_path_inversion, folder_inversion_results, "model_evaluations.data")):
        model_evals_tmp = \
            np.loadtxt(path.join(folder_path_inversion, folder_inversion_results, "model_evaluations.data"), skiprows=1)
    else:
        model_evals_tmp = None

    count = 0
    ci_size_abc = np.nan * np.ones(np.size(tolerance_vec))
    ci_size_pe = np.nan * np.ones(np.size(tolerance_vec))
    nr_samples = np.nan * np.ones(np.size(tolerance_vec))
    my_str = []

    for tolerance in tolerance_vec:

        try:  # Remove results
            shutil.rmtree(suqc_output_path)
        except OSError as e:
            print("Error: %s : %s" % (suqc_output_path, e.strerror))

        # Parameter setup
        random_state = RandomState(seed)
        data_saver = DataSaver(path.join(folder_path_inversion, folder_inversion_results, folder_propagation_results))

        data = inv_param.get_data()
        dim = get_dimension(inv_param)
        candidates = inv_result.get_candidates()
        distance_measure_results = inv_result.get_data_misfit()

        # Get point estimate from inversion data
        pe_input, pe_output = calc_pe_from_inversion(inv_result=inv_result, inv_param=inv_param,
                                                     model_evals=model_evals_tmp, model=inv_param.get_model(),
                                                     random_state=random_state)

        # Get posterior samples for chosen tolerance
        posterior_samples, n_samples_posterior = calc_posterior_samples(inv_param=inv_param, inv_result=inv_result,
                                                                        tolerance=tolerance)

        if n_samples_posterior == 0:
            continue  # jump to next loop iteration

        if n_samples_posterior <= 800:
            continue  # jump to next loop iteration if not enough posterior samples are available

        # write results of postprocessed inversion with the defined tolerance
        data_saver.write_results_inversion_postprocessing(point_estimate_value=pe_input, samples=posterior_samples,
                                                          acceptance_rate=n_samples_posterior / np.size(
                                                              distance_measure_results))

        if BOOL_PROPAGATION:
            # propagation part
            # 1) propagation abc
            propagation_param_abc = PropagationParameterPosterior(model=inv_param.get_model(), prior=posterior_samples,
                                                                  seed=seed,
                                                                  bool_averaged=bool_averaged,
                                                                  no_runs_averaged=no_runs_averaged,
                                                                  path2results=data_saver.get_path_to_files(),
                                                                  n_samples=n_samples_propagation,
                                                                  posterior_samples=posterior_samples, name="ABC")

            propagation_abc_calc = PropagationCalculatorPosterior(param=propagation_param_abc)
            propagation_abc_calc.propagate()
            propagation_result_abc = propagation_abc_calc.get_result()
            propagation_abc = propagation_result_abc.get_qoi_samples()
            random_selected_posterior_samples = propagation_result_abc.get_prior_samples()
            n_samples_propagation = propagation_result_abc.get_n_samples()

            file_writer_abc = FileWriterPropagation(param=propagation_param_abc, result=propagation_result_abc,
                                                    data_saver=data_saver)
            file_writer_abc.write_parameters_propagation_after_inversion(seed=seed, tolerance=tolerance)

            # Remove outputs
            try:
                shutil.rmtree(suqc_output_path)
            except OSError as e:
                print("Error: %s : %s" % (suqc_output_path, e.strerror))

            # 2) propagation pe
            propagation_param_pe = PropagationParameterPosterior(model=inv_param.get_model(), prior=posterior_samples,
                                                                 seed=seed,
                                                                 bool_averaged=bool_averaged,
                                                                 no_runs_averaged=no_runs_averaged,
                                                                 path2results=data_saver.get_path_to_files(),
                                                                 n_samples=n_samples_propagation,
                                                                 posterior_samples=pe_input, name="PE")

            propagation_pe_calc = PropagationCalculatorPointEstimate(param=propagation_param_pe)
            propagation_pe_calc.propagate()
            propagation_result_pe = propagation_pe_calc.get_result()

            file_writer_pe = FileWriterPropagation(param=propagation_param_pe, result=propagation_result_pe,
                                                   data_saver=data_saver)
            file_writer_pe.write_parameters_propagation_after_inversion(seed=seed)

            print("Performed forward propagation for %d samples" % (n_samples_propagation * 2))
            print("Time for propagation (%d samples): %.3f minutes" % (n_samples_propagation * 2,
                                                                       propagation_result_pe.get_computation_time()
                                                                       + propagation_result_abc.get_computation_time()))
        else:
            # pure evaluation of existing propagation
            propagation_abc = pickle.load(open(path.join(path_to_propagation_results, 'propagation_old.pickle'), 'rb'))
            random_selected_posterior_samples = pickle.load(
                open(path.join(path_to_propagation_results, 'selected_samples.pickle'), 'rb'))
            propagation_result_abc = PropagationResult(propagation_abc, random_selected_posterior_samples,
                                                       len(propagation_abc))
            n_samples_propagation = propagation_result_abc.get_n_samples()

            propagation_result_pe = PropagationResult(pe_output, pe_input,
                                                      n_samples_propagation)

            vadere_model = None

        # todo pass result object instead of samples
        propagation_pe = propagation_result_pe.get_qoi_samples()
        propagation_abc = propagation_result_abc.get_qoi_samples()

        # ******************* Evaluation - specifically for calibration publication

        if len(posterior_samples) <= 5:
            warnings.warn('Tolerance is too low - only %d samples (should be >= 5). ' % len(posterior_samples))
            continue

        # Regression
        # todo encapsulate returns
        # this one gives the confidence intervals
        _, _, regr_pe_stat, ci_pe, regr_data_stat, ci_data, regr_abc_stat, ci_abc, slope_abc, slope_pe, slope_data = \
            regression_statsmodels(widths=widths, n_samples_propagation=n_samples_propagation,
                                   propagation_abc=propagation_abc, propagation_pe=propagation_pe, data=data,
                                   random_state=RandomState(seed), data_saver=data_saver, bool_intercept=bool_intercept)

        widths_reshaped, model_evals_reshaped, regr_abc_sklearn, regr_pe_sklearn, regr_data_sklearn = \
            regression_sklearn(widths=widths, n_samples_propagation=n_samples_propagation,
                               propagation_abc=propagation_abc, propagation_pe=propagation_pe, data=data)

        # *******************  Save results to file
        # todo: check for multi-dim in- and output
        # results_mat = np.vstack((random_selected_posterior_samples, model_eval_propagation))

        if BOOL_PROPAGATION:
            data_saver.save_to_pickle(data=random_selected_posterior_samples, name="selected_samples")
            data_saver.save_to_pickle(data=propagation_abc, name="propagation_selected_samples")
            data_saver.write_var_to_file(variable=seed, name="seed")
            data_saver.write_var_to_file(variable=propagation_pe, name="propagation_pe")
            data_saver.write_var_to_file(variable=propagation_abc, name="propagation_abc")

            write_results_propagation_after_inversion(data_saver=data_saver, confidence_interval=ci_pe,
                                                      slope=slope_pe, result=propagation_result_pe, name_str="PE")
            write_results_propagation_after_inversion(data_saver=data_saver, confidence_interval=ci_abc,
                                                      slope=slope_abc, result=propagation_result_abc,
                                                      name_str="ABC")
            if ci_data is not None:
                write_results_propagation_after_inversion(data_saver=data_saver, confidence_interval=ci_data,
                                                          slope=slope_data, result=None, name_str="Data")

            # ******************* Plot results

            plot_posteriors(samples=posterior_samples,
                            random_selected_posterior_samples=random_selected_posterior_samples,
                            dim=dim, vadere_model=inv_param.get_model(), data_saver=data_saver)
            plot_boxplot(propagation_abc=propagation_abc, propagation_pe=propagation_pe, widths=widths, data=data,
                         col_abc=col_abc, col_pe=col_pe, col_data=col_data, label_data=label_data,
                         data_saver=data_saver)
            plot_posterior_per_width(widths, propagation_abc, propagation_pe, data, col_data, col_pe, col_abc,
                                     label_data,
                                     label_pe=label_pe, label_abc=label_abc, data_saver=data_saver)

            # Plot regression (STATSMODELS)

            # todo encapsulate parameters
            plot_regression_statsmodels(data_saver=data_saver, data=data, tmp_model_eval_widths=widths_reshaped,
                                        tmp_model_eval_propagation=model_evals_reshaped, regr_abc=regr_abc_stat,
                                        regr_pe=regr_pe_stat, regr_data=regr_data_stat, ci_abc=ci_abc,
                                        ci_pe=ci_pe, ci_data=ci_data, widths=widths,
                                        propagation_pe=propagation_pe, col_abc=col_abc, col_data=col_data,
                                        col_pe=col_pe, label_abc=label_abc, label_pe=label_pe,
                                        label_data=label_data, bool_intercept=bool_intercept)

            # Plot regression (SKLEARN)
            # todo encapsulate parameters
            plot_regression_sklearn(widths=widths, data=data, propagation_pe=propagation_pe,
                                    tmp_model_eval_widths=widths_reshaped,
                                    tmp_model_eval_propagation=model_evals_reshaped,
                                    regression_abc=regr_abc_sklearn, regression_pe=regr_pe_sklearn,
                                    regression_data=regr_data_sklearn, data_saver=data_saver, label_abc=label_abc,
                                    label_data=label_data, label_pe=label_pe, col_abc=col_abc, col_data=col_data,
                                    col_pe=col_pe)

            ci_size_abc[count] = np.diff(ci_abc)
            ci_size_pe[count] = np.diff(ci_pe)

            nr_samples[count] = n_samples_posterior

            my_str.append('%.3e & %.3f & %.3f & %.3f & %.3f \\\\ \n' % (
            tolerance, n_samples_posterior / len(candidates) * 100, np.diff(ci_data), ci_size_pe[count],
            ci_size_abc[count]))
            count = count + 1


        print("*** FINISHED ***")

    print(repr(my_str))

    plt.figure()
    plt.semilogx(tolerance_vec, ci_size_pe, 'o-', label='Point estimate')
    plt.semilogx(tolerance_vec, ci_size_abc, 'x--', label='ABC')
    plt.xlabel('Tolerance $\epsilon$')
    plt.ylabel('Size of confidence interval of linear fit')
    plt.show()

    plt.figure()
    plt.loglog(tolerance_vec, nr_samples, 'o--')
    plt.xlabel('Tolerance $\epsilon$')
    plt.ylabel('Number of posterior samples')
    plt.show()
