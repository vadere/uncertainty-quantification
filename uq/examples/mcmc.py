import distutils.core
import distutils.filelist
import os
import shelve
import time
from datetime import datetime

import matplotlib.pyplot as mlt
from numpy.random import RandomState
from scipy.stats import norm
from suqc import SingleKeyVariation

from uq.examples.imports import *

path2scenario = os.path.join(path2tutorial, "single_ped_10m_free_flow_meas_area.scenario")
print(path2scenario)

run_local = True
seed = int(np.random.uniform(0, 2 ** 32 - 1))
nr_steps = 10
burn_in = int(0.1 * nr_steps)
jump_width = 0.1
prior_mean = 2.0
prior_var = 1.0
meas_noise = 0.1
true_parameter_value = 1.34
batch_jump_width = 10
acceptance_rate_limits = [0.4, 0.6]
loglvl = "OFF"


###############################################################################################################
# Example taken from suq-controller


def setup(**attrs):
    # Make sure we have any requirements needed to interpret 'attrs'.
    return distutils.core.setup(**attrs)


def evaluate_prior(value: np.ndarray):
    return norm.pdf(value, prior_mean, prior_var)


def evaluate_likelihood(value, data, model_eval, meas_noise: float):
    f_d = np.linalg.norm(np.power(np.power(meas_noise, -1 / 2) * (data - model_eval), 2) / 2)
    likelihood = np.exp(-f_d)
    return likelihood


def save_workspace_to_file():
    date_str = datetime.now().strftime("%d-%m-%y_%H-%M-%S")
    filename = '/results/' + date_str + '/shelve.out'
    os.mkdir('/results/' + date_str + '/')
    my_shelf = shelve.open(filename, 'n')  # 'n' for new


def _call_suq_controller(in_path2scenario: str, key: str, value: np.ndarray, qoi: str, model: str,
                         in_run_local: bool, in_loglvl: str) -> float:
    model = VadereConsoleWrapper(model_path=model, loglvl=in_loglvl)
    suq_setup = SingleKeyVariation(scenario_path=in_path2scenario,  # -> path to the Vadere .scenario file to vary
                                   key=key,  # -> parameter key to change
                                   values=value,  # -> values to set for the parameter
                                   qoi=qoi,  # -> output file name to collect
                                   model=model)  # -> path to Vadere console jar file to use for simulation

    if in_run_local:
        par_var, ret_data = suq_setup.run(njobs=1)
    else:
        par_var, ret_data = suq_setup.remote(njobs=1)

    evacuation_time = ret_data["evacuationTime"].values[0]

    return evacuation_time


if __name__ == "__main__":  # main required by Windows to run in parallel

    initial_point = prior_mean

    # generate data ("measurement data")
    data = _call_suq_controller(in_path2scenario=path2scenario, key="speedDistributionMean",
                                value=np.array([true_parameter_value]),
                                qoi="evac_time.txt", model=path2model, in_run_local=run_local, in_loglvl="OFF")

    start = time.time()

    # init
    candidates = np.zeros(shape=(nr_steps + 2, 1))
    samples = -np.ones(shape=(nr_steps + 2, 1))
    posterior_samples = -np.ones(shape=(nr_steps + 2, 1))
    posterior_candidates = -np.ones(shape=(nr_steps + 2, 1))
    model_eval = -np.ones(shape=(nr_steps + 2, 1))
    count_accepted = 0
    count_rejected = 0

    # Initialize random object
    random_state = RandomState(seed)

    candidates[0] = initial_point
    samples[0] = initial_point

    # evaluate at starting point
    model_eval[0] = _call_suq_controller(path2scenario, "speedDistributionMean", np.array([candidates[0]]),
                                         "evac_time.txt", path2model, run_local, loglvl)
    posterior_samples[0] = evaluate_prior(candidates[0]) * evaluate_likelihood(candidates[0], data, model_eval[0],
                                                                               meas_noise)
    posterior_candidates[0] = posterior_samples[0]

    iterations = 0

    while iterations <= nr_steps:

        # adaptive jump width
        # if (np.mod(iterations, batch_jump_width) == 0) & (iterations > 0):
        #    acceptance_rate = count_accepted / iterations
        #    print("Acceptance rate {}".format(acceptance_rate))

        #   if acceptance_rate > 1:
        #      print("Berechnung acceptance rate passt nicht!")
        # if acceptance_rate > acceptance_rate_limits[1]:
        #    jump_width = jump_width / acceptance_rate
        #   print("New jump width {}".format(jump_width))
        # elif acceptance_rate < acceptance_rate_limits[0]:
        #   jump_width = jump_width * acceptance_rate
        #  print("New jump width {}".format(jump_width))

        # new candidate
        candidates[iterations + 1] = random_state.normal(samples[iterations], jump_width, 1)
        while candidates[iterations + 1] <= 0.5 or candidates[iterations + 1] >= 2.5:
            candidates[iterations + 1] = random_state.normal(samples[iterations], jump_width, 1)

        model_eval[iterations + 1] = _call_suq_controller(path2scenario, "speedDistributionMean",
                                                          candidates[iterations + 1], "evac_time.txt", path2model,
                                                          run_local, loglvl)
        # evaluate posterior
        posterior_candidates[iterations + 1] = evaluate_prior(candidates[iterations + 1]) * evaluate_likelihood(
            candidates[iterations + 1], data, model_eval[iterations + 1], meas_noise)

        if posterior_candidates[iterations + 1] >= posterior_samples[iterations]:
            # accepted
            # print('** accepted ({}, {})'.format(candidates[iterations+1], model_eval))
            count_accepted = count_accepted + 1
            samples[iterations + 1] = candidates[iterations + 1]
            posterior_samples[iterations + 1] = posterior_candidates[iterations + 1]
        else:
            u = random_state.uniform(0, 1, 1)
            acceptance_criterion = posterior_candidates[iterations + 1] / posterior_samples[iterations]
            if u <= acceptance_criterion:
                # accepted
                # print('** accepted ({}, {})'.format(candidates[iterations+1], model_eval))
                count_accepted = count_accepted + 1
                samples[iterations + 1] = candidates[iterations + 1]
                posterior_samples[iterations + 1] = posterior_candidates[iterations + 1]
            else:
                # rejected
                # print('** rejected ({}, {})'.format(candidates[iterations+1], model_eval))
                count_rejected = count_rejected + 1
                samples[iterations + 1] = samples[iterations]
                posterior_samples[iterations + 1] = posterior_samples[iterations]

        iterations = iterations + 1

    stop = time.time()
    computation_time = stop - start

    # evaluation
    print(' ')
    print('Computation time {} min'.format(computation_time / 60), )

    print('Mean of candidates (without burn-in): {}'.format(np.mean(samples[burn_in:len(samples)])))

    # save data to file
    save_workspace_to_file()

    # plot samples & candidates
    mlt.figure(1)
    plt_samples = mlt.plot(samples, label="Samples", marker='o', linestyle='dashed')
    plt_candidates = mlt.plot(candidates, label="Candidates", marker='x', linestyle='dotted')
    mlt.legend()

    mlt.figure(2)
    mlt.semilogy(posterior_samples, label="Samples Posterior", marker='o', linestyle='dashed')
    mlt.semilogy(posterior_candidates, label="Candidates Posterior", marker='x', linestyle='dotted')
    mlt.legend()

    mlt.figure(3)
    mlt.hist(samples[burn_in:len(samples)])
    mlt.show()
    mlt.xlabel('Samples')

    print(samples)
