import os
import pickle
import unittest
from typing import Tuple

import numpy as np
import numpy.testing as nptest
import pytest
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.utils.data_eval import sample_mode
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.Surrogate1DFactory import Surrogate1DFactory
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

RUN_LOCAL = True


class TestMetropolisSamplingVadere(unittest.TestCase):

    def config_vadere_1d(self, true_parameter_value: float) -> Tuple[InversionParameterMetropolis, MetropolisSampling]:

        # Parameters
        key = "speedDistributionMean"
        qoi = "waitingtime.txt"

        path2tutorial = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../.."))
        path2model = os.path.join(path2tutorial, "scenarios", "vadere-console.jar")
        path2scenario = os.path.join(path2tutorial, "scenarios", "rimea_01_pathway_discrete.scenario")
        run_local = True
        seed = 69835153

        nr_steps = 100

        # jump width
        bool_adaptive_jump_width = True
        acceptance_rate_limits = np.array([0.4, 0.6])
        jump_width = 0.1

        legal_limits_parameter = np.array([0.5, 2.5])
        prior_params = {"Type": "Normal", "Mean": 2.0, "Variance": 1.0, "Dim": 1, "Limits": legal_limits_parameter}
        rho = PriorFactory().create_prior_by_type(params=prior_params)
        initial_point = 2.0

        bool_noise = True
        meas_noise = 0.01
        batch_jump_width = 10

        # Surrogate model parameters
        bool_surrogate = False
        nr_points_surrogate = None
        surrogate_misfit = None
        limits_surrogate = None

        bool_load_surrogate = False
        surrogate_path = None

        dim = 1

        # initialize random object
        random_state = RandomState(seed=seed)

        # configure model
        vadere = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=1, log_lvl="OFF")
        vadere.set_data_saver(DataSaver(os.getcwd()))

        # generate data from Vadere
        if bool_noise:
            noise = np.random.normal(0, meas_noise, 1)[0]
        else:
            noise = 0

        # generate data
        data = vadere.eval_model(true_parameter_value, random_state=random_state) + noise

        param = InversionParameterMetropolis(model=vadere, prior=rho, seed=seed, bool_averaged=True, no_runs_averaged=2,
                                             path2results=os.getcwd(), bool_surrogate=bool_surrogate,
                                             bool_load_surrogate=bool_load_surrogate,
                                             surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                                             nr_points_surrogate=nr_points_surrogate,
                                             surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                             bool_surrogate_data_misfit=False,
                                             nr_points_surrogate_error=nr_points_surrogate,
                                             bool_write_data=True, n_samples=nr_steps, data=data,
                                             true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                             meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                             bool_plot=False, run_local=RUN_LOCAL, initial_point=initial_point,
                                             jump_width=jump_width, bool_adaptive_jump_width=bool_adaptive_jump_width,
                                             batch_jump_width=batch_jump_width,
                                             acceptance_rate_limits=acceptance_rate_limits,
                                             burn_in=0)

        # construct surrogate
        if bool_surrogate:
            if bool_load_surrogate:  # load existing surrogate
                vadere_model = pickle.load(open(os.path.abspath(surrogate_path), 'rb'))
            else:  # generate new surrogate
                # vadere_model = Surrogate1D(vadere, limits=limits_surrogate, nr_points=nr_points_surrogate)
                surrogate_factory = Surrogate1DFactory()
                surrogate_factory.create_surrogate_from_model(model=vadere, limits=limits_surrogate,
                                                              nr_points=nr_points_surrogate)
        else:
            vadere_model = vadere

        # Prior
        initial_point = rho.get_mean()

        # metropolis_config = MetropolisSampling(model=vadere_model, nr_iterations=nr_steps, random_state=random_state,
        #                                      initial_point=initial_point, jump_width=jump_width, data=data,
        #                                       meas_noise=meas_noise, batch_jump_width=batch_jump_width,
        #                                       acceptance_rate_limits=acceptance_rate_limits, prior_dist=rho,
        #                                       bool_acceptance_rate=bool_adaptive_jump_width,
        #                                       limits=legal_limits_parameter,
        #                                       bool_surrogate=bool_surrogate, surrogate_data_misfit=surrogate_misfit,
        #                                       dim=dim)
        metropolis_config = MetropolisSampling(param=param)

        return param, metropolis_config

    def config_vadere_2d(self, true_parameter_value: np.ndarray) -> InversionParameterMetropolis:

        # Parameters
        key = ["speedDistributionMean", "queueWidthLoading"]  # second parameter has no effect
        qoi = "egress.txt"

        path2tutorial = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../.."))
        path2model = os.path.join(path2tutorial, "scenarios", "vadere-console.jar")
        path2scenario = os.path.join(path2tutorial, "scenarios", "rimea_01_pathway_discrete.scenario")
        seed = 69835153

        nr_steps = 100

        # jump width
        bool_adaptive_jump_width = True
        acceptance_rate_limits = np.array([0.4, 0.6])
        jump_width = np.array([0.1, 1])

        legal_limits_parameter = np.array([[0.5, 1], [2.5, 10]])
        prior_params = {"Type": "Uniform", "Low": legal_limits_parameter[0, :], "High": legal_limits_parameter[1, :],
                        "Dim": 2}
        rho = PriorFactory().create_prior_by_type(params=prior_params)
        initial_point = np.array([2.0, 3])

        bool_noise = True
        meas_noise = 0.01
        batch_jump_width = 10

        # Surrogate model parameters
        bool_surrogate = False
        nr_points_surrogate = None
        surrogate_misfit = None
        limits_surrogate = None

        bool_load_surrogate = False
        surrogate_path = None

        # initialize random object
        random_state = RandomState(seed=seed)

        # configure model
        vadere = VadereModel(RUN_LOCAL, path2scenario, path2model, key, qoi, n_jobs=1, log_lvl="OFF")
        vadere.set_data_saver(DataSaver(os.getcwd()))

        # generate data from Vadere
        if bool_noise:
            noise = np.random.normal(0, meas_noise, 1)[0]
        else:
            noise = 0

        # generate data
        data = vadere.eval_model(true_parameter_value, random_state=random_state) + noise

        param = InversionParameterMetropolis(model=vadere, prior=rho, seed=seed, bool_averaged=True, no_runs_averaged=2,
                                             path2results=os.getcwd(), bool_surrogate=bool_surrogate,
                                             bool_load_surrogate=bool_load_surrogate,
                                             surrogate_path=surrogate_path, limits_surrogate=limits_surrogate,
                                             nr_points_surrogate=nr_points_surrogate,
                                             surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                             bool_surrogate_data_misfit=False,
                                             nr_points_surrogate_error=nr_points_surrogate,
                                             bool_write_data=True, n_samples=nr_steps, data=data,
                                             true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                             meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                             bool_plot=False, run_local=RUN_LOCAL, initial_point=initial_point,
                                             jump_width=jump_width, bool_adaptive_jump_width=bool_adaptive_jump_width,
                                             batch_jump_width=batch_jump_width,
                                             acceptance_rate_limits=acceptance_rate_limits,
                                             burn_in=None)

        return param

    # todo 2d inversion where both parameters have an impact on the qoi

    @pytest.mark.vadere
    def test_inversion_2d(self):

        true_parameter_value = np.array([1.34, 0.0])

        param = self.config_vadere_2d(true_parameter_value=true_parameter_value)

        result = InversionResultMetropolis()
        calc = InversionCalculator(param=param, result=result)
        sampling = MetropolisSampling(param=param)
        calc.set_sampling_strategy(sampling_config=sampling)
        calc.inversion()
        # result = calc.get_result()

        # Make sure that the inferred distribution has a mode close to the true parameter value
        nptest.assert_allclose(sample_mode(result.get_samples()[0, :]), true_parameter_value[0], rtol=0.05)

    @pytest.mark.vadere
    def test_inversion_1d(self):

        true_parameter_value = 1.34

        param, metropolis_config = self.config_vadere_1d(true_parameter_value=true_parameter_value)

        calc = InversionCalculator(param=param, result=InversionResultMetropolis())
        sampling = MetropolisSampling(param=param)
        calc.set_sampling_strategy(sampling_config=sampling)
        calc.inversion()
        result = calc.get_result()

        # Make sure that the inferred distribution has a mode close to the true parameter value
        nptest.assert_allclose(sample_mode(result.get_samples()), true_parameter_value, rtol=0.05)

        result2 = InversionResultMetropolis()
        metropolis_config.run_sampling(result=result2)

        # Make sure that the inferred distribution has a mode close to the true parameter value
        nptest.assert_allclose(sample_mode(result2.get_samples()), true_parameter_value, rtol=0.05)

    @pytest.mark.vadere
    def test_inversion_one_dimensional(self):

        # Parameters
        key = "speedDistributionMean"
        qoi = "waitingtime.txt"

        path2tutorial = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../.."))
        path2model = os.path.join(path2tutorial, "scenarios", "vadere-console.jar")
        path2scenario = os.path.join(path2tutorial, "scenarios", "rimea_01_pathway_discrete.scenario")
        run_local = True
        seed = 69835153

        nr_steps = 100

        # jump width
        bool_adaptive_jump_width = True
        acceptance_rate_limits = np.array([0.4, 0.6])
        jump_width = 0.1

        legal_limits_parameter = np.array([0.5, 2.5])
        prior_params = {"Type": "Normal", "Mean": 2.0, "Variance": 1.0, "Dim": 1, "Limits": legal_limits_parameter}

        bool_noise = True
        meas_noise = 0.01
        true_parameter_value = 1.34
        batch_jump_width = 10

        # Surrogate model parameters
        bool_surrogate = False
        nr_points_surrogate = None
        limits_surrogate = np.array([0.5, 2.2])

        bool_load_surrogate = False
        surrogate_path = None

        if type(key) == str:
            dim = 1
        else:
            dim = len(key)

        # initialize random object
        random_state = RandomState(seed=seed)

        # generate data from Vadere
        if bool_noise:
            noise = np.random.normal(0, meas_noise, 1)[0]
        else:
            noise = 0

        # configure model
        vadere = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=1, log_lvl="OFF")

        # generate data
        data = vadere.eval_model(true_parameter_value, random_state=random_state) + noise

        random_state = RandomState(seed)

        # construct surrogate
        if bool_surrogate:
            if bool_load_surrogate:  # load existing surrogate
                vadere_model = pickle.load(open(os.path.abspath(surrogate_path), 'rb'))
            else:  # generate new surrogate
                # vadere_model = Surrogate1D(vadere, limits=limits_surrogate, nr_points=nr_points_surrogate)
                surrogate_factory = Surrogate1DFactory()
                surrogate_factory.create_surrogate_from_model(model=vadere, limits=limits_surrogate,
                                                              nr_points=nr_points_surrogate)
        else:
            vadere_model = vadere

        # Prior
        my_prior = PriorFactory().create_prior_by_type(params=prior_params)
        initial_point = my_prior.get_mean()

        param = InversionParameterMetropolis(model=vadere_model, prior=my_prior, seed=seed, bool_averaged=True,
                                             no_runs_averaged=None,
                                             path2results=os.getcwd(), bool_surrogate=bool_surrogate,
                                             bool_load_surrogate=bool_load_surrogate,
                                             surrogate_path=None, limits_surrogate=limits_surrogate,
                                             nr_points_surrogate=None,
                                             surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                             bool_surrogate_data_misfit=False,
                                             nr_points_surrogate_error=None,
                                             bool_write_data=True, n_samples=nr_steps, data=data,
                                             true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                             meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                             bool_plot=False, run_local=RUN_LOCAL, initial_point=initial_point,
                                             jump_width=jump_width, bool_adaptive_jump_width=bool_adaptive_jump_width,
                                             batch_jump_width=batch_jump_width,
                                             acceptance_rate_limits=acceptance_rate_limits,
                                             burn_in=0)

        metropolis_config = MetropolisSampling(param)
        # metropolis_config = MetropolisSampling(vadere_model, nr_steps, random_state, initial_point, jump_width, data,
        #                                       meas_noise, batch_jump_width, acceptance_rate_limits, my_prior,
        #                                       bool_adaptive_jump_width, legal_limits_parameter, bool_surrogate,
        #                                       surrogate_data_misfit=None, dim=dim)

        # samples, posterior_samples, computation_time, candidates, posterior_candidates, jump_widths, \
        # acceptance_rate, model_eval, data_misfit_accepted \
        result = InversionResultMetropolis()
        result = metropolis_config.run_sampling(result)

        # Make sure that the inferred distribution has a mode close to the true parameter value
        nptest.assert_allclose(sample_mode(result.get_samples()), true_parameter_value, rtol=0.05)
