import os
import random
import unittest
import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.testing as nptest

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.calc.RejectionSampling import RejectionSampling
from uq.utils.data_eval import sample_mean
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.LinearModel import LinearModel, NoisyLinearModel
from uq.utils.prior.PriorFactory import PriorFactory

warnings.filterwarnings("ignore")

RUN_LOCAL = True  # for CI


class TestRejectionSamplingVadere(unittest.TestCase):
    def test_1d_inversion_rejection_deterministic_model(self, bool_plot: bool = False):
        true_parameter_value = 0.5

        m = random.random()
        t = random.random()
        linear_model = LinearModel(m=m, t=t)
        linear_model.set_data_saver(DataSaver(os.getcwd()))

        legal_limits_parameter = np.array([-10, 10])
        prior_params = {"Type": "Uniform", "Low": -2, "High": 3, "Dim": 1}
        seed = random.randint(0, 2 ** 32 - 1)
        rho = PriorFactory().create_prior_by_type(prior_params)
        nr_steps = 10 ** 5

        data = linear_model.eval_model(true_parameter_value)

        abc_threshold_relative = 0.001
        surrogate_noise_std = None

        param = InversionParameterRejection(model=linear_model, prior=rho, seed=seed, bool_averaged=False,
                                            no_runs_averaged=None, path2results=os.getcwd(), bool_surrogate=False,
                                            bool_load_surrogate=False, surrogate_path=None, limits_surrogate=None,
                                            nr_points_surrogate=None, surrogate_fit_type=None,
                                            nr_points_averaged_surrogate=None, bool_surrogate_data_misfit=False,
                                            nr_points_surrogate_error=None, bool_write_data=True, n_samples=nr_steps,
                                            data=data, true_parameter_value=true_parameter_value, bool_noise=False,
                                            meas_noise=None, legal_limits_parameters=legal_limits_parameter,
                                            bool_plot=False, run_local=RUN_LOCAL,
                                            abc_threshold_relative=abc_threshold_relative,
                                            surrogate_noise_std=surrogate_noise_std)

        result = InversionResult()
        calc = InversionCalculator(param=param, result=result)
        calc.set_sampling_strategy(sampling_config=RejectionSampling(param))
        calc.inversion()

        print(sample_mean(result.get_samples()))

        if bool_plot:
            plt.figure()
            legend_str = "N: %d, acceptance: %f" % (np.size(result.get_samples()), result.get_total_acceptance_ratio())
            plt.hist(result.get_samples(), label=legend_str)
            plt.legend()
            plt.show()

            plt.figure()
            plt.plot(result.get_candidates(), result.get_data_misfit(), '.')
            plt.plot(result.get_samples(), result.get_data_misfit_accepted(), '.')
            plt.show()

        nptest.assert_allclose(sample_mean(result.get_samples()), true_parameter_value, rtol=0.01)

    def test_1d_inversion_rejection_noisy_model(self, bool_plot: bool = False):
        true_parameter_value = 0.5
        m = random.random()
        while m < 0.1:
            m = random.random()
        t = random.random()
        noise_level = 1e-1
        noisy_linear_model = NoisyLinearModel(m=m, t=t, noise_level=noise_level)
        noisy_linear_model.set_data_saver(DataSaver(os.getcwd()))

        legal_limits_parameter = np.array([-10, 10])
        prior_params = {"Type": "Uniform", "Low": -2, "High": 3, "Dim": 1}
        seed = random.randint(0, 2 ** 32 - 1)
        rho = PriorFactory().create_prior_by_type(prior_params)
        nr_steps = 10 ** 5

        data, _, _ = noisy_linear_model.eval_model_averaged(true_parameter_value, nr_runs_averaged=100)

        abc_threshold_relative = 0.001
        surrogate_noise_std = None

        param = InversionParameterRejection(model=noisy_linear_model, prior=rho, seed=seed, bool_averaged=False,
                                            no_runs_averaged=None, path2results=os.getcwd(), bool_surrogate=False,
                                            bool_load_surrogate=False, surrogate_path=None, limits_surrogate=None,
                                            nr_points_surrogate=None, surrogate_fit_type=None,
                                            nr_points_averaged_surrogate=None, bool_surrogate_data_misfit=False,
                                            nr_points_surrogate_error=None, bool_write_data=True, n_samples=nr_steps,
                                            data=data, true_parameter_value=true_parameter_value, bool_noise=False,
                                            meas_noise=None, legal_limits_parameters=legal_limits_parameter,
                                            bool_plot=False, run_local=RUN_LOCAL,
                                            abc_threshold_relative=abc_threshold_relative,
                                            surrogate_noise_std=surrogate_noise_std)

        result = InversionResult()
        calc = InversionCalculator(param=param, result=result)
        calc.set_sampling_strategy(sampling_config=RejectionSampling(param))
        calc.inversion()

        print(sample_mean(result.get_samples()))

        if bool_plot:
            h_f = plt.figure()
            legend_str = "N: %d, acceptance: %f" % (np.size(result.get_samples()), result.get_total_acceptance_ratio())
            plt.hist(result.get_samples(), label=legend_str)
            plt.plot(sample_mean(result.get_samples()) * np.ones(2, ), np.asarray(h_f.gca().get_ylim()),
                     label="Mean: %f" %sample_mean(result.get_samples()))
            plt.legend()
            plt.show()

            plt.figure()
            plt.plot(result.get_candidates(), result.get_data_misfit(), '.')
            plt.plot(result.get_samples(), result.get_data_misfit_accepted(), '.')
            plt.show()

        nptest.assert_allclose(sample_mean(result.get_samples()), true_parameter_value, rtol=0.1)
