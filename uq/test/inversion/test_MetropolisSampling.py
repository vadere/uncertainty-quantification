import os
import random
import unittest

import matplotlib.pyplot as plt
import numpy as np
import numpy.testing as nptest

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.utils.data_eval import sample_mode
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.LinearModel import LinearModel
from uq.utils.model.MultiplicativeModel import MultiplicativeModel
from uq.utils.prior.PriorFactory import PriorFactory

RUN_LOCAL = True


class TestMetropolisSampling(unittest.TestCase):

    def test_inversion_1d(self, bool_plot: bool = False):
        true_parameter_value = 2.00

        m = random.random()
        t = random.random()
        linear_model = LinearModel(m=m, t=t)
        linear_model.set_data_saver(DataSaver(os.getcwd()))

        legal_limits_parameter = np.array([-10, 10])
        prior_params = {"Type": "Uniform", "Low": -2, "High": 5, "Dim": 1}
        bool_surrogate = False
        seed = random.randint(0, 2 ** 32 - 1)
        rho = PriorFactory().create_prior_by_type(prior_params)
        nr_steps = 10 ** 4
        burn_in = int(nr_steps / 10)

        bool_noise = False
        meas_noise = 1e-5
        data = linear_model.eval_model(true_parameter_value)

        jump_width = 0.1
        initial_point = 0.0
        bool_adaptive_jump_width = True
        batch_jump_width = 100
        acceptance_rate_limits = np.array([0.4, 0.6])

        param = InversionParameterMetropolis(model=linear_model, prior=rho, seed=seed, bool_averaged=True,
                                             no_runs_averaged=2,
                                             path2results=os.getcwd(), bool_surrogate=bool_surrogate,
                                             bool_load_surrogate=None,
                                             surrogate_path=None, limits_surrogate=None,
                                             nr_points_surrogate=None,
                                             surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                             bool_surrogate_data_misfit=False,
                                             nr_points_surrogate_error=None,
                                             bool_write_data=True, n_samples=nr_steps, data=data,
                                             true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                             meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                             bool_plot=False, run_local=RUN_LOCAL, initial_point=initial_point,
                                             jump_width=jump_width, bool_adaptive_jump_width=bool_adaptive_jump_width,
                                             batch_jump_width=batch_jump_width,
                                             acceptance_rate_limits=acceptance_rate_limits,
                                             burn_in=burn_in)

        calc = InversionCalculator(param=param, result=InversionResultMetropolis())
        sampling = MetropolisSampling(param=param)
        calc.set_sampling_strategy(sampling_config=sampling)
        calc.inversion()
        result = calc.get_result()

        if bool_plot:
            plt.figure()
            plt.plot(result.get_samples())
            plt.show()

            plt.figure()
            x = np.linspace(legal_limits_parameter[0], legal_limits_parameter[1])
            plt.plot(x, linear_model.eval_model(x))
            plt.show()

        # More formal checks
        # make sure than number of samples / candidates is equal to nr_steps (+1 for initial point)
        self.assertTrue(len(result.get_samples()) == nr_steps + 1)
        self.assertTrue(len(result.get_candidates()) == nr_steps + 1)
        self.assertTrue(len(result.get_posterior_candidates()) == nr_steps + 1)
        self.assertTrue(len(result.get_posterior_samples()) == nr_steps + 1)
        self.assertEqual(result.get_total_acceptance_ratio(),
                         np.sum(result.get_samples()[1:] == result.get_candidates()[1:]) / nr_steps)

        # Make sure that the inferred distribution has a mode close to the true parameter value
        nptest.assert_allclose(sample_mode(result.get_samples(), burn_in=burn_in), true_parameter_value, rtol=0.01)

    def test_inversion_2d(self, bool_plot: bool = False):
        model = MultiplicativeModel()
        model.set_data_saver(DataSaver(os.getcwd()))

        true_parameter_value = np.array([0.1, 0.5])

        legal_limits_parameter = np.array([[0, 0], [2, 2]])
        prior_params = {"Type": "Uniform", "Low": legal_limits_parameter[0, :], "High": legal_limits_parameter[1, :],
                        "Dim": 2}
        bool_surrogate = False
        seed = random.randint(0, 2 ** 32 - 1)
        rho = PriorFactory().create_prior_by_type(prior_params)
        nr_steps = 10 ** 4
        burn_in = int(nr_steps / 10)

        bool_noise = False
        meas_noise = 1e-5
        data = model.eval_model(true_parameter_value)

        jump_width = 0.1
        initial_point = np.array([1.0, 1.0])
        bool_adaptive_jump_width = True
        batch_jump_width = 100
        acceptance_rate_limits = np.array([0.4, 0.6])

        param = InversionParameterMetropolis(model=model, prior=rho, seed=seed, bool_averaged=True,
                                             no_runs_averaged=2,
                                             path2results=os.getcwd(), bool_surrogate=bool_surrogate,
                                             bool_load_surrogate=None,
                                             surrogate_path=None, limits_surrogate=None,
                                             nr_points_surrogate=None,
                                             surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                             bool_surrogate_data_misfit=False,
                                             nr_points_surrogate_error=None,
                                             bool_write_data=True, n_samples=nr_steps, data=data,
                                             true_parameter_value=true_parameter_value, bool_noise=bool_noise,
                                             meas_noise=meas_noise, legal_limits_parameters=legal_limits_parameter,
                                             bool_plot=False, run_local=RUN_LOCAL, initial_point=initial_point,
                                             jump_width=jump_width, bool_adaptive_jump_width=bool_adaptive_jump_width,
                                             batch_jump_width=batch_jump_width,
                                             acceptance_rate_limits=acceptance_rate_limits,
                                             burn_in=burn_in)

        calc = InversionCalculator(param=param, result=InversionResultMetropolis())
        sampling = MetropolisSampling(param=param)
        calc.set_sampling_strategy(sampling_config=sampling)
        calc.inversion()
        result = calc.get_result()

        if bool_plot:
            plt.figure()
            plt.plot(result.get_samples()[0, :], result.get_samples()[1, :], '.:')
            plt.plot(true_parameter_value[0], true_parameter_value[1], 'rx')
            plt.show()

        samples = result.get_samples()[:, burn_in:]
        samples_x1 = samples[0, :]
        samples_x2 = samples[1, :]

        # make sure that posterior values are along isolines
        # based on multiplative model
        nptest.assert_almost_equal(np.mean(data / samples_x1 - samples_x2), 0, decimal=2)
        nptest.assert_almost_equal(np.mean(data / samples_x2 - samples_x1), 0, decimal=2)
