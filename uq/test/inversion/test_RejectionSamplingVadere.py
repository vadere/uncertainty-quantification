import os
import unittest
import warnings

import numpy as np
import numpy.testing as nptest
import pytest
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.calc.RejectionSampling import RejectionSampling
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

warnings.filterwarnings("ignore")

RUN_LOCAL = True  # for CI


class TestRejectionSamplingVadere(unittest.TestCase):

    # todo test inversion with a surrogate for the model itself and with a surrogate for the data misfit

    def configure_vadere_model(self, true_parameter_value: float, nr_steps: int) -> InversionParameterRejection:
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        random_state = RandomState(seed)

        key = "speedDistributionMean"
        qoi = "mean_density.txt"
        legal_limits_parameter = np.array([0.1, 3.0])
        abc_threshold_relative = 0.5
        surrogate_noise_std = 0

        path2tutorial = os.path.dirname(os.path.realpath(__file__))
        path2model = os.path.abspath(os.path.join(path2tutorial, "../../scenarios", "vadere-console.jar"))
        path2scenario = os.path.abspath(
            os.path.join(path2tutorial, "../../scenarios", "Liddle_bhm_v3_fixed_seed.scenario"))

        vadere_model = VadereModel(RUN_LOCAL, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF",
                                   data_saver=DataSaver(os.getcwd()))
        prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.5, "Dim": 1, "Limits": legal_limits_parameter}
        rho = PriorFactory().create_prior_by_type(params=prior_params)

        # Artificial data
        data = vadere_model.eval_model(true_parameter_value, random_state=random_state)

        param = InversionParameterRejection(model=vadere_model, prior=rho, seed=seed, bool_averaged=True,
                                            no_runs_averaged=2,
                                            path2results=os.getcwd(), bool_surrogate=False,
                                            bool_load_surrogate=False,
                                            surrogate_path=None, limits_surrogate=None, nr_points_surrogate=None,
                                            surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                            bool_surrogate_data_misfit=False, nr_points_surrogate_error=None,
                                            bool_write_data=True, n_samples=nr_steps, data=data,
                                            true_parameter_value=true_parameter_value, bool_noise=False,
                                            meas_noise=None,
                                            legal_limits_parameters=legal_limits_parameter, bool_plot=False,
                                            run_local=RUN_LOCAL, abc_threshold_relative=abc_threshold_relative,
                                            surrogate_noise_std=surrogate_noise_std)

        return param

    @pytest.mark.vadere
    def test_1d_inversion_rejection(self, bool_plot: bool = False):
        true_parameter_value = 1.34
        nr_steps = 100

        param = self.configure_vadere_model(true_parameter_value=true_parameter_value, nr_steps=nr_steps)

        result = InversionResult()
        calc = InversionCalculator(param=param, result=result)
        calc.set_sampling_strategy(sampling_config=RejectionSampling(param))
        calc.inversion()

        # todo more precise test (without increasing the number of steps a lot)
        nptest.assert_allclose(np.mean(result.get_samples()), true_parameter_value, rtol=0.5)
