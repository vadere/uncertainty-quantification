import copy
import unittest

import numpy.testing as nptest
from numpy.random import RandomState

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.config_matrix_constantine_fct import config_matrix_constantine


class TestSeedActiveSubspaceCalculatorGradient(unittest.TestCase):
    # use the true gradients
    def test_same_seed_true_gradients(self):
        seed = 254354
        bool_gradient = True  # is an analytical gradient available? -> True
        step_size = None
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping
        case = 3

        test_model, x_lower, x_upper, m, density_type, test_input = \
            config_matrix_constantine(case=case, random_state=RandomState())
        bool_averaged = False

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=False, bool_print=False,
                                                 bool_plot=False, path2results=None)

        # True gradients
        as_calculator1 = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator1.identify_active_subspace()
        results1 = as_calculator1.get_result()

        params.set_model(model=test_model)
        as_calculator2 = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator2.identify_active_subspace()
        results2 = as_calculator2.get_result()

        nptest.assert_array_equal(results1.get_error_lambda_hat(), results2.get_error_lambda_hat())
        nptest.assert_array_equal(results1.get_error_c_hat(), results2.get_error_c_hat())
        nptest.assert_array_equal(results1.get_activity_scores(), results2.get_activity_scores())
        nptest.assert_array_equal(results1.get_size_subspace(), results2.get_size_subspace())
        nptest.assert_array_equal(results1.get_n_samples(), results2.get_n_samples())
        nptest.assert_array_equal(results1.get_lambda_hat(), results2.get_lambda_hat())
        nptest.assert_array_equal(results1.get_W_active(), results2.get_W_active())
        nptest.assert_array_equal(results1.get_lambda_true(), results2.get_lambda_true())
        nptest.assert_array_equal(results1.get_idx_gap(), results2.get_idx_gap())
        nptest.assert_array_equal(results1.get_distance_subspace(), results2.get_distance_subspace())

        self.assertTrue(results2.equals(results1))

    def test_different_seeds_true_gradients(self):
        seed1 = 254354
        seed2 = 558734

        bool_gradient = True  # is an analytical gradient available? -> True
        step_size = None
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping
        case = 3

        bool_save_data = False
        bool_print = False
        bool_plot = False

        test_model, x_lower, x_upper, m, density_type, test_input = \
            config_matrix_constantine(case=case, random_state=RandomState())
        path2results = None  # os.path.abspath("../results")
        bool_averaged = None
        no_runs_averaged = None

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=None, bool_averaged=bool_averaged,
                                                 no_runs_averaged=no_runs_averaged, bool_save_data=bool_save_data,
                                                 bool_print=bool_print, bool_plot=bool_plot, path2results=path2results)

        # True gradients
        params1 = params
        params1.set_seed(seed1)
        as_calculator1 = ActiveSubspaceCalculatorGradient(param=params1)
        as_calculator1.identify_active_subspace()
        results1 = as_calculator1.get_result()

        params2 = params
        params2.set_seed(seed2)
        as_calculator2 = ActiveSubspaceCalculatorGradient(param=params2)
        as_calculator2.identify_active_subspace()
        results2 = as_calculator2.get_result()

        nptest.assert_raises(AssertionError, nptest.assert_array_equal,
                             results1.get_activity_scores(), results2.get_activity_scores())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal,
                             results1.get_lambda_hat(), results2.get_lambda_hat())

        self.assertRaises(AssertionError, self.assertTrue, results2.equals(results1))
        self.assertRaises(AssertionError, self.assertTrue, results1.equals(results2))

    # use the finite difference approximation
    def test_same_seed_finite_differences(self):
        seed = 254354
        bool_gradient = False  # is an analytical gradient available? -> True
        step_size = 1e-3
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping
        case = 3

        bool_save_data = False
        bool_print = False
        bool_plot = False

        test_model, x_lower, x_upper, m, density_type, test_input = \
            config_matrix_constantine(case=case, random_state=RandomState())
        path2results = None  # os.path.abspath("../results")
        bool_averaged = None
        no_runs_averaged = None

        # True gradients
        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=no_runs_averaged, bool_save_data=bool_save_data,
                                                 bool_print=bool_print, bool_plot=bool_plot, path2results=path2results)

        as_calculator1 = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator1.identify_active_subspace()
        results1 = as_calculator1.get_result()

        as_calculator2 = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator2.identify_active_subspace()
        results2 = as_calculator2.get_result()

        nptest.assert_array_equal(results1.get_activity_scores(), results2.get_activity_scores())
        nptest.assert_array_equal(results1.get_lambda_hat(), results2.get_lambda_hat())

        self.assertTrue(results2.equals(results1))
        self.assertTrue(results1.equals(results2))

    def test_different_seeds_finite_differences(self):
        seed1 = 3875615
        seed2 = 986465
        bool_gradient = False  # is an analytical gradient available? -> True
        step_size = 1e-3
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping
        case = 3

        test_model, x_lower, x_upper, m, density_type, test_input = \
            config_matrix_constantine(case=case, random_state=RandomState())
        bool_averaged = None
        no_runs_averaged = None

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=None, bool_averaged=bool_averaged,
                                                 no_runs_averaged=no_runs_averaged, bool_save_data=False,
                                                 bool_print=False, bool_plot=False, path2results=None)

        # True gradients
        params1 = copy.deepcopy(params)
        params1.set_seed(seed=seed1)
        as_calculator1 = ActiveSubspaceCalculatorGradient(param=params1)
        as_calculator1.identify_active_subspace()
        results1 = as_calculator1.get_result()

        params2 = copy.deepcopy(params)
        params2.set_seed(seed=seed2)
        as_calculator2 = ActiveSubspaceCalculatorGradient(param=params2)
        as_calculator2.identify_active_subspace()
        results2 = as_calculator2.get_result()

        # max_rel_error_eig, error_c_hat, activity_scores, true_activity_scores, n, path2results, \
        #     n_samples, lambda_eig, w_active, test_y, lambda_eig_true, idx_gap, idx_gap_true, distance_subspace,  \
        #     true_distance, C_hat

        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_error_lambda_hat(),
                             results2.get_error_lambda_hat())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_error_c_hat(),
                             results2.get_error_c_hat())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_activity_scores(),
                             results2.get_activity_scores())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_lambda_hat(),
                             results2.get_lambda_hat())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_W_active(),
                             results2.get_W_active())

        self.assertRaises(AssertionError, self.assertTrue, results2.equals(results1))
        self.assertRaises(AssertionError, self.assertTrue, results1.equals(results2))
