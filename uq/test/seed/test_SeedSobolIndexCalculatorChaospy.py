import copy
import unittest

import numpy as np
import numpy.testing as nptest

from uq.sensitivity_analysis.calc.SobolIndexCalculatorChaospy import SobolIndexCalculatorChaospy
from uq.sensitivity_analysis.calc.SobolIndexParameterChaospy import SobolIndexParameterChaospy
from uq.utils.model.IshigamiModel import IshigamiModel
from uq.utils.prior.UniformGenMult import UniformGenMult


class TestSeedSobolIndexCalculatorChaospy(unittest.TestCase):

    def configure_ishigami(self) -> SobolIndexParameterChaospy:
        model = IshigamiModel(a=7, b=0.05)
        bool_averaged = False
        no_runs_averaged = None

        M = 10000

        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=model.get_dimension())
        bool_load_approx_model = False
        bool_load_samples = False
        order = 4

        param = SobolIndexParameterChaospy(model=model, prior=rho, M=M, order=order,
                                           bool_load_approx_model=bool_load_approx_model,
                                           path_to_approx_model=None, bool_load_samples=bool_load_samples,
                                           path_to_samples=None, seed=None, bool_averaged=bool_averaged,
                                           no_runs_averaged=no_runs_averaged)

        return param

    def test_same_seed_ishigami_function(self):
        param = self.configure_ishigami()
        seed = int(np.random.uniform(0, 2 ** 32 - 1))
        param.set_seed(seed)

        calc1 = SobolIndexCalculatorChaospy(param)
        calc1.calc_sobol_indices()
        result1 = calc1.get_result()

        calc2 = SobolIndexCalculatorChaospy(param)
        calc2.calc_sobol_indices()
        result2 = calc1.get_result()

        nptest.assert_array_equal(result1.get_total_indices(), result2.get_total_indices())
        nptest.assert_array_equal(result1.get_first_order_indices(), result2.get_first_order_indices())

        self.assertTrue(result1.equals(result2))

    def test_different_seeds_ishigami_function(self):
        param = self.configure_ishigami()
        seed1 = int(np.random.uniform(0, 2 ** 32 - 1))
        seed2 = int(np.random.uniform(0, 2 ** 32 - 1))

        param1 = copy.deepcopy(param)
        param1.set_seed(seed1)

        calc1 = SobolIndexCalculatorChaospy(param1)
        calc1.calc_sobol_indices()
        result1 = calc1.get_result()

        param2 = copy.deepcopy(param)
        param2.set_seed(seed2)

        calc2 = SobolIndexCalculatorChaospy(param2)
        calc2.calc_sobol_indices()
        result2 = calc2.get_result()

        nptest.assert_raises(AssertionError, nptest.assert_array_equal, result1.get_total_indices(),
                             result2.get_total_indices())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, result1.get_first_order_indices(),
                             result2.get_first_order_indices())

        self.assertRaises(AssertionError, self.assertTrue, result1.equals(result2))
