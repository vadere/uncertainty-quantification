import copy
import os
import unittest
import warnings

import numpy as np
import numpy.testing as nptest
import pytest
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterMetropolis import InversionParameterMetropolis
from uq.inversion.calc.InversionResultMetropolis import InversionResultMetropolis
from uq.inversion.calc.MetropolisSampling import MetropolisSampling
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

warnings.filterwarnings("ignore")

RUN_LOCAL = True  # for CI


class TestSeedInversionCalculatorBIVadere(unittest.TestCase):

    def configure_vadere_model(self) -> InversionParameterMetropolis:
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        random_state = RandomState(seed)

        key = "speedDistributionMean"
        qoi = "mean_density.txt"
        initial_point_input = 0.75
        legal_limits_parameter = np.array([0.1, 3.0])
        true_parameter_value = 1.34
        nr_steps = 12
        burn_in = int(0.1 * nr_steps)
        jump_width = 0.5
        batch_jump_width = 100
        acceptance_rate_limits = np.array([0.4, 0.6])

        path2tutorial = os.path.dirname(os.path.realpath(__file__))
        path2model = os.path.abspath(os.path.join(path2tutorial, "../../scenarios", "vadere-console.jar"))
        path2scenario = os.path.abspath(
            os.path.join(path2tutorial, "../../scenarios", "Liddle_bhm_v3_fixed_seed.scenario"))

        vadere_model = VadereModel(RUN_LOCAL, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")
        vadere_model.set_data_saver(DataSaver(os.getcwd()))
        prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.5, "Dim": 1, "Limits": legal_limits_parameter}
        rho = PriorFactory().create_prior_by_type(params=prior_params)

        # Artificial data
        data = vadere_model.eval_model(true_parameter_value, random_state=random_state)

        param = InversionParameterMetropolis(model=vadere_model, prior=rho, seed=seed, bool_averaged=True,
                                             no_runs_averaged=2, path2results=os.getcwd(), bool_surrogate=False,
                                             bool_load_surrogate=False, surrogate_path=None, limits_surrogate=None,
                                             nr_points_surrogate=None, surrogate_fit_type=None,
                                             nr_points_averaged_surrogate=None, bool_surrogate_data_misfit=False,
                                             nr_points_surrogate_error=None, bool_write_data=True, n_samples=nr_steps,
                                             data=data, true_parameter_value=true_parameter_value, bool_noise=False,
                                             meas_noise=None, legal_limits_parameters=legal_limits_parameter,
                                             bool_plot=False, run_local=RUN_LOCAL, initial_point=initial_point_input,
                                             jump_width=jump_width, bool_adaptive_jump_width=True,
                                             batch_jump_width=batch_jump_width,
                                             acceptance_rate_limits=acceptance_rate_limits, burn_in=burn_in)

        return param

    # assure that the same seed leads to the same result
    @pytest.mark.vadere
    def test_bayesian_inversion_same_seeds(self, bool_plot: bool = False):
        param = self.configure_vadere_model()

        calc1 = InversionCalculator(param=param, result=InversionResultMetropolis())
        sampling1 = MetropolisSampling(param=param)
        calc1.set_sampling_strategy(sampling_config=sampling1)
        calc1.inversion()
        result1 = calc1.get_result()

        calc2 = InversionCalculator(param=param, result=InversionResultMetropolis())
        sampling2 = MetropolisSampling(param=param)
        calc2.set_sampling_strategy(sampling_config=sampling2)
        calc2.inversion()
        result2 = calc2.get_result()

        nptest.assert_almost_equal(result1.get_samples(), result2.get_samples())
        nptest.assert_almost_equal(result1.get_candidates(), result2.get_candidates())
        nptest.assert_almost_equal(result1.get_acceptance_ratio(), result2.get_acceptance_ratio())
        self.assertTrue(result1.equals(result2))

    # Assure that results are dependent on the seed
    @pytest.mark.vadere
    def test_bayesian_inversion_different_seeds(self, bool_plot: bool = False):
        seed1 = int(np.random.uniform(0, 2 ** 32 - 1))
        seed2 = int(np.random.uniform(0, 2 ** 32 - 1))

        param = self.configure_vadere_model()

        param1 = copy.deepcopy(param)
        param1.set_seed(seed1)
        calc1 = InversionCalculator(param=param1, result=InversionResultMetropolis())
        sampling1 = MetropolisSampling(param=param1)
        calc1.set_sampling_strategy(sampling_config=sampling1)
        calc1.inversion()
        results1 = calc1.get_result()

        param2 = copy.deepcopy(param)
        param2.set_seed(seed2)
        calc2 = InversionCalculator(param=param2, result=InversionResultMetropolis())
        sampling2 = MetropolisSampling(param=param2)
        calc2.set_sampling_strategy(sampling_config=sampling2)
        calc2.inversion()
        results2 = calc2.get_result()

        nptest.assert_raises(AssertionError, nptest.assert_almost_equal, results1.get_samples(), results2.get_samples())
        nptest.assert_raises(AssertionError, nptest.assert_almost_equal, results1.get_candidates(),
                             results2.get_candidates())

        nptest.assert_raises(AssertionError, self.assertTrue, results1.equals(results2))
