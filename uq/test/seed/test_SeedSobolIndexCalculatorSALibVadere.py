import os
import unittest

import numpy as np
import numpy.testing as nptest
import pytest

from uq.sensitivity_analysis.calc.SobolIndexCalculatorSALib import SobolIndexCalculatorSALib
from uq.sensitivity_analysis.calc.SobolIndexParameterSALib import SobolIndexParameterSALib
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.UniformGenMult import UniformGenMult


class TestSeedSobolIndexCalculatorSALib(unittest.TestCase):
    @pytest.mark.vadere
    def test_same_seed(self):
        seed = 78613651
        bool_averaged = True
        no_runs_averaged = 2
        x_lower = np.array([0.5, 0.0])  # lower bounds for parameters
        x_upper = np.array([2.2, 1.0])  # upper bounds for parameters
        # parameters
        key = ["attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation"]  # uncertain parameters

        qoi = "mean_density.txt"  # quantity of interest
        run_local = True  # run on local machine (vs. run on server)

        # model
        cur_dir = os.path.dirname(os.path.realpath(__file__))
        path2tutorial = os.path.abspath(os.path.join(cur_dir, "../../inversion"))

        path2model = os.path.abspath(os.path.join(cur_dir, "../../scenarios", "vadere-console.jar"))

        path2scenario = os.path.abspath(os.path.join(cur_dir, "../../scenarios", "Liddle_bhm_v3_free_seed.scenario"))
        path2scenario = os.path.abspath(os.path.join(cur_dir, "../../scenarios", "Liddle_bhm_v3_fixed_seed.scenario"))

        # test_model = TestModel()
        test_model = VadereModel(run_local, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF")

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=len(key))
        params = SobolIndexParameterSALib(model=test_model, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                          no_runs_averaged=no_runs_averaged, N=1, path2results=None)

        calc1 = SobolIndexCalculatorSALib(params)
        results1 = calc1.calc_sobol_indices()

        calc2 = SobolIndexCalculatorSALib(params)
        results2 = calc2.calc_sobol_indices()

        nptest.assert_array_almost_equal(results1.get_first_order_indices(), results2.get_first_order_indices())
        nptest.assert_array_almost_equal(results1.get_total_sobol_indices(), results2.get_total_sobol_indices())
        nptest.assert_array_almost_equal(results1.get_param_values(), results2.get_param_values())

    # test_different_seed:
    # Saltelli sampling calculates fixed abscissas that are independent of the seed just depend on the number of samples
