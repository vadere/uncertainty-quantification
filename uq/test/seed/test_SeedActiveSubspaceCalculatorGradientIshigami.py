import copy
import os
import unittest

import numpy as np
import numpy.testing as nptest

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa
from uq.utils.model.IshigamiModel import IshigamiModel

RUN_LOCAL = False


class TestSeedActiveSubspaceCalculatorGradientIshigami(unittest.TestCase):

    def configure_ishigami(self) -> ActiveSubspaceParameterGradient:
        model = IshigamiModel(a=7, b=0.05)
        bool_averaged = False
        no_runs_averaged = None

        alpha = 10
        k = 1 + 1
        bool_gradient = True
        step_size_relative = None
        step_size = None
        M_boot = 7
        case = None

        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55

        param = ActiveSubspaceParameterGradient(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                bool_gradient=bool_gradient, M_boot=M_boot,
                                                step_size_relative=step_size_relative, step_size=step_size, case=case,
                                                seed=None, bool_averaged=bool_averaged,
                                                no_runs_averaged=no_runs_averaged, bool_save_data=True,
                                                bool_print=True, bool_plot=False, path2results=os.getcwd())

        return param

    def config_vadere(self) -> ActiveSubspaceParameterGradient:
        scenario_name = "bottleneck_OSM_all_in_one_N60.scenario"  # 5in1

        bool_gradient = False  # is an analytical gradient available? -> True
        bool_averaged = True
        no_runs_averaged = 3  # average over multiple runs?

        alpha = 1
        k = 1  # desired dimension of subspace +1
        M_boot = 5  # Constantine: Typically between 100 and 10^5
        case = None

        # parameter limits

        x_lower = np.array([1.0, 0.5, 0.1, 160, 30])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200, 70])  # upper bounds for parameters

        step_size = 0.025  # Number of pedestrians wird um 1 erhöht

        # parameters
        key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation",
               "sources.[id==3].spawnNumber",  # "bottleneck_width",
               "pedPotentialHeight"]  # uncertain parameters

        qoi = "flow.txt"

        step_size_relative = step_size * (x_upper - x_lower)

        # configure setup
        test_model, _, _ = configure_vadere_sa(run_local=RUN_LOCAL, scenario_name=scenario_name, key=key,
                                                 qoi=qoi)

        param = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                bool_gradient=bool_gradient, M_boot=M_boot,
                                                step_size_relative=step_size_relative, step_size=step_size, case=case,
                                                seed=None, bool_averaged=bool_averaged,
                                                no_runs_averaged=no_runs_averaged, bool_save_data=True,
                                                bool_print=True, bool_plot=False, path2results=os.getcwd())
        return param

    # @unittest.skip("just temporarily: takes too long")
    def test_same_seed_activity_scores_ishigami(self):
        seed = int(np.random.uniform(0, 2 ** 32 - 1))
        param = self.configure_ishigami()

        param.set_seed(seed)

        as_calculator1 = ActiveSubspaceCalculatorGradient(param=param)
        as_calculator1.identify_active_subspace()
        results1 = as_calculator1.get_result()

        as_calculator2 = ActiveSubspaceCalculatorGradient(param=param)
        as_calculator2.identify_active_subspace()
        results2 = as_calculator2.get_result()

        nptest.assert_array_equal(results1.get_activity_scores(), results2.get_activity_scores())
        nptest.assert_array_equal(results1.get_lambda_hat(), results2.get_lambda_hat())

        self.assertTrue(results1.equals(other=results2))
        self.assertTrue(results2.equals(other=results1))

    def test_different_seeds_activity_scores_ishigami(self):
        seed1 = int(np.random.uniform(0, 2 ** 32 - 1))
        seed2 = int(np.random.uniform(0, 2 ** 32 - 1))

        param = self.configure_ishigami()

        param1 = copy.deepcopy(param)
        param1.set_seed(seed1)

        as_calculator1 = ActiveSubspaceCalculatorGradient(param=param1)
        as_calculator1.identify_active_subspace()
        results1 = as_calculator1.get_result()

        param2 = copy.deepcopy(param)
        param2.set_seed(seed2)

        as_calculator2 = ActiveSubspaceCalculatorGradient(param=param2)
        as_calculator2.identify_active_subspace()
        results2 = as_calculator2.get_result()

        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_activity_scores(),
                             results2.get_activity_scores())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_lambda_hat(),
                             results2.get_lambda_hat())

        self.assertRaises(AssertionError, self.assertTrue, results1.equals(results2))
        self.assertRaises(AssertionError, self.assertTrue, results2.equals(results1))
