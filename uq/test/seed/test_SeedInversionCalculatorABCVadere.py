import copy
import os
import unittest
import warnings

import numpy as np
import numpy.testing as nptest
import pytest
from numpy.random import RandomState

from uq.inversion.calc.InversionCalculator import InversionCalculator
from uq.inversion.calc.InversionParameterRejection import InversionParameterRejection
from uq.inversion.calc.InversionResult import InversionResult
from uq.inversion.calc.RejectionSampling import RejectionSampling
from uq.inversion.run_inversion_ABC_corridor import run_inversion_ABC_corridor_fct
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.PriorFactory import PriorFactory

warnings.filterwarnings("ignore")

RUN_LOCAL = True  # for CI


class TestSeedInversionCalculatorABCVadere(unittest.TestCase):
    @pytest.mark.vadere
    def configure_vadere_model(self) -> InversionParameterRejection:
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        random_state = RandomState(seed)

        key = "speedDistributionMean"
        qoi = "mean_density.txt"
        legal_limits_parameter = np.array([0.1, 3.0])
        true_parameter_value = 1.34
        nr_steps = 12
        abc_threshold_relative = 0.5
        surrogate_noise_std = 0

        path2tutorial = os.path.dirname(os.path.realpath(__file__))
        path2model = os.path.abspath(os.path.join(path2tutorial, "../../scenarios", "vadere-console.jar"))
        path2scenario = os.path.abspath(
            os.path.join(path2tutorial, "../../scenarios", "Liddle_bhm_v3_fixed_seed.scenario"))

        vadere_model = VadereModel(RUN_LOCAL, path2scenario, path2model, key, qoi, n_jobs=-1, log_lvl="OFF",
                                   data_saver=DataSaver(os.getcwd()))
        prior_params = {"Type": "Uniform", "Low": 0.5, "High": 2.5, "Dim": 1, "Limits": legal_limits_parameter}
        rho = PriorFactory().create_prior_by_type(params=prior_params)

        # Artificial data
        data = vadere_model.eval_model(true_parameter_value, random_state=random_state)

        param = InversionParameterRejection(model=vadere_model, prior=rho, seed=seed, bool_averaged=True,
                                            no_runs_averaged=2,
                                            path2results=os.getcwd(), bool_surrogate=False, bool_load_surrogate=False,
                                            surrogate_path=None, limits_surrogate=None, nr_points_surrogate=None,
                                            surrogate_fit_type=None, nr_points_averaged_surrogate=None,
                                            bool_surrogate_data_misfit=False, nr_points_surrogate_error=None,
                                            bool_write_data=True, n_samples=nr_steps, data=data,
                                            true_parameter_value=true_parameter_value, bool_noise=False,
                                            meas_noise=None,
                                            legal_limits_parameters=legal_limits_parameter, bool_plot=False,
                                            run_local=RUN_LOCAL, abc_threshold_relative=abc_threshold_relative,
                                            surrogate_noise_std=surrogate_noise_std)

        return param

    # assure that the same seed leads to the same result
    @pytest.mark.vadere
    def test_same_seeds_abc(self, bool_plot: bool = False):
        param = self.configure_vadere_model()

        calc1 = InversionCalculator(param=param, result=InversionResult())
        calc1.set_sampling_strategy(sampling_config=RejectionSampling(param))
        calc1.inversion()
        result1 = calc1.get_result()

        calc2 = InversionCalculator(param=param, result=InversionResult())
        calc2.set_sampling_strategy(sampling_config=RejectionSampling(param))
        calc2.inversion()
        result2 = calc2.get_result()

        nptest.assert_array_equal(result1.get_samples(), result2.get_samples())
        nptest.assert_array_equal(result1.get_candidates(), result2.get_candidates())
        nptest.assert_array_equal(result1.get_acceptance_ratio(), result2.get_acceptance_ratio())
        self.assertTrue(result1.equals(result2))

    # Assure that results are dependent on the seed
    @pytest.mark.vadere
    def test_different_seeds_abc(self, bool_plot: bool = False):
        seed1 = int(np.random.uniform(0, 2 ** 32 - 1))
        seed2 = int(np.random.uniform(0, 2 ** 32 - 1))

        param = self.configure_vadere_model()

        param1 = copy.deepcopy(param)
        param1.set_seed(seed1)
        calc1 = InversionCalculator(param=param1, result=InversionResult())
        calc1.set_sampling_strategy(sampling_config=RejectionSampling(param1))
        calc1.inversion()
        results1 = calc1.get_result()

        param2 = copy.deepcopy(param)
        param2.set_seed(seed2)
        calc2 = InversionCalculator(param=param2, result=InversionResult())
        calc2.set_sampling_strategy(sampling_config=RejectionSampling(param2))

        calc2.inversion()
        results2 = calc2.get_result()

        nptest.assert_raises(AssertionError, nptest.assert_almost_equal, results1.get_samples(), results2.get_samples())
        nptest.assert_raises(AssertionError, nptest.assert_almost_equal, results1.get_candidates(),
                             results2.get_candidates())

        nptest.assert_raises(AssertionError, self.assertTrue, results1.equals(results2))

    @unittest.skip("not ready")  # todo corridor with fundamental diagram output does not work currently
    @pytest.mark.vadere
    def test_abc_same_seed_vadere_corridor(self):
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        result1 = run_inversion_ABC_corridor_fct(method="ABC", nr_steps=10, run_local=RUN_LOCAL, burn_in=0,
                                                 jump_widths=np.array([0.1]), seed=seed, bool_write_data=False)

        result2 = run_inversion_ABC_corridor_fct(method="ABC", nr_steps=10, run_local=RUN_LOCAL, burn_in=0,
                                                 jump_widths=np.array([0.1]), seed=seed, bool_write_data=False)

        self.assertEqual(len(result1), len(result2))

        for i in range(0, len(result1)):
            nptest.assert_array_equal(result1[i].get_samples(), result2[i].get_samples())
            nptest.assert_array_equal(result1[i].get_candidates(), result2[i].get_candidates())
            self.assertRaises(AssertionError, self.assertTrue, result1[i].equals(result2[i]))

    @unittest.skip("not ready")  # todo corridor with fundamental diagram output does not work currently
    @pytest.mark.vadere
    def test_abc_different_seeds_vadere_corridor(self):
        result1 = run_inversion_ABC_corridor_fct(
            method="ABC", nr_steps=12, run_local=RUN_LOCAL, burn_in=0, jump_widths=np.array([0.1]), seed=872456,
            bool_write_data=False)
        result2 = run_inversion_ABC_corridor_fct(
            method="ABC", nr_steps=12, run_local=RUN_LOCAL, burn_in=0, jump_widths=np.array([0.1]), seed=468168,
            bool_write_data=False)

        self.assertEqual(len(result1), len(result2))
        for i in range(0, len(result1)):
            nptest.assert_raises(AssertionError, nptest.assert_almost_equal, result1[i].get_samples(),
                                 result2[i].get_samples())
            nptest.assert_raises(AssertionError, nptest.assert_almost_equal, result1[i].get_candidates(),
                                 result2[i].get_candidates())
            self.assertRaises(AssertionError, self.assertTrue, result1[i].equals(result2[i]))
