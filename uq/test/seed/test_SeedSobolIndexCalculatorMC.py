import copy
import os
import unittest

import numpy as np

from uq.sensitivity_analysis.calc.SobolIndexCalculatorMC import SobolIndexCalculatorMC
from uq.sensitivity_analysis.calc.SobolIndexParameterMC import SobolIndexParameterMC
from uq.utils.model.IshigamiModel import IshigamiModel
from uq.utils.prior.UniformGenMult import UniformGenMult


class TestSobolIndexCalculatorMC(unittest.TestCase):

    def configure_ishigami(self) -> SobolIndexParameterMC:
        # Parameters according to Sobol' and levitan (1999)
        model = IshigamiModel(a=7, b=0.05)
        dim = model.get_dimension()
        M = 50

        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55

        rho = UniformGenMult(x_lower, x_upper, dim)

        param = SobolIndexParameterMC(model=model, prior=rho, M=M, seed=None, bool_averaged=False,
                                      no_runs_averaged=None, path2results=os.getcwd(), qoi_dim=1)
        return param

    def test_same_seed_ishigami(self, bool_plot: bool = False):
        param = self.configure_ishigami()

        seed = int(np.random.rand(1) * (2 ** 32 - 1))

        param.set_seed(seed=seed)

        sobol_calc1 = SobolIndexCalculatorMC(param=param)
        sobol_calc1.calc_sobol_indices()
        result1 = sobol_calc1.get_result()

        sobol_calc2 = SobolIndexCalculatorMC(param=param)
        sobol_calc2.calc_sobol_indices()
        result2 = sobol_calc2.get_result()

        self.assertTrue(result1.equals(result2))

    def test_different_seeds_ishigami(self, bool_plot: bool = False):
        param = self.configure_ishigami()

        seed1 = int(np.random.rand(1) * (2 ** 32 - 1))
        seed2 = int(np.random.rand(1) * (2 ** 32 - 1))

        param1 = copy.deepcopy(param)
        param1.set_seed(seed1)

        sobol_calc1 = SobolIndexCalculatorMC(param=param)
        sobol_calc1.calc_sobol_indices()
        result1 = sobol_calc1.get_result()

        param2 = copy.deepcopy(param)
        param2.set_seed(seed2)

        sobol_calc2 = SobolIndexCalculatorMC(param=param)
        sobol_calc2.calc_sobol_indices()
        result2 = sobol_calc2.get_result()

        self.assertRaises(AssertionError, self.assertTrue, result1.equals(result2))
