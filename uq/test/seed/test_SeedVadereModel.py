import os
import unittest

import numpy as np
from numpy.random import RandomState

from uq.utils.model.VadereModel import VadereModel

KEY = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==3].spawnNumber",
       "sources.[id==3].distributionParameters", "obstacles.[id==1].y",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters

TEST_INPUT = np.array([[1.34], [0.26], [180], [1], [8.5], [0.5]])  # legal test input
QOI = "mean_density.txt"  # quantity of interest
RUN_LOCAL = True  # run on local machine (vs. run on server)

cur_dir = os.path.dirname(os.path.realpath(__file__))
PATH2TUTORIAL = os.path.abspath(os.path.join(cur_dir, "../../inversion"))
PATH2SCENARIOS = os.path.abspath(os.path.join(cur_dir, "../../scenarios"))

PATH2MODEL = os.path.join(PATH2SCENARIOS, "vadere-console.jar")
PATH2SCENARIO_FIXED_SEED = os.path.join(PATH2SCENARIOS, "Liddle_osm_v3_fixed_seed.scenario")
PATH2SCENARIO_FREE_SEED = os.path.join(PATH2SCENARIOS, "Liddle_osm_v3_free_seed.scenario")

NJOBS = -1
LOG_LEVEL = "OFF"


class TestSeedVadereModel(unittest.TestCase):

    def test_eval_model_fixed_seed_osm_5dim(self):
        key_osm = ["attributesPedestrian.speedDistributionMean",
                   "attributesPedestrian.speedDistributionStandardDeviation",
                   "sources.[id==3].spawnNumber",
                   "sources.[id==3].distributionParameters",
                   "obstacles.[id==1].y"]  # uncertain parameters

        test_input_osm = np.array([[1.34], [0.26], [180], [1], [8.5]])  # legal test input
        model_fixed_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FIXED_SEED,
                                       path2model=PATH2MODEL,
                                       key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        result_single_1 = model_fixed_seed.eval_model(parameter_value=test_input_osm)
        result_single_2 = model_fixed_seed.eval_model(parameter_value=test_input_osm)

        np.testing.assert_array_equal(result_single_1, result_single_2)

    def test_eval_model_fixed_seed_osm_1dim(self):
        key_osm = ["finishTime"]
        test_input_osm = np.array([[200.0]])
        model_fixed_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FIXED_SEED,
                                       path2model=PATH2MODEL, key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        result_single_1 = model_fixed_seed.eval_model(parameter_value=test_input_osm)
        result_single_2 = model_fixed_seed.eval_model(parameter_value=test_input_osm)

        np.testing.assert_array_equal(result_single_1, result_single_2)

    def test_same_seed_eval_model_fixed_seed_osm_1dim(self):
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        key_osm = ["finishTime"]
        test_input_osm = np.array([[200.0]])
        model_fixed_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FIXED_SEED,
                                       path2model=PATH2MODEL,
                                       key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        result_single_1 = model_fixed_seed.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed))
        result_single_2 = model_fixed_seed.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed))

        np.testing.assert_array_equal(result_single_1, result_single_2)

    def test_different_seeds_eval_model_fixed_seed_osm_1dim(self):
        seed1 = int(np.random.uniform(0, 2 ** 32 - 1))
        seed2 = int(np.random.uniform(0, 2 ** 32 - 1))

        key_osm = ["finishTime"]
        test_input_osm = np.array([[200.0]])
        model_fixed_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FIXED_SEED,
                                       path2model=PATH2MODEL, key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        result_single_1 = model_fixed_seed.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed1))
        result_single_2 = model_fixed_seed.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed2))

        self.assertRaises(AssertionError, np.testing.assert_array_equal, result_single_1, result_single_2)

    def test_eval_model_free_seed_osm(self):
        key_osm = ["finishTime"]
        test_input_osm = np.array([[200.0]])
        seed = int(np.random.uniform(0, 2 ** 32 - 1))
        model_free_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FREE_SEED, path2model=PATH2MODEL,
                                      key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        # no impact of Randomstate since useFixedSeed is false  -> free seed
        result_single_1 = model_free_seed.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed))
        result_single_2 = model_free_seed.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed))

        self.assertRaises(AssertionError, np.testing.assert_array_equal, result_single_1, result_single_2)

    def test_same_seed_eval_model_free_seed_osm(self):
        key_osm = ["finishTime"]
        test_input_osm = np.array([[200.0]])
        model_free_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FREE_SEED, path2model=PATH2MODEL,
                                      key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        # no impact of Randomstate since the seed is not fixed
        result_single_1 = model_free_seed.eval_model(parameter_value=test_input_osm)
        result_single_2 = model_free_seed.eval_model(parameter_value=test_input_osm)

        self.assertRaises(AssertionError, np.testing.assert_array_equal, result_single_1, result_single_2)

    def test_different_seeds_eval_model_free_seed_osm(self):
        path_fixed_seed_osm = os.path.join(PATH2SCENARIOS, "Liddle_osm_v3_free_seed.scenario")

        key_osm = ["finishTime"]
        test_input_osm = np.array([[200.0]])

        seed1 = int(np.random.uniform(0, 2 ** 32 - 1))
        seed2 = int(np.random.uniform(0, 2 ** 32 - 1))

        test_model = VadereModel(run_local=RUN_LOCAL, path2scenario=path_fixed_seed_osm, path2model=PATH2MODEL,
                                 key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        result_single_1 = test_model.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed1))
        result_single_2 = test_model.eval_model(parameter_value=test_input_osm, random_state=RandomState(seed2))

        self.assertRaises(AssertionError, np.testing.assert_array_equal, result_single_1, result_single_2)
