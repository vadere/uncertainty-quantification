import unittest

import numpy.testing as nptest

from uq.active_subspace.calc.ActiveSubspaceCalculatorLLM import ActiveSubspaceCalculatorLLM
from uq.active_subspace.calc.ActiveSubspaceParameterLLM import ActiveSubspaceParameterLLM
from uq.active_subspace.config_matrix_constantine_fct import config_matrix_constantine


class TestSeedActiveSubspaceCalculatorLLM(unittest.TestCase):

    # use local linear model
    def test_same_seed_local_linear_model(self):
        seed = 1236091

        case = 1
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        factor_N = 10

        test_model, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case)

        llm_params = ActiveSubspaceParameterLLM(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                case=case, seed=seed, factor_N=factor_N)

        # local linear model

        llm_calc1 = ActiveSubspaceCalculatorLLM(param=llm_params)
        result1 = llm_calc1.identify_active_subspace()

        llm_calc2 = ActiveSubspaceCalculatorLLM(param=llm_params)
        result2 = llm_calc2.identify_active_subspace()

        # Assure that the results are identical with the same seed
        nptest.assert_array_equal(result1.get_activity_scores(), result2.get_activity_scores())
        nptest.assert_array_equal(result1.get_error_lambda_hat(), result2.get_error_lambda_hat())
        nptest.assert_array_equal(result1.get_W_active(), result2.get_W_active())
        nptest.assert_equal(result1.get_idx_gap(), result2.get_idx_gap())
        nptest.assert_array_equal(result1.get_lambda_hat(), result2.get_lambda_hat())

    # use local linear model
    def test_different_seeds_local_linear_model(self):
        seed1 = 1236091
        seed2 = 568453

        case = 1
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        factor_N = 10

        test_model, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case)

        llm_params = ActiveSubspaceParameterLLM(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                case=case, seed=None, factor_N=factor_N)

        # local linear model
        llm_params.set_seed(seed1)
        llm_calc1 = ActiveSubspaceCalculatorLLM(param=llm_params)
        results1 = llm_calc1.identify_active_subspace()

        llm_params.set_seed(seed2)
        llm_calc2 = ActiveSubspaceCalculatorLLM(param=llm_params)
        results2 = llm_calc2.identify_active_subspace()

        # Assure that the results not identical when using different seeds
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_activity_scores(),
                             results2.get_activity_scores())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_error_lambda_hat(),
                             results2.get_error_lambda_hat())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_W_active(),
                             results2.get_W_active())
        nptest.assert_raises(AssertionError, nptest.assert_array_equal, results1.get_lambda_hat(),
                             results2.get_lambda_hat())

    # todo make sure that results of LLM fit the theoretical results (for good enough approximation)
