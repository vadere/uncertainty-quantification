import os
import unittest

import numpy as np
import numpy.testing as nptest
from numpy.random import RandomState

from uq.utils.model.VadereModel import VadereModel

KEY = ["attributesPedestrian.speedDistributionMean", "attributesPedestrian.speedDistributionStandardDeviation",
       "sources.[id==3].spawnNumber",
       "sources.[id==3].distributionParameters", "obstacles.[id==1].y",
       "obstacleRepulsionMaxWeight"]  # uncertain parameters

TEST_INPUT = np.array([[1.34], [0.26], [180], [1], [8.5], [0.5]])  # legal test input
QOI = "mean_density.txt"  # quantity of interest
RUN_LOCAL = True  # run on local machine (vs. run on server)

cur_dir = os.path.dirname(os.path.realpath(__file__))
PATH2TUTORIAL = os.path.abspath(os.path.join(cur_dir, "../../inversion"))
PATH2SCENARIOS = os.path.abspath(os.path.join(cur_dir, "../../scenarios"))

PATH2MODEL = os.path.join(PATH2SCENARIOS, "vadere-console.jar")
PATH2SCENARIO_FIXED_SEED = os.path.join(PATH2SCENARIOS, "Liddle_bhm_v3_fixed_seed.scenario")

NJOBS = -1
LOG_LEVEL = "OFF"


class TestVadereModel(unittest.TestCase):

    # def test_scenario_elements_exist(self):
    # todo test if scenario elements in keys exist - also checks in VadereModel are missing for this test

    def test_qoi_not_defined(self):
        path2scenario = os.path.join(PATH2SCENARIOS, "C-180-180-180.scenario")
        nptest.assert_raises(UserWarning, VadereModel, run_local=RUN_LOCAL, path2scenario=path2scenario,
                             path2model=PATH2MODEL,
                             key=KEY, qoi=QOI,
                             n_jobs=NJOBS, log_lvl=LOG_LEVEL)

    def test_key_defined(self):
        key = "fictional_key"
        nptest.assert_raises(UserWarning, VadereModel, run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FIXED_SEED,
                             path2model=PATH2MODEL, key=key, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

    def test_assigning_model_parameters(self):
        # test_model = TestModel()
        test_model = VadereModel(RUN_LOCAL, PATH2SCENARIO_FIXED_SEED, PATH2MODEL, KEY, QOI, NJOBS, LOG_LEVEL)

        self.assertEqual(test_model.get_dimension(), len(KEY))
        self.assertEqual(test_model.get_path_2_model(), PATH2MODEL)
        self.assertEqual(test_model.get_path_2_scenario(), PATH2SCENARIO_FIXED_SEED)
        self.assertEqual(test_model.get_key(), KEY)
        self.assertEqual(test_model.get_qoi(), QOI)
        self.assertEqual(test_model.get_loglvl(), LOG_LEVEL)
        self.assertEqual(test_model.get_n_jobs(), NJOBS)

    def test_eval_model_averaged_vadere_internal_seed(self):
        test_model = VadereModel(RUN_LOCAL, PATH2SCENARIO_FIXED_SEED, PATH2MODEL, KEY, QOI, NJOBS, LOG_LEVEL)
        # assure that default value passing works
        result_single = test_model.eval_model(TEST_INPUT, random_state=None)
        single_output, _, _ = test_model.eval_model_averaged(TEST_INPUT, 1, random_state=None)
        single_output_averaged, _, _ = test_model.eval_model_averaged(TEST_INPUT, 3, random_state=None)

        np.testing.assert_almost_equal(result_single, single_output)
        np.testing.assert_almost_equal(single_output_averaged, single_output)

        # assure that averaging gives the same results as single run for fixed seed
        result_averaged, _, _ = test_model.eval_model_averaged(TEST_INPUT, 3)
        np.testing.assert_almost_equal(result_single, result_averaged)

    # Test that varying the input parameter has an input on the result
    # (Of course dependent on the relationship between input and output parameter)
    def test_impact_of_input_parameters(self):
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        key_osm = ["finishTime"]
        test_input_osm1 = np.array([200.0])
        test_input_osm2 = np.array([100.0])

        model_fixed_seed = VadereModel(run_local=RUN_LOCAL, path2scenario=PATH2SCENARIO_FIXED_SEED,
                                       path2model=PATH2MODEL, key=key_osm, qoi=QOI, n_jobs=NJOBS, log_lvl=LOG_LEVEL)

        result1 = model_fixed_seed.eval_model(parameter_value=test_input_osm1,
                                              random_state=RandomState(seed))

        result2 = model_fixed_seed.eval_model(parameter_value=test_input_osm2,
                                              random_state=RandomState(seed))

        self.assertRaises(AssertionError, nptest.assert_array_equal, result1, result2)

    def test_eval_model_averaged_vadere_external_seed(self):
        # assure that averaging with the same random seeds works
        seed = 39636

        test_model = VadereModel(RUN_LOCAL, PATH2SCENARIO_FIXED_SEED, PATH2MODEL, KEY, QOI, NJOBS, LOG_LEVEL)

        result_averaged1, input1, result1 = test_model.eval_model_averaged(parameter_value=TEST_INPUT,
                                                                           nr_runs_averaged=3,
                                                                           random_state=RandomState(seed))

        result_averaged2, input2, result2 = test_model.eval_model_averaged(parameter_value=TEST_INPUT,
                                                                           nr_runs_averaged=3,
                                                                           random_state=RandomState(seed))

        # make sure the generated inputs are identical
        np.testing.assert_array_almost_equal(input1, input2)

        # make sure the individual results are identical
        np.testing.assert_array_almost_equal(result1, result2)

        # make sure the averaged results are identical
        np.testing.assert_allclose(result_averaged1, result_averaged2)

        np.testing.assert_allclose(1, 1)
