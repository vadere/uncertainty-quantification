import random
import unittest

import numpy as np

from uq.utils.model.MultiplicativeModel import MultiplicativeModel


class TestMultiplicativeModel(unittest.TestCase):
    def test_model_output_single(self):
        x = random.random()
        y = random.random()
        model = MultiplicativeModel()
        self.assertEqual(model.eval_model(np.array([x, y])), x * y)

    def test_model_output(self):
        x = np.linspace(-10,10)
        y = np.linspace(-5,5)
        parameter_value = np.vstack((x,y))
        model = MultiplicativeModel()
        result = model.eval_model(parameter_value=parameter_value)
        np.testing.assert_array_equal(x*y, result)