import random
import unittest

import numpy as np

from uq.utils.model.LinearModel import LinearModel
from uq.utils.model.LinearModel import NoisyLinearModel


class TestLinearModel(unittest.TestCase):

    def test_linear_model_parameters(self):
        m = random.random()
        t = random.random()
        model = LinearModel(m=m, t=t)
        self.assertEqual(m, model.get_m())
        self.assertEqual(t, model.get_t())

    def test_constant_linear_model_output(self):
        # constant model
        m = 0
        t = np.random.rand(1)
        model = LinearModel(m=m, t=t)
        N = 100
        x = np.linspace(start=-10, stop=10, num=N)
        result = model.eval_model(x)

        self.assertEqual(np.size(result), N)  # check length of result
        self.assertTrue(np.all(result == t))  # constant result

    def test_gradient_linear_model(self):
        m = random.random()
        t = random.random()
        model = LinearModel(m=m, t=t)
        N = 100
        x = np.linspace(start=-10, stop=10, num=N)
        # todo test for vector
        for i in range(0, N):
            df = model.eval_gradient(x[i])
            df_approx = model.approximate_gradient(x[i], step_size=0.1)
            np.testing.assert_almost_equal(df, df_approx)  # for a linear model, the approximation is exact

    def test_noisy_linear_model_parameters(self):
        m = random.random()
        t = random.random()
        noise_level = random.random()
        model = NoisyLinearModel(m=m, t=t, noise_level=noise_level)
        self.assertEqual(m, model.get_m())
        self.assertEqual(t, model.get_t())
        self.assertEqual(noise_level, model.get_noise_level())

    def test_noisy_linear_model_without_noise(self):
        N = 100
        x = np.linspace(start=-10, stop=10, num=N)
        m = random.random()
        t = random.random()
        noise_level = 0.0
        noisy_model = NoisyLinearModel(m=m, t=t, noise_level=noise_level)
        model = LinearModel(m=m, t=t)
        np.testing.assert_array_equal(model.eval_model(x), noisy_model.eval_model(x))

    def test_noisy_linear_model_average(self):
        # since noise is centered around 0, averaged model should be similar to model without noise
        m = random.random()
        t = random.random()
        noise_level = random.random() * 0.01
        noisy_model = NoisyLinearModel(m=m, t=t, noise_level=noise_level)
        model = LinearModel(m=m, t=t)

        # at single value
        x = random.randint(-100, 100)
        mean_noisy_result, _, _ = noisy_model.eval_model_averaged(parameter_value=x, nr_runs_averaged=1000)
        result = model.eval_model(x)
        np.testing.assert_allclose(result, mean_noisy_result, rtol=0.1)

        # at vector
        N = 100
        x = np.linspace(start=-10, stop=10, num=N)
        mean_noisy_result, all_x, all_y = noisy_model.eval_model_averaged(parameter_value=x, nr_runs_averaged=1000)
        result = model.eval_model(x)

        np.testing.assert_allclose(result, mean_noisy_result, rtol=0.1)
