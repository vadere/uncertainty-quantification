import os
import unittest

import chaospy as cp
import matplotlib.pyplot as plt
import numpy as np
import numpy.testing as nptest
import pytest

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.utils import transform_coordinates_from_unit, assert_allclose_eigenvectors, \
    relative_error_constantine_2017
from uq.utils.model.CircuitModel import CircuitModel
from uq.utils.model.ExponentialModel import ExponentialModel
from uq.utils.model.IshigamiModel import IshigamiModel2DOutputTest
from uq.utils.model.Model import Model
from uq.utils.prior.UniformGenMult import UniformGenMult


class TestConvergenceActiveSubspace(unittest.TestCase):
    # todo: why is there no clear convergence? 
    def test_convergence_testmodel(self, bool_plot: bool = False):
        rtol_activity_scores = 1e-1
        testmodel = ExponentialModel()
        density_type = "uniform"
        dim = testmodel.get_dimension()
        x_lower = -1.0 * np.ones(shape=dim)  # lower bounds for parameters
        x_upper = np.ones(shape=dim)  # lower bounds for parameters
        test_input = x_lower
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        bool_averaged = False
        key = ["x1", "x2"]

        alpha_vec = np.array([5e1, 1e2, 5e2, 1e3, 5e3, 1e4]) / np.log(dim) / k
        idx = 0
        activity_scores = np.zeros(shape=(dim, len(alpha_vec)))
        error_c_gradients = np.zeros(shape=len(alpha_vec))
        error_eig = np.zeros(shape=(dim, len(alpha_vec)))
        error_as = np.zeros(shape=(dim, len(alpha_vec)))

        n_samples = np.zeros(len(alpha_vec))
        n_trials = 10

        params = ActiveSubspaceParameterGradient(model=testmodel, x_lower=x_lower, x_upper=x_upper, alpha=None, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=None, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=False, bool_print=False,
                                                 bool_plot=False, path2results=None, force_idx_gap=None)

        for alpha in alpha_vec:
            averaged_error_eig = np.zeros(shape=(1, dim))
            averaged_error_as = np.zeros(shape=(1, dim))
            averaged_scores = np.zeros(shape=(1, dim))
            all_scores = np.zeros(shape=(n_trials, dim))
            for j in range(0, n_trials):
                seed = int(np.random.rand(1) * (2 ** 32 - 1))

                # Active Subspaces
                params.set_seed(seed=seed)
                params.set_alpha(alpha=alpha)

                as_calc = ActiveSubspaceCalculatorGradient(param=params)
                as_calc.identify_active_subspace()
                as_results = as_calc.get_result()

                # max_rel_error_eig, \
                error_c_gradients[idx] = as_results.get_error_c_hat()
                n_samples[idx] = as_results.get_n_samples()
                # , tmp_activity_scores, true_activity_scores, size_subspace, \
                # path_to_files, , lambda_eig, w_active, test_y, lambda_eig_true, idx_gap, idx_gap_true, \
                # distance_subspace, true_distance

                tmp_error_i = np.abs(
                    as_results.get_lambda_hat() - as_results.get_lambda_true()) / as_results.get_lambda_true()
                averaged_error_eig = averaged_error_eig + tmp_error_i
                averaged_scores = averaged_scores + as_results.get_activity_scores()
                all_scores[j, :] = as_results.get_activity_scores()
                tmp_error_as_i = np.abs(as_results.get_activity_scores() - as_results.get_activity_scores_true()) \
                                 / as_results.get_activity_scores_true()
                averaged_error_as = averaged_error_as + tmp_error_as_i

                true_activity_scores_save = as_results.get_activity_scores_true()
                nptest.assert_almost_equal(true_activity_scores_save, as_results.get_activity_scores_true())

            error_eig[:, idx] = averaged_error_eig / n_trials
            error_as[:, idx] = averaged_error_as / n_trials
            activity_scores[:, idx] = averaged_scores / n_trials
            nptest.assert_almost_equal(np.mean(all_scores, axis=0), activity_scores[:, idx])

            idx = idx + 1

        # Compare the approximated scores to the true scores
        # print(abs_relative_error(activity_scores[:, -1], true_activity_scores))
        nptest.assert_allclose(activity_scores[:, -1], as_results.get_activity_scores_true(), rtol=0.1)
        error_min_samples = np.abs(
            activity_scores[:, 0] - as_results.get_activity_scores_true()) / as_results.get_activity_scores_true()
        error_max_samples = np.abs(
            activity_scores[:, -1] - as_results.get_activity_scores_true()) / as_results.get_activity_scores_true()

        # make sure that the result with most samples is better than the results with the lowest number of samples
        self.assertTrue((error_min_samples >= error_max_samples).all())

        # check the convergence of the results
        for i in range(0, dim):
            relative_error_tmp_i = np.abs(activity_scores[i, :] - as_results.get_activity_scores_true()[i]) / \
                                   as_results.get_activity_scores_true()[i]
            relative_error_tmp_i = error_as[i, :]
            p = np.polyfit(np.log10(n_samples), np.log10(relative_error_tmp_i), 1)
            # Accuracy rate of M^(-1/2) - Monte Carlo

            nptest.assert_almost_equal(p[0], -0.5, decimal=1)

            if bool_plot:
                p1 = np.poly1d(p)
                plt.figure()
                plt.loglog(n_samples, relative_error_tmp_i, 'o-', label='Data')
                plt.loglog(n_samples, np.power(10, p1(np.log10(n_samples))), label='Fit: Slope %.2f' % p[0])
                p2 = np.poly1d([-0.5, p[1]])
                plt.loglog(n_samples, np.power(10, p2(np.log10(n_samples))), label='-1/2 Slope')
                print(p1)
                print(p2)
                plt.legend()
                plt.show()

        if bool_plot:
            plt.figure()
            for i in range(0, dim):
                plt.loglog(n_samples, activity_scores[i, :], 'o--', label="Estimated score %s" % key[i])

                plt.loglog(n_samples, as_results.get_activity_scores_true()[i] * np.ones(len(alpha_vec)), 'k:',
                           label="True score %s" % key[i])
            plt.xlabel("Number of samples")
            plt.ylabel("Activity score")
            plt.legend()

            plt.figure()
            for i in range(0, dim):
                plt.loglog(n_samples,
                           np.abs(activity_scores[i, :] - as_results.get_activity_scores_true()[i] * np.ones(
                               len(alpha_vec))) /
                           as_results.get_activity_scores_true()[i], 'o--',
                           label="Estimated activity score %s" % key[i])
                plt.loglog(n_samples, error_eig[i, :], 'x:', label='Estimated eigenvalues of C')

            plt.plot()
            plt.loglog(n_samples, error_c_gradients, label='Estimated C')
            plt.xlabel("Number of samples")
            plt.ylabel("Error")
            plt.legend()

            plt.show()

    def test_calc_eigenvalues(self):
        tmp = np.random.rand(10, 10)
        C = np.matmul(tmp, tmp.transpose())
        # Check eigendecomposition result (distance to C_hat)
        as_calc = ActiveSubspaceCalculatorGradient(ActiveSubspaceParameterGradient())
        lambda_eig, lambda_eig_true, w_vec, wh = as_calc.calc_eigenvalues(C_hat=C, prior=None)

        # Compare original C with composed C
        np.testing.assert_array_almost_equal(np.matmul(w_vec * lambda_eig, wh), C)

    def test_activity_sores_implementations_dims(self):

        x_lower = np.array([50, 25, 0.5, 1.2, 0.25, 50])  # table 3, constantine-2017
        x_upper = np.array([150, 70, 3.0, 2.5, 1.2, 300])  # table 3, constantine-2017

        circuit_model = CircuitModel()
        density_type = "uniform"

        for idx_gap in range(0, circuit_model.get_dimension()):
            self.activity_sores_implementations(model=circuit_model, density_type=density_type, x_lower=x_lower,
                                                x_upper=x_upper, force_idx_gap=idx_gap)

    def activity_sores_implementations(self, model: Model, density_type: str, x_lower: np.ndarray, x_upper: np.ndarray,
                                       force_idx_gap: int = 0):

        dim = model.get_dimension()

        test_input = x_lower
        alpha = 10
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False

        path2results = None

        params = ActiveSubspaceParameterGradient(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=True, path2results=path2results, force_idx_gap=force_idx_gap)

        as_calc = ActiveSubspaceCalculatorGradient(param=params)
        as_calc.identify_active_subspace()
        results = as_calc.get_result()

        n_samples = np.ceil(params.get_alpha() * params.get_k() * np.log(dim)).astype(np.int)  # number of samples

        # Check which implementation performs better - comparison with implementation from P. Constantine
        # C is not constructed, instead just the gradients are used
        _, Sigma, Wh = np.linalg.svd(a=np.transpose(results.get_gradients_transformed()), full_matrices=False)
        W = np.transpose(Wh)
        lambda_constantine = np.square(Sigma) / n_samples
        idx_gap = results.get_idx_gap()
        activity_scores_constantine = np.matmul(np.square(W[:, 0:idx_gap + 1]), lambda_constantine[0:idx_gap + 1])
        np.testing.assert_allclose(results.get_activity_scores(), activity_scores_constantine.flatten())

    # todo make a test for multi-dimensional qoi (qoi_dim > 1) -> ideally also construct a multi-dim test function
    @unittest.skip("not ready")  # todo finish this test
    def test_multidim_qoi(self):
        ishigami2d = IshigamiModel2DOutputTest(a=7, b=0.05)
        density_type = "uniform"
        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55
        test_input = x_lower
        bool_gradient = True
        M_boot = 10
        alpha = 2
        k = 1 + 1
        bool_averaged = False
        seed = 19234676
        path2results = os.getcwd()

        params = ActiveSubspaceParameterGradient(model=ishigami2d, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=True, path2results=path2results, force_idx_gap=0)

        as_calc = ActiveSubspaceCalculatorGradient(param=params)
        as_calc.identify_active_subspace()
        as_calc.get_result()

        self.assertTrue(True)

    def test_w_active_transformation_dims(self):
        # make sure it works for all dimensions

        x_lower = np.array([50, 25, 0.5, 1.2, 0.25, 50])  # table 3, constantine-2017
        x_upper = np.array([150, 70, 3.0, 2.5, 1.2, 300])  # table 3, constantine-2017

        circuit_model = CircuitModel()

        for idx_gap in range(0, circuit_model.get_dimension()):
            self.check_w_active_transformation(model=circuit_model, x_lower=x_lower, x_upper=x_upper,
                                               force_idx_gap=idx_gap)

    def check_w_active_transformation(self, model: Model, x_lower: np.ndarray, x_upper: np.ndarray, force_idx_gap: int):

        density_type = "uniform"

        test_input = x_lower
        alpha = 10
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False

        path2results = None

        params = ActiveSubspaceParameterGradient(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=True, path2results=path2results, force_idx_gap=force_idx_gap)

        as_calc = ActiveSubspaceCalculatorGradient(param=params)
        as_calc.identify_active_subspace()
        results = as_calc.get_result()

        w_active = results.get_W_active()

        # todo: check on which interval the active variable lives

        # transform -1 coordinate (all coordinates are on [-1,1] interval)
        y_min = -1  # active variable
        x_min = w_active * y_min
        x_transformed_min = transform_coordinates_from_unit(x_lower=x_lower, x_upper=x_upper, value=x_min)

        # transform 1 coordinate (all coordinates are on [-1,1] interval)
        y_max = 1  # active variable
        x_max = w_active * y_max
        x_transformed_max = transform_coordinates_from_unit(x_lower=x_lower, x_upper=x_upper, value=x_max)

        # make sure that the transformed vectors are still in bounds (x_lower, x_upper)

        # first check array dimensions
        self.assertTrue((x_transformed_min >= np.expand_dims(x_lower, axis=1)).all())
        self.assertTrue((x_transformed_min <= np.expand_dims(x_upper, axis=1)).all())
        self.assertTrue((x_transformed_max >= np.expand_dims(x_lower, axis=1)).all())
        self.assertTrue((x_transformed_max <= np.expand_dims(x_upper, axis=1)).all())

    def test_w_active_different_dims(self):

        x_lower = np.array([50, 25, 0.5, 1.2, 0.25, 50])  # table 3, constantine-2017
        x_upper = np.array([150, 70, 3.0, 2.5, 1.2, 300])  # table 3, constantine-2017

        circuit_model = CircuitModel()
        density_type = "uniform"

        for idx_gap in range(0, circuit_model.get_dimension()):
            self.check_w_active(model=circuit_model, density_type=density_type, x_lower=x_lower, x_upper=x_upper,
                                force_idx_gap=idx_gap)

    def check_w_active(self, model: Model, density_type: str, x_lower: np.ndarray, x_upper: np.ndarray,
                       force_idx_gap: int = 0):
        # todo maybe this test can be more general, not just for one setup

        test_input = x_lower
        alpha = 10
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False

        path2results = None

        params = ActiveSubspaceParameterGradient(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=True, path2results=path2results, force_idx_gap=force_idx_gap)

        as_calc = ActiveSubspaceCalculatorGradient(param=params)
        as_calc.identify_active_subspace()
        results = as_calc.get_result()

        w_active = results.get_W_active()

        W1_pinv = np.linalg.pinv(w_active)
        W1_pinv_test = np.matmul(np.linalg.inv(np.matmul(np.transpose(w_active), w_active)), np.transpose(w_active))

        nptest.assert_almost_equal(W1_pinv, W1_pinv_test)
        nptest.assert_almost_equal(np.matmul(W1_pinv, w_active), np.eye(N=results.get_size_subspace()))

    def test_convergence_circuit_model(self, bool_plot: bool = False):
        x_lower = np.array([50, 25, 0.5, 1.2, 0.25, 50])  # table 3, constantine-2017
        x_upper = np.array([150, 70, 3.0, 2.5, 1.2, 300])  # table 3, constantine-2017

        activity_scores_constantine = np.array(
            [2.377860943309341, 1.619026815363377, 0.261741461441246, 0.075234628507027, 0.000000116801952,
             0.000065807942335])  # calculated with constantine's matlab scripts (constantine-2017)
        first_eigenvector_constantine = np.array(
            [0.740716965496176, -0.611203856808294, -0.245751018607724, 0.131755257933889, 0.000164166339828,
             0.003896711210227])
        eigenvalues_C_hat_constantine = np.array(
            [4.333929773365277, 0.172154546775767, 0.043837280605887, 0.008740767183207, 0.000130619772892,
             0.000000006588527])

        relative_error_alpha_constantine = \
            np.array([[1.0336576094e-01, 7.4484410837e-02, 3.2199103665e-02, 2.3745185339e-02, 7.3669706642e-03,
                       7.6656239406e-03, 3.1840067208e-03],
                      [3.7870122253e-02, 2.9914292211e-02, 1.5144943581e-02, 8.7714103148e-03, 4.4700495080e-03,
                       2.3181577249e-03, 1.1608862367e-03],
                      [5.5327059088e-03, 3.8373262294e-03, 2.1444430111e-03, 1.2336853412e-03, 6.8385574590e-04,
                       4.6215573641e-04, 1.6012529246e-04],
                      [4.7273235250e-03, 3.6211927084e-03, 1.7887246903e-03, 1.0458653419e-03, 6.1191072469e-04,
                       4.0120287646e-04, 1.2149120835e-04],
                      [9.2850207014e-09, 5.7034221317e-09, 3.2097814749e-09, 1.9626299343e-09, 7.6701151806e-10,
                       4.7641920831e-10, 2.2068876806e-10],
                      [8.8701371027e-06, 5.8444088089e-06, 2.9941349453e-06, 1.8812300039e-06, 8.3129987185e-07,
                       5.5281073089e-07, 2.2441932087e-07]])

        relative_error_w_constantine = \
            np.array([[2.7714612059e-02, 1.5914810891e-02, 7.2787857034e-03, 5.6092823993e-03, 2.0922412569e-03,
                       1.5990443476e-03, 5.9951083677e-04],
                      [2.6597648919e-02, 1.8193945825e-02, 8.0722129599e-03, 5.7725628927e-03, 2.3120501167e-03,
                       1.7137227490e-03, 6.8505176689e-04],
                      [1.6522393310e-02, 8.9553701043e-03, 4.2085081539e-03, 3.1165970129e-03, 1.1959665435e-03,
                       7.5160538294e-04, 3.5689233942e-04],
                      [1.5741593060e-02, 1.0683193332e-02, 4.3969836718e-03, 3.4894171678e-03, 1.4456638503e-03,
                       8.6030138247e-04, 3.7841702257e-04],
                      [2.1623799573e-05, 1.1406089340e-05, 8.2572277276e-06, 4.7396837444e-06, 1.9973336116e-06,
                       1.3417606303e-06, 5.2939720012e-07],
                      [8.0536892552e-04, 4.8504474422e-04, 3.4402170980e-04, 1.7722519510e-04, 8.3146852712e-05,
                       5.4381142033e-05, 2.1796009955e-05]])

        circuit_model = CircuitModel()
        dim = circuit_model.get_dimension()
        density_type = "uniform"

        test_input = x_lower
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False

        path2results = None
        key = ["$R_b1$", "$R_b2$", "$R_f$", "$R_c1$", "$R_c2$", "$beta$"]

        alpha_vec = np.array([5e1, 1e2, 5e2, 1e3, 5e3, 1e4, 5e4]) / np.log(dim) / k

        params = ActiveSubspaceParameterGradient(model=circuit_model, x_lower=x_lower, x_upper=x_upper, alpha=None, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=True, path2results=path2results, force_idx_gap=0)

        as_calc = ActiveSubspaceCalculatorGradient(param=params)

        # todo: restructure the calculation with abscissas and weights so that the logic is in a function?
        #  or combine with active_subspace_with_gradients

        # Gauss-Legendre quadrature approximation of C
        # abscissas1, weights1 = np.polynomial.legendre.leggauss(np.power(7,6)) -> MemoryError

        distribution = cp.J(cp.Uniform(-1, 1), cp.Uniform(-1, 1), cp.Uniform(-1, 1), cp.Uniform(-1, 1),
                            cp.Uniform(-1, 1), cp.Uniform(-1, 1))
        abscissas, weights = cp.generate_quadrature(dim, distribution, rule="gauss_legendre")
        abscissas_scaled = transform_coordinates_from_unit(x_lower, x_upper, abscissas)

        assert ((np.max(abscissas_scaled, axis=1) <= x_upper).all())
        assert ((np.max(abscissas_scaled, axis=1) >= x_lower).all())

        gradient_evals = circuit_model.eval_gradient(abscissas_scaled)

        # transformation of weights for interval [a,b] instead of [-1,1] -> factor (b-a)/2
        gradient_evals_scaled = np.matmul(0.5 * np.diag(x_upper - x_lower), gradient_evals)

        gradient_evals_scaled_weighted = np.multiply(gradient_evals_scaled, np.sqrt(weights))
        C_tmp = np.matmul(gradient_evals_scaled_weighted, np.transpose(gradient_evals_scaled_weighted))

        n_samples = len(weights)
        tmp_sum = np.zeros(shape=(dim, dim))
        for i in range(0, n_samples):
            tmp = np.expand_dims(gradient_evals_scaled[:, i], axis=1) * gradient_evals_scaled[:, i]
            tmp_sum = tmp_sum + weights[i] * tmp
        C_quadrature_approximation = tmp_sum

        nptest.assert_array_almost_equal(C_quadrature_approximation, C_tmp)

        # calc eigenvalues
        rho = UniformGenMult(x_lower, x_upper, dim)

        as_calc.calc_activity_scores_from_C(C_hat=C_quadrature_approximation, prior=rho)
        results_quad = as_calc.get_result()

        first_eigenvector_quad = results_quad.get_W_active()[:, 0]

        nptest.assert_array_almost_equal(results_quad.get_lambda_hat(), eigenvalues_C_hat_constantine)
        nptest.assert_array_almost_equal(results_quad.get_activity_scores(), activity_scores_constantine)
        assert_allclose_eigenvectors(first_eigenvector_quad, first_eigenvector_constantine)

        # ------------------------------------------------------------------ Calc activity scores with Algorithm 1.1
        idx = 0
        activity_scores = np.zeros(shape=(dim, len(alpha_vec)))
        activity_score_error_av = np.zeros(shape=(dim, len(alpha_vec)))

        first_eigenvector = np.zeros(shape=(dim, len(alpha_vec)))
        first_eigenvector_error_av = np.zeros(shape=(dim, len(alpha_vec)))
        n_samples = np.zeros(len(alpha_vec))
        n_trials = 10

        params = ActiveSubspaceParameterGradient(model=circuit_model, x_lower=x_lower, x_upper=x_upper, alpha=None, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=False, bool_print=False,
                                                 bool_plot=False, path2results=path2results, force_idx_gap=0)

        for alpha in alpha_vec:
            activity_score_av = np.zeros(shape=dim)
            first_eigenvector_entry_av = np.zeros(shape=dim)
            tmp_activity_score_error_av = np.zeros(shape=dim)
            tmp_first_eigenvector_error_av = np.zeros(shape=dim)
            for trial in range(0, n_trials):  # in constantine-2017 scores are averaged over 10 trials
                seed = int(np.random.rand(1) * (2 ** 32 - 1))

                # Active Subspaces
                params.set_seed(seed=seed)
                params.set_alpha(alpha=alpha)

                as_calc = ActiveSubspaceCalculatorGradient(param=params)
                as_calc.identify_active_subspace()
                as_results = as_calc.get_result()

                # normalize
                nptest.assert_array_almost_equal(np.linalg.norm(as_results.get_W_active()[:, 0]), 1)
                tmp_first_eigenvector = as_results.get_W_active()[:, 0] * np.sign(as_results.get_W_active()[0, 0])

                first_eigenvector_entry_av = first_eigenvector_entry_av + tmp_first_eigenvector
                activity_score_av = activity_score_av + as_results.get_activity_scores()
                tmp_activity_score_error = relative_error_constantine_2017(as_results.get_activity_scores(),
                                                                           results_quad.get_activity_scores())
                tmp_activity_score_error_av = tmp_activity_score_error_av + tmp_activity_score_error

                tmp_first_eigenvector_error = relative_error_constantine_2017(
                    tmp_first_eigenvector * np.sign(tmp_first_eigenvector[0]),
                    first_eigenvector_quad * np.sign(first_eigenvector_quad[0]))
                tmp_first_eigenvector_error_av = tmp_first_eigenvector_error_av + tmp_first_eigenvector_error

            n_samples[idx] = as_results.get_n_samples()
            activity_scores[:, idx] = activity_score_av / n_trials
            activity_score_error_av[:, idx] = tmp_activity_score_error_av / n_trials
            first_eigenvector[:, idx] = first_eigenvector_entry_av / n_trials
            first_eigenvector_error_av[:, idx] = tmp_first_eigenvector_error_av / n_trials

            idx = idx + 1

        # compare this implementation with results of constantine from constantine-2017 (Matlab code)
        nptest.assert_allclose(activity_score_error_av, relative_error_alpha_constantine, rtol=1)
        nptest.assert_allclose(first_eigenvector_error_av, relative_error_w_constantine, rtol=1)

        # check convergence rate
        for i in range(0, dim):
            p = np.polyfit(np.log10(n_samples), np.log10(activity_score_error_av[i, :]), 1)
            # Convergence rate of M^(-1/2) according to constantine-2017
            nptest.assert_almost_equal(p[0], -0.5, decimal=1)

        if bool_plot:
            col = np.array(
                [[0, 0.4470, 0.7410], [0.8500, 0.3250, 0.0980], [0.9290, 0.6940, 0.1250], [0.4940, 0.1840, 0.5560],
                 [0.4660, 0.6740, 0.1880], [0.3010, 0.7450, 0.9330]])

            # Plot relative error in activity scores
            plt.figure()
            for i in range(0, dim):
                # plt.loglog(n_samples, relative_error_alpha[i, :], 'o-', label=key[i], color=col[i, :])
                plt.loglog(n_samples, activity_score_error_av[i, :], 'o-', label=key[i], color=col[i, :])
                plt.loglog(n_samples, relative_error_alpha_constantine[i, :], 'x:', color=col[i, :])
            plt.ylabel("Relative error (activity score)")
            plt.xlabel("Number of MC samples")
            plt.legend()
            plt.ylim([1e-9, 1e0])
            plt.xlim([5e1, 5e4])
            plt.grid()

            # Plot relative error in first eigenvector scores
            plt.figure()
            for i in range(0, dim):
                plt.loglog(n_samples, first_eigenvector_error_av[i, :], 'o-', label=key[i], color=col[i, :])
                plt.loglog(n_samples, relative_error_w_constantine[i, :], 'x:', color=col[i, :])

            plt.ylabel("Relative error (first eigenvector)")
            plt.xlabel("Number of MC samples")
            plt.legend()
            plt.ylim([1e-9, 1e0])
            plt.xlim([5e1, 5e4])
            plt.grid()
            plt.show()


    @unittest.skip("not ready")  # todo finish this test
    @pytest.mark.vadere
    def test_multidim_qoi_vadere(self):
        seed = 267673926

        scenario_name = "bottleneck_OSM_all_in_one_N60.scenario"  # 5in1

        bool_gradient = False  # is an analytical gradient available? -> True
        bool_averaged = True
        no_runs_averaged = 3  # average over multiple runs?

        alpha = 1
        k = 1  # desired dimension of subspace +1
        M_boot = 0  # Constantine: Typically between 100 and 10^5
        case = None

        density_type = "uniform"  # input parameter density

        test_input = np.array([[1], [1.34], [0.26], [180], [50]])  # legal test input

        # parameter limits
        x_lower = np.array([1.0, 0.5, 0.1, 160, 30])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200, 70])  # upper bounds for parameters

        step_size = 0.025  # Number of pedestrians wird um 1 erhöht

        # parameters
        key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation",
               "sources.[id==3].spawnNumber",  # "bottleneck_width",
               "pedPotentialHeight"]  # uncertain parameters

        qoi = "flow.txt"  # quantity of interest

        step_size_relative = step_size * (x_upper - x_lower)

        # configure setup
        cur_dir = os.path.dirname(os.path.realpath(__file__))
        path2output = os.getcwd()
        scenario_path = os.path.join(cur_dir, "../../scenarios/")
        path2model = os.path.abspath(os.path.join(scenario_path, "vadere-console.jar"))
        path2scenario = os.path.abspath(os.path.join(scenario_path, scenario_name))

        # test_model = TestModel()
        test_model = VadereModel(run_local=True, path2scenario=path2scenario, path2model=path2model, key=key,
                                 qoi=qoi, n_jobs=-1, log_lvl="OFF", qoi_dim=5)

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot,
                                                 step_size_relative=step_size_relative, step_size=step_size, case=case,
                                                 seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=no_runs_averaged, bool_save_data=False,
                                                 bool_print=False, bool_plot=False, path2results=None)

        calc = ActiveSubspaceCalculatorGradient(param=params)
        calc.identify_active_subspace()
        calc.get_result()

        self.assertTrue(True)

