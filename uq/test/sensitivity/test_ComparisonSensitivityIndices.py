import unittest

import matplotlib.pyplot as plt
import numpy as np
import numpy.testing as nptest
from numpy.random import RandomState

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.config_matrix_constantine_fct import config_matrix_constantine
from uq.sensitivity_analysis.calc.SobolIndexCalculatorMC import SobolIndexCalculatorMC
from uq.sensitivity_analysis.calc.SobolIndexCalculatorSALib import SobolIndexCalculatorSALib
from uq.sensitivity_analysis.calc.SobolIndexParameterMC import SobolIndexParameterMC
from uq.sensitivity_analysis.calc.SobolIndexParameterSALib import SobolIndexParameterSALib
from uq.utils.model.ExponentialModel import ExponentialModel
from uq.utils.model.IshigamiModel import IshigamiModel
from uq.utils.prior.UniformGenMult import UniformGenMult


# Compares the indices obtained with Active Subspaces and with Sobol' method
class TestSensitivityIndicesTestModel(unittest.TestCase):

    def test_compare_indices_quadratic_model(self, bool_plot: bool = False):
        case = 2
        seed = 354365
        print("gradients_compared_to_constantine_one_case:  CASE " + str(case))
        bool_gradient = True
        step_size = None
        alpha = 20
        k = 10
        M_boot = 0
        M = 5000
        N = 1000

        path2results = None
        bool_averaged = False
        no_runs_averaged = None

        # Model
        _, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case, RandomState(seed))

        # Activity scores with true gradients
        quadratic_model, _, _, _, _, _ = config_matrix_constantine(case, RandomState(seed))  # re-init
        rho = UniformGenMult(x_lower, x_upper, m)

        as_params = ActiveSubspaceParameterGradient(model=quadratic_model, x_lower=x_lower, x_upper=x_upper,
                                                    alpha=alpha, k=k, bool_gradient=bool_gradient, M_boot=M_boot,
                                                    step_size_relative=None, step_size=step_size, case=case, seed=seed,
                                                    bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                                    bool_save_data=False, bool_print=False, bool_plot=False,
                                                    path2results=path2results)

        as_calculator = ActiveSubspaceCalculatorGradient(param=as_params)
        as_calculator.identify_active_subspace()
        as_results = as_calculator.get_result()

        # Sobol' indices with MC method
        quadratic_model, _, _, _, _, _ = config_matrix_constantine(case, RandomState(seed))  # re-init
        rho = UniformGenMult(x_lower, x_upper, m)

        params = SobolIndexParameterMC(model=quadratic_model, prior=rho, M=M, seed=seed, bool_averaged=bool_averaged,
                                       no_runs_averaged=no_runs_averaged, path2results=None, qoi_dim=1)

        sobol_calc = SobolIndexCalculatorMC(param=params)
        sobol_calc.calc_sobol_indices()
        sobol_results = sobol_calc.get_result()

        # Sobol indices with SALib
        quadratic_model, _, _, _, _, _ = config_matrix_constantine(case, RandomState(seed))  # re-init

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=quadratic_model.get_dimension())

        params = SobolIndexParameterSALib(model=quadratic_model, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                          no_runs_averaged=no_runs_averaged, N=N, path2results=None)
        calc = SobolIndexCalculatorSALib(params)
        results = calc.calc_sobol_indices()

        total_sobol_indices = results.get_total_sobol_indices()

        # Compare scores by Jansen  with activity scores for test model
        nptest.assert_almost_equal(sobol_results.get_total_indices_jansen(), total_sobol_indices, decimal=1)

        nptest.assert_almost_equal(
            np.abs(as_results.get_activity_scores()) / np.sqrt(np.sum(np.square(as_results.get_activity_scores()))),
            np.abs(total_sobol_indices) / np.sqrt(np.sum(np.square(total_sobol_indices))),
            decimal=1)
        nptest.assert_almost_equal(
            np.abs(as_results.get_activity_scores()) / np.sqrt(np.sum(np.square(as_results.get_activity_scores()))),
            np.abs(sobol_results.get_total_indices_jansen()) / np.sqrt(
                np.sum(np.square(sobol_results.get_total_indices_jansen()))),
            decimal=1)

        if bool_plot:
            # Plot resulting scores

            # Normalization according constantine-2017 -> necessary since activity scores are extremely large!
            plt.figure()
            plt.plot(
                np.abs(as_results.get_activity_scores()) / np.sqrt(np.sum(np.square(as_results.get_activity_scores()))),
                'o:',
                label="Activity scores")
            plt.plot(np.abs(total_sobol_indices) / np.sqrt(np.sum(np.square(total_sobol_indices))), 'd--',
                     label='Sobol\' total effect indices (SALib)')
            plt.plot(np.abs(sobol_results.get_total_indices_jansen()) / np.sqrt(
                np.sum(np.square(sobol_results.get_total_indices_jansen()))), 's-.',
                     label='Sobol\' total effect indices (Jansen)')
            plt.legend()
            plt.show()

    def test_compare_indices_testmodel_theorem42(self, bool_plot: bool = False):
        # test according to theorem 4.2 in constanine-2017

        rtol_scores = 1

        testmodel = ExponentialModel()
        density_type = "uniform"
        dim = testmodel.get_dimension()
        x_lower = -1.0 * np.ones(shape=dim)  # lower bounds for parameters
        x_upper = np.ones(shape=dim)  # lower bounds for parameters
        rho = UniformGenMult(x_lower, x_upper, dim)

        test_input = x_lower
        alpha = 20
        k = 1 + 1
        M = 100
        N = 500

        bool_gradient = True
        M_boot = 0
        step_size = None
        seed = 2456354
        bool_averaged = False

        as_params = ActiveSubspaceParameterGradient(model=testmodel, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                    bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                    step_size=step_size, case=None, seed=seed,
                                                    bool_averaged=bool_averaged, no_runs_averaged=None,
                                                    bool_save_data=False, bool_print=False, bool_plot=False,
                                                    path2results=None)

        # Activity scores
        as_calculator = ActiveSubspaceCalculatorGradient(param=as_params)
        as_calculator.identify_active_subspace()
        as_results = as_calculator.get_result()

        # Sobol indices (SA Lib)
        seed = 78613651
        no_runs_averaged = 1

        # Defining the Model Inputs
        key = ["x1", "x2"]

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=len(key))

        params = SobolIndexParameterSALib(model=testmodel, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                          no_runs_averaged=no_runs_averaged, N=N, path2results=None)
        calc = SobolIndexCalculatorSALib(params)
        results = calc.calc_sobol_indices()

        total_sobol_indices = results.get_total_sobol_indices()

        sobol_params = SobolIndexParameterMC(model=testmodel, prior=rho, M=M, seed=seed, bool_averaged=bool_averaged,
                                             no_runs_averaged=no_runs_averaged, path2results=None, qoi_dim=1)

        sobol_calc = SobolIndexCalculatorMC(param=sobol_params)
        sobol_calc.calc_sobol_indices()
        sobol_results = sobol_calc.get_result()

        # Check comparison between derivative-based scores and activity scores (Theorem 4.1)
        nu = np.diag(as_results.get_C_hat())  # derivative-based indices
        if as_results.get_idx_gap() < dim:
            eps = 10 ** np.ceil(np.log10(np.finfo(float).eps))
            assert (((as_results.get_activity_scores() - nu) <= eps).all())
        else:
            nptest.assert_all_close(as_results.get_activity_scores(), nu)

        # Check comparison between Sobol' indices and activity scores (Theorem 4.2) -> corrected factor 4/pi^2

        if sobol_results.get_variance_AB() is not None:
            variance_Y = sobol_results.get_variance_AB()
        else:
            variance_Y = sobol_results.get_variance_A()

        nptest.assert_array_less(sobol_results.get_total_indices_jansen(),
                                 4 / (variance_Y * np.pi ** 2)
                                 * (as_results.get_activity_scores()
                                    + as_results.get_lambda_hat()[as_results.get_idx_gap() + 1]))
        nptest.assert_array_less(sobol_results.get_total_indices_constantine(),
                                 4 / (variance_Y * np.pi ** 2)
                                 * (as_results.get_activity_scores()
                                    + as_results.get_lambda_hat()[as_results.get_idx_gap() + 1]))

    def test_compare_indices_testmodel(self, bool_plot: bool = False):
        rtol_scores = 1

        testmodel = ExponentialModel()
        density_type = "uniform"
        dim = testmodel.get_dimension()
        x_lower = -1.0 * np.ones(shape=dim)  # lower bounds for parameters
        x_upper = np.ones(shape=dim)  # lower bounds for parameters
        rho = UniformGenMult(x_lower, x_upper, dim)

        test_input = x_lower
        alpha = 10
        k = 1 + 1
        M = 100
        N = 500

        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False
        no_runs_averaged = 1

        as_params = ActiveSubspaceParameterGradient(model=testmodel, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                    bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                    step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                    no_runs_averaged=no_runs_averaged, bool_save_data=False,
                                                    bool_print=False, bool_plot=False, path2results=None)

        # Activity scores
        as_calculator = ActiveSubspaceCalculatorGradient(param=as_params)
        as_calculator.identify_active_subspace()
        as_results = as_calculator.get_result()

        # Sobol indices (SA Lib)
        seed = 78613651
        no_runs_averaged = 1

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=testmodel.get_dimension())

        params = SobolIndexParameterSALib(model=testmodel, prior=rho, seed=seed, bool_averaged=bool_averaged,
                                          no_runs_averaged=no_runs_averaged, N=N, path2results=None)
        calc = SobolIndexCalculatorSALib(params)
        results = calc.calc_sobol_indices()

        total_sobol_indices = results.get_total_sobol_indices()

        sobol_params = SobolIndexParameterMC(model=testmodel, prior=rho, M=M, seed=seed, bool_averaged=bool_averaged,
                                             no_runs_averaged=no_runs_averaged, path2results=None, qoi_dim=1)

        sobol_calc = SobolIndexCalculatorMC(param=sobol_params)
        sobol_calc.calc_sobol_indices()
        sobol_results = sobol_calc.get_result()

        # Check relative distance between scores
        nptest.assert_allclose(np.diff(total_sobol_indices), np.diff(as_results.get_activity_scores()),
                               rtol=rtol_scores)

        # Compare scores by Jansen / Saltelli with activity scores for test model
        nptest.assert_allclose(sobol_results.get_total_indices_jansen(), as_results.get_activity_scores(), rtol=0.1)

        if bool_plot:
            plt.figure()
            plt.plot(as_results.get_activity_scores(), 'o:', label="Activity scores")
            plt.plot(total_sobol_indices, 'd--', label='Sobol\' total effect indices (SALib)')
            plt.plot(sobol_results.get_total_indices_jansen(), 's-.',
                     label='Sobol\' total effect indices (Jansen method)')
            plt.legend()
            plt.show()

    def test_ishigami_indices_theorem42(self, bool_plot: bool = False):
        # Parameters according to Sobol' and levitan (1999)
        model = IshigamiModel(a=7, b=0.05)
        dim = model.get_dimension()
        M = int(1e5)
        N = 2 * (np.floor(M / (1 + model.get_dimension()))).astype(int)

        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55

        rho = UniformGenMult(x_lower, x_upper, dim)
        density_type = "uniform"
        alpha = 10
        k = 1 + 1
        bool_gradient = True  # no gradient available
        M_boot = 0
        step_size = None
        bool_averaged = False
        no_runs_averaged = None
        test_input = x_lower

        n_trials = 50
        total_indices_constantine = np.nan * np.zeros(shape=(n_trials, dim))
        total_indices_jansen = np.nan * np.zeros(shape=(n_trials, dim))
        first_indices_jansen = np.nan * np.zeros(shape=(n_trials, dim))
        first_incides_saltelli_2010 = np.nan * np.zeros(shape=(n_trials, dim))
        activity_scores = np.nan * np.zeros(shape=(n_trials, dim))
        derivate_based_scores_nu = np.nan * np.zeros(shape=(n_trials, dim))
        lambda_eig = np.nan * np.zeros(shape=(n_trials, dim))

        idx_gap = np.nan * np.zeros(n_trials)
        variance_AB = np.nan * np.zeros(n_trials)

        err_total_constantine = np.nan * np.zeros(shape=(n_trials, dim))
        err_total_jansen = np.nan * np.zeros(shape=(n_trials, dim))
        err_first_jansen = np.nan * np.zeros(shape=(n_trials, dim))
        err_first_saltelli_2010 = np.nan * np.zeros(shape=(n_trials, dim))

        true_first_order_indices = model.get_true_first_order_indices()
        true_total_effects = model.get_true_total_effect_indices()

        as_params = ActiveSubspaceParameterGradient(model=model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                    bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                    step_size=step_size, case=None, seed=None,
                                                    bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                                    bool_save_data=False, bool_print=False, bool_plot=False,
                                                    path2results=None)

        sobol_params = SobolIndexParameterMC(model=model, prior=rho, M=M, seed=None, bool_averaged=bool_averaged,
                                             no_runs_averaged=no_runs_averaged, path2results=None, qoi_dim=1)

        for i in range(0, n_trials):
            seed = int(np.random.rand(1) * (2 ** 32 - 1))

            sobol_params.set_seed(seed=seed)

            # Calculate Sobol' indices
            sobol_calc = SobolIndexCalculatorMC(param=sobol_params)
            sobol_calc.calc_sobol_indices()
            sobol_results = sobol_calc.get_result()

            if sobol_results.get_variance_AB() is not None:
                variance_Y = sobol_results.get_variance_AB()
            else:
                variance_Y = sobol_results.get_variance_A()

            variance_AB[i] = variance_Y

            total_indices_constantine[i, :] = sobol_results.get_total_indices_constantine()
            total_indices_jansen[i, :] = sobol_results.get_total_indices_jansen()
            first_indices_jansen[i, :] = sobol_results.get_first_indices_jansen()
            first_incides_saltelli_2010[i, :] = sobol_results.get_first_incides_saltelli_2010()

            err_total_constantine[i, :] = np.abs(sobol_results.get_total_indices_constantine() - true_total_effects)
            err_total_jansen[i, :] = np.abs(sobol_results.get_total_indices_jansen() - true_total_effects)
            err_first_jansen[i, :] = np.abs(sobol_results.get_first_indices_jansen() - true_total_effects)
            err_first_saltelli_2010[i, :] = np.abs(sobol_results.get_first_incides_saltelli_2010() - true_total_effects)

            as_params.set_seed(seed=seed)
            as_calculator = ActiveSubspaceCalculatorGradient(param=as_params)
            as_calculator.identify_active_subspace()
            as_results = as_calculator.get_result()

            activity_scores[i, :] = as_results.get_activity_scores()
            lambda_eig[i, :] = as_results.get_lambda_hat()
            idx_gap[i] = as_results.get_idx_gap()

            derivate_based_scores_nu[i, :] = np.diag(as_results.get_C_hat())  # derivative-based indices

        # Compare to exact indices
        nptest.assert_allclose(np.mean(first_incides_saltelli_2010, axis=0), true_first_order_indices,
                               atol=1e-2)
        nptest.assert_allclose(np.mean(first_indices_jansen, axis=0), true_first_order_indices, atol=1e-2)

        nptest.assert_allclose(np.mean(total_indices_jansen, axis=0), true_total_effects, atol=1e-2)
        nptest.assert_allclose(np.mean(total_indices_constantine, axis=0), true_total_effects, atol=1e-2)

        # Make sure that always the same active subspace is identified
        idx_gap_unique = np.argmax(np.bincount(idx_gap.astype(int)))
        idx_runs_same_gap = [idx for idx, val in enumerate(idx_gap) if val == idx_gap_unique]

        # Theorem 4.1 from constantine-2017
        nu_mean = np.mean(derivate_based_scores_nu[idx_runs_same_gap, :], axis=0)
        as_mean = np.mean(activity_scores[idx_runs_same_gap, :], axis=0)
        if idx_gap_unique < dim:
            assert (((as_mean - nu_mean) <= np.finfo(float).eps).all())
        else:
            np.assert_all_close(as_mean, nu_mean)

        # Theorem 4.2 from constantine-2017
        sobol_mean = np.mean(total_indices_jansen, axis=0)
        mean_lambda = np.mean(lambda_eig[idx_runs_same_gap, :], axis=0)
        mean_varAB = np.mean(variance_AB)

        if idx_gap_unique < dim:
            lambda_first_inactive = mean_lambda[idx_gap_unique + 1]
        else:
            lambda_first_inactive = 0

        assert ((sobol_mean - 4 / (mean_varAB * np.square(np.pi)) *
                 (as_mean + lambda_first_inactive) <= np.finfo('float').eps).all())

    def test_ishigami_indices(self, bool_plot: bool = False):
        # Parameters according to Sobol' and levitan (1999)
        model = IshigamiModel(a=7, b=0.05)
        bool_averaged = False
        dim = model.get_dimension()
        M_vec = np.array([50, 1e2, 5e2, 1e3, 5e3, 1e4, 5e4, 1e5]).astype(int)
        N_vec = 2 * (np.floor(M_vec / (1 + model.get_dimension()))).astype(int)

        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55

        rho = UniformGenMult(x_lower, x_upper, dim)

        n_trials = 50
        total_indices_constantine = np.zeros(shape=(len(M_vec), n_trials, dim))
        total_indices_jansen = np.zeros(shape=(len(M_vec), n_trials, dim))
        first_indices_jansen = np.zeros(shape=(len(M_vec), n_trials, dim))
        first_incides_saltelli_2010 = np.zeros(shape=(len(M_vec), n_trials, dim))

        err_total_constantine = np.zeros(shape=(len(M_vec), n_trials, dim))
        err_total_jansen = np.zeros(shape=(len(M_vec), n_trials, dim))
        err_first_jansen = np.zeros(shape=(len(M_vec), n_trials, dim))
        err_first_saltelli_2010 = np.zeros(shape=(len(M_vec), n_trials, dim))

        true_first_order_indices = model.get_true_first_order_indices()
        true_total_effects = model.get_true_total_effect_indices()

        sobol_params = SobolIndexParameterMC(model=model, prior=rho, M=None, seed=None, bool_averaged=bool_averaged,
                                             no_runs_averaged=None, path2results=None, qoi_dim=1)

        idx = 0
        for M in M_vec:
            sobol_params.set_M(M)
            for i in range(0, n_trials):
                seed = int(np.random.rand(1) * (2 ** 32 - 1))
                sobol_params.set_seed(seed)

                sobol_calc = SobolIndexCalculatorMC(param=sobol_params)
                sobol_calc.calc_sobol_indices()
                sobol_results = sobol_calc.get_result()

                total_indices_constantine[idx, i, :] = sobol_results.get_total_indices_constantine()
                total_indices_jansen[idx, i, :] = sobol_results.get_total_indices_jansen()
                first_indices_jansen[idx, i, :] = sobol_results.get_first_indices_jansen()
                first_incides_saltelli_2010[idx, i, :] = sobol_results.get_first_incides_saltelli_2010()

                err_total_constantine[idx, i, :] = np.abs(
                    sobol_results.get_total_indices_constantine() - true_total_effects)
                err_total_jansen[idx, i, :] = np.abs(sobol_results.get_total_indices_jansen() - true_total_effects)
                err_first_jansen[idx, i, :] = np.abs(sobol_results.get_first_indices_jansen() - true_total_effects)
                err_first_saltelli_2010[idx, i, :] = np.abs(
                    sobol_results.get_first_incides_saltelli_2010() - true_total_effects)

            idx = idx + 1

        # Compare to exact indices
        nptest.assert_allclose(np.mean(first_incides_saltelli_2010[-1, :, :], axis=0), true_first_order_indices,
                               atol=1e-2)
        nptest.assert_allclose(np.mean(first_indices_jansen[-1, :, :], axis=0), true_first_order_indices, atol=1e-2)

        nptest.assert_allclose(np.mean(total_indices_jansen[-1, :, :], axis=0), true_total_effects, atol=1e-2)
        nptest.assert_allclose(np.mean(total_indices_constantine[-1, :, :], axis=0), true_total_effects, atol=1e-2)

        if bool_plot:
            # Plot results
            col = np.array(
                [[0, 0.4470, 0.7410], [0.8500, 0.3250, 0.0980], [0.9290, 0.6940, 0.1250], [0.4940, 0.1840, 0.5560],
                 [0.4660, 0.6740, 0.1880], [0.3010, 0.7450, 0.9330]])
            plt.figure()
            for idim in range(0, dim):
                plt.semilogx(N_vec, true_total_effects[idim] * np.ones(len(M_vec)), '-', color=col[idim, :],
                             label='Exact index (' + model.get_key()[idim] + ')')
                plt.semilogx(N_vec, np.mean(total_indices_jansen[:, :, idim], axis=1), 'x:', color=col[idim, :],
                             label='Jansen-1999 (' + model.get_key()[idim] + ')')
                plt.semilogx(N_vec, np.mean(total_indices_constantine[:, :, idim], axis=1), '+--', color=col[idim, :],
                             label='Saltelli-2010 (' + model.get_key()[idim] + ')')
            plt.legend()
            plt.xlabel('Number of samples (2N)')
            plt.ylabel('Sobol\' total order effects')
            plt.title('Averaged over %d runs' % n_trials)
            plt.show()

            # error plot (total indices)
            plt.figure()
            for idim in range(0, dim):
                plt.loglog(M_vec, np.mean(
                    np.abs(total_indices_jansen[:, :, idim] - true_total_effects[idim]) / true_total_effects[idim],
                    axis=1),
                           'x:', color=col[idim, :], label='Jansen-1999 (' + model.get_key()[idim] + ')')
                plt.loglog(M_vec,
                           np.mean(np.abs(total_indices_constantine[:, :, idim] - true_total_effects[idim]) /
                                   true_total_effects[idim], axis=1),
                           '+--', color=col[idim, :], label='Saltelli-2010 (' + model.get_key()[idim] + ')')

            y_start = np.mean(np.abs(total_indices_jansen[0, :, :] - true_total_effects) / true_total_effects)
            # plt.xlim([1e1, 1e5])
            x_limits = np.array([np.min(M_vec), np.max(M_vec)])
            slope = -1 / 2
            diff_x = np.log10(x_limits[1]) - np.log10(x_limits[0])
            np.log10(y_start)
            plt.loglog(x_limits, np.array([y_start, np.power(10, np.log10(y_start) + diff_x * slope)]), '-k',
                       label='slope = -1/2')
            plt.legend()
            plt.xlabel('Sampling factor $M$')
            plt.ylabel('Relative error in Sobol\' total order effects')
            plt.title('Averaged over %d runs' % n_trials)
            plt.show()

            # First order plot
            plt.figure()
            for idim in range(0, dim):
                plt.semilogx(N_vec, true_first_order_indices[idim] * np.ones(len(M_vec)), '-', color=col[idim, :],
                             label='True indices ' + model.get_key()[idim])
                plt.semilogx(N_vec, np.mean(first_incides_saltelli_2010[:, :, idim], axis=1), 'x:',
                             color=col[idim, :])
                plt.semilogx(N_vec, np.mean(first_indices_jansen[:, :, idim], axis=1), '+--', color=col[idim, :])
            plt.legend()
            plt.xlabel('Number of samples (2N)')
            plt.ylabel('Sobol\' first order indices')
            plt.title('Averaged over %d runs' % n_trials)
            plt.show()

            # Error plot (first indices)
            plt.figure()
            for idim in range(0, dim):
                if np.abs(true_first_order_indices[idim]) > 0:
                    plt.loglog(M_vec, np.mean(
                        np.abs(first_indices_jansen[:, :, idim] - true_first_order_indices[idim]) /
                        true_first_order_indices[idim], axis=1),
                               'x:', color=col[idim, :], label='Jansen-1999 (' + model.get_key()[idim] + ')')
                    plt.loglog(M_vec,
                               np.mean(
                                   np.abs(first_incides_saltelli_2010[:, :, idim] - true_first_order_indices[idim]) /
                                   true_first_order_indices[idim], axis=1),
                               '+--', color=col[idim, :], label='Saltelli-2010 (' + model.get_key()[idim] + ')')
                else:
                    plt.loglog(M_vec, np.mean(
                        np.abs(first_indices_jansen[:, :, idim] - true_first_order_indices[idim]), axis=1),
                               'x:', color=col[idim, :], label='Jansen-1999 (' + model.get_key()[idim] + '), absolute')
                    plt.loglog(M_vec,
                               np.mean(np.abs(first_incides_saltelli_2010[:, :, idim] - true_first_order_indices[idim]),
                                       axis=1),
                               '+--', color=col[idim, :],
                               label='Saltelli-2010 (' + model.get_key()[idim] + '), absolute')

            y_start = np.mean(np.abs(total_indices_jansen[0, :, :] - true_total_effects) / true_total_effects)
            # plt.xlim([1e1, 1e5])
            x_limits = np.array([np.min(M_vec), np.max(M_vec)])
            slope = -1 / 2
            diff_x = np.log10(x_limits[1]) - np.log10(x_limits[0])
            np.log10(y_start)
            plt.loglog(x_limits, np.array([y_start, np.power(10, np.log10(y_start) + diff_x * slope)]), '-k',
                       label='slope = -1/2')
            plt.legend()
            plt.xlabel('Sampling factor $M$')
            plt.ylabel('Relative error in Sobol\' first order effects')
            plt.title('Averaged over %d runs' % n_trials)
            plt.show()


