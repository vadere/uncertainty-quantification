import os
import unittest

import numpy as np
import pytest

from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.utils.model.VadereModel import VadereModel

cur_dir = os.path.dirname(os.path.realpath(__file__))
PATH2TUTORIAL = os.path.abspath(os.path.join(cur_dir, "../../inversion"))
PATH2SCENARIOS = os.path.abspath(os.path.join(cur_dir, "../../scenarios"))

PATH2MODEL = os.path.join(PATH2SCENARIOS, "vadere-console.jar")
PATH2SCENARIO_FIXED_SEED = os.path.join(PATH2SCENARIOS, "bottleneck_OSM_all_in_one_N60.scenario")


class TestActiveSubspaceParametersGradient(unittest.TestCase):
    # todo move to functional tests
    @pytest.mark.vadere
    def test_initialization(self):
        seed = 267673926

        bool_gradient = False  # is an analytical gradient available? -> True
        bool_averaged = True
        no_runs_averaged = 3  # average over multiple runs?

        alpha = 1
        k = 1  # desired dimension of subspace +1
        M_boot = 0  # Constantine: Typically between 100 and 10^5
        case = " "

        density_type = "uniform"  # input parameter density

        test_input = np.array([[1], [1.34], [0.26], [180], [0.5]])  # legal test input

        # parameter limits
        x_lower = np.array([1.0, 0.5, 0.1, 160, 30])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200, 70])  # upper bounds for parameters

        step_size = 0.025  # Number of pedestrians wird um 1 erhöht

        # parameters
        key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation",
               "sources.[id==3].spawnNumber",  # "bottleneck_width",
               "pedPotentialHeight"]  # uncertain parameters

        qoi = "flow.txt"

        step_size_relative = step_size * (x_upper - x_lower)

        vadere_model = VadereModel(run_local=True, path2scenario=PATH2SCENARIO_FIXED_SEED, path2model=PATH2MODEL,
                                   key=key, qoi=qoi, n_jobs=-1, log_lvl="OFF")

        ActiveSubspaceParameterGradient(model=vadere_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                        bool_gradient=bool_gradient, M_boot=M_boot,
                                        step_size_relative=step_size_relative, step_size=step_size, case=case,
                                        seed=seed, bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                                        bool_save_data=True, bool_print=True, bool_plot=False, path2results=os.getcwd())

        self.assertTrue(True)
