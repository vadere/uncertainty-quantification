import os
import unittest

import matplotlib.pyplot as plt
import numpy as np
import numpy.testing as nptest

from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceCalculatorLLM import ActiveSubspaceCalculatorLLM
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.calc.ActiveSubspaceParameterLLM import ActiveSubspaceParameterLLM
from uq.active_subspace.config_matrix_constantine_fct import config_matrix_constantine
from uq.sensitivity_analysis.sensitivity_salib.config import configure_vadere_sa
from uq.utils.model.ExponentialModel import ExponentialModel


class TestActiveSubspace(unittest.TestCase):
    """ Approximate the gradients of the model with a local linear model and check the resulting active subspace """

    def test_local_linear_model(self) -> None:
        seed = 123609
        case = 1
        bool_gradient = True  # is an analytical gradient available? -> True
        step_size = 10 ** -1  # for (finite-difference) approximation of gradients
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        factor_N = 10

        test_model, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case)

        # local linear model

        params = ActiveSubspaceParameterLLM(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                            case=case, seed=seed, factor_N=factor_N)

        calculator = ActiveSubspaceCalculatorLLM(param=params)
        calculator.identify_active_subspace()

        # Assure that running of the local linear model worked
        self.assertTrue(True)

    # Use many samples to check if the eigenvalues and eigenvalue gap (of the C matrix) approximate the true matrix
    def test_gradients_with_many_samples(self, bool_plot: bool = False) -> None:
        seed = 56158435
        print("test_gradients_with_many_samples")
        case = 2
        bool_gradient = True  # is an analytical gradient available? -> True
        step_size = 10 ** -1  # for (finite-difference) approximation of gradients
        alpha = 10000  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping

        test_model, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case)
        bool_averaged = False

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=False, bool_print=False,
                                                 bool_plot=False, path2results=None)

        # True gradients
        as_calculator = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator.identify_active_subspace()
        results = as_calculator.get_result()

        self.assertLessEqual(np.linalg.norm(results.get_activity_scores() - results.get_activity_scores_true()),
                             11)  # based upon run on 2020-02-05 (alpha = 1e4)
        self.assertLessEqual(np.max(results.get_error_lambda_hat()), 0.5)  # based upon run on 2020-02-05 (alpha = 1e4)

        nptest.assert_allclose(results.get_lambda_hat(), results.get_lambda_true(),
                               rtol=1)  # based upon run on 2020-02-05 (alpha = 1e4)
        nptest.assert_equal(results.get_idx_gap(), results.get_idx_gap_true())
        # nptest.assert_allclose(error_c_gradients, np.zeros(shape=np.size(error_c_gradients)), rtol=10)

    def test_gradients_compared_to_constantine_case1(self) -> None:
        # use true gradients
        self.gradients_compared_to_constantine_one_case(case=1)

    def test_gradients_compared_to_constantine_case2(self) -> None:
        # use true gradients
        self.gradients_compared_to_constantine_one_case(case=2)

    def test_gradients_compared_to_constantine_case3(self) -> None:
        # use true gradients
        self.gradients_compared_to_constantine_one_case(case=3)

    def test_approx_gradients_compared_to_constantine_case1_h0(self) -> None:
        # approximate gradients
        self.approx_gradients_compared_to_constantine(case=1, step_size=1e-1)

    def test_approx_gradients_compared_to_constantine_case1_h1(self) -> None:
        self.approx_gradients_compared_to_constantine(case=1, step_size=1e-3)

    def test_approx_gradients_compared_to_constantine_case1_h2(self) -> None:
        self.approx_gradients_compared_to_constantine(case=1, step_size=1e-5)

    def test_approx_gradients_compared_to_constantine_case2_h0(self) -> None:
        # approximate gradients
        self.approx_gradients_compared_to_constantine(case=2, step_size=1e-1)

    def test_approx_gradients_compared_to_constantine_case2_h1(self) -> None:
        self.approx_gradients_compared_to_constantine(case=2, step_size=1e-3)

    def test_approx_gradients_compared_to_constantine_case2_h2(self) -> None:
        self.approx_gradients_compared_to_constantine(case=2, step_size=1e-5)

    def test_approx_gradients_compared_to_constantine_case3_h0(self) -> None:
        # approximate gradients
        self.approx_gradients_compared_to_constantine(case=3, step_size=1e-1)

    def test_approx_gradients_compared_to_constantine_case3_h1(self) -> None:
        self.approx_gradients_compared_to_constantine(case=3, step_size=1e-3)

    def test_approx_gradients_compared_to_constantine_case3_h2(self) -> None:
        self.approx_gradients_compared_to_constantine(case=3, step_size=1e-5)

    # Check the finite difference approximation to gradients when calculating the active subspace
    def approx_gradients_compared_to_constantine(self, case: int, step_size: float = 1e-5, bool_plot=False) -> None:
        print("test_approx_gradients_compared_to_constantine: CASE " + str(case))
        seed = 9895489
        rtol_activity_scores = 1
        rtol_eigenvalues = 5
        bool_gradient = False  # is an analytical gradient available? -> True
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping

        test_model, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case)
        bool_averaged = None

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=False, bool_print=False,
                                                 bool_plot=False, path2results=None)

        as_calculator = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator.identify_active_subspace()
        results = as_calculator.get_result()

        # Check size of relative error in eigenvalue
        self.assertLessEqual(np.max(np.abs(results.get_error_lambda_hat())), 1)

        nptest.assert_allclose(results.get_lambda_true(), results.get_lambda_hat(), rtol=rtol_eigenvalues)

        if case > 1:
            idx_gap_step_size_true, _ = as_calculator.find_largest_gap_log(lambda_eig=results.get_lambda_true(),
                                                                            step_size=step_size)

            if idx_gap_step_size_true is results.get_idx_gap_true():
                nptest.assert_equal(results.get_idx_gap_true(), results.get_idx_gap())

                # Check relative error in activity scores
                nptest.assert_allclose(results.get_activity_scores(), results.get_activity_scores_true(),
                                       rtol=rtol_activity_scores)

        if bool_plot:
            # Check eigenvalues, Figure 3.2, d
            self.plot_eigenvalues_threshold(results.get_lambda_true(), results.get_lambda_true(),
                                            rtol_eigenvalues, case, step_size)
            self.plot_activity_scores_threshold(results.get_activity_scores(), results.get_activity_scores_true(),
                                                rtol_activity_scores, case, step_size)

    def test_parameter_dimensions(self):
        seed = 267673926

        scenario_name = "Liddle_osm_v4_short.scenario"

        bool_gradient = False  # is an analytical gradient available? -> True
        bool_averaged = True
        no_runs_averaged = 3  # average over multiple runs?

        alpha = 1
        k = 1  # desired dimension of subspace +1
        M_boot = 0  # Constantine: Typically between 100 and 10^5
        case = " "

        # parameter limits
        x_lower = np.array([1.0, 0.5, 0.1, 160, 30, 3.0])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200, 70, 5.0])  # upper bounds for parameters

        step_size = 0.025  # Number of pedestrians wird um 1 erhöht

        # parameters
        key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation",
               "sources.[id==3].spawnNumber",  # "bottleneck_width",
               "pedPotentialHeight"]  # uncertain parameters

        qoi = "max_density.txt"  # quantity of interest

        step_size_relative = step_size * (x_upper - x_lower)

        # configure setup
        test_model, _, _ = configure_vadere_sa(run_local=True, scenario_name=scenario_name, key=key,
                                                 qoi=qoi)

        nptest.assert_raises(ValueError, ActiveSubspaceParameterGradient, model=test_model, x_lower=x_lower,
                             x_upper=x_upper, alpha=alpha, k=k, bool_gradient=bool_gradient, M_boot=M_boot,
                             step_size_relative=step_size_relative, step_size=step_size, case=case, seed=seed,
                             bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged, bool_save_data=False,
                             bool_print=False, bool_plot=False, path2results=None)

    # Calculate active subspace with the true gradients
    # Quadratic model (constantine-2015, p. 45ff)
    def gradients_compared_to_constantine_one_case(self, case: int = 1, bool_plot: bool = False) -> None:
        seed = 354365
        rtol_eigenvalues = 3
        rtol_activity_scores = 1
        print("gradients_compared_to_constantine_one_case:  CASE " + str(case))
        bool_gradient = True  # is an analytical gradient available? -> True
        step_size = None
        alpha = 2  # oversampling factor
        k = 6  # desired dimension of subspace +1
        M_boot = 0  # for bootstrapping

        test_model, x_lower, x_upper, m, density_type, test_input = config_matrix_constantine(case)

        params = ActiveSubspaceParameterGradient(model=test_model, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=step_size, case=case, seed=seed, bool_averaged=None,
                                                 no_runs_averaged=None, bool_save_data=False, bool_print=False,
                                                 bool_plot=False, path2results=None)

        # Calculate activity scores
        as_calculator = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator.identify_active_subspace()
        results = as_calculator.get_result()

        # Check size of relative error in eigenvalue
        self.assertLessEqual(np.max(np.abs(results.get_error_lambda_hat())),
                             1)  # based upon run on 2020-02-05 (alpha = 1e4)

        # Check eigenvalues, Figure 3.2, d
        nptest.assert_allclose(results.get_lambda_hat(), results.get_lambda_true(),
                               rtol=rtol_eigenvalues)  # based upon run on 2020-02-05 (alpha = 1e4)

        if case > 1:
            # Gap testing makes no sense for case 1 since there is no clear gap
            nptest.assert_equal(results.get_idx_gap(), results.get_idx_gap_true())

        nptest.assert_allclose(results.get_activity_scores(), results.get_activity_scores_true(),
                               rtol_activity_scores, case, step_size)  # based upon run on 2020-02-05 (alpha = 1e4)

        if bool_plot:
            self.plot_eigenvalues_threshold(results.get_lambda_hat(), results.get_lambda_true(), rtol_eigenvalues,
                                            case, step_size)
            self.plot_activity_scores_threshold(results.get_activity_scores(), results.get_activity_scores_true(),
                                                rtol_activity_scores, case, step_size)

    def test_step_size_single_int_parameter(self):
        seed = 267673926

        scenario_name = "Liddle_osm_v4_short.scenario"

        bool_gradient = False  # is an analytical gradient available? -> True
        bool_averaged = True
        no_runs_averaged = 3  # average over multiple runs?

        alpha = 1
        k = 1  # desired dimension of subspace +1
        M_boot = 0  # Constantine: Typically between 100 and 10^5
        case = None

        test_input = np.array([[1], [1.34], [0.26], [180], [180], [0.5]])  # legal test input

        # parameter limits
        x_lower = np.array([1.0, 0.5, 0.1, 160, 30])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200, 70])  # upper bounds for parameters

        step_size = 0.01  # Number of pedestrians wird um 1 erhöht

        # parameters
        key = ["queueWidthLoading", "attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation",
               "sources.[id==3].spawnNumber",  # "bottleneck_width",
               "pedPotentialHeight"]  # uncertain parameters

        qoi = "max_density.txt"  # quantity of interest

        step_size_relative = step_size * (x_upper - x_lower)

        # configure setup
        test_model, _, _ = configure_vadere_sa(run_local=True, scenario_name=scenario_name, key=key,
                                                 qoi=qoi)

        nptest.assert_raises(UserWarning, ActiveSubspaceParameterGradient, model=test_model, x_lower=x_lower,
                             x_upper=x_upper, alpha=alpha, k=k, bool_gradient=bool_gradient,
                             M_boot=M_boot, step_size_relative=step_size_relative, step_size=step_size, case=case,
                             seed=seed, bool_averaged=bool_averaged, no_runs_averaged=no_runs_averaged,
                             bool_save_data=False, bool_print=False, bool_plot=False, path2results=None)

    def test_step_size_multiple_int_parameter(self):
        seed = 267673926

        scenario_name = "Liddle_osm_v4_short.scenario"

        bool_gradient = False  # is an analytical gradient available? -> True
        bool_averaged = True
        no_runs_averaged = 3  # average over multiple runs?

        alpha = 1
        k = 1  # desired dimension of subspace +1
        M_boot = 0  # Constantine: Typically between 100 and 10^5
        case = " "

        # parameter limits
        x_lower = np.array([1, 20, 5, 160, 1])  # lower bounds for parameters
        x_upper = np.array([5, 300, 10, 200, 50])  # upper bounds for parameters
        test_input = x_lower

        step_size = 0.01  # Number of pedestrians wird um 1 erhöht

        # Uncertain parameters
        key = ["sources.[id==1].spawnNumber", "sources.[id==2].spawnNumber", "sources.[id==3].spawnNumber",
               "sources.[id==4].spawnNumber", "sources.[id==5].spawnNumber"]

        qoi = "max_density.txt"  # quantity of interest

        step_size_relative = step_size * (x_upper - x_lower)

        # configure setup
        test_model, _, _ = configure_vadere_sa(run_local=True, scenario_name=scenario_name, key=key,
                                                 qoi=qoi)

        nptest.assert_raises(UserWarning, ActiveSubspaceParameterGradient, model=test_model,
                             x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                             bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=step_size_relative,
                             step_size=step_size, case=case, seed=seed, bool_averaged=bool_averaged,
                             no_runs_averaged=no_runs_averaged, bool_save_data=False, bool_print=False, bool_plot=False,
                             path2results=None)

    def test_activity_scores_testmodel(self, bool_plot: bool = False) -> None:

        rtol_eigenvalues = 5
        rtol_activity_scores = 0.5

        testmodel = ExponentialModel()
        density_type = "uniform"
        dim = testmodel.get_dimension()
        x_lower = -1.0 * np.ones(shape=dim)  # lower bounds for parameters
        x_upper = np.ones(shape=dim)  # lower bounds for parameters
        test_input = x_lower
        alpha = 10
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False

        params = ActiveSubspaceParameterGradient(model=testmodel, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=False, path2results=os.getcwd())

        # Calculate activity scores
        as_calculator = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator.identify_active_subspace()
        results = as_calculator.get_result()

        # Check eigenvalues, Figure 3.2, d
        nptest.assert_allclose(results.get_lambda_hat(), results.get_lambda_true(),
                               rtol=rtol_eigenvalues)  # based upon run on 2020-02-05 (alpha = 1e4)

        nptest.assert_equal(results.get_idx_gap(), results.get_idx_gap_true())

        nptest.assert_allclose(results.get_activity_scores(), results.get_activity_scores_true(),
                               rtol_activity_scores)

    def test_activity_scores_io(self, bool_plot: bool = False) -> None:

        testmodel = ExponentialModel()
        density_type = "uniform"
        dim = testmodel.get_dimension()
        x_lower = -1.0 * np.ones(shape=dim)  # lower bounds for parameters
        x_upper = np.ones(shape=dim)  # lower bounds for parameters
        test_input = x_lower
        alpha = 10
        k = 1 + 1
        bool_gradient = True
        M_boot = 0
        seed = 2456354
        bool_averaged = False

        params = ActiveSubspaceParameterGradient(model=testmodel, x_lower=x_lower, x_upper=x_upper, alpha=alpha, k=k,
                                                 bool_gradient=bool_gradient, M_boot=M_boot, step_size_relative=None,
                                                 step_size=None, case=None, seed=seed, bool_averaged=bool_averaged,
                                                 no_runs_averaged=None, bool_save_data=True, bool_print=True,
                                                 bool_plot=False, path2results=os.getcwd())

        as_calculator = ActiveSubspaceCalculatorGradient(param=params)
        as_calculator.identify_active_subspace()
        results = as_calculator.get_result()

        assert True  # make sure this statement is reached

    # ---------------------------------------------------------- Plot routines

    def plot_eigenvalues_threshold(self, lambda_eig, lambda_eig_true, rtol_eigenvalues: float, case: int,
                                   step_size: float) -> None:
        # Show eigenvalues
        plt.figure()
        plt.semilogy(lambda_eig, 'd-', label='Estimated eigenvalues')
        plt.semilogy(lambda_eig_true, 'x-', label='True eigenvalues')
        upper_threshold = lambda_eig_true * (1 + rtol_eigenvalues)
        lower_threshold = lambda_eig_true / (1 + rtol_eigenvalues)
        plt.plot(upper_threshold, ':k')

        plt.plot(lower_threshold, ':k')
        if step_size is None:
            plt.title(r"Eigenvalues, Case {i:d}, rtol: {k:.2f}".format(i=case, k=rtol_eigenvalues))
        else:
            plt.title(r"Eigenvalues, Case {i:d}, step size: {j:.2e}, rtol: {k:.2f}".format(i=case, j=step_size,
                                                                                           k=rtol_eigenvalues))
        plt.legend()
        plt.show()

    def plot_activity_scores_threshold(self, activity_scores, true_activity_scores, rtol_activity_scores: float,
                                       case: int, step_size: float):
        plt.figure()
        plt.semilogy(activity_scores, 'd-', label='Estimated activity scores')
        plt.semilogy(true_activity_scores, 'x-', label='True activity scores')
        upper_threshold = true_activity_scores * (1 + rtol_activity_scores)
        lower_threshold = true_activity_scores / (1 + rtol_activity_scores)
        plt.plot(upper_threshold, ':k')

        plt.plot(lower_threshold, ':k')
        if step_size is None:
            plt.title(r"Activity scores, Case {i:d}, rtol: {k:.2f}".format(i=case, k=rtol_activity_scores))
        else:
            plt.title(r"Activity scores, Case {i:d}, Step size: {j:.2e}, rtol: {k:.2f}".format(i=case, j=step_size,
                                                                                               k=rtol_activity_scores))
        plt.legend()
        plt.show()

