import unittest

import numpy as np
import numpy.testing as nptest

from uq.sensitivity_analysis.calc.SobolIndexCalculatorChaospy import SobolIndexCalculatorChaospy
from uq.sensitivity_analysis.calc.SobolIndexParameterChaospy import SobolIndexParameterChaospy
from uq.utils.model.IshigamiModel import IshigamiModel
from uq.utils.prior.UniformGenMult import UniformGenMult

RUN_LOCAL = True


class TestSobolIndexCalculatorChaospy(unittest.TestCase):

    # Calculate indices for ishigami function and compare with true indices
    def test_sobol_indices_for_ishigami_function(self):
        model = IshigamiModel(a=7, b=0.05)
        bool_averaged = False
        no_runs_averaged = None

        M = 10000

        x_lower = np.array([-np.pi, -np.pi, -np.pi])  # https://uqworld.org/t/ishigami-function/55
        x_upper = np.array([np.pi, np.pi, np.pi])  # https://uqworld.org/t/ishigami-function/55

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=model.get_dimension())
        bool_load_approx_model = False
        bool_load_samples = False
        order = 6

        seed = 62389962

        params = SobolIndexParameterChaospy(model=model, prior=rho, M=M, order=order,
                                            bool_load_approx_model=bool_load_approx_model,
                                            path_to_approx_model=None, bool_load_samples=bool_load_samples,
                                            path_to_samples=None, seed=seed, bool_averaged=bool_averaged,
                                            no_runs_averaged=no_runs_averaged)

        calc = SobolIndexCalculatorChaospy(params)
        calc.calc_sobol_indices()
        result = calc.get_result()

        nptest.assert_allclose(result.get_total_indices(), model.get_true_total_effect_indices(), atol=0.003)  # 0.085)
        nptest.assert_allclose(result.get_total_indices(), model.get_true_first_order_indices(), atol=0.1)  # 0.18)

