import os
import unittest

import matplotlib.pyplot as plt
import numpy as np
import pytest
import seaborn as sns

from uq.sensitivity_analysis.calc.SobolIndexCalculatorChaospy import SobolIndexCalculatorChaospy
from uq.sensitivity_analysis.calc.SobolIndexParameterChaospy import SobolIndexParameterChaospy
from uq.utils.ioput.DataSaver import DataSaver
from uq.utils.model.VadereModel import VadereModel
from uq.utils.prior.UniformGenMult import UniformGenMult

RUN_LOCAL = True


class TestSobolIndexCalculatorChaospyVadere(unittest.TestCase):

    # todo move to functional tests
    @pytest.mark.vadere
    def test_sobol_indices_vadere_model(self, bool_plot=False):

        bool_averaged = True
        no_runs_averaged = 2  # fixed seed !
        x_lower = np.array([1.0, 0.5, 0.0, 160])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200])  # upper bounds for parameters
        key = ["searchRadius", "attributesPedestrian.speedDistributionMean",
               "attributesPedestrian.speedDistributionStandardDeviation",
               "sources.[id==3].spawnNumber"]  # uncertain parameters
        key_str_plot = ["free-flow mean", "free-flow dev", "spawn number", "inter-arrival time", "bottleneck width",
                        "obstacle repulsion"]
        qoi = "mean_density.txt"  # quantity of interest

        # Model
        current_dir = os.path.dirname(os.path.realpath(__file__))
        path2model = os.path.abspath(os.path.join(current_dir, "../../scenarios", "vadere-console.jar"))
        path2scenario = os.path.abspath(
            os.path.join(current_dir, "../../scenarios", "Liddle_bhm_v3_fixed_seed.scenario"))
        # path2scenario = os.path.join(current_dir, "../scenarios", "bottleneck_OSM_all_in_one_N60.scenario")

        test_model = VadereModel(run_local=RUN_LOCAL, path2scenario=path2scenario, path2model=path2model, key=key,
                                 qoi=qoi, n_jobs=-1, log_lvl="OFF")

        #  parameters
        M = 3
        order = 3  # order of polynomial
        n_runs = 1

        seed = 13674963

        bool_load_approx_model = False
        bool_load_samples = False

        bool_plot = False

        # Calculation

        rho = UniformGenMult(lower=x_lower, upper=x_upper, dim=test_model.get_dimension())
        params = SobolIndexParameterChaospy(model=test_model, prior=rho, M=M, order=order,
                                            bool_load_approx_model=bool_load_approx_model,
                                            path_to_approx_model=None, bool_load_samples=bool_load_samples,
                                            path_to_samples=None, seed=seed, bool_averaged=bool_averaged,
                                            no_runs_averaged=no_runs_averaged)

        data_saver = DataSaver(os.getcwd(), 'summary')

        if bool_plot:
            h1 = plt.figure()
            color_vec = sns.color_palette("hls", test_model.get_dimension())

        for i_run in range(0, n_runs):
            print("** ........................ RUN %d **" % i_run)

            params.set_path2results(data_saver.get_path_to_files())
            calc = SobolIndexCalculatorChaospy(params)
            calc.calc_sobol_indices()
            result = calc.get_result()

            # Plot results
            if bool_plot:
                plt.plot(result.get_total_indices(), ':', color='black')
                for i in range(0, test_model.get_dimension()):
                    if i_run == 1:
                        plt.plot(i, result.get_total_indices()[i], 'd', label=key_str_plot[i], color=color_vec[i])
                    else:
                        plt.plot(i, result.get_total_indices()[i], 'd', color=color_vec[i])

                plt.ylabel("Sensitivity indices")
                if i_run == 1:
                    plt.legend()

        print('*** FINISHED *** ')
        # save all curves in one file
        if bool_plot:
            data_saver.save_figure(h1, 'sensitivity_indices')
            plt.show()
        data_saver.save_to_pickle(data=test_model, name='vadere_config')  # does not change over the runs

        self.assertTrue(True)
