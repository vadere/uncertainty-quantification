# disable test
def unit_disabled(func):
    def wrapper(func):
        func.__test__ = False
        return func

    return wrapper
