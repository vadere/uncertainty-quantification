import unittest

import numpy as np

from uq.utils.prior.GaussianGen import GaussianGen
from uq.utils.prior.GaussianGenMult import GaussianGenMult
from uq.utils.prior.PriorFactory import PriorFactory
from uq.utils.prior.UniformGen import UniformGen
from uq.utils.prior.UniformGenMult import UniformGenMult


class TestPriorFactory(unittest.TestCase):

    def test_prior_factory_uniform_1d(self):
        dim = 1
        x_lower = -1 * np.ones(shape=dim)
        x_upper = np.ones(shape=dim)
        prior1 = UniformGen(lower=x_lower, upper=x_upper)
        params = {"Type": "Uniform", "Low": x_lower, "High": x_upper, "Dim": dim}
        prior_from_factory = PriorFactory().create_prior_by_type(params=params)
        self.assertTrue(prior1.equals(prior_from_factory))
        self.assertTrue(prior_from_factory.equals(prior1))

        prior_multidim = UniformGenMult(x_lower, x_upper, dim)
        self.assertRaises(AssertionError, self.assertTrue, prior_from_factory.equals(prior_multidim))

    def test_prior_factory_normal_1d(self):
        mean = -3
        dev = 0.1236
        prior = GaussianGen(mean=mean, variance=dev)
        params = {"Type": "Normal", "Dim": 1, "Mean": mean, "Variance": dev}
        prior_from_factory = PriorFactory().create_prior_by_type(params=params)

        prior_multidim = GaussianGenMult(mean=mean, variance=dev, dim=1)

        self.assertTrue(prior.equals(prior_from_factory))
        self.assertRaises(AssertionError, self.assertTrue, prior_from_factory.equals(prior_multidim))

    def test_prior_factory_uniformgenmult_multidim(self):
        dim = int(6)
        x_lower = -1 * np.ones(shape=(dim, 1))
        x_upper = np.ones(shape=(dim, 1))
        prior1 = UniformGenMult(x_lower, x_upper, dim)

        params = {"Type": "Uniform", "Low": x_lower, "High": x_upper, "Dim": dim}
        prior2 = PriorFactory().create_prior_by_type(params=params)

        self.assertTrue(prior1.equals(prior2))

    def test_prior_factory_gaussiangenmult_multidim(self):
        dim = int(6)
        mean = np.ones(shape=(dim, 1))
        cov = np.eye(N=dim)
        prior_gauss_factory = GaussianGenMult(mean=mean, variance=cov, dim=dim)
        params_normal = {"Type": "Normal", "Mean": mean, "Variance": cov, "Dim": dim}
        prior_gauss = PriorFactory().create_prior_by_type(params=params_normal)
        self.assertTrue(prior_gauss_factory.equals(prior_gauss))
