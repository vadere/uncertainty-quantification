import unittest

import numpy as np
import numpy.testing as nptest
import scipy
from numpy.random import RandomState
from scipy import stats

from uq.utils.prior.UniformGen import UniformGen


class TestUniformGen(unittest.TestCase):

    def test_mean_uniform_gen(self):
        lower = -1
        upper = 1
        prior = UniformGen(lower=lower, upper=upper)
        self.assertEqual(prior.get_mean(), (upper + lower) / 2)
        self.assertEqual(prior.get_lower(), lower)
        self.assertEqual(prior.get_upper(), upper)
        self.assertEqual(prior.get_dim(), 1)

        lower2 = -10
        upper2 = 5
        prior2 = UniformGen(lower=lower2, upper=upper2)
        self.assertEqual(prior2.get_mean(), (upper2 + lower2) / 2)
        self.assertEqual(prior2.get_lower(), lower2)
        self.assertEqual(prior2.get_upper(), upper2)
        self.assertEqual(prior2.get_dim(), 1)

        lower3 = np.random.rand(1) * 10 - 5
        upper3 = np.random.rand(1) * 10 + 5
        prior3 = UniformGen(lower=lower3, upper=upper3)
        self.assertEqual(prior3.get_mean(), (upper3 + lower3) / 2)
        self.assertEqual(prior3.get_lower(), lower3)
        self.assertEqual(prior3.get_upper(), upper3)
        self.assertEqual(prior3.get_dim(), 1)

    def test_samples(self):
        seed = int(np.random.uniform(0, 2 ** 32 - 1))

        lower3 = np.random.rand(1) * 10 - 5
        upper3 = np.random.rand(1) * 10 + 5
        prior3 = UniformGen(lower=lower3, upper=upper3)
        n = 10000
        samples = prior3.sample(n=n, random_state=RandomState(seed))
        self.assertTrue(np.all(samples >= lower3))
        self.assertTrue(np.all(samples <= upper3))
        self.assertTrue(len(samples) == n)
        nptest.assert_allclose(np.mean(samples), prior3.get_mean(), rtol=0.01)

        samples2 = stats.uniform.rvs(loc=lower3, scale=upper3 - lower3, size=n, random_state=RandomState(seed))

        statistic2, pvalue2 = stats.kstest(samples2, stats.uniform(loc=lower3, scale=upper3 - lower3).cdf)

        statistic, pvalue = stats.kstest(samples, stats.uniform(loc=lower3, scale=upper3 - lower3).cdf)

        nptest.assert_array_equal(samples, samples2)
        self.assertEqual(statistic, statistic2)
        self.assertEqual(pvalue, pvalue2)

    def test_sampling_uniform_1d(self):
        my_lower = -3
        my_upper = 0.75
        n_samples = 10 ** 6

        dist = UniformGen(my_lower, my_upper)
        samples = dist.sample(n_samples, RandomState())

        # assure that all bars have roughly the same height
        hist, bins = np.histogram(samples, density=True)
        self.assertLessEqual(np.max(np.diff(hist)), 10 ** -2)

        self.assertEqual(len(samples), n_samples)
        self.assertLessEqual(np.max(samples), my_upper)
        self.assertGreaterEqual(np.min(samples), my_lower)

        alpha = 0.05
        [D, pvalue] = scipy.stats.kstest(samples, stats.uniform(loc=my_lower, scale=(my_upper - my_lower)).cdf)
        self.assertGreater(pvalue, alpha)
