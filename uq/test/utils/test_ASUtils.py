import unittest
from typing import Union

import numpy as np
import numpy.testing as nptest
from numpy.random import RandomState

from uq.active_subspace.calc.ActiveSubspaceCalculator import ActiveSubspaceCalculator
from uq.active_subspace.calc.ActiveSubspaceCalculatorGradient import ActiveSubspaceCalculatorGradient
from uq.active_subspace.calc.ActiveSubspaceParameterGradient import ActiveSubspaceParameterGradient
from uq.active_subspace.config_matrix_constantine_fct import config_matrix_constantine
from uq.active_subspace.utils import transform_coordinates_from_unit, transform_coordinates_to_unit, \
    is_smaller_equal_than_machine_precision


class TestUtils(unittest.TestCase):
    def test_transformation_from_to_unit(self):
        print("test_transformation_to_unit")
        seed = 3476868
        case = 1
        test_model, x_lower, x_upper, m, density_type, test_input = \
            config_matrix_constantine(case, random_state=RandomState(seed))

        # test of transformation
        test_x = transform_coordinates_to_unit(x_lower, x_upper, test_input)
        test_y = transform_coordinates_from_unit(x_lower, x_upper, test_x)

        nptest.assert_almost_equal(test_y, np.expand_dims(test_input, axis=1))

    def test_scale_gradient_scalar(self):
        x_lower = np.array([1.0])  # lower bounds for parameters
        x_upper = np.array([5.0])  # upper bounds for parameters

        gradient_value = 0.7
        gradient_value_ndarray = np.array([0.7])

        self.scale_gradient(x_lower=x_lower, x_upper=x_upper, gradient=gradient_value)
        self.scale_gradient(x_lower=x_lower, x_upper=x_upper, gradient=gradient_value_ndarray)

    def test_scale_gradient_vector(self):
        x_lower = np.array([1.0, 0.5, 0.1, 160])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200])  # upper bounds for parameters
        gradient_row_vector = np.array([0.7, 0.3, 0.2, 0.1])
        gradient_col_vector = np.array([[0.7], [0.3], [0.2], [0.1]])

        self.scale_gradient(x_lower=x_lower, x_upper=x_upper, gradient=gradient_row_vector)
        self.scale_gradient(x_lower=x_lower, x_upper=x_upper, gradient=gradient_col_vector)

    def test_scale_gradient_matrix(self):
        x_lower = np.array([1.0, 0.5, 0.1, 160, 30])  # lower bounds for parameters
        x_upper = np.array([5.0, 2.2, 1.0, 200, 70])  # upper bounds for parameters

        gradient_matrix = np.array(
            [[0.7, 0.3, 0.2, 0.1], [0.3, 0.2, 0.1, -0.1], [0.8, 0.3, 0.5, 0.1], [0.9, 0.8, 0.7, 0.6],
             [0.3, 0.4, 0.5, 0.8]])
        self.scale_gradient(x_lower=x_lower, x_upper=x_upper, gradient=gradient_matrix)

    def scale_gradient(self, x_lower: Union[float, np.ndarray], x_upper: Union[float, np.ndarray],
                       gradient: Union[float, np.ndarray]):
        # make sure that all three versions lead to the same result

        tmp_bounds = 0.5 * np.diag(x_upper - x_lower)
        if np.ndim(gradient) == 0:
            gradient = np.array([gradient])
        values_grad_f = np.matmul(tmp_bounds, gradient)

        values_grad_f_v2 = np.matmul(0.5 * np.diag(x_upper - x_lower), gradient)

        param = ActiveSubspaceParameterGradient(model=None, x_lower=x_lower, x_upper=x_upper)
        values_grad_f_v3 = ActiveSubspaceCalculatorGradient(param).scale_gradient(gradient=gradient)

        np.testing.assert_array_almost_equal(values_grad_f, values_grad_f_v2)
        np.testing.assert_array_almost_equal(values_grad_f, values_grad_f_v3)

    def test_spectral_gap_eigenvalues_not_sorted(self):
        eigenvalues = np.array([1, 10, 0.8, 0.6, 0.5, 0.1])
        nptest.assert_raises(ValueError, ActiveSubspaceCalculator.find_largest_gap_log, lambda_eig=eigenvalues)

    def test_find_spectral_gap(self):
        eigenvalues = np.array([10, 1, 0.8, 0.6, 0.5, 0.1])
        idx, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=eigenvalues)
        self.assertEqual(idx, 0)

        eigenvalues = np.array([10, 10, 10, 10, 10, 10, 10, 10, 10, 9])
        idx, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=eigenvalues)
        self.assertEqual(idx, 8)

        eigenvalues = np.array([10, 10, 10, 10, 9.9, 9.9, 9.9, 9.9, 9.9])
        idx, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=eigenvalues)
        self.assertEqual(idx, 3)

        eigenvalues = np.array([10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
        idx, __ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=eigenvalues)
        self.assertEqual(idx, 0)

    def test_find_spectral_gap_above_step_size(self):
        # make sure the gap above the step size is found

        params = ActiveSubspaceParameterGradient(bool_gradient=False)

        # step_size below all values, same gap should be found
        eigenvalues = np.array([10, 1, 0.8, 0.6, 0.5, 0.1])
        idx1, _ = ActiveSubspaceCalculatorGradient(param=params).find_largest_gap_log(lambda_eig=eigenvalues,
                                                                                       step_size=1e-2)
        idx2, _ = ActiveSubspaceCalculator.find_largest_gap_log(lambda_eig=eigenvalues)

        self.assertEqual(idx1, idx2)

        # among equidistantly placed values
        eigenvalues = np.array([1e5, 1e4, 1e3, 1e2, 1e1, 1e-1])
        idx, _ = ActiveSubspaceCalculatorGradient(param=params).find_largest_gap_log(lambda_eig=eigenvalues,
                                                                                      step_size=2e1)
        self.assertEqual(idx, 0)

        # among equidistantly placed above the step size
        eigenvalues = np.array([1e5, 1e5, 1e5, 1e-3, 1e-3, 1e-3])
        idx, _ = ActiveSubspaceCalculatorGradient(param=params).find_largest_gap_log(lambda_eig=eigenvalues,
                                                                                      step_size=1e-3)
        self.assertEqual(idx, 2)

        # motivated by results from MDPI paper
        eigenvalues = np.array([0.82469732, 0.20899284, 0.10950086, 0.0369803, 0.01892891, 0.00414014])
        idx, _ = ActiveSubspaceCalculatorGradient(param=params).find_largest_gap_log(lambda_eig=eigenvalues,
                                                                                      step_size=0.025)
        self.assertEqual(idx, 0)

    def test_find_spectral_gap_duplicate_values(self):
        eigenvalues = np.array([1e-1, 1e-1, 2e-3, 1.9e-3, 1.8e-3, 1.8e-3, 1.7e-3, 1.6e-3])
        idx, _ = ActiveSubspaceCalculator().find_largest_gap_log(lambda_eig=eigenvalues)
        self.assertEqual(idx, 1)

    def test_find_spectral_gap_mixed_vector(self):
        # vector is not ascending
        eigenvalues = np.array([1e-3, 1e-1, 2e-3, 1.9e-3, 1.8e-3, 1.8e-3, 1.7e-3, 1.6e-3])
        self.assertRaises(ValueError, ActiveSubspaceCalculator().find_largest_gap_log, eigenvalues)

    def test_is_smaller_than_machine_precision(self):
        self.assertFalse(is_smaller_equal_than_machine_precision(1))
        self.assertTrue(is_smaller_equal_than_machine_precision(np.finfo(float).eps))
        self.assertTrue(is_smaller_equal_than_machine_precision(np.finfo(float).eps - np.finfo(float).eps / 10))
        self.assertFalse(is_smaller_equal_than_machine_precision(np.finfo(float).eps + 0.1))
        self.assertFalse(is_smaller_equal_than_machine_precision(-1))
