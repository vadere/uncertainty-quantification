import unittest

import numpy as np
import numpy.testing as nptest
import scipy.stats as scipy_stats
from numpy.random import RandomState

from uq.utils.prior.GaussianGen import GaussianGen
from uq.utils.prior.GaussianGenMult import GaussianGenMult


class TestGaussianGenMult(unittest.TestCase):

    def test_equals_gaussiangenmult(self):
        dim = int(6)
        mean = np.ones(shape=(dim, 1))
        cov = np.eye(N=dim)
        prior1 = GaussianGenMult(mean=mean, variance=cov, dim=dim)
        prior2 = GaussianGenMult(mean=mean, variance=cov, dim=dim)
        self.assertTrue(prior1.equals(prior2))

        mean2 = np.random.rand(dim)
        prior3 = GaussianGenMult(mean=mean2, variance=cov, dim=dim)
        self.assertRaises(AssertionError, self.assertTrue, prior1.equals(prior3))

        cov2 = 0.8 * np.eye(N=dim)
        prior4 = GaussianGenMult(mean=mean, variance=cov2, dim=dim)
        self.assertRaises(AssertionError, self.assertTrue, prior1.equals(prior4))

        dim = int(5)
        mean = np.ones(shape=(dim, 1))
        cov = np.eye(N=dim)
        prior5 = GaussianGenMult(mean=mean, variance=cov, dim=dim)
        self.assertRaises(AssertionError, self.assertTrue, prior5.equals(prior1))

    def test_gaussian_gen_mult(self):
        mean = np.array([1, -3])
        cov = np.array([[0.9, 0], [0, 0.1236]])
        prior_multidim = GaussianGenMult(mean=mean, variance=cov, dim=2)

        samples = prior_multidim.sample(n=10000, random_state=RandomState())
        sample_mean = np.mean(samples, axis=1)
        sample_cov = np.cov(samples)

        nptest.assert_allclose(sample_mean, mean, atol=1e-1)
        nptest.assert_allclose(sample_cov, cov, atol=1e-1)

    def test_sampling_gauss(self):
        # assure that samples are within the limits and reflective of the chosen distribution parameters (mean / std)
        # even though the distribution is truncated
        my_mean = 0
        my_std = 1

        n_samples = 10 ** 7

        dist = GaussianGen(my_mean, my_std)
        samples = dist.sample(n=n_samples, random_state=RandomState())

        # plt.figure()
        # plt.hist(samples, 1000)
        # plt.show()

        # test for normal distribution
        k2, p = scipy_stats.normaltest(samples)
        alpha = 1e-3

        # assure that null hypothesis cannot be rejected
        self.assertGreaterEqual(p, alpha)

        self.assertEqual(len(samples), n_samples)
        self.assertLess(np.abs(np.mean(samples) - my_mean), 10 ** -1)
        self.assertLess(np.abs(np.std(samples) - my_std), 10 ** -1)
