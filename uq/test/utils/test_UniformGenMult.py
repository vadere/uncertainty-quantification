import unittest

import numpy as np
import numpy.testing as nptest
import scipy
from numpy.random import RandomState
from scipy import stats

from uq.utils.prior.UniformGenMult import UniformGenMult


class TestUniformGenMult(unittest.TestCase):

    def test_mean_uniform_gen_mult(self):
        prior = UniformGenMult(np.array([-3, -3]), np.array([5, 5]), 2)
        nptest.assert_allclose(prior.get_mean(), np.array([-3, 5]))

        prior = UniformGenMult(np.array([-5, 5]), np.array([5, 15]), 2)
        nptest.assert_allclose(prior.get_mean(), [0, 10])

    def test_sampling_uniform(self):
        seed = (np.abs(np.random.rand(1) * (2 ** 30)).astype(int))
        dim = int(6)
        n_samples = int(1e6)
        x_lower = -1 * np.ones(shape=(dim, 1))
        x_upper = np.ones(shape=(dim, 1))
        prior = UniformGenMult(x_lower, x_upper, dim)
        samples = prior.sample(n_samples, RandomState(seed))
        #  for i in range(0, dim):
        #      plt.figure()
        #      plt.hist(samples[i, :])

        # make sure the largest / smallest samples are close to the bounds
        nptest.assert_allclose(np.max(samples, axis=1), x_upper.flatten(), rtol=1e-4)
        nptest.assert_allclose(np.min(samples, axis=1), x_lower.flatten(), rtol=1e-4)

        # assure that the samples are within the bounds
        self.assertTrue((samples <= x_upper).all())
        self.assertTrue((samples >= x_lower).all())

        # test distribution
        alpha = 0.05
        for i in range(0, dim):
            [D, pvalue] = scipy.stats.kstest(samples[i, :], stats.uniform(loc=x_lower[i], scale=(x_upper[i] - x_lower[i])).cdf)
            self.assertGreater(pvalue, alpha)
