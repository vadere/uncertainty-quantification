import unittest

import numpy as np
from numpy.random import RandomState

from uq.utils.prior.GaussianGen import GaussianGen
from uq.utils.prior.GaussianGenTrunc import GaussianGenTrunc
from uq.utils.prior.UniformGen import UniformGen


class TestPriorDistribution(unittest.TestCase):

    def test_params_gaussian_gen(self):
        prior = GaussianGen()
        self.assertEqual(prior.get_mean(), 0)

        mean = -3
        std = 0.1236
        prior = GaussianGen(mean=mean, variance=std)
        self.assertEqual(prior.get_mean(), mean)
        self.assertEqual(prior.get_deviation(), std)

    def test_gaussian_gen_trunc_eval(self):
        N_outer = 20
        for i_outer in range(0, N_outer):
            mean = np.random.rand(1) * 2 - 1
            std = np.abs(np.random.randn(1) * 1.5)
            lower_limit = -1
            upper_limit = 1
            prior = GaussianGenTrunc(mean=mean, variance=std, limits=np.asarray([lower_limit, upper_limit]))
            self.assertTrue(prior.eval_prior(mean) > 0)

            N_inner = 20
            for i_inner in range(0, N_inner):
                value = np.random.rand(1) * 4 - 2
                if lower_limit < value < upper_limit:
                    self.assertTrue(prior.eval_prior(value) >= 0)
                else:
                    self.assertTrue(prior.eval_prior(value) == 0)

    def test_params_gaussian_gen_trunc(self):
        mean = 3
        std = 0.263732
        lower_limit = -5
        upper_limit = 10
        prior = GaussianGenTrunc(mean=mean, variance=std, limits=np.asarray([lower_limit, upper_limit]))

        self.assertEqual(mean, prior.get_mean())
        self.assertEqual(std, prior.get_variance())
        self.assertEqual(lower_limit, prior.get_lower_limit())
        self.assertEqual(upper_limit, prior.get_upper_limit())

        # make sure that mean is in limit interval and that limits are different (lower < higher)

        self.assertRaises(ValueError, GaussianGenTrunc, mean=mean, variance=std,
                          limits=np.array([lower_limit, lower_limit]))
        self.assertRaises(ValueError, GaussianGenTrunc, mean=mean, variance=std,
                          limits=np.array([lower_limit, lower_limit + 1]))
        self.assertRaises(ValueError, GaussianGenTrunc, mean=mean, variance=std,
                          limits=np.array([upper_limit, lower_limit]))
        self.assertRaises(ValueError, GaussianGenTrunc, mean=mean, variance=std,
                          limits=np.array([upper_limit, upper_limit]))
        self.assertRaises(ValueError, GaussianGenTrunc, mean=mean, variance=std, limits=np.array([mean, mean]))

        # make sure that std is positive
        self.assertRaises(ValueError, GaussianGenTrunc, mean=mean, variance=-std,
                          limits=np.array([lower_limit, upper_limit]))

    def test_equals_uniformgen(self):
        lower = -1253973
        upper = 27
        prior1 = UniformGen(lower=lower, upper=upper)
        prior2 = UniformGen(lower=lower, upper=upper)
        prior3 = UniformGen(lower=lower - 0.1, upper=upper)
        prior4 = UniformGen(lower=lower - 0.1, upper=upper)
        prior5 = UniformGen(lower=lower, upper=upper + 0.1)
        prior6 = UniformGen(lower=lower, upper=upper + 0.1)

        mean = 6573.226
        std = 0.1236796
        prior_gauss = GaussianGen(mean=mean, variance=std)

        self.assertTrue(prior1.equals(prior2))
        self.assertTrue(prior3.equals(prior4))
        self.assertTrue(prior5.equals(prior6))

        self.assertRaises(AssertionError, self.assertTrue, prior1.equals(prior3))
        self.assertRaises(AssertionError, self.assertTrue, prior3.equals(prior5))
        self.assertRaises(AssertionError, self.assertTrue, prior5.equals(prior1))

        self.assertRaises(AssertionError, self.assertTrue, prior1.equals(prior_gauss))

    def test_uniformgen_limits(self):
        lower = 5.0
        upper = 3.0
        self.assertRaises(ValueError, UniformGen, lower=lower, upper=upper)

    def test_equals_gaussian_gen_trunc(self):
        mean = 65.226
        std = 0.1236796
        lower_limit = 23.35
        upper_limit = 72.396

        gauss1 = GaussianGenTrunc(mean=mean, variance=std, limits=np.array([lower_limit, upper_limit]))
        gauss2 = GaussianGenTrunc(mean=mean, variance=std, limits=np.array([lower_limit, upper_limit]))
        self.assertTrue(gauss1.equals(gauss2))

        gauss3 = GaussianGenTrunc(mean=mean + 0.1, variance=std, limits=np.array([lower_limit, upper_limit]))
        self.assertRaises(AssertionError, self.assertTrue, gauss1.equals(gauss3))

        gauss4 = GaussianGenTrunc(mean=mean, variance=std + 0.1, limits=np.array([lower_limit, upper_limit]))
        self.assertRaises(AssertionError, self.assertTrue, gauss3.equals(gauss4))

        gauss5 = GaussianGen(mean=mean, variance=std)
        self.assertRaises(AssertionError, self.assertTrue, gauss5.equals(gauss4))

    def test_equals_gaussian_gen(self):
        mean = 6573.226
        std = 0.1236796
        prior1 = GaussianGen(mean=mean, variance=std)
        prior2 = GaussianGen(mean=mean, variance=std)
        prior3 = GaussianGen(mean=mean + 0.1, variance=std)
        prior4 = GaussianGen(mean=mean + 0.1, variance=std)
        prior5 = GaussianGen(mean=mean + 0.1, variance=std + 0.1)

        prior6 = UniformGen(lower=std, upper=mean)

        self.assertTrue(prior1.equals(prior2))
        self.assertRaises(AssertionError, self.assertTrue, prior2.equals(prior3))
        self.assertRaises(AssertionError, self.assertTrue, prior3.equals(prior1))
        self.assertRaises(AssertionError, self.assertTrue, prior4.equals(prior5))

        self.assertRaises(AssertionError, self.assertTrue, prior5.equals(prior6))  # different types
