import unittest

import numpy as np
import numpy.testing as nptest
from numpy.random import RandomState

from uq.utils.data_eval import sample_mode
from uq.utils.prior.GaussianGenTrunc import GaussianGenTrunc
from uq.utils.prior.GaussianGenTruncMult import GaussianGenTruncMult


class TestGaussianGenTrunc(unittest.TestCase):

    def test_gaussian_gen_trunc_mult_attributes(self):
        dim = 2
        mean = np.random.rand(2) * 10 - 5
        variance = np.eye(dim) * np.random.rand(2)
        lower_limit = mean - 2
        upper_limit = mean + 2
        limits = np.array([lower_limit, upper_limit])
        rho = GaussianGenTruncMult(mean=mean, variance=variance, limits=limits, dim=dim)

        nptest.assert_array_equal(rho.get_mean(), mean)
        nptest.assert_array_equal(rho.get_cov(), variance)
        nptest.assert_array_equal(rho.get_lower_limit(), lower_limit)
        nptest.assert_array_equal(rho.get_upper_limit(), upper_limit)
        nptest.assert_array_equal(rho.get_limits(), limits)
        nptest.assert_array_equal(rho.get_dim(), dim)

    def test_sampling_limits_gaussian_gen_trun_mult(self):
        dim = 2
        mean = np.random.rand(2) * 10 - 5
        variance = np.eye(dim) * np.random.rand(2)
        lower_limit = mean - 2
        upper_limit = mean + 2
        limits = np.array([lower_limit, upper_limit])
        rho = GaussianGenTruncMult(mean=mean, variance=variance, limits=limits, dim=dim)

        n = 10000
        samples = rho.sample(n=n, random_state=RandomState())
        self.assertTrue(np.all(samples >= np.expand_dims(lower_limit, axis=1)))
        self.assertTrue(np.all(samples <= np.expand_dims(upper_limit, axis=1)))

    def test_sampling_truncated_gauss_symm_1d(self):
        # assure that samples are within the limits and reflective of the chosen distribution parameters (mean / std)
        # even though the distribution is truncated
        my_mean = np.random.rand() * 10
        my_std = np.random.rand()
        my_std = np.random.random()
        tmp = np.random.rand() * 2
        lower_limit = my_mean - tmp
        upper_limit = my_mean + tmp
        n_samples = 10 ** 7

        dist = GaussianGenTrunc(mean=my_mean, variance=my_std, limits=np.array([lower_limit, upper_limit]))
        samples = dist.sample(n=n_samples, random_state=RandomState())

        sample_mean = np.mean(samples)
        sample_std = np.std(samples)

        # plt.figure()
        # plt.hist(samples, 1000)
        # plt.show()

        self.assertEqual(len(samples), n_samples)

        # evaluate mean and std and assure that they reflect the chosen mean and std
        # -> only valid for symmetric distributions
        nptest.assert_allclose(sample_mean, my_mean, rtol=1e-1)

        # todo does this have to be true for a truncated distribution?
        # nptest.assert_allclose(sample_std, my_std, rtol=1e-1)

        self.assertGreaterEqual(np.min(samples), lower_limit)
        self.assertLessEqual(np.max(samples), upper_limit)

        # distance to the limits
        self.assertLess(np.abs(np.min(samples) - lower_limit), 1)
        self.assertLess(np.abs(np.max(samples) - upper_limit), 1)

    def test_sampling_truncated_gauss_asymm(self):
        # assure that samples are within the limits and reflective of the chosen distribution parameters (mean / std)
        # even though the distribution is truncated
        my_mean = np.random.rand() * 10
        my_std = np.random.rand()
        lower_limit = my_mean - np.random.rand() * 2
        upper_limit = my_mean + np.random.rand() * 2

        n_samples = 10 ** 7

        dist = GaussianGenTrunc(my_mean, my_std, np.array([lower_limit, upper_limit]))
        samples = dist.sample(n=n_samples, random_state=RandomState())

        # import matplotlib.pyplot as plt
        # plt.figure()
        # plt.hist(samples, 1000)
        # plt.show()

        self.assertEqual(len(samples), n_samples)

        # evaluate mean and std and assure that they reflect the chosen mean and std
        # -> only valid for symmetric distributions
        self.assertLess(np.abs(sample_mode(samples) - my_mean), 1)
        self.assertGreaterEqual(np.min(samples), lower_limit)
        self.assertLessEqual(np.max(samples), upper_limit)

        # distance to the limits
        self.assertLess(np.abs(np.min(samples) - lower_limit), 1)
        self.assertLess(np.abs(np.max(samples) - upper_limit), 1)
